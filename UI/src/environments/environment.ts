// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  // apiUrl: 'https://preview.wfxondemand.com/MicroServices/ItemLibrary/api/V1/',
  // apiUrl: 'http://192.168.1.193/MicroServices/ItemLibrary/api/V1/',
  apiUrl: 'http://192.168.2.167/MicroServices/ItemLibrary/api/V1/',
  aUrl:'http://192.168.2.252:1400/',
  nUrl:'http://192.168.2.252:1400/'
};
