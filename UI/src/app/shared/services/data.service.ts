import { WfxShowMessageservice } from './../../wfxlibrary/wfxsharedservices/wfxshowmessage.service';
import { Globals } from './../../globals';
import { environment } from './../../../environments/environment';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable(
  {
    providedIn: 'root'
  }
)
export class DataService {
  url = environment.apiUrl;

  constructor(private httpClient: HttpClient, private global: Globals,
    private msgsvc: WfxShowMessageservice) {
  }

  getCommonData() {
    return this.get(this.url + 'Common');
    // return this.httpClient.get(this.url + 'Common',
    // {headers: {guid: this.global.guid, 'Content-Type': 'application/json'}});
  }

  // CRUD

  post(url, data, params?) {
    if (!params) {
      params = {};
    }
    return this.httpClient.post(url, data, {params: params,
      headers: { guid: this.global.guid, ProductCatCode: this.global.productCatCode,
        'Content-Type': 'application/json' }}).pipe(map((res: any) => {
      this.showSnackBar(res);
      return res;
    }), catchError(err => {
      return this.handleError(err);
    }));
  }

  get(url, params?, data?) {
    if (!params) {
      params = {};
    }
    return this.httpClient.get(url, {params: params,
      headers: { guid: this.global.guid, ProductCatCode: this.global.productCatCode,
        'Content-Type': 'application/json' }}).pipe(map((res: any) => {
        this.showSnackBar(res);
      return res;
    }), catchError(err => {
      return this.handleError(err);
    }));
  }

  put(url, data, params?) {
    if (!params) {
      params = {};
    }

    return this.httpClient.put(url, data, {params: params,
      headers: { guid: this.global.guid, ProductCatCode: this.global.productCatCode,
        'Content-Type': 'application/json' }}).pipe(map((res: any) => {
        this.showSnackBar(res);
      // if (res.status === 'Success') {
      //   this.msgsvc.showSnackBar('Successfully Saved', '', res.status);
      // }
      return res;
    }), catchError(err => {
      return this.handleError(err);
    }));
  }

  delete(url, params?, data?) {
    if (!params) {
      params = {};
    }
    return this.httpClient.delete(url,
    {
      params: { params },
      headers: { guid: this.global.guid, ProductCatCode: this.global.productCatCode,
        'Content-Type': 'application/json' } }).pipe(map((res: any) => {
          this.showSnackBar(res);
      return res;
    }), catchError(err => {
      return this.handleError(err);
    }));
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error('Backend returned code + ' + error.status, + 'body was: ' + error.error);
    }
    // return an observable with a user-facing error message
    this.msgsvc.showSnackBar(error.error.message, '', 'Fail');
    return throwError('Something bad happened; please try again later.');
  }

  showSnackBar(res) {
    if ((res.status === 'Failed' || res.status === 'Info')) {
      // this.msgsvc.showSnackBar(res.message, '', res.status);
    }
  }
}

