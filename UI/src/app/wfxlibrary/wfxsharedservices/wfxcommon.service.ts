import { Injectable } from '@angular/core';
import { PageParam, SearchParam, SortParam, PagingParam } from '../wfxsharedmodels/wfxqueryparam';
import { Observable, of, throwError } from 'rxjs';
import { WfxShowMessageservice } from './wfxshowmessage.service';
import { environment } from '../../../environments/environment'; // 'src/environments/environment';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { WfxGlobal } from '../wfxsharedscript/wfxglobal';
import { WfxCommonFunctions } from '../wfxsharedfunction/wfxcommonfunction';
import { WFXResultModel } from '../wfxsharedmodels/wfxresultmodel';

let API_URL = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class WfxcommonService {
  _URL = '';
  constructor(
    private httpClient: HttpClient, private msgsvc: WfxShowMessageservice,
    private global: WfxGlobal, private cmf: WfxCommonFunctions) {
  }

  getList(
    url: string, pageParam?: PageParam[], searchParam: SearchParam[] = [], sortParam: SortParam[] = [],
    pagingParam: PagingParam[] = [], showErrorAlert: boolean = true, key: string = ''): Observable<any> {
    // if (this.global.isPLM) {
    //   API_URL = environment.PLMApiUrl;
    // }
    this._URL = API_URL + '/' + url;
    pageParam = this.cmf.setCommonPageParams(pageParam);
    this.setSessionVariables();
    let header = new HttpHeaders();
    // header = this.cmf.setCommonHeader(header);
    header = header.set('pageParams', JSON.stringify(pageParam));
    header = header.set('searchParams', JSON.stringify(searchParam));
    header = header.set('sortParams', JSON.stringify(sortParam));
    header = header.set('pagingParams', JSON.stringify(pagingParam));
    header = header.set('methodName', 'GetList');
    header = header.set('Content-Type', 'application/json; charset=utf-8');
    const httpOptions = { headers: header };
    // Method - 1 this.httpClient.get<AssetData>(this._URL, httpOptions);
    // Method - 2
    return this.httpClient.get(this._URL, httpOptions).pipe(
      map((res: any) => {
        if (showErrorAlert && (res.ErrorMsg !== '' || res.Status === 'Fail' || res.Status === 'Info')) {
          this.msgsvc.showSnackBar(res.ErrorMsg, '', res.Status);
        }
        if (key !== '') {
          this.global.gObjDDLHashData[key] = res.ResponseData;
        }
        return res;
      }),
      catchError(err => {
        return this.handleError(err);
      }
      ));
  }

  bindDDL(url: string, pageParam?: PageParam[], searchParam: SearchParam[] = [], sortParam: SortParam[] = [],
    pagingParam: PagingParam[] = [], showErrorAlert: boolean = true): Observable<any> {
    const key = this.cmf.createKeyFromPageParams(pageParam);
    if (this.global.gObjDDLHashData && this.global.gObjDDLHashData[key]) {
      const res: WFXResultModel = new WFXResultModel();
      res.ResponseData = this.global.gObjDDLHashData[key];
      return of(res);
    } else {
      return this.getList(url, pageParam, searchParam, sortParam, pagingParam, showErrorAlert, key);
    }
  }

  getDetail(url: string, pageParam?: PageParam[], showErrorAlert: boolean = true): Observable<any> {
    this._URL = API_URL + '/' + url;
    pageParam = this.cmf.setCommonPageParams(pageParam);
    this.setSessionVariables();
    let header = new HttpHeaders();
    // header = this.cmf.setCommonHeader(header);
    header = header.set('pageParams', JSON.stringify(pageParam));
    header = header.set('methodName', 'GetDetail');
    header = header.set('Content-Type', 'application/json; charset=utf-8');
    const httpOptions = { headers: header };
    return this.httpClient.get(this._URL, httpOptions).pipe(
      map((res: any) => {
        if (showErrorAlert && (res.ErrorMsg !== '' || res.Status === 'Fail' || res.Status === 'Info')) {
          this.msgsvc.showSnackBar(res.ErrorMsg, '', res.Status);
        }
        return res;
      }),
      catchError(err => {
        return this.handleError(err);
      }
      ));
  }

  saveData(url: string, Data: any, pageParam?: PageParam[], showErrorAlert: boolean = true): Observable<any> {
    this._URL = API_URL + '/' + url;
    pageParam = this.cmf.setCommonPageParams(pageParam);
    let header = new HttpHeaders();
    // header = this.cmf.setCommonHeader(header);
    header = header.set('pageParams', JSON.stringify(pageParam));
    const httpOptions = { headers: header };
    return this.httpClient.post(this._URL, Data, httpOptions).pipe(
      map((res: any) => {
        if (showErrorAlert && (res.ErrorMsg !== '' || res.Status === 'Fail' || res.Status === 'Info')) {
          this.msgsvc.showSnackBar(res.ErrorMsg, '', res.Status);
        }
        return res;
      }),
      catchError(err => {
        return this.handleError(err);
      }
      ));
  }

  updateData(url: string, Data?: any, pageParam?: PageParam[], showErrorAlert: boolean = true): Observable<any> {
    this._URL = API_URL + '/' + url;
    pageParam = this.cmf.setCommonPageParams(pageParam);
    let header = new HttpHeaders();
    // header = this.cmf.setCommonHeader(header);
    header = header.set('pageParams', JSON.stringify(pageParam));
    const httpOptions = { headers: header };
    return this.httpClient.put(this._URL, Data, httpOptions).pipe(
      map((res: any) => {
        if (showErrorAlert && (res.ErrorMsg !== '' || res.Status === 'Fail' || res.Status === 'Info')) {
          this.msgsvc.showSnackBar(res.ErrorMsg, '', res.Status);
        }
        return res;
      }),
      catchError(err => {
        return this.handleError(err);
      }
      ));
  }

  deleteData(url: string, pageParam: PageParam[], showErrorAlert: boolean = true): Observable<any> {
    this._URL = API_URL + '/' + url;
    pageParam = this.cmf.setCommonPageParams(pageParam);
    let header = new HttpHeaders();
    // header = this.cmf.setCommonHeader(header);
    header = header.set('pageParams', JSON.stringify(pageParam));
    const httpOptions = { headers: header };
    return this.httpClient.delete(this._URL, httpOptions).pipe(
      map((res: any) => {
        if (showErrorAlert && (res.ErrorMsg !== '' || res.Status === 'Fail' || res.Status === 'Info')) {
          this.msgsvc.showSnackBar(res.ErrorMsg, '', res.Status);
        }
        return res;
      }),
      catchError(err => {
        return this.handleError(err);
      }
      ));
  }

  setSessionVariables() {
    if (localStorage.length === 0 || !this.cmf.getSessionVariableValue('CompanyCode')
      || this.cmf.getSessionVariableValue('CompanyCode') === '') {
      const URL = API_URL + '/' + 'WFXCommon/GetSessionVariable';
      let pageParam: PageParam[] = [];
      pageParam = this.cmf.setCommonPageParams(pageParam);
      pageParam.push(new PageParam('getAsJson', '1'));
      let header = new HttpHeaders();
      // header = this.cmf.setCommonHeader(header);
      header = header.set('pageParams', JSON.stringify(pageParam));
      header = header.set('methodName', 'GetList');
      header = header.set('Content-Type', 'application/json; charset=utf-8');
      const httpOptions = { headers: header };
      return this.httpClient.get(URL, httpOptions).
        pipe(
          map((res: any) => {
            if (res.ErrorMsg !== '' || res.Status === 'Fail' || res.Status === 'Info') {
              this.msgsvc.showSnackBar(res.ErrorMsg, '', res.Status);
              return true;
            }
            if (res.ResponseData) {
              this.cmf.setSessionVariable('CompanyCode', res.ResponseData.CompanyCode);
              this.cmf.setSessionVariable('MemberCompanyCode', res.ResponseData.MemberCompanyCode);
              this.cmf.setSessionVariable('CompanyDivisionCode', res.ResponseData.CompanyDivisionCode);
              this.cmf.setSessionVariable('UserName', res.ResponseData.UserName);
              this.cmf.setSessionVariable('ApplicationURL', res.ResponseData.ApplicationURL);
            }
          })).subscribe(
            () => { },
            err => {
              return this.handleError(err);
            },
            () => { });
    }
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error('Backend returned code + ' + error.status, + 'body was: ' + error.error);
    }
    // return an observable with a user-facing error message
    this.msgsvc.showSnackBar(error.status.toString() + error.statusText, '', 'Fail');
    return throwError('Something bad happened; please try again later.');
  }
}
