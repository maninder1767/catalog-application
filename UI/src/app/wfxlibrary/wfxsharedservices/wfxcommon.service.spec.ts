import { TestBed } from '@angular/core/testing';

import { WfxcommonService } from './wfxcommon.service';

describe('WfxcommonService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WfxcommonService = TestBed.get(WfxcommonService);
    expect(service).toBeTruthy();
  });
});
