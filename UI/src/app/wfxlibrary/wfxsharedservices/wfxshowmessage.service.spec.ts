import { TestBed, inject } from '@angular/core/testing';
import { WfxShowMessageservice } from './wfxshowmessage.service';

describe('ErrorhandlerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WfxShowMessageservice]
    });
  });

  it('should be created', inject([WfxShowMessageservice], (service: WfxShowMessageservice) => {
    expect(service).toBeTruthy();
  }));
});
