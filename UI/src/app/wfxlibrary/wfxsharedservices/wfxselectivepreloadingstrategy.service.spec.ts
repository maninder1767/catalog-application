import { TestBed } from '@angular/core/testing';

import { WfxSelectivePreloadingStrategyService } from './wfxselectivepreloadingstrategy.service';

describe('SelectivePreloadingStrategyService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WfxSelectivePreloadingStrategyService = TestBed.get(WfxSelectivePreloadingStrategyService);
    expect(service).toBeTruthy();
  });
});
