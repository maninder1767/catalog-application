import { ValidatorFn, AbstractControl, ValidationErrors } from '@angular/forms';
import * as _moment from 'moment';
// tslint:disable-next-line:no-duplicate-imports
import { default as _rollupMoment } from 'moment';
const moment = _rollupMoment || _moment;

export class WfxCustomValidators {
    static minDate(date: string): ValidatorFn {
        return (control: AbstractControl): ValidationErrors | null => {
            if (control.value == null) {
                return null;
            }

            const controlDate = moment(control.value);

            if (!controlDate.isValid()) {
                return null;
            }

            const validationDate = moment(date);

            return !controlDate.isBefore(validationDate, 'day') ? null : {
                'minDate': {
                    'minDate': moment(validationDate).format(),
                    'actual': moment(controlDate).format()
                }
            };
        };
    }

    static maxDate(maxdate: string): ValidatorFn {
        return (control: AbstractControl): ValidationErrors | null => {
            if (control.value == null) {
                return null;
            }

            const controlDate = moment(control.value);

            if (!controlDate.isValid()) {
                return null;
            }

            const validationDate = moment(maxdate);

            return !controlDate.isAfter(validationDate, 'day') ? null : {
                'maxDate': {
                    'maxDate': moment(validationDate).format(),
                    'actual': moment(controlDate).format()
                }
            };
        };
    }
}
