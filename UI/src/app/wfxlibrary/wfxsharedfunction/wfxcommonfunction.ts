import {
  WfxComponentAttributes, WfxConfirmationDialog, WfxTitleBar, WfxSectionProperties, WfxToolTip, WFXGridCellAttributes, WFXAttribute
} from '../wfxsharedmodels/wfxcommonmodel';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ConfirmationdialogComponent } from '../wfxcomponent-ts/wfxconfirmationdialog';
import { ElementRef, Injectable } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { WfxShowMessageservice } from '../../wfxlibrary/wfxsharedservices/wfxshowmessage.service';
import { WfxCustomValidators } from './wfxcustomvalidators';
import * as _moment from 'moment';
import { default as _rollupMoment } from 'moment';
import { isNumber, isArray } from 'util';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { PageParam } from '../wfxsharedmodels/wfxqueryparam';
import { Title } from '@angular/platform-browser';
import { HttpHeaders } from '@angular/common/http';
import { TooltipPosition } from '@angular/material';
import { WfxGlobal } from '../../../app/wfxlibrary/wfxsharedscript/wfxglobal';
// import { WfxcommonService } from '../wfxsharedservices/wfxcommon.service';
const moment = _rollupMoment || _moment;

declare var $: any;
@Injectable()
export class WfxCommonFunctions {
  constructor(
    private msgsvc?: WfxShowMessageservice, public dialog?: MatDialog,
    private route?: ActivatedRoute, private router?: Router, private titleService?: Title,
    private global?: WfxGlobal) { }

  // this is hardcoded use in wfxgrid.ts also in function setMoneyTypeSuffixFormatter at the of filter symbol.
  ddlvalue = 'value';
  ddlvaluetext = 'text';
  addselecttext = 'NONE';

  /**
   * This function set the common pageParams which are necessary to get/post data in database.
   * @param pageParam Send PageParam class object.
   */
  setCommonPageParams(pageParam: PageParam[]): PageParam[] {
    let GUID = '';
    if (!this.checkIfSessionVariablesAreLatest()) {
      this.clearSessionVariable();
    }
    if (this.getSessionVariableValue('GUID') && this.getSessionVariableValue('GUID') !== '') {
      GUID = this.getSessionVariableValue('GUID');
    } else {
      GUID = this.getQueryParamValue('GUID');
    }
    if (!this.getSessionVariableValue('GUID') || this.getSessionVariableValue('GUID') === '') {
      this.setSessionVariable('GUID', GUID);
    }
    if (pageParam === undefined) {
      pageParam = [];
    }
    if (pageParam.findIndex(a => a.ParamName.toLowerCase() === 'GUID') === -1) {
      pageParam.push(new PageParam('GUID', GUID));
    }
    return pageParam;
  }

  /**
   * This function set the common header which are necessary to get/post data in database.
   * @param header Common Params like Authorization.
   */
  setCommonHeader(header: HttpHeaders) {
    return header.set('Authorization', 'V0ZYQWRtaW46cXJzdEAxMjM0Lg==');
  }

  /**
   * This function set the page title.
   * @param pageTitle Send title to set your page title.
   */
  setPageTitle(pageTitle: string) {
    if (pageTitle === undefined || pageTitle === '') {
      pageTitle = 'WFXERPAg';
    }
    this.titleService.setTitle(pageTitle);
  }

  /**
   * This function set the item default property which is necessary for page creation.
   * @param itemJsonDef WfxComponentAttributes class object to set the default property of items of other section than grid.
   */
  setItemDefaultProperty(itemJsonDef: WfxComponentAttributes) {
    if (itemJsonDef.visible === undefined) {
      itemJsonDef.visible = true;
    }
    if (itemJsonDef.mandatory === undefined) {
      itemJsonDef.mandatory = false;
    }
    if (itemJsonDef.disable === undefined) {
      itemJsonDef.disable = false;
    }
    if (itemJsonDef.mandatory === undefined) {
      itemJsonDef.tooltip.tooltipposition = 'below';
    }
    if (itemJsonDef.multiselect === undefined) {
      itemJsonDef.multiselect = false;
    }
    if (itemJsonDef.ddlvalue === undefined || itemJsonDef.ddlvalue === '') {
      itemJsonDef.ddlvalue = this.ddlvalue;
    }
    if (itemJsonDef.ddlvaluetext === undefined || itemJsonDef.ddlvaluetext === '') {
      itemJsonDef.ddlvaluetext = this.ddlvaluetext;
    }
    if (itemJsonDef.addselect === undefined) {
      itemJsonDef.addselect = false;
    }
    if (itemJsonDef.addselecttext === undefined || itemJsonDef.addselecttext === '') {
      itemJsonDef.addselecttext = this.addselecttext;
    }
    if (itemJsonDef.inputsubtype === undefined) {
      itemJsonDef.inputsubtype = 'text';
    }
    if (itemJsonDef.tooltip === undefined) {
      itemJsonDef.tooltip = new WfxToolTip();
    }
    if (itemJsonDef.tooltip.tooltipvaluetype === undefined) {
      itemJsonDef.tooltip.tooltipvaluetype = 'dbfield';
    }
    if (itemJsonDef.dateformat === undefined) {
      itemJsonDef.dateformat = 'll';
    }
    if (itemJsonDef.minvaluetype === undefined) {
      itemJsonDef.minvaluetype = 'text';
    }
    if (itemJsonDef.maxvaluetype === undefined) {
      itemJsonDef.maxvaluetype = 'text';
    }
    if (itemJsonDef.decimalplaces === undefined) {
      itemJsonDef.decimalplaces = 2;
    }
    if (itemJsonDef.addsearchoptioninddl === undefined) {
      itemJsonDef.addsearchoptioninddl = true;
    }
    if (itemJsonDef.floatlabel === undefined) {
      itemJsonDef.floatlabel = 'auto';
    }
    itemJsonDef.itemid = this.getItemID(itemJsonDef);
    itemJsonDef.tooltip.tooltipposition = this.getToolTipPosition(itemJsonDef);
    return itemJsonDef;
  }

  /**
   * This function return the id of section/titlebar/item.
   * @param itemJsonDef For item id itemjsondef is necessary.
   * @param titleBarDef For titlebar id titleBarDef is necessary.
   * @param sectionAttributes For section id sectionAttributes is necessary.
   */
  getItemID(itemJsonDef?: WfxComponentAttributes, titleBarDef?: WfxTitleBar, sectionAttributes?: WfxSectionProperties) {
    let itemID = '';
    if (itemJsonDef) {
      itemID = itemJsonDef.itemid;
    } else if (titleBarDef) {
      itemID = titleBarDef.titlebarid;
    } else if (sectionAttributes) {
      itemID = sectionAttributes.sectionid;
    }
    return itemID;
  }

  /**
   * This function returns the tooltip position.
   * @param itemJsonDef ItemJson Def for which you want to know the tooltipposition.
   */
  getToolTipPosition(itemJsonDef: WfxComponentAttributes) {
    let tooltipPosition: TooltipPosition = 'below';
    if (itemJsonDef && itemJsonDef.tooltip && itemJsonDef.tooltip.tooltipposition !== undefined
      && itemJsonDef.tooltip.tooltipposition) {
      tooltipPosition = itemJsonDef.tooltip.tooltipposition;
    }
    return tooltipPosition;
  }

  /**
   * This function returns the ParamValue value from pageparams class.
   * @param pageParams Send pageParams Object from which you want to get the value.
   * @param paramName Send paramName for which you want to get the ParamValue.
   */
  getParamValueFromParamName(pageParams: PageParam[], paramName: string) {
    return pageParams.filter(e => e.ParamName === paramName)[0].ParamValue;
  }

  /**
   * This function returns the ParamName value from pageparams class.
   * @param pageParams Send pageParams Object from which you want to get the value.
   * @param paramValue Send paramValue for which you want to get the ParamName.
   */
  getParamNameFromParamValue(pageParams: PageParam[], paramValue: string) {
    return pageParams.filter(e => e.ParamName === paramValue)[0].ParamName;
  }

  /**
   * This function creates and return the key from each pageParams.
   * @param pageParams Send pageParams Object from which key will create.
   */
  createKeyFromPageParams(pageParams: PageParam[]) {
    let key = '';
    if (pageParams.length > 0) {
      pageParams.forEach(e => {
        key += e.ParamName + '|' + this.getParamValueFromParamName(pageParams, e.ParamName) + '~';
      });
    }
    return key;
  }

  /**
   * This function creates and return the key from each pageParams.
   * @param pageParams Send pageParams Object from which key will create.
   */
  createURLParamsFromPageParams(pageParams: PageParam[]) {
    let urlParams = '';
    if (pageParams.length > 0) {
      pageParams.forEach(e => {
        urlParams += e.ParamName + '=' + this.getParamValueFromParamName(pageParams, e.ParamName) + '&';
      });
    }
    if (urlParams) {
      urlParams = urlParams.substring(0, urlParams.length - 1);
    }
    return urlParams;
  }

  /**
   * This function creates and return the key from each attributeList and rowData.
   * @param pageParams Send pageParams Object from which key will create.
   * @param params params object for rowData for dbField type attribute.
   */
  createKeyFromAttributeList(attributeList: WFXAttribute[], params) {
    const pageParam = this.createPageParamsFromAttributeList(attributeList, params);
    return this.createKeyFromPageParams(pageParam);
  }

  /**
   * This function will return the key for fetching the dropdown data from Params object.
   * @param params Params Object.
   */
  createKeyFromParams(params) {
    return this.createKeyFromAttributeList(this.createAttributeListFromParams(params), params);
  }

  /**
   * This function will return the attributeList from Params object.
   * @param params Params Object.
   */
  createAttributeListFromParams(params): WFXAttribute[] {
    let attributeList: WFXAttribute[] = [];
    const itemAttr = this.getCellEditorParams(params);
    if (itemAttr) {
      attributeList = itemAttr.attributelist;
    }
    return attributeList;
  }

  /**
   * This function will return the Attribute Param value for WFXAttribute class.
   * @param attributeParam Attribute Param from which you want to get the value.
   * @param params params object for rowData for dbField type attribute.
   */
  getAttributeParamValue(attr: WFXAttribute, params) {
    let attributeParamValue = '';
    if (attr.valuetype === 'text') {
      attributeParamValue = attr.attrvalue;
    } else if (attr.valuetype === 'dbfield') {
      if (attr.attrvalue === '') {
        attributeParamValue = params.data[attr.attrname];
      } else {
        attributeParamValue = params.data[attr.attrvalue];
      }
    } else if (attr.valuetype === 'filter') {
      for (const colDef of params.columnApi.columnController.columnDefs) {
        if (colDef.colId === attr.colid) {
          const itemAttr: WFXGridCellAttributes = this.getCellEditorParams(undefined, undefined, colDef);
          if (itemAttr) {
            const attributeList: WFXAttribute[] = itemAttr.attributelist;
            if (attributeList) {
              const filterDataPropertyName = attr.attrvalue === 'id' ? 'value' : 'id';
              const rowDataField = itemAttr.value;
              const key = this.createKeyFromAttributeList(attributeList, params);
              if (key && this.global.gObjDDLHashData[key]) {
                attributeParamValue = this.getFilterJsonData(this.global.gObjDDLHashData[key],
                  filterDataPropertyName, params.data[rowDataField], attr.attrvalue);
              }
            }
          }
        }
      }
    }
    return attributeParamValue;
  }

  /**
   * WFXRound for rounding the decimal values.
   * @param Value Value.
   * @param DecimalPlaces Decimal places.
   */
  WFXRound(Value, DecimalPlaces) {
    let signvalue = 0;
    if (Value < 0) {
      signvalue = -1;
    } else if (Value > 0) {
      signvalue = 1;
    }
    /*
    If the radix parameter is omitted, JavaScript assumes the following:
    If the string begins with "0x", the radix is 16 (hexadecimal)
    If the string begins with "0", the radix is 8 (octal). This feature is deprecated
    If the string begins with any other value, the radix is 10 (decimal)
    */
    const decimalPlaces = (isNaN(parseInt(DecimalPlaces, 10))) ? 0 : parseInt(DecimalPlaces, 10);
    const t = Math.pow(10, decimalPlaces);
    let retVal = (Math.round((Value * t) + (decimalPlaces > 0 ? 1 : 0) * (signvalue * (10 / Math.pow(100, decimalPlaces)))) / t).toString();
    if (decimalPlaces > 0) {
      const k = retVal.length;
      const mPos = retVal.indexOf('.');
      let scalevalue = '';
      if (mPos > -1) {
        scalevalue = retVal.substring(mPos + 1, k);
      } else {
        retVal = retVal + '.';
      }
      let scalelen = scalevalue.length;
      while (scalelen < decimalPlaces) {
        retVal = retVal + '0';
        scalelen = scalelen + 1;
      }
    }
    return retVal;
  }

  /**
   * This function shows the confirmation dialog on a page.
   * @param dialogTitle Title which will show on dialog box.
   * @param dialogMessage Dialog/Confirmation message which you want to display.
   * @param dialogFalseButtonName There will be two buttons. This is false button like 'No'. Button text which you want to display.
   * @param dialogTrueButtonName There will be two buttons. This is true button like 'Yes'. Button text which you want to display.
   */
  showConfirmationDialog(
    dialogTitle: string, dialogMessage: string, dialogFalseButtonName: string,
    dialogTrueButtonName: string) {
    const cond = new WfxConfirmationDialog();
    cond.dialogtitle = dialogTitle;
    cond.dialogmessage = dialogMessage;
    if (dialogFalseButtonName !== '' && dialogFalseButtonName !== undefined) {
      cond.dialogfalsebuttonname = dialogFalseButtonName;
    } else {
      cond.dialogfalsebuttonname = 'No';
    }
    if (dialogTrueButtonName !== '' && dialogTrueButtonName !== undefined) {
      cond.dialogtruebuttonname = dialogTrueButtonName;
    } else {
      cond.dialogtruebuttonname = 'Yes';
    }
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.panelClass = 'dialog-confirmation';
    dialogConfig.data = {
      id: 1,
      confirmationDialog: cond
    };
    const dialogRef = this.dialog.open(ConfirmationdialogComponent, dialogConfig);
    return dialogRef;
  }

  /**
   * This function add common class and return elementref.
   * @param elementRef This should be the component element ref.
   * @param className Class name of you want to add any other class for now default is 'col-lg-3'.
   */
  addComponentCSSClass(elementRef: ElementRef, className?: string) {
    if (className !== undefined && className !== '') {
      elementRef.nativeElement.className = className;
    } else {
      elementRef.nativeElement.className = 'col-lg-3';
    }

    return elementRef;
  }

  /**
   * This function returns the tooptip value for header section items.
   * @param itemJsonDef itemJsonDef object of WfxComponentAttributes from which we will check if any tooptip defined.
   * @param sectionJsonData sectionJsonData from which it will show the entered/selected data as tooptipValue.
   * @param itemType itemType can be 'select' or any other.
   * @param selectValue This is not used for now.
   */
  getToolTipValue(itemJsonDef: WfxComponentAttributes, sectionJsonData?: any, itemType?: string, selectValue?: any) {
    let tooltipValue = 'not found';
    if (itemJsonDef) {
      if (itemJsonDef.tooltip) {
        if (itemJsonDef.tooltip.tooltipvaluetype === 'dbfield') {
          if (itemType === 'select' && sectionJsonData && sectionJsonData[itemJsonDef.valuetext] !== ''
            && sectionJsonData[itemJsonDef.valuetext] !== undefined) {
            let selectedText = '';
            // for (const i of selectValue.value) {
            //     selectedText = selectedText + i[itemJsonDef.valuetext] + ',';
            // }
            // if (selectedText !== '') {
            //     selectedText = selectedText.substr(0, selectedText.length - 1);
            // }
            if (sectionJsonData && sectionJsonData[itemJsonDef.valuetext]) {
              selectedText = sectionJsonData[itemJsonDef.valuetext];
            }
            tooltipValue = selectedText;
          } else if (itemJsonDef.inputsubtype.toLowerCase() === 'date' && sectionJsonData
            && sectionJsonData[itemJsonDef.value] !== ''
            && sectionJsonData[itemJsonDef.value] !== null && sectionJsonData[itemJsonDef.value] !== undefined) {
            tooltipValue = moment(sectionJsonData[itemJsonDef.value]).format(itemJsonDef.dateformat);
          } else if (sectionJsonData && sectionJsonData[itemJsonDef.value] !== ''
            && sectionJsonData[itemJsonDef.value] !== null && sectionJsonData[itemJsonDef.value] !== undefined) {
            tooltipValue = sectionJsonData[itemJsonDef.value];
          } else {
            tooltipValue = itemJsonDef.caption;
          }
        } else {
          tooltipValue = itemJsonDef.tooltip.tooltipvalue;
        }
      } else {
        tooltipValue = itemJsonDef.caption;
      }
    }
    return tooltipValue;
  }

  /**
   * this function returns the all item id list comma seperated of header sections.
   * @param itemJsonDef itemJsonDef object of WfxComponentAttributes for which you want to get the itemidlist.
   */
  getHeaderItemIDList(itemJsonDef: WfxComponentAttributes[]) {
    let itemIDList = '';
    itemJsonDef.forEach(item => {
      itemIDList += item.itemid + ',';
    });
    return itemIDList;
  }

  /**
   * This function returns the itemValue which is hardcoded or enetered/selected by the user from sectionJsonData.
   * @param itemJsonDef itemJsonDef object of WfxComponentAttributes from which it will check that value is hardcoded or
   * it should pick from sectionJsonData.
   * @param sectionJsonData sectionJsonData if itemJsonDef.valuetype === 'dbfield' then it will pick the value from this object.
   * @param itemType itemType mandatory to send for 'select'
   */
  getItemValue(itemJsonDef: WfxComponentAttributes, sectionJsonData?: any, itemType?: string) {
    let itemValue: any;
    if (itemJsonDef) {
      if (itemJsonDef.valuetype === 'dbfield') {
        if (itemType === 'select' && sectionJsonData) {
          if (itemJsonDef.multiselect === true && sectionJsonData[itemJsonDef.value]
            && sectionJsonData[itemJsonDef.value].length > 0) {
            const selectedValue: any = [];
            for (let index = 0; index < sectionJsonData[itemJsonDef.value].length; index++) {
              selectedValue.push({
                [itemJsonDef.value]: sectionJsonData[itemJsonDef.value][index],
                [itemJsonDef.valuetext]:
                  sectionJsonData[itemJsonDef.valuetext] ? sectionJsonData[itemJsonDef.valuetext][index] : ''
              });
            }
            return selectedValue;
          } else if (itemJsonDef.multiselect === false && sectionJsonData[itemJsonDef.value]) {
            let selectedValue: any;
            selectedValue = {
              [itemJsonDef.value]: sectionJsonData[itemJsonDef.value].toString(),
              [itemJsonDef.valuetext]:
                sectionJsonData[itemJsonDef.valuetext] ? sectionJsonData[itemJsonDef.valuetext] : ''
            };
            return selectedValue;
          }
        } else if (sectionJsonData && sectionJsonData[itemJsonDef.value] !== undefined) {
          itemValue = sectionJsonData[itemJsonDef.value];
          if (itemJsonDef.inputsubtype && itemJsonDef.inputsubtype.toLowerCase() === 'date') {
            itemValue = moment(new Date(itemValue));
          }
        } else {
          itemValue = '';
        }
      }
    }
    return itemValue;
  }

  /**
   * This function returns the query/URL param value.
   * @param paramName Query/URL param name for which you want to know the queryParamValue.
   */
  getQueryParamValue(paramName) {
    // In which you send the parameter by ? then vaues goes in queryParams else paramMap
    // this.route.queryParams.subscribe(params => {
    //     this.action = params.action || params.Action;
    // });
    let queryParamValue;
    // queryParamValue = this.route.snapshot.queryParamMap.get(paramName.toLowerCase().toString());
    this.route.queryParams.subscribe(params => {
      for (const param in params) {
        if (param.toLowerCase() === paramName.toLowerCase()) {
          queryParamValue = params[param];
          break;
        }
      }
    });
    // if (!queryParamValue) {
    //     queryParamValue = this.route.snapshot.paramMap.get(paramName.toString());
    // }
    // queryParamValue = this.route.paramMap.subscribe(params => {
    //     return params.get(paramName.toString());
    // });
    // queryParamValue = this.route.paramMap.pipe(
    //     switchMap((params: ParamMap) => {
    //       return params.get('GUID');
    //     })
    //   );
    return queryParamValue;
  }

  /**
   * This function returns the object which you may be send when you navigated to URL.
   * @param router Rounter object of Router class from we will get the data object.
   */
  getRouteData(router: Router) {
    let data: any;
    if (router.getCurrentNavigation() && router.getCurrentNavigation().extras
      && router.getCurrentNavigation().extras.state && router.getCurrentNavigation().extras.state.data) {
      data = router.getCurrentNavigation().extras.state.data;
    }
    return data;
  }

  /**
   * This function will navigate to particular URL.
   * @param path Path of particular page on which you want to navigate.
   * @param navigationExtras if any extra Query/URL params tou want to send. You need to use NavigationExtras class.
   * @param reloadWindow if i am navigating the same route with diff params then ngoninit dont fire that why
   * it is the work around. or you can use constructor of that component. I have written a method in constructor also like this code
   * override the route reuse strategy
   * if same page is navigating then from this method ngonit will call again else it does not call.
   * this.router.routeReuseStrategy.shouldReuseRoute = function () {
   * return false;
   * };
   * or you have to do all the work of ngoninit in the constructor else use this.
   */
  routeNavigate(path: string, navigationExtras?: NavigationExtras, reloadWindow: boolean = false) {
    this.router.navigate([path], navigationExtras).then(() => {
      if (reloadWindow) {
        // if i am navigating the same route with diff params then ngoninit dont fire that why
        // it is the work around. or you can use constructor of that component. I have written a method in constructor also
        // or you have to do all the work of ngoninit in the constructor
        window.location.reload();
      }
    });
  }

  /**
   * This function returns the PropertyValue from json array of objects.
   * @param JsonData Pass json data from which we will filter the single row.
   * @param FilterPropertyName Property name in json data.
   * @param FilterPropertyValue Property value in json data.
   * @param ReturnProperty Send property name for which property value you want to get.
   */
  getFilterJsonData(JsonData, FilterPropertyName, FilterPropertyValue, ReturnProperty) {
    let returnPropertyValue = null;
    let filterJson;
    if (JsonData && FilterPropertyName && FilterPropertyValue) {
      filterJson = this.getFilterJsonArrayData(JsonData, FilterPropertyName, FilterPropertyValue);
    }
    if (filterJson && filterJson.length > 0) {
      returnPropertyValue = filterJson[0][ReturnProperty];
    }
    return returnPropertyValue;
  }

  /**
   * This function returns the filtered json from json array of objects.
   * @param JsonData Pass json data from which we will filter the single row.
   * @param FilterPropertyName Property name in json data from which you want to filter.
   * @param FilterPropertyValue Property value in json data from which you want to filter.
   */
  getFilterJsonArrayData(JsonData, FilterPropertyName, FilterPropertyValue) {
    const filterJson = JsonData.filter(e => e[FilterPropertyName] === FilterPropertyValue);
    return filterJson;
  }

  /**
   * Validate header section of each item. It will show auto error if there is any invalid item it gets.
   * It checks min/max value & mandatory item.
   * @param jsonDef jsonDef which is object of WfxComponentAttributes class. From this array it will check on each item.
   * @param sectionJsonData sectionJsonData from this it will know if there is any value entered/selected or it is invalid.
   * @param el el is object of ElementRef class you have to send this from this we will check if there is any invalid item.
   */
  validateHeaderSection(jsonDef: WfxComponentAttributes[], sectionJsonData: any, el: ElementRef): boolean {
    let form: FormGroup;
    form = this.createFormGroup(jsonDef, sectionJsonData);
    let isFormValid = true;
    if (form) {
      isFormValid = form.valid;
    }
    if (!isFormValid && el) {
      // const invalidFormCtrl = <FormControl[]>Object.keys(form.controls).map(key => form.controls[key]).filter(ctl => ctl.invalid);
      const allInvalidFormCtrl: any = [];
      for (const name in form.controls) {
        if (form.controls[name].invalid) {
          allInvalidFormCtrl.push({ name, errors: form.controls[name].errors });
        }
      }
      const invalidElements = el.nativeElement.querySelectorAll('.ng-invalid');
      if (invalidElements.length > 0) {
        for (const x of invalidElements) {
          if (x.id !== '') {
            x.focus();
            let minValue;
            let maxValue;
            let controlValue;
            let minMaxErrorType;
            if (allInvalidFormCtrl && allInvalidFormCtrl.length > 0) {
              const item = jsonDef.filter(e => e.itemid === x.id)[0];
              let invalidFormCtrl;
              if (item) {
                invalidFormCtrl = (allInvalidFormCtrl.filter(e => e.name === item.value));
              }
              if (invalidFormCtrl[0].errors && invalidFormCtrl[0].errors.required) {
                this.showCommonError('required', jsonDef.filter(e => e.value === invalidFormCtrl[0].name)[0].caption);
              } else if (invalidFormCtrl[0].errors && invalidFormCtrl[0].errors.minDate) {
                minMaxErrorType = 'minValue';
                if (item.inputsubtype.toLowerCase() === 'date') {
                  controlValue = moment(new Date(sectionJsonData[item.value])).format(item.dateformat || 'll');
                  minValue = item.minvalue.toLowerCase() === 'today' ?
                    moment(new Date()).format(item.dateformat || 'll') :
                    moment(new Date(sectionJsonData[item.minvalue])).format(item.dateformat || 'll');
                }
              } else if (invalidFormCtrl[0].errors && invalidFormCtrl[0].errors.maxDate) {
                minMaxErrorType = 'maxValue';
                if (item.inputsubtype.toLowerCase() === 'date') {
                  controlValue = moment(new Date(sectionJsonData[item.value])).format(item.dateformat || 'll');
                  maxValue = item.maxvalue.toLowerCase() === 'today' ?
                    moment(new Date()).format(item.dateformat || 'll') :
                    moment(new Date(sectionJsonData[item.maxvalue])).format(item.dateformat || 'll');
                }
              } else if (invalidFormCtrl[0].errors && invalidFormCtrl[0].errors.min) {
                minMaxErrorType = 'minValue';
                controlValue = sectionJsonData[item.value];
                if (item.minvaluetype.toLowerCase() === 'dbfield') {
                  minValue = sectionJsonData[item.minvalue];

                } else {
                  minValue = item.minvalue;
                }
              } else if (invalidFormCtrl[0].errors && invalidFormCtrl[0].errors.max) {
                minMaxErrorType = 'maxValue';
                controlValue = sectionJsonData[item.value];
                if (item.maxvaluetype.toLowerCase() === 'dbfield') {
                  maxValue = sectionJsonData[item.maxvalue];
                } else {
                  maxValue = item.maxvalue;
                }
              }
              if (invalidFormCtrl[0].errors && invalidFormCtrl[0].errors.minDate
                || invalidFormCtrl[0].errors && invalidFormCtrl[0].errors.maxDate
                || invalidFormCtrl[0].errors && invalidFormCtrl[0].errors.min
                || invalidFormCtrl[0].errors && invalidFormCtrl[0].errors.max) {

                if (item.inputsubtype.toLowerCase() === 'number' || item.inputsubtype.toLowerCase() === 'money') {
                  maxValue = Number(maxValue);
                  minValue = Number(minValue);
                  controlValue = Number(controlValue);
                }

                this.showCommonError(minMaxErrorType, jsonDef.filter(e => e.value === invalidFormCtrl[0].name)[0].caption,
                  {
                    minValue,
                    maxValue,
                    controlValue
                  });
              }
            }
            break;
          }
        }
      }
    }
    return isFormValid;
  }

  getGSTDefaultAdditionalCharges(options) {
    options.IsCalledFrom = 'ApplyGST';
    // const retSaveJsonData = this.GetGSTDefaultAdditionalChargesFromDB(options);
    // if (options.FromPage === 'WFXExpenseInvoice' || options.FromPage === 'APCreditNote'
    //     || options.FromPage === 'APDebitNote' || options.FromPage === 'ARCreditNote'
    //     || options.FromPage === 'ARDebitNote' || options.FromPage === 'WFXSampleDebitNote'
    //     || (options.FromPage === 'wfx_OrderEdit' && options.ArticleWiseGST === '1')
    //     || options.FromPage === 'wfx_OrderShipmentFromGMPOASN'
    //     || options.FromPage === 'WFXSalesInvoice' || options.FromPage === 'WFXDirectSalesInvoice'
    //     || options.FromPage === 'WFXDirectSales' || options.FromPage === 'WFXPreASNExportInvoice'
    //     || options.FromPage === 'wfx_DebitCreditNoteNewForFPPO') {
    //     if (typeof (retSaveJsonData) === 'string' && retSaveJsonData !== '') {
    //         return -1;
    //     } else {
    //         return retSaveJsonData;
    //     }
    // } else if (typeof (retSaveJsonData) === 'string') {
    //     return -1;
    // } else {
    //     return retSaveJsonData['GSTAdditionalCharges'].JsonData;
    // }
  }

  GetValidGSTDefaultAction(prevTemplateID, currTemplateID, currCount) {
    prevTemplateID = Number(prevTemplateID), currTemplateID = Number(currTemplateID), currCount = Number(currCount);
    const obj: any = [];
    obj.Continue = false, obj.Message = '', obj.Reset = false, obj.Update = false;
    if (prevTemplateID === currTemplateID) {// set 1
      obj.Continue = true, obj.Message = '', obj.Reset = false, obj.Update = false;
    }

    if (prevTemplateID === -1 && currTemplateID === 0) {// set 2
      obj.Continue = true, obj.Message = '', obj.Reset = false, obj.Update = false;
    }

    if (prevTemplateID <= 0 && currTemplateID > 0) {// set 3
      obj.Continue = false,
        obj.Message = 'System found and has applied a mapped additional charge template. Please review the changes.',
        obj.Reset = true, obj.Update = true;
    }

    if (prevTemplateID > 0 && currTemplateID > 0 && prevTemplateID !== currTemplateID) {// set 4
      obj.Continue = false,
        obj.Message = 'Additional charge template are updated as current valid additional charge' +
        'template is different from previous auto applied template. Please review the changes',
        obj.Reset = true, obj.Update = true;
    }

    if (prevTemplateID !== -1 && currTemplateID === -1 && currCount > 0) {// set 5
      obj.Continue = false,
        obj.Message = 'System did not found any mapped additional charge template, ' +
        'additional charges are reset. Please review the changes.',
        obj.Reset = true, obj.Update = false;
    }


    if (prevTemplateID > 0 && currTemplateID === 0 && currCount > 0) {// set 6
      obj.Continue = false,
        obj.Message = 'System did not found any mapped additional charge template, ' +
        'additional charges are reset. Please review the changes.',
        obj.Reset = true, obj.Update = false;
    }

    if (prevTemplateID !== -1 && currTemplateID === -1 && currCount === 0) {// set 7
      obj.Continue = true, obj.Message = '',
        obj.Reset = false, obj.Update = false;
    }
    return obj;
  }

  /**
   * This function create & return dynamic form group for which we can check if there is any invalid value withg the help of formgroup validators.
   * @param jsonDef jsonDef is an object of WfxComponentAttributes class from this we will create dynamic formgroup.
   * @param sectionJsonData From this object we will check the entered/selected value.
   */
  createFormGroup(jsonDef: WfxComponentAttributes[], sectionJsonData: any) {
    const group: any = {};
    if (jsonDef) {
      if (!Array.isArray(jsonDef)) {
        jsonDef = [jsonDef];
      }
      jsonDef.forEach(item => {
        sectionJsonData[item.value] = sectionJsonData[item.value].trim();
        group[item.value] =
          new FormControl(sectionJsonData && sectionJsonData[item.value] ?
            sectionJsonData[item.value] : '', [
            item.mandatory ? Validators.required : Validators.nullValidator,
            item.minvalue !== '' && item.minvalue !== undefined
              ? item.inputsubtype.toLowerCase() === 'date' ?
                WfxCustomValidators.minDate(item.minvalue.toLowerCase() === 'today' ?
                  moment(new Date()).format() :
                  moment(new Date(sectionJsonData[item.minvalue])).format()) :
                item.minvaluetype.toLowerCase() === 'dbfield' ?
                  Validators.min(sectionJsonData[item.minvalue]) :
                  Validators.min(Number(item.minvalue)) : Validators.nullValidator,
            item.maxvalue !== '' && item.maxvalue !== undefined
              ? item.inputsubtype.toLowerCase() === 'date' ?
                WfxCustomValidators.maxDate(item.maxvalue.toLowerCase() === 'today' ?
                  moment(new Date()).format() :
                  moment(new Date(sectionJsonData[item.maxvalue])).format()) :
                item.maxvaluetype.toLowerCase() === 'dbfield' ?
                  Validators.max(sectionJsonData[item.maxvalue]) :
                  Validators.max(Number(item.maxvalue)) : Validators.nullValidator,
            item.minlength !== undefined && item.minlength !== '' ?
              Validators.minLength(sectionJsonData[item.minlength]) : Validators.nullValidator,
            item.maxlength !== undefined && item.maxlength !== '' ?
              Validators.maxLength(sectionJsonData[item.maxlength]) : Validators.nullValidator

          ]);
      });
    }
    return new FormGroup(group);
  }

  /**
   * This function set and returns the jsondef of header sections to enable a list of items.
   * @param jsonDef jsonDef is an object of WfxComponentAttributes class from this we will get the itemids.
   * @param itemIDList Each item id which you want to enable.
   */
  makeHeaderItemNonDisable(jsonDef: WfxComponentAttributes[], itemIDList: string) {
    if (itemIDList !== '') {
      const itemArray = this.getIIDsArrayFromCommaSeperatedIDList(itemIDList);
      itemArray.forEach(col => {
        const index = jsonDef.findIndex(e => e.itemid === col);
        jsonDef[index].disable = false;
      });
      return jsonDef;
    } else {
      return jsonDef;
    }
  }

  /**
   * This function set and returns the jsondef of header sections to disable a list of items.
   * @param jsonDef jsonDef is an object of WfxComponentAttributes class from this we will get the itemids.
   * @param itemIDList Each item id which you want to disable.
   */
  makeHeaderItemDisable(jsonDef: WfxComponentAttributes[], itemIDList: string) {
    if (itemIDList !== '') {
      const itemArray = this.getIIDsArrayFromCommaSeperatedIDList(itemIDList);
      itemArray.forEach(col => {
        const index = jsonDef.findIndex(e => e.itemid === col);
        jsonDef[index].disable = true;
      });
      return jsonDef;
    } else {
      return jsonDef;
    }
  }

  /**
   * This function set and returns the jsondef of header sections to hide a list of items.
   * @param jsonDef jsonDef is an object of WfxComponentAttributes class from this we will get the itemids.
   * @param itemIDList Each item id which you want to hide.
   */
  hideHeaderItem(jsonDef: WfxComponentAttributes[], itemIDList: string) {
    if (itemIDList !== '') {
      const itemArray = this.getIIDsArrayFromCommaSeperatedIDList(itemIDList);
      itemArray.forEach(col => {
        const index = jsonDef.findIndex(e => e.itemid === col);
        jsonDef[index].visible = false;
      });
      return jsonDef;
    } else {
      return jsonDef;
    }
  }

  /**
   * This function set and returns the jsondef of header sections to show a list of items.
   * @param jsonDef jsonDef is an object of WfxComponentAttributes class from this we will get the itemids.
   * @param itemIDList Each item id which you want to show.
   */
  showHeaderItem(jsonDef: WfxComponentAttributes[], itemIDList: string) {
    if (itemIDList !== '') {
      const itemArray = this.getIIDsArrayFromCommaSeperatedIDList(itemIDList);
      itemArray.forEach(col => {
        const index = jsonDef.findIndex(e => e.itemid === col);
        jsonDef[index].visible = true;
      });
      return jsonDef;
    } else {
      return jsonDef;
    }
  }

  /**
   * This function set and returns the jsondef of header sections to mandatory a list of items.
   * @param jsonDef jsonDef is an object of WfxComponentAttributes class from this we will get the itemids.
   * @param itemIDList Each item id which you want to mandatory.
   */
  setHeaderItemMandatory(jsonDef: WfxComponentAttributes[], itemIDList: string) {
    if (itemIDList !== '') {
      const itemArray = this.getIIDsArrayFromCommaSeperatedIDList(itemIDList);
      itemArray.forEach(col => {
        const index = jsonDef.findIndex(e => e.itemid === col);
        jsonDef[index].mandatory = true;
      });
      return jsonDef;
    } else {
      return jsonDef;
    }
  }

  /**
   * This function set and returns the jsondef of header sections to un-mandatory a list of items.
   * @param jsonDef jsonDef is an object of WfxComponentAttributes class from this we will get the itemids.
   * @param itemIDList Each item id which you want to un-mandatory.
   */
  unsetHeaderItemMandatory(jsonDef: WfxComponentAttributes[], itemIDList: string) {
    if (itemIDList !== '') {
      const itemArray = this.getIIDsArrayFromCommaSeperatedIDList(itemIDList);
      itemArray.forEach(col => {
        const index = jsonDef.findIndex(e => e.itemid === col);
        jsonDef[index].mandatory = false;
      });
      return jsonDef;
    } else {
      return jsonDef;
    }
  }

  /**
   * This function returns the itemID array from comma separated string.
   * @param idList Comma separated idlist.
   */
  getIIDsArrayFromCommaSeperatedIDList(idList) {
    if (idList.endsWith(',')) {
      idList = idList.replace(/,\s*$/, '');
    }
    return idList.split(',');
  }

  /**
   * This function hide the toollist and return titleBarJsonDef which is object of WfxTitleBar class.
   * @param titleBarJsonDef titleBarJsonDef which is object of WfxTitleBar class from this object we will hide the toollist.
   * @param toolList Comma separated toollistid which you want to hide.
   */
  hideTools(titleBarJsonDef: WfxTitleBar, toolList: string) {
    if (toolList !== '') {
      const toolIDsArray = this.getIIDsArrayFromCommaSeperatedIDList(toolList);
      toolIDsArray.forEach(id => {
        const index = titleBarJsonDef.toollist.findIndex(e => e.toolname === id);
        if (index >= 0) {
          titleBarJsonDef.toollist[index].visible = false;
        }

      });
      return titleBarJsonDef;
    } else {
      return titleBarJsonDef;
    }
  }

  /**
   * This function show the toollist and return titleBarJsonDef which is object of WfxTitleBar class.
   * @param titleBarJsonDef titleBarJsonDef which is object of WfxTitleBar class from this object we will show the toollist.
   * @param toolList Comma separated toollistid which you want to show.
   */
  showTools(titleBarJsonDef: WfxTitleBar, toolList: string) {
    if (toolList !== '') {
      const toolIDsArray = this.getIIDsArrayFromCommaSeperatedIDList(toolList);
      toolIDsArray.forEach(id => {
        const index = titleBarJsonDef.toollist.findIndex(e => e.toolname === id);
        if (index >= 0) {
          titleBarJsonDef.toollist[index].visible = true;
        }
      });
      return titleBarJsonDef;
    } else {
      return titleBarJsonDef;
    }
  }

  /**
   * This function returns the minimum value if you have set for any item of header section.
   * @param itemJsonDef itemJsonDef which is object of WfxComponentAttributes class from this we will check min value is database driven or hardcoded.
   * @param sectionJsonData From this we will get the entered/selected value.
   */
  getMinValue(itemJsonDef, sectionJsonData) {
    if (itemJsonDef.minvalue && itemJsonDef.minvalue !== undefined && itemJsonDef.minvalue !== '') {
      if (itemJsonDef.inputsubtype && itemJsonDef.inputsubtype.toLowerCase() === 'date') {
        let minDt = Date();
        if (itemJsonDef.minvalue.toLowerCase() !== 'today' && sectionJsonData) {
          minDt = moment(new Date(sectionJsonData[itemJsonDef.minvalue])).format();
        } else {
          minDt = moment(new Date()).format();
        }
        return minDt;
      } else {
        const minValue = itemJsonDef.minvaluetype === 'dbfield' ?
          sectionJsonData[itemJsonDef.minvalue] : Number(itemJsonDef.minvalue);

        return minValue;
      }
    } else {
      return null;
    }
  }

  /**
   * This function returns the maximum value if you have set for any item of header section.
   * @param itemJsonDef itemJsonDef which is object of WfxComponentAttributes class from this we will check min value is database driven or hardcoded.
   * @param sectionJsonData From this we will get the entered/selected value.
   */
  getMaxValue(itemJsonDef, sectionJsonData) {
    if (itemJsonDef.maxvalue && itemJsonDef.maxvalue !== undefined && itemJsonDef.maxvalue !== '') {
      if (itemJsonDef.inputsubtype && itemJsonDef.inputsubtype.toLowerCase() === 'date') {
        let maxDt = Date();
        if (itemJsonDef.maxvalue.toLowerCase() !== 'today' && sectionJsonData) {
          // itemJsonDef.dateformat
          maxDt = moment(new Date(sectionJsonData[itemJsonDef.maxvalue])).format();
        } else {
          maxDt = moment(new Date()).format();
        }

        return maxDt;
      } else {
        const maxValue = itemJsonDef.maxvaluetype === 'dbfield' ?
          sectionJsonData[itemJsonDef.maxvalue] : Number(itemJsonDef.maxvalue);

        return maxValue;
      }
    } else {
      return null;
    }
  }

  /**
   * This function returns the value of session variables.
   * @param key Key for which you want to know the value.
   */
  getSessionVariableValue(key: string) {
    return localStorage.getItem(key.toLowerCase());
  }

  /**
   * This function set key and value of session variables.
   * @param key Key which you want to set.
   * @param value Value which you want to set.
   */
  setSessionVariable(key, value) {
    localStorage.setItem(key.toLowerCase(), value);
  }

  /**
   * This function clears the session variables.
   */
  clearSessionVariable() {
    localStorage.companycode = '';
    localStorage.membercompanycode = '';
    localStorage.companydivisioncode = '';
    localStorage.username = '';
    localStorage.applicationurl = '';
    localStorage.guid = '';
    // localStorage.clear();
  }

  /**
   * This function checks if session variables are latest.
   */
  checkIfSessionVariablesAreLatest() {
    if (localStorage.length > 0 && this.getQueryParamValue('GUID') !== this.getSessionVariableValue('GUID')) {
      return false;
    } else {
      return true;
    }
  }

  /**
   * This function open the window.
   * @param URL URL which you want to open.
   * @param WinName WindowName which will display on window.
   * @param Width Width for window.
   * @param Height Height for window.
   * @param Left Left for window.
   * @param Top Top for window.
   * @param print If you want print the opening window.
   * @param status If you want to show any custom status.
   * @param resizable Window is resizable or not.
   * @param opt Extra option like align etc (need to hanle extra properties).
   */
  openWindow(URL, WinName?, Width?, Height?, Left?, Top?, print?, status?, resizable?, opt?) {
    let align = '';
    if (opt) {
      align = opt.align ? opt.align : align;
    }
    if (align === 'center') {
      Left = (screen.availWidth - Width) / 2, Top = (screen.availHeight - Height) / 2;
    }
    let winObject;
    if (resizable === undefined) {
      resizable = 1;
    }
    if (print === '1') {
      winObject = window.open(URL, WinName, 'width=' + Width + ',height=' +
        Height + ',left=' + Left + ',top=' + Top + ',scrollbars=1,resizable=' +
        resizable + ',status=1,toolbar=yes,menubar=yes');
    } else {
      winObject = window.open(URL, WinName, 'width=' + Width + ',height=' +
        Height + ',left=' + Left + ',top=' + Top + ',scrollbars=1,resizable=' +
        resizable + ',status=' + status + '');
    }
    if (winObject) {
      winObject.focus();
    }
    return false;
  }

  /* ---------------------------- Grid function Start--------------------------------------- */

  /**
   * This function set the default grid item properties and returns the params which is object of this WFXGridCellAttributes.
   * @param params Params object of this WFXGridCellAttributes in which default property will set.
   */
  setGridItemDefaultProperties(params: WFXGridCellAttributes) {
    if (params && params.addselect === undefined) {
      params.addselect = true;
    }
    if (params && params.disable === undefined) {
      params.disable = false;
    }
    if (params && params.multiselect === undefined) {
      params.multiselect = false;
    }
    if (params && (params.addselecttext === undefined || params.addselecttext === '')) {
      params.addselecttext = this.addselecttext;
    }
    if (params && (params.ddlvalue === undefined || params.ddlvalue === '')) {
      params.ddlvalue = this.ddlvalue;
    }
    if (params && (params.ddlvaluetext === undefined || params.ddlvaluetext === '')) {
      params.ddlvaluetext = this.ddlvaluetext;
    }
    if (params && params.inputtype === undefined) {
      params.inputtype = 'text';
    }
    if (params && params.minvalue === undefined) {
      params.minvalue = '';
    }
    if (params && params.decimalplaces === undefined) {
      params.decimalplaces = 0;
    }
    if (params.multiselect) {
      params.addselecttext = 'ALL';
    }
    return params;
  }

  /**
   * This function will check the keys and stop the navigation.
   * @param event Send the entered event from which we will get the keycode.
   */
  checkNStopPropagation(event) {
    const key = event.which || event.keyCode;
    const KEY_LEFT = 37;
    const KEY_UP = 38;
    const KEY_RIGHT = 39;
    const KEY_DOWN = 40;
    const KEY_PAGE_UP = 33;
    const KEY_PAGE_DOWN = 34;
    const KEY_PAGE_HOME = 36;
    const KEY_PAGE_END = 35;

    const isNavigationKey = key === KEY_LEFT || key === KEY_RIGHT || key === KEY_UP
      || key === KEY_DOWN || key === KEY_PAGE_DOWN || key === KEY_PAGE_UP
      || key === KEY_PAGE_HOME || key === KEY_PAGE_END;

    if (isNavigationKey) {
      // this stops the grid from receiving the event and executing keyboard navigation
      event.stopPropagation();
    }
  }

  /**
   * PreventDefault and stopPropagation.
   * @param event Send the entered event for which you want to preventDefault and stopPropagation.
   */
  preventDefaultAndPropagation(event) {
    event.preventDefault();
    event.stopPropagation();
  }

  /**
   * This is the default filter for grid which will filter the date columns.
   * @param filterLocalDateAtMidnight Full Date.
   * @param cellValue Cell current Value.
   */
  myDateComparator(filterLocalDateAtMidnight, cellValue) {
    // comparator: function (filterLocalDateAtMidnight, cellValue) {
    // If you directly use in comparator in grid then use upper line!

    // const dateAsString = cellValue;
    // const dateParts = dateAsString.split('-');
    // const dateAsString = moment(cellValue).format('DD/MM/YYYY');
    // const dateParts = dateAsString.split('/');
    // const cellDate = new Date(Number(dateParts[2]), Number(dateParts[1]) - 1, Number(dateParts[0]));
    const cellDate = new Date(cellValue);
    // if (filterLocalDateAtMidnight.getTime() === cellDate.getTime()) {
    //   return 0;
    // }
    if (filterLocalDateAtMidnight.getFullYear() === cellDate.getFullYear()
      && filterLocalDateAtMidnight.getMonth() === cellDate.getMonth()
      && filterLocalDateAtMidnight.getDate() === cellDate.getDate()) {
      return 0;
    }
    if (cellDate < filterLocalDateAtMidnight) {
      return -1;
    }
    if (cellDate > filterLocalDateAtMidnight) {
      return 1;
    }
  }

  /**
   * This function will focus on a particular cell in grid.
   * @param gridApi GridAPI object.
   * @param rowIndex Current node rowIndex.
   * @param colKey The key can either be the colId (a string) or the colDef (an object).
   * If you passing colId then it will pick from colId else grid automatic assign your json field name to colId.
   * so Key can be the column colId, field, ColDef object or Column object.
   * @param floating floating: {null,'top','bottom'}.
   */
  focusGridCell(gridApi, rowIndex, colKey, floating = null) {
    gridApi.setFocusedCell(rowIndex, colKey, floating);
  }

  /**
   * This function will focus and open the cell in editable mode.
   * @param gridApi GridAPI object.
   * @param rowIndex Current node rowIndex.
   * @param colKey The key can either be the colId (a string) or the colDef (an object).
   * If you passing colId then it will pick from colId else grid automatic assign your json field name to colId.
   * so Key can be the column colId, field, ColDef object or Column object.
   */
  focusNEditGridCell(gridApi, rowIndex, colKey) {
    if (gridApi && colKey) {
      this.focusGridCell(gridApi, rowIndex, colKey, null);
      this.editGridCell(gridApi, rowIndex, colKey);
    }
  }

  /**
   * this function will open the cell in editable mode.
   * @param gridApi GridAPI object.
   * @param rowIndex Current node rowIndex.
   * @param colKey The key can either be the colId (a string) or the colDef (an object).
   * If you passing colId then it will pick from colId else grid automatic assign your json field name to colId.
   * so Key can be the column colId, field, ColDef object or Column object.
   */
  editGridCell(gridApi, rowindex, colkey) {
    gridApi.startEditingCell({
      rowIndex: rowindex,
      colKey: colkey,
      rowPinned: null,
      keyPress: null,
      charPress: null
    });
  }

  /**
   * This function will return the dataKeyField/Unique row id of grid.
   * @param gridApi GridAPI object.
   */
  getGridDataKeyField(gridApi) {
    let dataKeyField = '';
    if (gridApi.WFXGridProperties !== undefined && gridApi.WFXGridProperties.dataKeyField !== undefined) {
      dataKeyField = gridApi.WFXGridProperties.dataKeyField;
    }
    return dataKeyField;
  }

  /**
   * This function will add the row in grid on a particular index and will return the added node object.
   * @param gridApi GridAPI object.
   * @param addItem RowTemplate If you want to give any.
   * @param index On which index you want to add row.
   * @param focusNEditGridFirstCell As name suggest if you want to focus and open the cell first editable grid cell in editable mode.
   */
  gridAddRow(gridApi, addItem?: any, index?: number, focusNEditGridFirstCell: boolean = true) {
    if (index === undefined) {
      index = null;
    }
    let rowID = -1;
    const dataKeyField = this.getGridDataKeyField(gridApi);
    if (dataKeyField !== '' && addItem) {
      rowID = this.getMaxRowID(gridApi);
      gridApi.WFXGridProperties.NewRowStartIndex = rowID - 1;
      addItem[dataKeyField] = rowID;
    }
    // if (jsonData) {
    //   jsonData.push(addItem);
    // }
    if (gridApi.WFXData && gridApi.WFXData.WFXGridData) {
      if (gridApi.WFXData.WFXGridData === undefined) {
        gridApi.WFXData.WFXGridData = [];
      }
      gridApi.WFXData.WFXGridData.push(addItem);
    }
    const res = gridApi.updateRowData({ add: [addItem], addIndex: index });
    const AutoFocusOnThisJsonDataColumn = this.getFirstEditableColumnNameFromGrid(gridApi);
    if (focusNEditGridFirstCell && AutoFocusOnThisJsonDataColumn !== '' && AutoFocusOnThisJsonDataColumn !== undefined) {
      this.focusNEditGridCell(gridApi, res.add[0].rowIndex, AutoFocusOnThisJsonDataColumn);
    }
    return res;
  }


  /**
   * This function will return the max row ID which will be negative.
   * @param gridApi GridAPI object.
   */
  getMaxRowID(gridApi) {
    let NewRowStartIndex = -1;
    const dataKeyField = this.getGridDataKeyField(gridApi);
    if (gridApi.WFXGridProperties.NewRowStartIndex !== undefined) {
      NewRowStartIndex = gridApi.WFXGridProperties.NewRowStartIndex;
    } else {
      if (gridApi.WFXData && gridApi.WFXData.WFXGridData) {
        if (gridApi.WFXData.WFXGridData.length > 0) {
          let negativeIDExists = false;
          for (const rowData of gridApi.WFXData.WFXGridData) {
            if (rowData[dataKeyField] && rowData[dataKeyField] < 0) {
              negativeIDExists = true;
              break;
            }
          }
          if (negativeIDExists) {
            NewRowStartIndex = this.getMinValueFromArrayOfObjects(gridApi.WFXData.WFXGridData, dataKeyField) - 1;
          }
        }
      }
    }
    return NewRowStartIndex;
  }

  /**
   * It will return the smallest value From Array Of Objects.
   * @param data Json Data from which we will find the smallest value.
   * @param attributeName Attribute name for which you want to see smallest value exists.
   * This is the fastest method as provided here
   * https://codeburst.io/javascript-finding-minimum-and-maximum-values-in-an-array-of-objects-329c5c7e22a2
   */
  getMinValueFromArrayOfObjects(data, attributeName: string) {
    return data.reduce((min, p) => p[attributeName] < min ? p[attributeName] : min, data[0][attributeName]);
  }

  /**
   * This function will return the largest value From Array Of Objects.
   * @param data Json Data from which we will find the largest value.
   * @param attributeName Attribute name for which you want to see largest value exists.
   * This is the fastest method as provided here
   * https://codeburst.io/javascript-finding-minimum-and-maximum-values-in-an-array-of-objects-329c5c7e22a2
   */
  getMaxValueFromArrayOfObjects(data, attributeName: string) {
    return data.reduce((max, p) => p[attributeName] > max ? p[attributeName] : max, data[0][attributeName]);
  }

  /**
   * This function returns the first editable cell of grid.
   * @param gridApi GridAPI object.
   */
  getFirstEditableColumnNameFromGrid(gridApi) {
    let AutoFocusOnThisJsonDataColumn = '';
    for (const col of gridApi.columnController.allDisplayedColumns) {
      if (AutoFocusOnThisJsonDataColumn === '' && col.colDef.headerName !== '' && col.colDef.editable) {
        AutoFocusOnThisJsonDataColumn = this.getColKey(undefined, col.colDef);
        break;
      }
    }
    return AutoFocusOnThisJsonDataColumn;
  }


  /**
   * This function will update the grid cell from rowNodeID.
   * @param gridApi GridAPI object.
   * @param rowNodeID RowNode id which is unique id of the row.
   * @param colKey The key can either be the colId (a string) or the colDef (an object).
   * If you passing colId then it will pick from colId else grid automatic assign your json field name to colId.
   * so Key can be the column colId, field, ColDef object or Column object.
   * @param colValue Value which you want to update in column.
   */
  updateCellFromRowNodeID(gridApi, rowNodeID, colKey, colValue) {
    const rowNode = this.getRowNode(gridApi, rowNodeID);
    rowNode.setDataValue(colKey, colValue);
  }


  /**
   * This function will update the grid cell from rowNode object.
   * @param rowNode RowNode object.
   * @param colKey The key can either be the colId (a string) or the colDef (an object).
   * If you passing colId then it will pick from colId else grid automatic assign your json field name to colId.
   * so Key can be the column colId, field, ColDef object or Column object.
   * @param colValue value which you want to update in column.
   */
  updateCellFromRowNode(rowNode, colKey, colValue) {
    // The key can either be the colId (a string) or the colDef (an object).
    // If you passing colId then it will pick from colId else grid automatic assign your json field name to colId.
    // so Key can be the column colId, field, ColDef object or Column object.
    rowNode.setDataValue(colKey, colValue);
  }

  /**
   * This function will update the grid cell from rowNode object.
   * @param rowNode RowNode object.
   * @param data Which data to be replaced with the row Data.
   */
  updateRowDataFromRowNode(rowNode, data) {
    rowNode.setData(data);
  }

  /**
   * This function returns the rowNode via rownodeid.
   * @param gridApi GridAPI object.
   * @param rowNodeID RowNode id which is unique id of the row.
   */
  getRowNode(gridApi, rowNodeID) {
    return gridApi.getRowNode(rowNodeID);
  }

  /**
   * This function returns all the selected rows in grid.
   * @param gridApi GridAPI object.
   */
  getGridSelectRows(gridApi) {
    return gridApi.getSelectedRows();
  }

  /**
   * This function will open a popup as MatDialog box.
   * @param componentName Which component you want to open.
   * @param config config is a object of MatDialogConfig class. Send custom height and width if you want to use.
   * Default width is '800px'.
   * Default width is '550px'.
   * Default panelClass is 'popup-wrapper'.
   * Default disableClose property is true.
   * Default data is undefined.
   */
  openPopup(componentName, config: MatDialogConfig) {
    let width = '800px';
    let height = '550px';
    const panelClass = 'popup-wrapper';
    let disableClose = true;
    let data;
    if (config) {
      if (config.width) {
        width = config.width;
      }
      if (config.height) {
        height = config.height;
      }
      if (config.disableClose) {
        disableClose = config.disableClose;
      }
      if (config.data) {
        data = config.data;
      }
    }
    const dialogRef = this.dialog.open(componentName, {
      width,
      height,
      panelClass,
      disableClose,
      data
    });
    return dialogRef;
  }

  /**
   * This function will set the column auto fit means no horizontal scroll will come.
   * @param gridApi ridAPI object.
   */
  sizeColumnsToFit(gridApi) {
    gridApi.sizeColumnsToFit();
  }

  /**
   * This function will auto size all grid columns means according to column name/data.
   * @param gridColumnApi Grid column API object.
   */
  autoSizeAllGridColumn(gridColumnApi) {
    const allColumnIds = [];
    // getAllColumns()
    gridColumnApi.getAllDisplayedColumns().forEach((column) => {
      if (column.userProvidedColDef && !column.userProvidedColDef.suppressSizeToFit) {
        allColumnIds.push(column.colId);
      }
    });
    if (allColumnIds) {
      gridColumnApi.autoSizeColumns(allColumnIds);
    }
  }

  /**
   * This function will delete selectedRows from the grid and returns the deleted nodes.
   * @param gridApi GridAPI object.
   * @param deleteFromJsonAlso If you want to delete row from Json also. Default is true.
   * @param retainDeletedIDListInGridAPI if you want to retain DeletedIDList in GridAPI object. Then we can retrieve it. Default is false.
   */
  deleteSelectedRows(gridApi, deleteFromJsonAlso = true, retainDeletedIDListInGridAPI = false) {
    const selectedData = gridApi.getSelectedRows();
    let res;
    if (selectedData.length > 0) {
      const dataKeyField = this.getGridDataKeyField(gridApi);
      if (gridApi.WFXData) {
        const deletedIDList: number[] = [];
        selectedData.forEach((element, i) => {
          if (deleteFromJsonAlso && gridApi.WFXData.WFXGridData) {
            gridApi.WFXData.WFXGridData.splice(gridApi.WFXData.WFXGridData.findIndex(e => e.SNo === element.SNo), 1);
          }
          if (element[dataKeyField] > 0) {
            deletedIDList.push(element[dataKeyField]);
          }
        });
        if (retainDeletedIDListInGridAPI) {
          if (gridApi.WFXData.deletedIDList === undefined) {
            gridApi.WFXData.deletedIDList.push = [];
          }
          gridApi.WFXData.deletedIDList.push(deletedIDList);
        }
      }
      res = gridApi.updateRowData({ remove: selectedData });
    } else {
      this.msgsvc.showSnackBar('No row selected to delete.', '', 'Info');
    }
    return res;
  }

  /**
   * This function will delete particular Rows which you will send to delete from the grid and returns the deleted nodes.
   * @param gridApi GridAPI object.
   * @param selectedData Data array of which row want to delete.
   * @param deleteFromJsonAlso If you want to delete row from Json also. Default is true.
   * @param retainDeletedIDListInGridAPI If you want to retain DeletedIDList in GridAPI object. Then we can retrieve it. Default is false.
   */
  deleteParticularRow(gridApi, selectedData: any[], deleteFromJsonAlso = true, retainDeletedIDListInGridAPI = false) {
    let res;
    if (selectedData.length > 0) {
      const dataKeyField = this.getGridDataKeyField(gridApi);
      if (gridApi.WFXData) {
        const deletedIDList: number[] = [];
        selectedData.forEach((element, i) => {
          if (deleteFromJsonAlso && gridApi.WFXData.WFXGridData) {
            gridApi.WFXData.WFXGridData.splice(gridApi.WFXData.WFXGridData.findIndex(e => e[dataKeyField] === element[dataKeyField]), 1);
          }
          if (element[dataKeyField] > 0) {
            deletedIDList.push(element[dataKeyField]);
          }
        });
        if (retainDeletedIDListInGridAPI) {
          if (gridApi.WFXData.deletedIDList === undefined) {
            gridApi.WFXData.deletedIDList.push = [];
          }
          gridApi.WFXData.deletedIDList.push(deletedIDList);
        }
      }
      res = gridApi.updateRowData({ remove: selectedData });
    } else {
      this.msgsvc.showSnackBar('No row selected to delete.', '', 'Info');
    }
    return res;
  }

  /**
   * This function will return the all the displayed columns of the grid.
   * @param gridApi GridAPI object.
   */
  getDisplayedColumn(gridApi) {
    return gridApi.columnController.getAllDisplayedColumns();
  }

  /**
   * This function will validate the grid (only mandatory value for now) and auto show the error.
   * @param gridApi GridAPI object.
   * @param gridJsonData Grid Row data from which we will check value is entered/selected or not. It is not used for now.
   * @param checkOnlyEditedRow Check only those rows which i have edited. So pass the edited property name from which you know this row is edited.
   */
  validateGridJson(gridApi, checkOnlyEditedRow?: string, ...specificNodes) {
    let validGrid = true;
    if (gridApi) {
      if (specificNodes && specificNodes.length > 0) {
        specificNodes.forEach((node, index) => {
          if (!validGrid) {
            return false;
          }
          validGrid = this.checkValidGrid(gridApi, node, index, checkOnlyEditedRow);
        });
      } else {
        gridApi.forEachNode((node, index) => {
          if (!validGrid) {
            return false;
          }
          validGrid = this.checkValidGrid(gridApi, node, index, checkOnlyEditedRow);
        });
      }
    }
    return validGrid;
  }

  private checkValidGrid(gridApi, node, index, checkOnlyEditedRow?) {
    let validGrid = true;
    const allDisplayedColumns = this.getDisplayedColumn(gridApi);
    if (!checkOnlyEditedRow || (checkOnlyEditedRow && node.data[checkOnlyEditedRow])) {
      for (const col of allDisplayedColumns) {
        const cellEditorParams = this.getCellEditorParams(col, node);
        if (validGrid && col.colDef.editable && cellEditorParams && cellEditorParams.mandatory) {
          const value = gridApi.getValue(col.colId, node);
          if ((typeof value !== 'boolean' && !value) || value === '' || value === undefined) {
            this.showCommonError('required', col.colDef.headerName);
            this.focusGridCell(gridApi, index, col.colId);
            validGrid = false;
            break;
          }
        }
      }
    }
    return validGrid;
  }

  /**
   * This function show the error.
   * @param errorType It can be required/minValue/maxValue rest we need to handle if any.
   * @param colText Column name is which is diplayed to user.
   * @param option Option object for send the controlValue/minValue/maxValue.
   */
  showCommonError(errorType, colText, option?: any) {
    let errorMsg = '';
    let controlValue;
    if (option) {
      controlValue = option.controlValue;
    }
    if (errorType === 'required') {
      errorMsg = colText + ' is Mandatory!';
    } else if (errorType === 'minValue') {
      errorMsg = 'Invalid min value in ' + colText + '!';
      if (option) {
        const minValue = option.minValue;
        if (((isNaN(minValue) && minValue !== '') || isNumber(minValue))
          && ((isNaN(controlValue) && controlValue !== '') || isNumber(controlValue))) {
          errorMsg = errorMsg + ' Min value should be ' + minValue + ' but control value is : ' + controlValue + '!';
        }
      }
    } else if (errorType === 'maxValue') {
      errorMsg = 'Invalid max value in ' + colText + '!';
      if (option) {
        const maxValue = option.maxValue;
        if (((isNaN(maxValue) && maxValue !== '') || isNumber(maxValue))
          && ((isNaN(controlValue) && controlValue !== '') || isNumber(controlValue))) {
          errorMsg = errorMsg + ' Max value should be ' + maxValue + ' but control value is : ' + controlValue + '!';
        }
      }
    }
    this.msgsvc.showSnackBar(errorMsg, '', 'Error');
  }

  /**
   * This function default ddlCellRenderer function which will return the default text for cell.
   * @param params params object.
   * @param setDefaultValue Set default value which can be None/ALL or any other.
   * @param jsonDDLValueColName Field name from which you have stored value in json.
   * @param jsonDDLValueTextColName Field name from which you have stored value text in json.
   */
  ddlCellRenderer(params, setDefaultValue = false, jsonDDLValueColName?, jsonDDLValueTextColName?) {
    // set DDL Text after change and set code / text when render
    const cellEditorParams = this.getCellEditorParams(params);
    let ddlvalue = cellEditorParams.ddlvalue;
    let ddlvaluetext = cellEditorParams.ddlvaluetext;
    const ddlOptionList = cellEditorParams.ddlvalues;
    if (ddlvalue === undefined) {
      ddlvalue = this.ddlvalue;
    }
    if (ddlvaluetext === undefined) {
      ddlvaluetext = this.ddlvaluetext;
    }
    if (typeof (params.value) === 'object') {
      return '';
    }
    if (params.value === -1 || params.value === '-1') {
      return this.setSelectNoneValue(params, jsonDDLValueColName, jsonDDLValueTextColName, false, true);
    } else if (params.value && (params.value > 0 || params.value !== '')) {
      if (ddlOptionList && ddlOptionList.length > 0) {
        const optn = ddlOptionList.find(e => e[ddlvalue] === params.value);
        if (optn) {
          return optn[ddlvaluetext];
        } else {
          return params.value;
        }
      } else {
        return params.value;
      }
    } else {
      // if (ddlOptionList && ddlOptionList.length > 0 && setDefaultValue) {
      //     const optn = ddlOptionList.find(e => e[ddlvalue] === -1);
      //     if (optn) {
      //         if (jsonDDLValueTextColName !== '') {
      //             params.data[jsonDDLValueTextColName] = optn[ddlvaluetext];
      //         }
      //         if (jsonDDLValueColName !== '') {
      //             params.data[jsonDDLValueColName] = ddlOptionList.find(e =>
      //                 e[ddlvaluetext] === params.data[jsonDDLValueTextColName])[ddlvalue];
      //         }
      //         return optn[ddlvaluetext];
      //     }
      // }
      if (setDefaultValue) {
        return this.setSelectNoneValue(params, jsonDDLValueColName, jsonDDLValueTextColName, true, true);
      }

    }
  }

  /**
   * Thjis function set the default value for dropdown if you want to show any like None/All any other.
   * @param params Params object.
   * @param jsonDDLValueColName Field name from which you have stored value in json.
   * @param jsonDDLValueTextColName Field name from which you have stored value text in json.
   * @param updateInJson Update in json also or not default is false.
   * @param returnValue You want to get the value or not default is false.
   */
  setSelectNoneValue(params, jsonDDLValueColName?, jsonDDLValueTextColName?, updateInJson = false, returnValue = false) {
    const cellEditorParams = this.getCellEditorParams(params);
    const defaultText = cellEditorParams.addselecttext || this.addselecttext;
    if (updateInJson) {
      if (jsonDDLValueColName) {
        params.data[jsonDDLValueColName] = '-1';
      }
      if (jsonDDLValueTextColName) {
        params.data[jsonDDLValueTextColName] = defaultText;
      }
    }
    if (returnValue) {
      return defaultText;
    }
  }

  /**
   * This function will update the value and value text field in json and row.
   * @param params Params object.
   */
  ddlCellValueChanged(params) {
    const jsonDDLValueTextColName = params.colDef.field;
    const cellEditorParams = this.getCellEditorParams(params);
    const ddlOptionList = cellEditorParams.ddlvalues;
    let jsonDDLValueColName = cellEditorParams.value;
    if (jsonDDLValueColName === undefined) {
      jsonDDLValueColName = jsonDDLValueTextColName + 'ID';
    }
    let ddlvalue = cellEditorParams.ddlvalue;
    let ddlvaluetext = cellEditorParams.ddlvaluetext;
    if (ddlvalue === undefined) {
      ddlvalue = this.ddlvalue;
    }
    if (ddlvaluetext === undefined) {
      ddlvaluetext = this.ddlvaluetext;
    }
    if (params.data[jsonDDLValueTextColName] && params.data[jsonDDLValueTextColName].toString() === '-1') {
      this.setSelectNoneValue(params, jsonDDLValueColName, jsonDDLValueTextColName, true);
    } else if (params.data[jsonDDLValueTextColName] && typeof (params.data[jsonDDLValueTextColName]) !== 'object') {
      params.data[jsonDDLValueColName] = params.data[jsonDDLValueTextColName];
      const value = ddlOptionList.find(e => e[ddlvalue] === params.data[jsonDDLValueColName]);
      if (value) {
        params.data[jsonDDLValueTextColName] = value[ddlvaluetext];
      }
    } else {
      params.data[jsonDDLValueTextColName] = '';
    }
  }

  /**
   * This function will update the value in Json before updating in json value text.
   * @param params Params object.
   * @param selectedValue value which you want to update in json.
   */
  updateDDLValueInJson(params, selectedValue?) {
    const jsonDDLValueTextColName = params.colDef.field;
    const cellEditorParams = this.getCellEditorParams(params);
    let jsonDDLValueColName = cellEditorParams.value;
    if (jsonDDLValueColName === undefined) {
      jsonDDLValueColName = jsonDDLValueTextColName + 'ID';
    }
    if (selectedValue) {
      params.data[jsonDDLValueColName] = selectedValue;
    }
  }


  /**
   * This function will update the value and value text field in json and row.
   * @param params Params object.
   */
  updateDDLValueOnDDLChanged(params, selectedValue?, ddlvalues?) {
    const jsonDDLValueTextColName = params.colDef.field;
    const cellEditorParams = this.getCellEditorParams(params);
    const ddlOptionList = cellEditorParams.ddlvalues || ddlvalues;
    let jsonDDLValueColName = cellEditorParams.value;
    if (jsonDDLValueColName === undefined) {
      jsonDDLValueColName = jsonDDLValueTextColName + 'ID';
    }
    let ddlvalue = cellEditorParams.ddlvalue;
    let ddlvaluetext = cellEditorParams.ddlvaluetext;
    if (ddlvalue === undefined) {
      ddlvalue = this.ddlvalue;
    }
    if (ddlvaluetext === undefined) {
      ddlvaluetext = this.ddlvaluetext;
    }
    if (params.itemAttr.multiselect) {
      if (selectedValue && selectedValue.length > 0) {
        if (selectedValue.toString() === '-1'
          || (Array.isArray(selectedValue) && selectedValue.length > 0 && selectedValue[0].toString() === params.itemAttr.addselecttext)) {
          this.setSelectNoneValue(params, jsonDDLValueColName, jsonDDLValueTextColName, true);
        } else {
          params.data[jsonDDLValueColName] = selectedValue;
        }

        // selectedValue.forEach(e =>
        //   params.data[jsonDDLValueTextColName].push(ddlOptionList.filter(item => {
        //     return item[ddlvalue] === e;
        //   })[0][ddlvaluetext]));
      } else {
        params.data[jsonDDLValueTextColName] = '';
        params.data[jsonDDLValueColName] = '';
      }
    } else {
      if (params.data[jsonDDLValueTextColName] && (params.data[jsonDDLValueTextColName].toString() === params.itemAttr.addselecttext ||
        params.data[jsonDDLValueTextColName].toString() === '-1')) {
        this.setSelectNoneValue(params, jsonDDLValueColName, jsonDDLValueTextColName, true);
      } else if (params.data[jsonDDLValueTextColName] && typeof (params.data[jsonDDLValueTextColName]) !== 'object' && ddlOptionList) {
        const value = ddlOptionList.find(e => e[ddlvaluetext] === params.data[jsonDDLValueTextColName]);
        if (value) {
          params.data[jsonDDLValueColName] = value[ddlvalue];
        }
      } else {
        params.data[jsonDDLValueTextColName] = '';
      }
    }
  }

  /**
   * This function return the cellEditor params which you have send.
   * @param params We will get from Params object.
   * @param dataNode We will get from node object.
   */
  getCellEditorParams(params?, dataNode?, colDef?) {
    let itemAttr: WFXGridCellAttributes;
    const currentColDef = this.getGridColDef(params, colDef);
    if (currentColDef && currentColDef.cellEditorParams && (params || dataNode)) {
      if (typeof currentColDef.cellEditorParams === 'function') {
        itemAttr = currentColDef.cellEditorParams(dataNode || params).itemAttr;
      } else {
        itemAttr = currentColDef.cellEditorParams.itemAttr;
      }
      return itemAttr;
    } else if (currentColDef && currentColDef.cellEditorParams) {
      if (typeof currentColDef.cellEditorParams === 'function') {
        itemAttr = currentColDef.cellEditorParams().itemAttr;
      } else {
        itemAttr = currentColDef.cellEditorParams.itemAttr;
      }
      return itemAttr;
    } else {
      return null;
    }
  }

  /**
   * This function will return the colKey.
   * @param params Params object.
   */
  getColKey(params, currentColDef?) {
    currentColDef = params ? this.getGridColDef(params) : currentColDef;
    return currentColDef.colId || currentColDef.field;
  }

  /**
   * This function will return true if the grid cell is number/money type and value is also number.
   * @param params Params object.
   */
  isGridCellNumbericNValueIsNumber(params): boolean {
    if (this.isGridCellNumeric(params)) {
      return this.isValueIsNumber(params);
    } else {
      return false;
    }
  }

  /**
   * This function will return true if the value is number.
   * @param params Params object.
   */
  isValueIsNumber(params): boolean {
    if (isNaN(params.value)) {
      return false;
    } else {
      return true;
    }
  }

  /**
   * This function will return true if grid cell is number/money type.
   * @param params Params object.
   */
  isGridCellNumeric(params): boolean {
    const itermAttr: WFXGridCellAttributes = this.getCellEditorParams(params);
    if (itermAttr.inputsubtype === 'money' || itermAttr.inputsubtype === 'number') {
      return true;
    } else {
      return false;
    }
  }

  /**
   * This function will return true if grid cell is number/money type.
   * @param params Params object.
   */
  isGridCellOfDropdownType(params): boolean {
    if (this.getGridColDef(params) && this.getGridColDef(params).cellEditor === 'wfxSelectEditor') {
      return true;
    } else {
      return false;
    }
  }

  /**
   * This function return the sum of particular column in array of json object.
   * @param JsonData Json data from which you want the sum of column.
   * @param ReturnPropertyName Property name for which you want to get the sum.
   */
  GetSumColumnJsonArrayData(JsonData, ReturnPropertyName) {
    let colSum = 0;
    const jsonArr = JsonData;
    for (const json of jsonArr) {
      colSum = colSum + Number(json[ReturnPropertyName]);
    }
    return colSum;
  }

  /**
   * This function will stop editing on a particulare cell.
   * @param params Params object.
   * @param gridApi GridAPI object.
   */
  stopEditingGrid(params, gridApi) {
    if (Number(params.event.keyCode) === 9) {
      // tab click
      gridApi.stopEditing();
      this.preventDefaultAndPropagation(params.event);
    }
  }

  /**
   * This function will stop selecting row on particular event. Default on tab click.
   * @param params Params object.
   */
  stopSelectingRow(params) {
    if (Number(params.event.keyCode) === 32 && !params.editing) {
      this.preventDefaultAndPropagation(params.event);
    }
  }

  /**
   * This function will save/update the cell property of grid.
   * @param params Params object.
   * @param propertyName Property name which you want to save/update.
   * @param propertyValue Property value.
   */
  updateExtraDataObjectPropertyValue(params, propertyName, propertyValue) {
    if (!params.colDef.cellEditorParams) {
      params.colDef.cellEditorParams = {};
    }
    if (typeof params.colDef.cellEditorParams === 'function') {
      params.colDef.cellEditorParams(params, params.rowIndex + '_' + propertyName, propertyValue);
    } else {
      params.colDef.cellEditorParams.itemAttr.editableproperties[params.rowIndex + '_' + propertyName] = propertyValue;
    }
  }

  /**
   * This function will return the properties value of grid cell like cell is invalidCell or not.
   * @param params Params object.
   * @param propertyName For which property you want to get the value.
   */
  getExtraDataObjectPropertyValue(params, propertyName) {
    if (params.colDef.cellEditorParams) {
      if (typeof params.colDef.cellEditorParams === 'function') {
        return params.colDef.cellEditorParams(params).itemAttr.editableproperties[params.rowIndex + '_' + propertyName];
      } else {
        return params.colDef.cellEditorParams.itemAttr.editableproperties[params.rowIndex + '_' + propertyName];
      }
    } else {
      return null;
    }
  }


  createPageParamsFromAttributeList(attributeList: WFXAttribute[], params) {
    const pageParam: PageParam[] = [];
    if (attributeList && attributeList.length > 0) {
      for (const attr of attributeList) {
        if (attr.valuetype.toString().toLowerCase() === 'dbfield') {
          if (attr.attrvalue === '') {
            pageParam.push(new PageParam(attr.attrname, params.data[attr.attrname]));
          } else {
            pageParam.push(new PageParam(attr.attrname, params.data[attr.attrvalue]));
          }
        } else {
          pageParam.push(new PageParam(attr.attrname, attr.attrvalue));
        }
      }
    }
    return pageParam;
  }

  /**
   * This function will return the dynamic sent data from json Def if you want the same name attr name as Param Name with the data.
   * @param paramlist Send paramlist which you sent in the json def for particular cell.
   * @param paramlistdata Resolved ParamListData.
   */
  returnDataOfJsonDefAttrNameAsParamNameWithData(paramlist, paramlistdata, dontInsertIf1ValueIsInvalid = true) {
    let data = [];
    let invalidValidValueFound = false;
    paramlist.forEach((e, index) => {
      if (!paramlistdata[index]) {
        invalidValidValueFound = true;
      } else {
        data.push({ ParamName: e.attrname, ParamValue: paramlistdata[index] });
      }
    });
    if (invalidValidValueFound) {
      data = [];
    }
    return data;
  }

  /**
   * This function will return the grid cell value.
   * @param gridApi GridAPI object.
   * @param colKey The key can either be the colId (a string) or the colDef (an object).
   * If you passing colId then it will pick from colId else grid automatic assign your json field name to colId.
   * so Key can be the column colId, field, ColDef object or Column object.
   * @param node Current rowNode.
   */
  getGridCellValue(gridApi, colKey, node) {
    return gridApi.getValue(colKey, node);
  }

  getGridColDef(params, colDef?) {
    return params && params.colDef || params && params.column && params.column.colDef || colDef;
  }

  /* ---------------------------- Grid function End--------------------------------------- */


}
