/*
WFXControls v1.0.0 (2019-April)
Copyright (c) 2019-2019 WFX.
License: https://www.wfxcloud.com/contact.html
*/
/* eslint-disable */

import {
    Component, Input, Output, EventEmitter, ElementRef, OnChanges, SimpleChanges, OnInit,
    ChangeDetectorRef, AfterViewChecked, ViewContainerRef, ViewChild
} from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { WfxComponentAttributes } from '../wfxsharedmodels/wfxcommonmodel';
import { WfxCommonFunctions } from '../wfxsharedfunction/wfxcommonfunction';


@Component({
    selector: 'app-wfxlabel',
    template: `
    <form [hidden]="!itemJsonDef.visible">
    <div class="sectionLabelDiv">
    <mat-label [id]="itemJsonDef.itemid" [matTooltip] = "tooltipValue"
        [matTooltipPosition] = "itemJsonDef.tooltip.tooltipposition" matTooltipClass = "sectiontooltipClass">
        {{value}}
    </mat-label>
</div>
</form>`
})

export class WfxLabelComponent implements OnInit, OnChanges, AfterViewChecked {
    @ViewChild('vc', { read: ViewContainerRef, static: false }) vc: ViewContainerRef;
    constructor(containerElement: ElementRef, private cdr: ChangeDetectorRef, public cmf: WfxCommonFunctions) {
        this.elementRef = containerElement;
    }

    @Input() itemJsonDef: WfxComponentAttributes;
    @Input() ParentComponent: Component;
    @Input() sectionJsonData: any;
    @Output() sectionJsonDataChange = new EventEmitter<any>();

    elementRef: ElementRef;

    get value() {
        return this.cmf.getItemValue(this.itemJsonDef, this.sectionJsonData);
    }

    get tooltipValue() {
        return this.cmf.getToolTipValue(this.itemJsonDef, this.sectionJsonData);
    }

    ngOnInit() {
        if (this.itemJsonDef) {
            this.elementRef = this.cmf.addComponentCSSClass(this.elementRef, this.itemJsonDef.classname);
            this.itemJsonDef = this.cmf.setItemDefaultProperty(this.itemJsonDef);
        }
    }

    ngOnChanges(changes: SimpleChanges) {
    }

    ngAfterViewChecked(): void {
        this.cdr.detectChanges();
    }
}
