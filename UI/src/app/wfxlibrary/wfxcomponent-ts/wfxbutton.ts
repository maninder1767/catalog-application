/*
WFXControls v1.0.0 (2019-April)
Copyright (c) 2019-2019 WFX.
License: https://www.wfxcloud.com/contact.html
*/
/* eslint-disable */

import {
    Component, Input, Output, EventEmitter, ElementRef, OnChanges, SimpleChanges, OnInit,
    ChangeDetectorRef, AfterViewChecked
} from '@angular/core';
import { WfxComponentAttributes } from '../wfxsharedmodels/wfxcommonmodel';
import {WfxCommonFunctions} from '../wfxsharedfunction/wfxcommonfunction';

@Component({
    selector: 'app-wfxbutton',
    template: `
    <form [hidden]="!itemJsonDef.visible">
    <div class="sectionButtonDiv">
        <button mat-raised-button [id]="itemJsonDef.itemid" [disabled]="itemJsonDef.disable" (click)="ButtonOnClick($event)"
        [matTooltip] = "tooltipValue" [matTooltipPosition] = "itemJsonDef.tooltip.tooltipposition" matTooltipClass = "sectiontooltipClass">
            {{itemJsonDef.caption}}
        </button>
    </div>
    </form>`
})

export class WfxButtonComponent implements OnInit, OnChanges, AfterViewChecked {
    constructor(
        containerElement: ElementRef, private cdr: ChangeDetectorRef,
        public cmf: WfxCommonFunctions) {
        this.elementRef = containerElement;
    }

    @Input() ParentComponent: Component;
    @Input() itemJsonDef: WfxComponentAttributes;
    @Output() OnClick = new EventEmitter<any>();
    elementRef: ElementRef;

    get tooltipValue() {
        return this.cmf.getToolTipValue(this.itemJsonDef);
    }

    ngOnInit() {
        this.elementRef = this.cmf.addComponentCSSClass(this.elementRef);
    }

    ngOnChanges(changes: SimpleChanges) {
    }

    ngAfterViewChecked(): void {
        this.cdr.detectChanges();
    }

    ButtonOnClick(e) {
        this.OnClick.emit(e);
    }
}
