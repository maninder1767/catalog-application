/*
WFXControls v1.0.0 (2019-April)
Copyright (c) 2019-2019 WFX.
License: https://www.wfxcloud.com/contact.html
*/
/* eslint-disable */

import {
    Component, Input, Output, EventEmitter, ElementRef, OnChanges, SimpleChanges, OnInit,
    ChangeDetectorRef, AfterViewChecked
} from '@angular/core';
import { WfxSectionProperties } from '../wfxsharedmodels/wfxcommonmodel';
import {WfxCommonFunctions} from '../wfxsharedfunction/wfxcommonfunction';

@Component({
    selector: 'app-wfxsection',
    template: `
    <form>
    <div>
    <div class="row" [id]="sectionAttributes.sectionid"
        [style.class]="sectionAttributes.itemtype==='Content'?'headerSectionClass':'gridSectionClass'">
    <ng-content></ng-content>
    </div>
    </div>
    </form>`,
    styles: [``]
})

export class WfxSectionComponent implements OnInit, OnChanges, AfterViewChecked {
  elementRef: ElementRef;
    constructor(containerElement: ElementRef, private cdr: ChangeDetectorRef, public cmf: WfxCommonFunctions) {
      this.elementRef = containerElement;
    }

    @Input() ParentComponent: Component;
    @Input() sectionAttributes: WfxSectionProperties;
    @Input() sectionJsonData?: any;
    @Input() colLgClass = 'col-lg-12';

    ngOnInit() {
        this.sectionAttributes.sectionid = this.cmf.getItemID(undefined, undefined, this.sectionAttributes);
        this.elementRef = this.cmf.addComponentCSSClass(this.elementRef, this.colLgClass);
    }

    ngOnChanges(changes: SimpleChanges) {
    }

    ngAfterViewChecked(): void {
        this.cdr.detectChanges();
    }
}
