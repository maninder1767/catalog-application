import { Component, ViewContainerRef, AfterViewInit, ViewChild } from '@angular/core';
import { ICellEditorAngularComp } from 'ag-grid-angular';
import { WFXGridCellAttributes } from '../wfxsharedmodels/wfxcommonmodel';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import * as _moment from 'moment';
import { default as _rollupMoment } from 'moment';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { WfxCommonFunctions } from '../wfxsharedfunction/wfxcommonfunction';
declare var $: any;
const moment = _rollupMoment || _moment;

export const MY_FORMATS = {
  parse: {
    dateInput: 'L',
  },
  display: {
    dateInput: 'L', // It will show the display of calender
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'llll',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

moment.updateLocale('en', {
  longDateFormat: {
    LT: 'h:mm A',
    LTS: 'h:mm:ss A',
    L: 'YYYY-MM-DD',
    l: 'M/D/YYYY',
    LL: 'MMMM Do YYYY',
    ll: 'MMM D YYYY',
    LLL: 'MMMM Do YYYY LT',
    lll: 'MMM D YYYY LT',
    LLLL: 'dddd, MMMM Do YYYY LT',
    llll: 'ddd, MMM D YYYY'
  }
});


@Component({
  selector: 'app-wfxgriddatepicker',
  template: `
    <div class="custom-input" (keydown)="onKeyDown($event)" [style.width]="columnWidth">
    <mat-form-field style="width:100%">
        <input matInput #input [matDatepicker]="dp" [min]="itemAttr.minvalue" [max]="itemAttr.maxvalue"
            [(ngModel)]="value" (dateInput)="addEvent('input', $event)" (dateChange)="dateOnChange($event)">
        <mat-datepicker-toggle matSuffix [for]="dp"></mat-datepicker-toggle>
        <mat-datepicker #dp panelClass="ag-custom-component-popup" [disabled]="itemAttr.disable ? false : null"
        (selectedChanged)="onSelectChange($event)" (opened)="datePickerOpenChange($event)"></mat-datepicker>
    </mat-form-field>
    <!--(input)="value = $event.targetElement.value"-->
    </div>
    `,
  styles: [``],
  providers: [
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})
export class WfxGridDatePickerComponent implements AfterViewInit, ICellEditorAngularComp {
  constructor(public cmf: WfxCommonFunctions) { }
  private params: any;
  value: any;
  columnWidth: string;
  @ViewChild('input', { read: ViewContainerRef, static: true }) public input;
  @ViewChild('dp', { static: true }) dp;
  // @ViewChildren('input', { read: ViewContainerRef })
  // public inputs: QueryList<any>;
  // private focusedInput = 0;
  itemAttr: WFXGridCellAttributes;
  focusAfterAttached: any;

  agInit(params: any): void {
    this.params = params;
    this.focusAfterAttached = this.params.cellStartedEdit;
    this.value = params.charPress ? this.params.charPress : `${moment(this.params.value).format('L')}`;
    this.columnWidth = this.params.column.actualWidth - 2 + 'px';
    this.itemAttr = this.params.itemAttr;
    if (this.itemAttr === undefined || !this.itemAttr) {
      this.itemAttr = new WFXGridCellAttributes();
    }
    this.itemAttr = this.cmf.setGridItemDefaultProperties(this.itemAttr);
    if (this.itemAttr.minvalue) {
      this.itemAttr.minvalue = moment(this.itemAttr.minvalue).format();
    }
    if (this.itemAttr.maxvalue) {
      this.itemAttr.maxvalue = moment(this.itemAttr.maxvalue).format();
    }
  }

  // dont use afterGuiAttached for post gui events - hook into ngAfterViewInit instead for this
  ngAfterViewInit() {
    // this.picker.open();
    this.focusOnInputNextTick(this.input);
    if (this.focusAfterAttached) {
      setTimeout(() => {
        this.input.element.nativeElement.focus();
        // this.input.element.nativeElement.select();
        this.input.element.nativeElement.value = this.value;
        $('.ag-popup-editor').css('width', this.columnWidth);
        // this.dp.open();
      });
    }
  }

  private focusOnInputNextTick(input: ViewContainerRef) {
    setTimeout(() => {
      input.element.nativeElement.focus();
    }, 0);
  }

  // Return the DOM element of your editor, this is what the grid puts into the DOM
  // getGui(): HTMLElement {
  //   return this.input.element.nativeElement;
  // }

  // Gets called once after GUI is attached to DOM.
  // Useful if you want to focus or highlight a component
  // (this is not possible when the element is not attached)
  // afterGuiAttached() {
  // }


  getValue() {
    const date = new Date(this.value);
    if (date instanceof Date && !isNaN(date.valueOf())) {
      return `${moment(this.value).format('L')}`;
    } else {
      return '';
    }
  }

  addEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    if (type !== 'input') {
      this.dateOnChange();
    }
  }

  dateOnChange(e?) {
    this.params.stopEditing();
  }

  isPopup(): boolean {
    return true;
  }

  /*
   * A little over complicated for what it is, but the idea is to illustrate how you might tab between multiple inputs
   * say for example in full row editing
   */
  onKeyDown(event): void {
    const key = event.which || event.keyCode;
    if (key === 13) {
      this.dp.open();
      this.preventDefaultAndPropagation(event);
    }
    this.cmf.checkNStopPropagation(event);
  }

  private preventDefaultAndPropagation(event) {
    event.preventDefault();
    event.stopPropagation();
  }

  onSelectChange(e?): void {
    setTimeout(function () {
      this.params.stopEditing();
    }.bind(this));
  }
  datePickerOpenChange(e?) {
    // this.params.api.gridCore.popupService.activePopupElements.push(document.getElementsByTagName('mat-datepicker-content')[0]);
  }

  // will reject the number if it greater than 1,000,000
  // not very practical, but demonstrates the method.
  isCancelAfterEnd(): boolean {
    // return true if you want to cancel the input and set the old value
    if (this.itemAttr.mandatory && (!moment(this.value).isValid()
      || this.value === '')) {
      return true;
    }
    return false;
  }
}
