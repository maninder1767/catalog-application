/*
WFXControls v1.0.0 (2019-April)
Copyright (c) 2019-2019 WFX.
License: https://www.wfxcloud.com/contact.html
*/
/* eslint-disable */

import {
  Component, Input, Output, EventEmitter, ElementRef, OnChanges, SimpleChanges, OnInit,
  ChangeDetectorRef, AfterViewChecked, ViewContainerRef, ViewChild
} from '@angular/core';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { WfxComponentAttributes } from '../wfxsharedmodels/wfxcommonmodel';
import { WfxCommonFunctions } from '../wfxsharedfunction/wfxcommonfunction';

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-wfxtextbox',
  template: `
    <form [hidden]="!itemJsonDef.visible || !isVisible">
    <div class="sectionTextBoxDiv">
    <mat-form-field (click)="onClick($event)" [floatLabel]="itemJsonDef.floatlabel">
        <input autocomplete="off" matInput class="sectionTextBox" [id]="itemJsonDef.itemid" [style.width]="itemJsonDef.width"
        [required]="itemJsonDef.mandatory"
        [readonly] = "itemJsonDef.readonly" [value]="value" (input)="value = $event.target.value"
        (keypress)="txtOnKeyPress($event)"
        [maxlength]="itemJsonDef.maxlength" [placeholder]="itemJsonDef.caption"
        [type]="itemJsonDef.inputsubtype" (change)="txtOnChange($event)"
        [errorStateMatcher]="matcher" [formControl]="formControl"
        [matTooltip] = "tooltipValue" [matTooltipPosition] = "itemJsonDef.tooltip.tooltipposition" matTooltipClass = "sectiontooltipClass">
       <button mat-button *ngIf="itemJsonDef.addclearicon && value !=''" matSuffix mat-icon-button aria-label="Clear" >
        <mat-icon (click)="value='';txtOnChange($event)">close</mat-icon>
       </button>
        <mat-error *ngIf="formControl.hasError('required')">
            {{itemJsonDef.caption}} is <strong>required</strong>
        </mat-error>
        <mat-error *ngIf="!formControl.hasError('required') && formControl.invalid">
            <strong>{{getErrorMessage()}}</strong>
        </mat-error>
    </mat-form-field>
</div>
</form>`
})

export class WfxTextboxComponent implements OnInit, OnChanges, AfterViewChecked {
  @ViewChild('vc', { read: ViewContainerRef, static: false }) vc: ViewContainerRef;
  constructor(containerElement: ElementRef, private cdr: ChangeDetectorRef, public cmf: WfxCommonFunctions) {
    this.elementRef = containerElement;
  }

  @Input() itemJsonDef: WfxComponentAttributes;
  @Input() ParentComponent: Component;
  @Input() sectionJsonData: any;
  @Output() sectionJsonDataChange = new EventEmitter<any>();
  @Output() OnChange = new EventEmitter<any>();
  @Output() OnClick = new EventEmitter<any>();

  matcher = new MyErrorStateMatcher();
  formControl = new FormControl();
  elementRef: ElementRef;
  minValue: any;
  maxValue: any;
  isVisible = true;
  isDisable = false;

  get value() {
    return this.cmf.getItemValue(this.itemJsonDef, this.sectionJsonData);
  }

  set value(val) {
    if (this.itemJsonDef.valuetype === 'dbfield' && this.sectionJsonData) {
      this.sectionJsonData[this.itemJsonDef.value] = val;
    } else {
      this.itemJsonDef.value = val;
    }
  }

  get tooltipValue() {
    return this.cmf.getToolTipValue(this.itemJsonDef, this.sectionJsonData);
  }

  ngOnInit() {
    if (this.itemJsonDef) {
      this.elementRef = this.cmf.addComponentCSSClass(this.elementRef);
      this.itemJsonDef = this.cmf.setItemDefaultProperty(this.itemJsonDef);
      this.formControl = new FormControl({ value: this.value, disabled: this.itemJsonDef.disable || this.isDisable });
      this.minValue = this.cmf.getMinValue(this.itemJsonDef, this.sectionJsonData);
      this.maxValue = this.cmf.getMaxValue(this.itemJsonDef, this.sectionJsonData);

      const validators = [];
      if (this.itemJsonDef.mandatory) {
        validators.push(Validators.required);
      }
      if (this.itemJsonDef.minvalue) {
        validators.push(Validators.min(this.minValue));
      }
      if (this.itemJsonDef.maxvalue) {
        validators.push(Validators.max(this.maxValue));
      }
      this.formControl.setValidators(validators);
    }
  }

  ngOnChanges(changes: SimpleChanges) {
  }

  ngAfterViewChecked(): void {
    this.cdr.detectChanges();
  }

  txtOnChange(e) {
    this.sectionJsonDataChange.emit(this.sectionJsonData);
    this.OnChange.emit(e);
  }

  txtOnKeyPress(e) {
    const key = e.which || e.keyCode;
    if (key === 101 && this.itemJsonDef.inputsubtype === 'number') {
      // blocking e
      this.preventDefaultAndPropagation(e);
    }
  }

  private preventDefaultAndPropagation(event) {
    event.preventDefault();
    event.stopPropagation();
  }

  getErrorMessage() {
    return this.formControl.hasError('min') ? 'Min value is: ' +
      this.minValue + ' but control value is : ' + this.formControl.value :
      this.formControl.hasError('max') ? 'Max value is: ' +
        this.maxValue + ' but control value is : ' + this.formControl.value : '';
  }

  disable(e) {
    this.isDisable = true;
  }

  enable(e) {
    this.isDisable = false;
  }

  hide(e) {
    this.isVisible = false;
  }

  show(e) {
    this.isVisible = true;
  }

  onClick($event)
  {
    this.OnClick.emit($event);
  }
}
