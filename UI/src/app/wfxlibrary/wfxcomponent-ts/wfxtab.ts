/*
WFXControls v1.0.0 (2019-April)
Copyright (c) 2019-2019 WFX.
License: https://www.wfxcloud.com/contact.html
*/
/* eslint-disable */

import {
  Component, Input, Output, EventEmitter, ElementRef, OnChanges, SimpleChanges, OnInit,
  ChangeDetectorRef, AfterViewChecked
} from '@angular/core';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { WfxTab } from '../wfxsharedmodels/wfxcommonmodel';

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-wfxtab',
  template: `
    <form>
    <div class="sectionTabDiv">
    <!--[selectedIndex]="selectedTabIndex"-->
        <mat-tab-group (selectedTabChange)="onSelectedTabChange($event)">
            <mat-tab *ngFor="let tab of itemJsonDef" [disabled]="tab.disable" label="{{tab.text}}">
            <ng-template mat-tab-label> {{tab.text}}
                <mat-icon [matTooltipPosition] = "tab.tooltip && tab.tooltip.tooltipposition &&
                tab.tooltip.tooltipposition !== undefined ? tab.tooltip.tooltipposition : 'below'"
            [matTooltip]="tab.tooltip && tab.tooltip.tooltipvalue !== ''
                && tab.tooltip.tooltipvalue !== undefined ? tab.tooltip.tooltipvalue : tab.text"
                matTooltipClass = "sectiontooltipClass">
                </mat-icon>
            </ng-template>
            </mat-tab>
        </mat-tab-group>
    </div>
    </form>`,
  styles: [``]
})

export class WfxTabComponent implements OnInit, OnChanges, AfterViewChecked {
  constructor(containerElement: ElementRef, private cdr: ChangeDetectorRef) {
  }

  @Input() ParentComponent: Component;
  @Input() itemJsonDef: WfxTab[];
  @Input() selectedTabIndex?: any;
  @Output() OnClick = new EventEmitter<any>();

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
  }

  ngAfterViewChecked(): void {
    this.cdr.detectChanges();
  }

  onSelectedTabChange(e) {
    const selectedTabCode = this.itemJsonDef.filter(z => z.text === e.tab.textLabel)[0].code;
    const obj = {
      selectedTabCode: selectedTabCode,
      selectedTabName: e.tab.textLabel,
      selectedTabIndex: e.index,
      tabData: e
    };
    this.OnClick.emit(obj);
  }
}
