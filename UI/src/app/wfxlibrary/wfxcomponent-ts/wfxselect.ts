/*
WFXControls v1.0.0 (2019-April)
Copyright (c) 2019-2019 WFX.
License: https://www.wfxcloud.com/contact.html
*/
/* eslint-disable */

import {
  Component, Input, Output, EventEmitter, ElementRef, OnChanges, SimpleChanges, OnInit,
  ChangeDetectorRef, AfterViewChecked, ViewChild, OnDestroy, AfterViewInit
} from '@angular/core';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { WfxComponentAttributes } from '../wfxsharedmodels/wfxcommonmodel';
import { WfxCommonFunctions } from '../wfxsharedfunction/wfxcommonfunction';
import { ReplaySubject, Subject, of } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { MatSelect } from '@angular/material/select';


/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-wfxselect',
  template: `
    <form [hidden]="!itemJsonDef.visible">
    <div class="sectionSelectBoxDiv" [style.width]="itemJsonDef.width">
    <mat-form-field [floatLabel]="itemJsonDef.floatlabel">
    <mat-select [id]="itemJsonDef.itemid" #selectRef class="sectionSelectBox" [formControl]="selectFormControl"
        [compareWith]="compareFn" [placeholder]="itemJsonDef.caption" [value]="value"
        [multiple]="itemJsonDef.multiselect" #select (selectionChange)="selectOnChange($event)"
        (openedChange)="openedChange($event)" [required]="itemJsonDef.mandatory"
        [errorStateMatcher]="matcher" [matTooltip] = "tooltipValue" [matTooltipPosition] = "itemJsonDef.tooltip.tooltipposition"
        matTooltipClass = "sectiontooltipClass" disableOptionCentering = "true" (click) = "selectOnClick($event)">
        <mat-option *ngIf="itemJsonDef.addsearchoptioninddl">
            <ngx-mat-select-search class="selectSearchBox"
            [placeholderLabel]="itemJsonDef.placeholderlabel" [preventHomeEndKeyPropagation]="true"
            [noEntriesFoundLabel]="'no matching entry found'" [clearSearchInput]="true"
            [formControl]="selectFilterFormControl">
            </ngx-mat-select-search>
        </mat-option>
        <mat-option class="selectOptionClass" *ngIf="!isLoading && itemJsonDef.addselect && !selectFilterFormControl.value">
          {{itemJsonDef.addselecttext}}
        </mat-option>
        <mat-option class="selectOptionClass" *ngIf="isOpen && isLoading">
            <mat-label>
                <div class="select-placeholder-container">
                    <span>Loading...</span>
                    <mat-spinner class="spinner" diameter="20"></mat-spinner>
                </div>
            </mat-label>
        </mat-option>
        <mat-option class="selectOptionClass" *ngFor="let optn of optionList | async" [value]="optn[itemJsonDef.ddlvalue]">
         <img class="savedsearch-icon" *ngIf="itemJsonDef.addimginddl" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAdVBMVEX///8AAAAwMDDo6OiGhoa0tLTCwsJ0dHR8fHzv7+/s7Ozh4eFhYWHd3d35+fnb29tdXV2lpaU7Ozubm5uBgYGOjo6rq6u8vLxkZGTS0tJqamrMzMwrKyuSkpJTU1MVFRVFRUUkJCRUVFQ/Pz9KSkocHBwQEBBNUkhuAAAGWklEQVR4nO2d6VrbMBBFFZZCKBBIyhIKZS3v/4gNkKS5E0luNBPrKp3z045lnU+RrdHmEBzHcRzHcRzHcRzHcRzHcRzHcf4jho97M8ZvR5ffa2dlSxwPlrxf7aLkyQC4O6mdIXOOB4JJ7RwZM5SCg8HTsHamTFkrwg/OaufKkJOY4GAwqp0vO6JFOGO/dsasiNTCLx5r58yKVBHO3hq1s2ZDohZ+clM7cyaki3BG7cxZkCvC3XjzZ4tw8F47e3qSD9I532pnUE2+CHfgcSpr4e1QKtfOoRbpMwsN39aOtIyshRezY6OdqoiRIgxhDIcua+dRhayFp59Hr+BY22/EaBGGSzj2UDmPKmQt/CrCXTKURTiP6tHwoG4eVcRroTT8UTWPOuK1UBoeV82jClkLbxcnJrtimKiF8m3Rbj1M1UKp3u6zNFULQ7iF482+8RPvwg+e4MR1vTzqSNbCEPDEz3p5VBELKuacpf69bZGuheJl0WoEnKmFInh6qpRDLZki3MczV/UyqSFTC8P1TjxoMg/ScIGnzqtlUkO6ORPku6LRzsRMLQw/8VSbQzO5B2k42IU/aa4WinfFr1p5VJGthaJ82+xKzNXC8A3PNTlZIRnaf4LRb5tNtmwtFJHTbSINarK1UL4Nm6yG+SIUozLTSpnUkK+FslHa4tsw+yCVp5/r5FFFLqj44DV7tgU6ivAczzbYVZptkc74jqcb7GbrKMJwg6fbCyy6amF4wPPttdny78IZd3ieYiL0w8EGdNTCEO7xB8ebJL7Cw/XU7k36PChnva9XkZhkPDH6B+yV52G9CPPTFDfmzqS7XGG4Phf/rPuizTiqarhehPJlYYG+7V5uGPkLXXZftTHqVkOxYSy4nXRftjnaEdZiw9iKmIfuywpQlmKpYaQWds+mLUQ39FFqGJ1Tedd9XRGq17+p4Yup119UHVqmhm/d15WheWeYGo67ryvjjcXQ1ApQxGFoeHmY5OZXt2H66o2YHK0ZKiY4omFugTLedsvz09daR+VJoWFuSWSvhmEqDMuX3LIahkM0LG/Y0BqK/oLyishriM348sFkXkMcax0Xp8NriM+aveJ0eA13vwx3vx6ewv3KB3l4DeF2ir4MWkMxibM8zqc1FBNzysN8WkOcmKMYL6c1xIk5is5vVkMxXq7oUWQ1FEMgZjE+j6EInhT9iayG2Ln8qkiJ1RCjQ820FVJD8aDRTOogNRTdNJot/EgNRV+bJilSQ1ykGR3o+ldIDZ/hZqpBUk5DMflKNZjPaSgCC9XMGk5Ds8AisBriKJBuzwJOQ/yTHqrSojQUE3F1G6FSGorAQpcYpeEPuJVy/RulIc74V66VZjQUgYXyVoyGhoFF4DQ0DCwCpyHeSbsanNHwEe6kXeFHaGgZWARKQ7GiX7sugdAQh0bLB3/nEBrixhrqzdAIDfFPqgssAqOhGBpVryrhMxSLNtTp8RliD8aLOj0+Qwws9Dv20RnaBhaB0FAMjeqX6NEZ4p4FBiv66QxtA4tAaIhDowZbh7AZigeNwdYhbIYisDBIkc0QAwvNDIUFbIY459Jip2w2w99wF3VgEegMxXp3i49+kRmKoVGLJMkMrQOLQGeIazRNtgJPG56PpiDciyH+SU12d00Znn31B620mvowFEOjJhucJAyXLfyn5V36MMSh0XuTNOOGK30ly2ZFH4Y459Ji34+EITR/FxOQ+zDEpdI2G2lFDSEKXXyIsgdDEVjYbDMVNcR5c/MRyh4MxdCoTaJRQ9wbYR6j9WCIQ6MWgUVIGOLIwfzP0oMh7qthtGNf1BDbTvNf9mAId7Dazy5qCLuQLqavbt9QBBZGXy+Pvw9X49DF0Mj2DbEHw+r7nnHD8+floWUQun1D3KLIamvQVLt0UYp/G7/bN8RHuNUXeJKxxejg9OVotRdh+4ZwA7PPRhDFh2Jo1GrnRCJDDCx+WyVLZGg8Q2EBj6FolJrt0EtjKPe3NftMFMseQ9N3FHzsvuQfQcPpyX6KITaLD9O/3JzRNa5rHlh+NVGxf+lWsduRn9SwfLOWVgwNqzmnoWaPvTYMjUJDXkPT/fgZDW16gokNVat+WzC0/kIynaH5NzHIDMf232WlMnzexkdN7rvv2xcX2xnPervfI+D1btLo90odx3Ecx3Ecx3Ecx3Ecx3Ecx3GAP/A4TTkfzHSGAAAAAElFTkSuQmCC">
            {{optn[itemJsonDef.ddlvaluetext]}}
            <span *ngIf="itemJsonDef.addclearicon" class="check-option-delete">
            <svg  (click)="onAction($event,optn.id);" class="search-close"
                viewBox="0 0 10 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path id="+" transform="translate(13.0415 -13) rotate(45)" fill="#4A4A4A"
                d="M7.62939 17.6738H12.9272V18.6748H7.62939V23.9482H6.62842V18.6748H1.35498V17.6738H6.62842V12.376H7.62939V17.6738Z" />
            </svg>
            </span>
        </mat-option>
    </mat-select>
    <mat-error *ngIf="selectFormControl.hasError('required')">
        {{itemJsonDef.caption}} is <strong>required</strong>
    </mat-error>
</mat-form-field>
</div>
</form>`
})

export class WfxSelectComponent implements OnInit, OnChanges, AfterViewChecked, OnDestroy, AfterViewInit {
  constructor(containerElement: ElementRef, private cdr: ChangeDetectorRef, public cmf: WfxCommonFunctions) {
    this.elementRef = containerElement;
  }

  @ViewChild('selectRef', { static: true }) selectRef;
  @Input() selectOptionList: any[];
  @Input() ParentComponent: Component;
  @Input() itemJsonDef: WfxComponentAttributes;
  @Input() sectionJsonData: any;
  @Output() sectionJsonDataChange = new EventEmitter<any>();
  @Output() OnOpenedChange = new EventEmitter<any>();
  @Output() OnChange = new EventEmitter<any>();
  @Input() ddlLoading = false;
  @Input() dependentOn: any;
  @Input() dependent = false;
  @Output() action = new EventEmitter<any>();

  isLoading = false;
  isOpen = false;
  matcher = new MyErrorStateMatcher();
  elementRef: ElementRef;
  interval: any;

  /** list of dropdown Options */
  protected dropdownList: any[] = [];
  /** control for the selected Records for multi-selection */
  public selectFormControl: FormControl;
  /** control for the MatSelect filter keyword multi-selection */
  public selectFilterFormControl: FormControl = new FormControl();
  /** list of Records filtered by search keyword */
  public optionList: ReplaySubject<any[]> = new ReplaySubject<any[]>(1);
  @ViewChild('select', { static: true }) select: MatSelect;

  /** Subject that emits when the component has been destroyed. */
  protected OnDestroy = new Subject<void>();

  get value() {
    return this.cmf.getItemValue(this.itemJsonDef, this.sectionJsonData, 'select');
  }

  set value(val) {
  }

  get tooltipValue() {
    return this.cmf.getToolTipValue(this.itemJsonDef, this.sectionJsonData, 'select', this.selectFormControl);
  }

  ngOnInit() {
    if (this.itemJsonDef) {
      this.elementRef = this.cmf.addComponentCSSClass(this.elementRef);
      this.itemJsonDef = this.cmf.setItemDefaultProperty(this.itemJsonDef);
      let validators;
      if (this.itemJsonDef.mandatory) {
        validators = [Validators.required];
      }
      this.selectFormControl.setValidators(validators);
    }
  }

  compareFn(a: any, b: any) {
    if (a && b && b[this.itemJsonDef.value]) {
      return a.toString() === b[this.itemJsonDef.value].toString();
    } else if (a && b) {
      return a.toString() === b.toString();
    } else {
      return a === b;
    }
    // return a && b && b[this.itemJsonDef.value] ?
    // a.toString() === b[this.itemJsonDef.value].toString() : a.toString() === b.toString();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.dropdownList = this.selectOptionList;
    this.isLoading = this.ddlLoading;
    // set initial selection
    this.selectFormControl = new FormControl({ value: this.value, disabled: this.itemJsonDef.disable });
    // load the initial Records list
    if (this.dropdownList) {
      this.optionList.next(this.dropdownList.slice());
    }
    // listen for search field value changes
    this.selectFilterFormControl.valueChanges.pipe(takeUntil(this.OnDestroy)).subscribe(() => {
      this.filterRecordsMulti();
    });
  }
  ngAfterViewInit() {
    this.setInitialValue();
  }

  ngAfterViewChecked(): void {
    this.cdr.detectChanges();
  }

  ngOnDestroy() {
    this.OnDestroy.next();
    this.OnDestroy.complete();
  }

  selectOnChange(e) {
    const shadow = this;
    if (this.sectionJsonData) {
      this.sectionJsonData[this.itemJsonDef.value] = e.value;
    }
    let valuetext;
    if (!this.itemJsonDef.multiselect && e.value) {
      valuetext = this.selectOptionList.find(x => x[shadow.itemJsonDef.ddlvalue] === e.value)[this.itemJsonDef.ddlvaluetext];
    } else {
      if (e.value) {
        const selectedValueArray = this.selectOptionList.filter(x => e.value.includes(x[shadow.itemJsonDef.ddlvalue]));
        valuetext = [];
        for (const x of selectedValueArray) {
          valuetext.push(x[shadow.itemJsonDef.ddlvaluetext]);
        }
      }
    }
    if (this.sectionJsonData && valuetext) {
      this.sectionJsonData[this.itemJsonDef.valuetext] = valuetext;
    }

    this.sectionJsonDataChange.emit(this.sectionJsonData);
    this.OnChange.emit(e);
    // if (typeof this.itemJsonDef.onchange === 'function') {
    //     this.itemJsonDef.onchange(e, this.ParentComponent);
    // }
  }

  setDDLLoadingFalse() {
    this.isLoading = false;
  }

  openedChange(e) {
    if (this.dependent && (!this.dependentOn || this.dependentOn === undefined)) {
      this.selectRef.close();
    }
    // if Open then e true
    this.isOpen = e;
    this.isLoading = true;
    setTimeout(() => {
      this.setDDLLoadingFalse();
    }, 5000);
    if (this.selectOptionList && this.selectOptionList.length > 0) {
      this.isLoading = false;
    }
    // if (this.isOpen && (!this.selectOptionList || (this.selectOptionList && this.selectOptionList.length === 0))) {
    //     this.startTimer();
    // }
    this.OnOpenedChange.emit(e);
  }

  // startTimer() {
  // while (!this.selectOptionList || (this.selectOptionList && this.selectOptionList.length === 0)) {
  // }
  // }

  // Sets the initial value after the filteredRecords are loaded initially
  protected setInitialValue() {
    const shadow = this;
    this.optionList
      .pipe(take(1), takeUntil(this.OnDestroy))
      .subscribe(() => {
        // setting the compareWith property to a comparison function
        // triggers initializing the selection according to the initial value of
        // the form control (i.e. _initializeSelection())
        // this needs to be done after the filteredRecords are loaded initially
        // and after the mat-option elements are available
        this.select.compareWith = (a: any, b: any) => shadow.compareFn(a, b);
        // a && b && a === b.idtesting1;
        // this.select.compareWith = (a: any, b: any) => a && b && a.Code === b.Code;
      });
  }

  protected filterRecordsMulti() {
    if (!this.dropdownList) {
      return;
    }
    // get the search keyword
    let search = this.selectFilterFormControl.value;
    if (!search) {
      this.optionList.next(this.dropdownList.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the Records
    this.optionList.next(
      this.dropdownList.filter(e => e[this.itemJsonDef.ddlvaluetext].toLowerCase().indexOf(search) > -1)
      // this.dropdownList.filter(e => e.Text.toLowerCase().indexOf(search) > -1)
    );
  }

  selectOnClick(e) {
  }

  onAction(event,id) {
    this.action.emit(id);
    event.stopPropagation();
  }
}
