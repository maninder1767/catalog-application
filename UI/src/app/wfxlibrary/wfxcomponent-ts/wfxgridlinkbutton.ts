import { Component, QueryList, ViewChildren, ViewContainerRef, AfterViewInit } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { WfxCommonFunctions } from '../wfxsharedfunction/wfxcommonfunction';
import { WFXGridCellAttributes } from '../wfxsharedmodels/wfxcommonmodel';

@Component({
  selector: 'app-wfxgridlinkbutton',
  template: `
    <div class="custom-input" *ngIf="params && params.node && !params.node.rowPinned && !params.node.group">
        <input type="button" [disabled]="itemAttr.disable" (click)="linkButtonOnClick($event)"
        value="{{value}}" [class.gridLinkBtnBlankImage] = "linkValueIsBlank">
        <!--<button mat-flat-button [disabled]="itemAttr.disable" (click)="linkButtonOnClick($event)">
            {{value}}
        </button>-->
    </div>
    `,
  styles: [``]
})
export class WfxGridLinkButtonComponent implements AfterViewInit, ICellRendererAngularComp {
  constructor(public cmf: WfxCommonFunctions) { }
  params: any;
  value: string;
  columnWidth: string;
  @ViewChildren('input', { read: ViewContainerRef })
  public inputs: QueryList<any>;
  private focusedInput = 0;
  mainComponent: any;
  itemAttr: WFXGridCellAttributes;
  linkValueIsBlank = false;

  agInit(params: any): void {
    // colID should be there on col level https://www.ag-grid.com/javascript-grid-cell-rendering-components/
    this.params = params;
    this.value = this.params.value;
    this.mainComponent = this.params.mainComponent;
    this.columnWidth = params.column.actualWidth - 2 + 'px';
    if (this.value === '' || this.value === undefined) {
      this.linkValueIsBlank = true;
    } else {
      this.linkValueIsBlank = false;
    }
    this.itemAttr = this.params.itemAttr;
    if (this.itemAttr === undefined || !this.itemAttr) {
      this.itemAttr = new WFXGridCellAttributes();
    }
    this.itemAttr = this.cmf.setGridItemDefaultProperties(this.itemAttr);
    // if (params.action === undefined) {
    //     throw new Error('Missing action parameter for ActionCellRendererComponent');
    // }
  }

  ngAfterViewInit() {
  }

  getValue() {
    if (this.value) {
      return `${this.value}`;
    } else {
      return '';
    }
  }

  isPopup(): boolean {
    return false;
  }

  refresh(): boolean {
    return false;
  }

  linkButtonOnClick(e) {
    // this.params.context.componentParent.methodFromParent(e);
    // this.params.columnApi.columnController.columnDefs[2].editable = true;
    // console.log(this.params);
    // const params = { rowNodes: [this.cmf.getRowNode(this.params.api, 1)], columns: ['Theme'], force: true };
    // this.params.api.refreshCells(params);
    // this.params.context.componentParent.updateRule(e);
    if (typeof this.itemAttr.action === 'function') {
      this.itemAttr.action(this.params, this.params.context.mainParentComponent);
    }

  }
}
export type CellAction = (params) => void;
