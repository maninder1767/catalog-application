/*
WFXControls v1.0.0 (2019-April)
Copyright (c) 2019-2019 WFX.
License: https://www.wfxcloud.com/contact.html
*/
/* eslint-disable */

import {
    Component, Input, Output, EventEmitter, ElementRef, OnChanges, SimpleChanges, OnInit,
    ChangeDetectorRef, AfterViewChecked, ViewChild, OnDestroy, AfterViewInit, Pipe, PipeTransform
} from '@angular/core';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { WfxComponentAttributes } from '../wfxsharedmodels/wfxcommonmodel';
import { WfxCommonFunctions } from '../wfxsharedfunction/wfxcommonfunction';
import { Subject, of } from 'rxjs';
/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
    isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
        const isSubmitted = form && form.submitted;
        return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
    }
}

@Component({
    selector: 'app-wfxlist',
    template: `
      <form [hidden]="!itemJsonDef.visible">
      <div class="sectionSelectBoxDiv">
      <mat-form-field [floatLabel]="itemJsonDef.floatlabel">
      <div class="saved-search-textbox">
      <input autocomplete="off" [formControl]="formControl" matInput  #search [id]="itemJsonDef.itemid" [placeholder]="itemJsonDef.caption" [title]="itemJsonDef.caption">
         <svg *ngIf="search.value !=''" (click)="search.value='';" class="search-close"
          viewBox="0 0 10 10" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path id="+" transform="translate(13.0415 -13) rotate(45)" fill="#4A4A4A"
          d="M7.62939 17.6738H12.9272V18.6748H7.62939V23.9482H6.62842V18.6748H1.35498V17.6738H6.62842V12.376H7.62939V17.6738Z" />
      </svg>

      </div>
          
      <div class="wfxselectoption-container" [style.height]="itemJsonDef.height">
       <ng-container *ngFor="let optn of selectOptionList | filter : search.value ">
            <div class="wfxselectoption">
                 <img class="savedsearch-icon" *ngIf="itemJsonDef.addimginddl" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTUiIGhlaWdodD0iMTUiIHZpZXdCb3g9IjAgMCAxNSAxNSIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj48cGF0aCBmaWxsLXJ1bGU9ImV2ZW5vZGQiIGNsaXAtcnVsZT0iZXZlbm9kZCIgZD0iTTEwLjA0MzUgNS41MjE3NEMxMC4wNDM1IDguMDE4ODcgOC4wMTg4NyAxMC4wNDM1IDUuNTIxNzQgMTAuMDQzNUMzLjAyNDYxIDEwLjA0MzUgMSA4LjAxODg3IDEgNS41MjE3NEMxIDMuMDI0NjEgMy4wMjQ2MSAxIDUuNTIxNzQgMUM4LjAxODg3IDEgMTAuMDQzNSAzLjAyNDYxIDEwLjA0MzUgNS41MjE3NFoiIHN0cm9rZT0iIzRBNEE0QSIgc3Ryb2tlLWxpbmVqb2luPSJyb3VuZCIvPjxwYXRoIGQ9Ik04LjcxODcyIDguNzE4NzVMMTQuMDAwMSAxNC4wMDAxIiBzdHJva2U9IiM0QTRBNEEiIHN0cm9rZS1saW5lY2FwPSJyb3VuZCIgc3Ryb2tlLWxpbmVqb2luPSJyb3VuZCIvPjwvc3ZnPg==">
                 <div class="wfxselectoptiontext">
                    <app-wfxlabel #lbl [ParentComponent]="wfxlist"  [itemJsonDef]="ItemJsonDef[0]"
                     [sectionJsonData]="getSectionJsonData(optn[itemJsonDef.ddlvaluetext])" (click)=onLabelClick(optn[itemJsonDef.ddlvalue])></app-wfxlabel>
                 </div>
                 <div class="wfxselectoptiondelete" *ngIf="itemJsonDef.addclearicon">
                    <svg  (click)="onAction($event,optn.id);" class="search-close"
                        viewBox="0 0 10 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path id="+" transform="translate(13.0415 -13) rotate(45)" fill="#4A4A4A"
                        d="M7.62939 17.6738H12.9272V18.6748H7.62939V23.9482H6.62842V18.6748H1.35498V17.6738H6.62842V12.376H7.62939V17.6738Z" />
                    </svg>
                 </div>
                 <div class="wfxselectoptioncheckbox" *ngIf="itemJsonDef.multiselect && optn.id !== -1">
                 <app-wfxcheckbox (sectionJsonDataChange)="onCheckboxChange(optn)" [itemJsonDef]="itemJsonDef.ckeckBoxJsonDef"
                 [sectionJsonData]="getCheckBoxSectionJsonData(optn)"></app-wfxcheckbox>
                 </div>
            </div>
            </ng-container></div>
        <mat-error  *ngIf="formControl.hasError('required')">
          {{itemJsonDef.caption}} is <strong>required</strong>
      </mat-error>
  </mat-form-field>
  </div>
  </form>`
})

export class WfxListComponent implements OnInit, OnChanges, AfterViewChecked, OnDestroy, AfterViewInit {
    constructor(containerElement: ElementRef, private cdr: ChangeDetectorRef, public cmf: WfxCommonFunctions) {
        this.elementRef = containerElement;
    }

    @ViewChild('selectRef', { static: true }) selectRef;
    @Input() selectOptionList: any[];
    @Input() ParentComponent: Component;
    @Input() itemJsonDef: WfxComponentAttributes;
    @Input() sectionJsonData: any;
    @Output() sectionJsonDataChange = new EventEmitter<any>();
    @Output() OnOpenedChange = new EventEmitter<any>();
    @Output() labelClick = new EventEmitter<any>();
    @Output() action = new EventEmitter<any>();
    formControl = new FormControl();
    minValue: any;
    maxValue: any;
    protected OnDestroy = new Subject<void>();
    matcher = new MyErrorStateMatcher();
    elementRef: ElementRef;
    interval: any;
    lname: any;

    ItemJsonDef: WfxComponentAttributes[] = [{
        readonly: false, mandatory: false, caption: 'First Name', disable: false,
        value: 'lname', valuetype: 'dbfield',
        tooltip: { tooltipvalue: 'First Name', tooltipvaluetype: 'dbfield', tooltipposition: 'below' },
        inputtype: 'textbox', itemid: 'txtFirstName'
    },
    {
        readonly: false, mandatory: true, caption: '',
        value: 'checkBoxValue', disable: false, labelposition: '', valuetype: '',
        tooltip: { tooltipvalue: '', tooltipvaluetype: '', }
    }];

    checkboxJsonData = {
        fname: '', checkBoxValue: false, selectedRadioValue: '', SwitchOn: false
    };

    get value() {
        return this.cmf.getItemValue(this.itemJsonDef, this.sectionJsonData, 'select');
    }

    set value(val) {
    }

    get tooltipValue() {
        return this.cmf.getToolTipValue(this.itemJsonDef, this.sectionJsonData, 'select');
    }

    getSectionJsonData(type) {
        return { lname: type };
    }

    getCheckBoxSectionJsonData(optn) {
        return { checkBoxValue: optn.IsSelected };
    }
    getErrorMessage() {
        return this.formControl.hasError('min') ? 'Min value is: ' +
            this.minValue + ' but control value is : ' + this.formControl.value :
            this.formControl.hasError('max') ? 'Max value is: ' +
                this.maxValue + ' but control value is : ' + this.formControl.value : '';
    }

    ngOnInit() {
        if (this.itemJsonDef) {
            this.elementRef = this.cmf.addComponentCSSClass(this.elementRef, this.itemJsonDef.classname);
            this.itemJsonDef = this.cmf.setItemDefaultProperty(this.itemJsonDef);
            let validators;
            if (this.itemJsonDef.mandatory) {
                validators = [Validators.required];
            }
        }
    }

    ngOnChanges(changes: SimpleChanges) {
    }

    ngAfterViewInit() {
    }

    ngAfterViewChecked(): void {
        this.cdr.detectChanges();
    }

    ngOnDestroy() {
        this.OnDestroy.next();
        this.OnDestroy.complete();
    }

    onAction(event, id) {
       
        this.action.emit(id);
        event.stopPropagation();
    }

    onLabelClick(value) {
        console.log('id wfxlist', value);
        this.labelClick.emit(value);
    }

    onCheckboxChange(data) {
        this.sectionJsonDataChange.emit(data);
    }
}
@Pipe({
    name: 'filter'
})
export class FilterListPipe implements PipeTransform {
    transform(items: any[], filterBy: string): any[] {
        if (!items) { return []; }
        return items.filter(item => item.text.toLowerCase().indexOf(filterBy.toLowerCase()) !== -1);
    }
}
