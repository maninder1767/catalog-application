import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
    selector: 'app-confirmationdialog',
    template: `
    <div class="confirm_icon"><svg width="21" height="19"
        viewBox="0 0 21 19" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M11.3043 0L2.82609 7.91304L0 5.08696" transform="translate(5 1)" stroke="white"
        stroke-linecap="round" stroke-linejoin="round">
        </path><path d="M0.5 1.5H20.5" transform="translate(0 17)" stroke="white" stroke-linecap="square">
        </path></svg>
    </div>
    <div class="confirm_msg"><h2 mat-dialog-title>{{data.confirmationDialog.dialogtitle}}</h2>
    <p mat-dialog-content>{{data.confirmationDialog.dialogmessage}}</p>
    <div mat-dialog-actions>
        <button mat-button class='button delete_no' mat-dialog-close>{{data.confirmationDialog.dialogfalsebuttonname}}</button>
        <!-- The mat-dialog-close directive optionally accepts a value as a result for the dialog. -->
        <button mat-button class='button delete_yes' [mat-dialog-close]="true" cdkFocusInitial>
            {{data.confirmationDialog.dialogtruebuttonname}}</button>
        <!-- [mat-dialog-close]="true" or (click)="dialogRef.close(false) -->
    </div>
    </div>`

})
export class ConfirmationdialogComponent implements OnInit {
    constructor(@Inject(MAT_DIALOG_DATA) public data: any) {
    }
    ngOnInit() {
        // console.log(this.data);
    }
}
