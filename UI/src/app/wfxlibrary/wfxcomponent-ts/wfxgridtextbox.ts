import { Component, ViewContainerRef, AfterViewInit, ViewChild } from '@angular/core';
import { ICellEditorAngularComp } from 'ag-grid-angular';
import { WfxCommonFunctions } from '../wfxsharedfunction/wfxcommonfunction';
import { WFXGridCellAttributes } from '../wfxsharedmodels/wfxcommonmodel';
declare var $: any;

@Component({
  selector: 'app-wfxgridtextbox',
  template: `
    <div class="wfxgrid-textbox-div" (keydown)="onKeyDown($event)" [style.width]="columnWidth">
    <mat-form-field style="width:100%">
        <input #input class="wfxgrid-textbox-editable" matInput [(ngModel)]="value"
        [disabled]="itemAttr.disable" [maxlength]="itemAttr.maxlength" [type]="itemAttr.inputsubtype">
    </mat-form-field>
    </div>
    `,
  styles: [``]
})
export class WfxGridTextBoxComponent implements AfterViewInit, ICellEditorAngularComp {
  constructor(public cmf: WfxCommonFunctions) { }
  params: any;
  value: any;
  columnWidth: string;
  @ViewChild('input', { read: ViewContainerRef, static: true }) public input;
  // @ViewChildren('input', { read: ViewContainerRef })
  // public inputs: QueryList<any>;
  // private focusedInput = 0;
  itemAttr: WFXGridCellAttributes;
  selectAfterAttached: any;
  cancelBeforeStart = false;

  agInit(params: any): void {
    this.params = params;
    this.columnWidth = params.column.actualWidth - 2 + 'px';
    this.itemAttr = this.params.itemAttr;
    if (this.itemAttr === undefined || !this.itemAttr) {
      this.itemAttr = new WFXGridCellAttributes();
    }
    this.itemAttr = this.cmf.setGridItemDefaultProperties(this.itemAttr);
    this.value = params.charPress ? params.charPress : params.value;
    if (!params.charPress) {
      this.selectAfterAttached = this.params.cellStartedEdit;
    }
  }

  // Return the DOM element of your editor, this is what the grid puts into the DOM
  // getGui(): HTMLElement {
  //   return this.input.element.nativeElement;
  // }

  // dont use afterGuiAttached for post gui events - hook into ngAfterViewInit instead for this
  ngAfterViewInit() {
    this.input.element.nativeElement.style.width = '100%';
    this.focusOnInputNextTick(this.input);
    // this.input.element.nativeElement.focus();
    if (this.selectAfterAttached) {
      window.setTimeout(() => {
        this.input.element.nativeElement.select();
      });
    }
  }

  private focusOnInputNextTick(input: ViewContainerRef) {
    setTimeout(() => {
      input.element.nativeElement.focus();
    }, 0);
  }

  getValue() {
    if (this.value) {
      return `${this.value}`;
    } else {
        return '';
    }
  }

  isPopup(): boolean {
    return true;
  }

  /*
   * A little over complicated for what it is, but the idea is to illustrate how you might tab between multiple inputs
   * say for example in full row editing
   */
  onKeyDown(event): void {
    const key = event.which || event.keyCode;
    this.cmf.checkNStopPropagation(event);
  }

  private preventDefaultAndPropagation(event) {
    event.preventDefault();
    event.stopPropagation();
  }

  // Gets called once after GUI is attached to DOM.
  // Useful if you want to focus or highlight a component
  // (this is not possible when the element is not attached)
  afterGuiAttached() {
    // if (this.selectAfterAttached) {
    //   this.input.element.nativeElement.focus();
    //   this.input.element.nativeElement.select();
    // }
  }

  // If doing full row edit, then gets called when tabbing into the cell.
  focusIn() {
    // const eInput = this.getGui();
    // eInput.focus();
    // eInput.onselect();
  }

  isCancelBeforeStart(): boolean {
    return this.cancelBeforeStart;
  }

  // will reject the number if it greater than 1,000,000
  // not very practical, but demonstrates the method.
  isCancelAfterEnd(): boolean {
    // return true if you want to cancel the input and set the old value
    return false;
  }
}
