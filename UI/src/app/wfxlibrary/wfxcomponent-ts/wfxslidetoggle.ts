/*
WFXControls v1.0.0 (2019-April)
Copyright (c) 2019-2019 WFX.
License: https://www.wfxcloud.com/contact.html
*/
/* eslint-disable */

import {
    Component, Input, Output, EventEmitter, ElementRef, OnChanges, SimpleChanges, OnInit,
    ChangeDetectorRef, AfterViewChecked
} from '@angular/core';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { WfxComponentAttributes } from '../wfxsharedmodels/wfxcommonmodel';
import {WfxCommonFunctions} from '../wfxsharedfunction/wfxcommonfunction';

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
    isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
        const isSubmitted = form && form.submitted;
        return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
    }
}

@Component({
    selector: 'app-wfxslidetoggle',
    template: `
    <form [hidden]="!itemJsonDef?.visible">
    <div class="sectionSlideToggleDiv">
    <mat-slide-toggle [id]="itemJsonDef?.itemid" color="accent" [checked]="value" [formControl]="formControl"
        (change) = 'matSlideOnChange($event)' [labelPosition]="itemJsonDef?.labelposition"
        (input)="value = $event.checked" [matTooltip] = "tooltipValue"
        [matTooltipPosition] = "itemJsonDef?.tooltip.tooltipposition" matTooltipClass = "sectiontooltipClass">
        {{itemJsonDef?.caption}}
    </mat-slide-toggle>
    </div>
    </form>`
})

export class WfxSlideToggleComponent implements OnInit, OnChanges, AfterViewChecked {
    constructor(containerElement: ElementRef, private cdr: ChangeDetectorRef, public cmf: WfxCommonFunctions) {
        this.elementRef = containerElement;
    }

    @Input() ParentComponent: Component;
    @Input() itemJsonDef: WfxComponentAttributes;
    @Input() sectionJsonData: any;
    @Output() sectionJsonDataChange = new EventEmitter<any>();
    @Output() OnChange = new EventEmitter<any>();

    matcher = new MyErrorStateMatcher();
    formControl = new FormControl();
    elementRef: ElementRef;

    get value() {
        return this.cmf.getItemValue(this.itemJsonDef, this.sectionJsonData);
    }

    set value(val) {
    }

    get tooltipValue() {
        return this.cmf.getToolTipValue(this.itemJsonDef, this.sectionJsonData);
    }

    ngOnInit() {
        if (this.itemJsonDef) {
            this.elementRef = this.cmf.addComponentCSSClass(this.elementRef, this.itemJsonDef.classname);
            this.itemJsonDef = this.cmf.setItemDefaultProperty(this.itemJsonDef);
            this.formControl = new FormControl({ value: this.value, disabled: this.itemJsonDef.disable });
            let validators;
            if (this.itemJsonDef.mandatory) {
                validators = [Validators.required];
            }
            this.formControl.setValidators(validators);
        }
    }

    ngOnChanges(changes: SimpleChanges) {
    }

    ngAfterViewChecked(): void {
        this.cdr.detectChanges();
    }

    matSlideOnChange(e) {
        if (this.itemJsonDef.valuetype === 'dbfield' && this.sectionJsonData) {
            this.sectionJsonData[this.itemJsonDef.value] = this.formControl.value;
        } else {
            this.itemJsonDef.value = this.formControl.value;
        }
        this.sectionJsonDataChange.emit(this.sectionJsonData);
        this.OnChange.emit(e);
    }
}
