import { Component, QueryList, ViewContainerRef, AfterViewInit, ViewChild } from '@angular/core';
import { ICellEditorAngularComp } from 'ag-grid-angular';
import { WfxCommonFunctions } from '../wfxsharedfunction/wfxcommonfunction';
import { WFXGridCellAttributes } from '../wfxsharedmodels/wfxcommonmodel';

@Component({
  selector: 'app-wfxgridtextarea',
  template: `
    <div class="wfxgrid-textarea-div" (keydown)="onKeyDown($event)" [style.width]="columnWidth">
    <mat-form-field style="width:100%">
        <textarea class="wfxgrid-textarea-editable" #input matInput matTextareaAutosize [style.width]="columnWidth" [(ngModel)]="value"
            [disabled]="itemAttr.disable" [maxlength]="itemAttr.maxlength">
        </textarea>
    </mat-form-field>
    </div>
    `,
  styles: [``]
})
export class WfxGridTextAreaComponent implements AfterViewInit, ICellEditorAngularComp {
  constructor(public cmf: WfxCommonFunctions) { }
  private params: any;
  value: '';
  columnWidth: string;
  @ViewChild('input', { read: ViewContainerRef, static: true }) public input;
  public inputs: QueryList<any>;
  disable = false;
  itemAttr: WFXGridCellAttributes;
  focusAfterAttached: any;

  agInit(params: any): void {
    this.params = params;
    this.value = params.charPress ? params.charPress : params.value;
    if (!params.charPress) {
      this.focusAfterAttached = this.params.cellStartedEdit;
    }
    this.columnWidth = params.column.actualWidth - 2 + 'px';
    this.itemAttr = this.params.itemAttr;
    if (this.itemAttr === undefined || !this.itemAttr) {
      this.itemAttr = new WFXGridCellAttributes();
    }
    this.itemAttr = this.cmf.setGridItemDefaultProperties(this.itemAttr);
  }

  // dont use afterGuiAttached for post gui events - hook into ngAfterViewInit instead for this
  ngAfterViewInit() {
    this.focusOnInputNextTick(this.input);
    if (this.focusAfterAttached) {
      window.setTimeout(() => {
        this.input.element.nativeElement.focus();
        this.input.element.nativeElement.select();
      });
    }
  }

  private focusOnInputNextTick(input: ViewContainerRef) {
    setTimeout(() => {
      input.element.nativeElement.focus();
    }, 0);
  }

  getValue() {
    if (this.value) {
      return `${this.value}`;
    } else {
      return '';
    }
  }

  isPopup(): boolean {
    return true;
  }

  /*
   * A little over complicated for what it is, but the idea is to illustrate how you might tab between multiple inputs
   * say for example in full row editing
   */
  onKeyDown(event): void {
    const key = event.which || event.keyCode;
    if (key === 13 && event.shiftKey) {
      this.input.element.nativeElement.value = this.value + '\n';
      this.value = this.input.element.nativeElement.value;
      this.preventDefaultAndPropagation(event);
    } else {
      this.cmf.checkNStopPropagation(event);
    }
  }

  // will reject the number if it greater than 1,000,000
  // not very practical, but demonstrates the method.
  isCancelAfterEnd(): boolean {
    // return true if you want to cancel the input and set the old value
    // this.cmf.updateExtraDataObjectPropertyValue(this.params, 'invalidcell', true);
    return false;
  }

  private preventDefaultAndPropagation(event) {
    event.preventDefault();
    event.stopPropagation();
  }

}
