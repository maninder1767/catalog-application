import { Component, OnInit, ViewContainerRef, ViewChild, AfterViewInit } from '@angular/core';
import { ICellRendererAngularComp, ICellEditorAngularComp } from 'ag-grid-angular';
declare var $: any;

@Component({
  selector: 'app-wfxgridcheckbox',
  template: `
  <div id="gridCheckBoxDiv" (keydown)="onKeyDown($event)" *ngIf="params && params.node && !params.node.rowPinned">
  <mat-checkbox #gridCheckBox [ngModel]="checked"
    (ngModelChange)="onChange($event)" (keydown)="onKeyDown($event)"></mat-checkbox>
  </div> `,
  styles: [``]
})
export class WfxGridCheckBoxComponent implements AfterViewInit, ICellRendererAngularComp, ICellEditorAngularComp {
  params: any;
  checked = false;
  @ViewChild('gridCheckBox', { read: ViewContainerRef, static: false }) public gridCheckBox;

  agInit(params: any): void {
    if (params.node.rowPinned) {
      return;
    }
    const _this = this;
    this.params = params;
    this.checked = this.params.value === true;
  }

  ngAfterViewInit() {
    // this.gridCheckBox.element.nativeElement.focus();
  }

  getValue() {
    return this.checked;
  }

  // demonstrates how you can do "inline" editing of a cell
  onChange(checked: boolean) {
    this.checked = checked;
    this.params.node.setDataValue(this.params.colDef, this.checked ? true : false);
  }

  refresh(params: any): boolean {
    return false;
  }

  onKeyDown(event) {
  }
}
