import { Component, ViewContainerRef, AfterViewInit, ViewChild } from '@angular/core';
import { ICellEditorAngularComp } from 'ag-grid-angular';
import { WfxCommonFunctions } from '../wfxsharedfunction/wfxcommonfunction';
import { WFXGridCellAttributes } from '../wfxsharedmodels/wfxcommonmodel';
declare var $: any;

@Component({
  selector: 'app-wfxgridnumbertextbox',
  template: `
    <div class="wfxgrid-textbox-div" (keydown)="onKeyDown($event)" [style.width]="columnWidth">
    <mat-form-field style="width:100%">
        <input #input class="wfxgrid-numbertextbox-editable" matInput [(ngModel)]="value" appWfxnumbercdirective
        [disabled]="itemAttr.disable" [maxlength]="itemAttr.maxlength"
        [decimalplaces]="itemAttr.decimalplaces"
        [decimal]="itemAttr.decimalplaces > 0 ? true : false">
        <!--(keypress)="onKeyPress($event)" [min]="itemAttr.minvalue" [max]="itemAttr.maxvalue"-->
    </mat-form-field>
    </div>
    `,
  styles: [``]
})
export class WfxGridNumberTextBoxComponent implements AfterViewInit, ICellEditorAngularComp {
  constructor(public cmf: WfxCommonFunctions) { }
  params: any;
  value: any;
  columnWidth: string;
  @ViewChild('input', { read: ViewContainerRef, static: true }) public input;
  // @ViewChildren('input', { read: ViewContainerRef })
  // public inputs: QueryList<any>;
  // private focusedInput = 0;
  itemAttr: WFXGridCellAttributes;
  selectAfterAttached: any;
  cancelBeforeStart = false;

  agInit(params: any): void {
    this.params = params;
    this.columnWidth = params.column.actualWidth - 2 + 'px';
    this.itemAttr = this.params.itemAttr;
    if (this.itemAttr === undefined || !this.itemAttr) {
      this.itemAttr = new WFXGridCellAttributes();
    }
    this.itemAttr = this.cmf.setGridItemDefaultProperties(this.itemAttr);
    let allowedNumericCharacters = '1234567890';
    if (this.itemAttr.decimalplaces) {
      allowedNumericCharacters = allowedNumericCharacters + '.';
    }
    this.cancelBeforeStart = this.params.charPress && (allowedNumericCharacters.indexOf(this.params.charPress) < 0);
    this.value = !this.cancelBeforeStart && params.charPress ? params.charPress : this.params.value;
    if (!params.charPress) {
      this.selectAfterAttached = this.params.cellStartedEdit;
    }
    // if (this.itemAttr.inputsubtype === 'number') {
    //   $(() => {
    //     $('input').on('input', () => {
    //       const match = (/(\d{0,2})[^.]*((?:\.\d{0,4})?)/g).exec(this.value.replace(/[^\d.]/g, ''));
    //       this.value = match[1] + match[2];
    //     });
    //   });
    // }
  }

  // Return the DOM element of your editor, this is what the grid puts into the DOM
  // getGui(): HTMLElement {
  //   return this.input.element.nativeElement;
  // }

  // dont use afterGuiAttached for post gui events - hook into ngAfterViewInit instead for this
  ngAfterViewInit() {
    this.input.element.nativeElement.style.width = '100%';
    this.focusOnInputNextTick(this.input);
    // this.input.element.nativeElement.focus();
    if (this.selectAfterAttached) {
      window.setTimeout(() => {
        this.input.element.nativeElement.select();
      });
    }
  }

  private focusOnInputNextTick(input: ViewContainerRef) {
    setTimeout(() => {
      input.element.nativeElement.focus();
    }, 0);
  }

  getValue() {
    if (this.value) {
      return Number(this.value);
    } else {
      if (isNaN(this.value) || !this.value) {
        this.value = 0;
      }
      this.value = this.value ? Number(this.value).toFixed(this.itemAttr.decimalplaces) : this.value;
      return Number(this.value);
    }
  }

  isPopup(): boolean {
    return true;
  }

  /*
   * A little over complicated for what it is, but the idea is to illustrate how you might tab between multiple inputs
   * say for example in full row editing
   */
  onKeyDown(event): void {
    const key = event.which || event.keyCode;
    // if (key === 9) {
    //     // tab
    //     this.preventDefaultAndPropagation(event);

    //     // either move one input along, or cycle back to 0
    //     this.focusedInput = this.focusedInput === this.inputs.length - 1 ? 0 : this.focusedInput + 1;

    //     const focusedInput = this.focusedInput;
    //     const inputToFocusOn = this.inputs.find((item: any, index: number) => {
    //         return index === focusedInput;
    //     });

    //     this.focusOnInputNextTick(inputToFocusOn);
    // } else if (key === 13) {
    //     // enter
    //     // perform some validation on enter - in this example we assume all inputs are mandatory
    //     // in a proper application you'd probably want to inform the user that an input is blank
    //     this.inputs.forEach(input => {
    //         if (!input.element.nativeElement.value) {
    //             this.preventDefaultAndPropagation(event);
    //             this.focusOnInputNextTick(input);
    //         }
    //     });
    // }

    if ((this.itemAttr.inputsubtype === 'number' || this.itemAttr.inputsubtype === 'money')
      && (key === 69)) {
      // character e  || key === 38 || key === 40 up, down arraw
      this.preventDefaultAndPropagation(event);
    }
    this.cmf.checkNStopPropagation(event);
  }

  private preventDefaultAndPropagation(event) {
    event.preventDefault();
    event.stopPropagation();
  }

  // Gets called once after GUI is attached to DOM.
  // Useful if you want to focus or highlight a component
  // (this is not possible when the element is not attached)
  afterGuiAttached() {
    // if (this.selectAfterAttached) {
    //   this.input.element.nativeElement.focus();
    //   this.input.element.nativeElement.select();
    // }
  }

  // If doing full row edit, then gets called when tabbing into the cell.
  focusIn() {
    // const eInput = this.getGui();
    // eInput.focus();
    // eInput.onselect();
  }

  isCancelBeforeStart(): boolean {
    return this.cancelBeforeStart;
  }

  // will reject the number if it greater than 1,000,000
  // not very practical, but demonstrates the method.
  isCancelAfterEnd(): boolean {
    // return true if you want to cancel the input and set the old value
    if (this.itemAttr.inputsubtype === 'number' || this.itemAttr.inputsubtype === 'money') {
      if (this.itemAttr.minvalue !== '' &&
        (this.itemAttr.minvaluetype === 'text' && Number(this.value) < Number(this.itemAttr.minvalue))
        || this.itemAttr.minvaluetype === 'dbfield' && Number(this.value) < Number(this.params.data[this.itemAttr.minvalue])) {
        return true;
      }
      if (this.itemAttr.maxvalue !== '' &&
        (this.itemAttr.maxvaluetype === 'text' && Number(this.value) > Number(this.itemAttr.maxvalue))
        || this.itemAttr.maxvaluetype === 'dbfield' && Number(this.value) > Number(this.params.data[this.itemAttr.maxvalue])) {
        return true;
      }
      if (isNaN(this.value) || !this.value) {
        this.value = 0;
      }
      this.value = this.value ? Number(this.value).toFixed(this.itemAttr.decimalplaces) : this.value;
      return false;
    }
  }

  // onKeyPress(e) {
  // stop decimal places
  // const input = String.fromCharCode(e.charCode);
  // const reg = /^\d*(?:[.,]\d{1,2})?$/;
  // if (!reg.test(input)) {
  //   e.preventDefault();
  // }
  // if (e.keyCode === 46 && this.value.split('.').length === 2) {
  //   return false;
  // }
  // }
}
