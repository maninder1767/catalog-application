import { Component, ViewChild, ViewContainerRef, AfterViewInit, OnDestroy, OnChanges } from '@angular/core';
import { ICellEditorAngularComp } from 'ag-grid-angular';
import { FormControl } from '@angular/forms';
import { ReplaySubject, Subject } from 'rxjs';
import { MatSelect } from '@angular/material/select';
import { takeUntil, take } from 'rxjs/operators';
import { WfxCommonFunctions } from '../wfxsharedfunction/wfxcommonfunction';
import { WFXResultModel, WFXGridCellAttributes } from '../wfxsharedmodels/wfxcommonmodel';
import { ICellEditorParams } from 'ag-grid-community';
import { PageParam } from '../wfxsharedmodels/wfxqueryparam';
import { WfxcommonService } from '../wfxsharedservices/wfxcommon.service';
import { MatOption } from '@angular/material';
import { WfxShowMessageservice } from '../wfxsharedservices/wfxshowmessage.service';
import { ItemLibraryDataService } from '../../wfxitemshared/services/itemdata.service';

@Component({
  selector: 'app-wfxgridselect',
  template: `
    <mat-card>
    <!--[compareWith]="compareFn" -->
        <div class="wfxgrid-select-div" #group tabindex="0" (keydown)="onKeyDown($event)" [style.width]="columnWidth"
        [style.min-width]="columnWidth">
        <mat-form-field style="width:100%">
            <mat-select #select panelClass="ag-custom-component-popup"
            [formControl]="selectFormControl" (openedChange)="openedChange($event)"
                [multiple]="itemAttr.multiselect" (selectionChange)="selectOnChange($event)"
                disableOptionCentering = "true">
            <mat-option class="gridSelectSearch">
                <ngx-mat-select-search [placeholderLabel]="'Find...'" [preventHomeEndKeyPropagation]="true"
                    [noEntriesFoundLabel]="'no matching entry found'" [clearSearchInput]="true"
                    [formControl]="selectFilterFormControl">
                </ngx-mat-select-search>
            </mat-option>
            <mat-option class="gridSelectOptionClass" #allSelected *ngIf="!isLoading &&
              itemAttr.addselect  && !selectFilterFormControl.value"
              [value]="-1"> {{itemAttr.addselecttext}}
            </mat-option>
            <mat-option class="gridSelectOptionClass" *ngIf="isOpen && isLoading">
                <mat-label>
                    <div class="gridSelect-placeholder-container">
                        <span>Loading...</span>
                        <mat-spinner class="spinner" diameter="20"></mat-spinner>
                    </div>
                </mat-label>
            </mat-option>
            <mat-option [disabled]="disableOptions" class="gridSelectOptionClass" *ngFor="let optn of optionList | async"
            [value]="optn[itemAttr.ddlvalue]">
                {{optn[itemAttr.ddlvaluetext]}}
            </mat-option>
            </mat-select>
        </mat-form-field>
        </div>
        <!--(click)="toggleAllSelection()"-->
    </mat-card>
`,
  styles: []
})
export class WfxGridSelectComponent implements ICellEditorAngularComp, AfterViewInit, OnDestroy, OnChanges {
  constructor(public cmf: WfxCommonFunctions, public commonsvc: WfxcommonService,
    private msgsvc: WfxShowMessageservice, private itemsvc: ItemLibraryDataService) { }
  params: any;


  isLoading = true;
  isOpen = false;
  columnWidth: string;

  @ViewChild('group', { read: ViewContainerRef, static: true })
  public group;

  /** list of dropdown Options */
  protected dropdownList: any[] = [];
  /** control for the selected Records for multi-selection */
  public selectFormControl: FormControl;
  /** control for the MatSelect filter keyword multi-selection */
  public selectFilterFormControl: FormControl = new FormControl();
  /** list of Records filtered by search keyword */
  public optionList: ReplaySubject<any[]> = new ReplaySubject<any[]>(1);
  @ViewChild('select', { static: true }) select: MatSelect;
  @ViewChild('allSelected', { static: false }) private allSelected: MatOption;

  /** Subject that emits when the component has been destroyed. */
  protected OnDestroy = new Subject<void>();

  selectedIndexes = [];
  selectedValues = [];
  selectedValuesText = [];
  addedSelectedValues = [];

  selectedValueText: string;
  selectedValue: string;
  selectedIndex: number;

  itemAttr: WFXGridCellAttributes;
  disableOptions = false;

  // aggrid ICellEditorAngularComp lifecycle https://www.ag-grid.com/javascript-grid-cell-editor/
  agInit(params: any): void {
    this.params = params;
    this.columnWidth = params.column.actualWidth - 2 + 'px';
    this.itemAttr = this.params.itemAttr;
    if (this.itemAttr === undefined || !this.itemAttr) {
      this.itemAttr = new WFXGridCellAttributes();
    }
    this.itemAttr = this.cmf.setGridItemDefaultProperties(this.itemAttr);
    this.dropdownList = this.itemAttr.ddlvalues;
    if (!this.dropdownList || this.itemAttr.autobindddl) {
      this.isLoading = true;
      let pageParam: PageParam[] = [];
      if (this.itemAttr.attributelist && this.itemAttr.attributelist.length > 0) {
        pageParam = this.cmf.createPageParamsFromAttributeList(this.itemAttr.attributelist, this.params);
      } else {
        const fn = params.context.componentParent.gridProperties.id + '_getDDLPageParams';
        if (typeof params.context.mainParentComponent[fn] === 'function') {
          pageParam = params.context.mainParentComponent[fn](params);
          if (!pageParam || pageParam.length <= 0) {
            this.msgsvc.showSnackBar('No pageparams found!', '', 'Error');
            return;
          }
        } else {
          pageParam.push(new PageParam('MetaDataFor', 'City'));
          pageParam.push(new PageParam('fromPage', 'wfxgrid'));
          pageParam.push(new PageParam('objectName', 'DDLCity'));
        }
      }
      if (pageParam.length <= 0) {
        this.msgsvc.showSnackBar('There is not any params for getting the dropdown data. Please check!', '', 'Error');
        return;
      }
      // this.commonsvc.bindDDL('WFXCommon/GetData', pageParam)
      const key = this.cmf.createKeyFromPageParams(pageParam);
      const urlParam = this.cmf.createURLParamsFromPageParams(pageParam);
      this.itemsvc.bindDDL('Master/Data?' + urlParam, key)
        .pipe()
        .subscribe((res: any) => {
          this.isLoading = false;
          // this.dropdownList = res.ResponseData;
          this.dropdownList = res.data;
          this.setSelectedValue();
          if (this.dropdownList && this.dropdownList.length > 0) {
            this.optionList.next(this.dropdownList.slice());
          }
          this.toggleAllSelection();
        });
    } else {
      this.isLoading = this.params.context.componentParent.ddlLoading;
    }
    if (!this.itemAttr.autobindddl && (!this.dropdownList || (this.dropdownList && this.dropdownList.length <= 0))) {
      this.isLoading = false;
    }
    if (this.itemAttr.multiselect) {
      if (this.params.value && ((Array.isArray(this.params.value) && this.params.value.length > 0)
        || (!Array.isArray(this.params.value) && this.params.value !== ''))) {
        if (Array.isArray(this.params.value) && this.params.value.length > 0) {
          this.selectedValuesText = this.params.value;
        } else {
          this.selectedValuesText.push(this.params.value);
        }
        this.setSelectedValue();
      }
      this.selectFormControl = new FormControl({ value: this.selectedValuesText, disabled: this.itemAttr.disable });
    } else {
      this.selectedValueText = this.params.value;
      this.setSelectedValue();
      this.selectFormControl = new FormControl({ value: this.selectedValueText, disabled: this.itemAttr.disable });
    }

    if (this.dropdownList) {
      // load the initial Records list
      this.optionList.next(this.dropdownList.slice());
    }

    // listen for search field value changes
    this.selectFilterFormControl.valueChanges
      .pipe(takeUntil(this.OnDestroy))
      .subscribe(() => {
        this.filterRecordsMulti();
      });
  }

  ngOnChanges() {
  }

  ngOnDestroy() {
    this.OnDestroy.next();
    this.OnDestroy.complete();
  }

  // Gets called once after GUI is attached to DOM.
  // Useful if you want to focus or highlight a component
  // (this is not possible when the element is not attached)
  // afterGuiAttached() {
  // }

  // // Return the DOM element of your editor, this is what the grid puts into the DOM
  // getGui() {
  // }

  // Gets called once by grid after editing is finished
  // if your editor needs to do any cleanup, do it here
  destroy() {
  }

  // Gets called once before editing starts, to give editor a chance to
  // cancel the editing before it even starts.
  isCancelBeforeStart() {
    // if (!this.dropdownList) {
    //   return true;
    // }
    return false;
  }

  // Gets called once when editing is finished (eg if enter is pressed).
  // If you return true, then the result of the edit will be ignored.
  isCancelAfterEnd() {
    if (this.itemAttr.multiselect) {
      this.cmf.updateDDLValueOnDDLChanged(this.params, this.selectFormControl.value);
    }
    return false;
  }

  // If doing full row edit, then gets called when tabbing into the cell.
  focusIn() {
  }

  // If doing full row edit, then gets called when tabbing out of the cell.
  focusOut() {
  }

  compareFn(a: any, b: any, _this) {
    _this = this || _this;
    // b is selectedValueText and a is selectedValueCode
    if (_this.itemAttr.multiselect) {
      let selectedValue;
      if (b !== _this.itemAttr.addselecttext && _this.dropdownList && _this.dropdownList.length > 0) {
        selectedValue = _this.dropdownList.filter(item => {
          return item[_this.itemAttr.ddlvaluetext] === b;
        })[0][_this.itemAttr.ddlvalue].toString();
      }
      if (a && b && selectedValue && _this.selectedValuesText && _this.selectedValues && a.toString() === selectedValue.toString() &&
        _this.selectedValues.includes(a.toString()) && _this.selectedValuesText.includes(b.toString())) {
        return true;
      } else if (b === _this.itemAttr.addselecttext && _this.selectedValuesText.includes(b.toString())
        && _this.selectedValues.includes('-1')) {
        return true;
      } else if (a && b) {
        return a.toString() === b.toString();
      } else {
        return a === b;
      }
    } else {
      if (a && b && _this.selectedValueText && _this.selectedValueText.toString() !== ''
        && _this.selectedValue && _this.selectedValue.toString() !== '' &&
        _this.selectedValueText.toString() === b.toString() && a.toString() === _this.selectedValue.toString()) {
        return true;
      } else if (a && b) {
        return a.toString() === b.toString();
      } else {
        return a === b;
      }
    }
  }

  // dont use afterGuiAttached for post gui events - hook into ngAfterViewInit instead for this
  ngAfterViewInit() {
    // window.setTimeout(() => {
    //     this.group.element.nativeElement.focus();
    // }, 1000);
    // this.select.open();
    this.selectfavouriteValueBasedOnSelectedIndex();
    this.setInitialValue();
    this.group.element.nativeElement.style.width = '100%';
    window.setTimeout(() => {
      this.focusNOpenSelect();
    }, 0);
  }

  focusNOpenSelect() {
    this.group.element.nativeElement.focus();
    this.select.open();
    this.toggleAllSelection();
  }

  setSelectedValue() {
    if (this.itemAttr.multiselect) {
      if (this.selectedValuesText && this.selectedValuesText.toString() === this.itemAttr.addselecttext) {
        this.selectedValues.push('-1');
      } else if (this.selectedValuesText && this.dropdownList) {
        this.selectedValuesText.forEach(e =>
          this.selectedValues.push(this.dropdownList.filter(item => {
            return item[this.itemAttr.ddlvaluetext] === e;
          })[0][this.itemAttr.ddlvalue]));
      }
    } else {
      if (this.selectedValueText &&
        (this.selectedValueText.toString() === this.itemAttr.addselecttext || this.selectedValueText.toString() === '-1')) {
        this.selectedValue = '-1';
      } else {
        if (this.dropdownList && this.selectedValueText) {
          this.selectedIndex = this.dropdownList.findIndex(item => {
            return item[this.itemAttr.ddlvaluetext] === this.selectedValueText;
          });
          if (this.selectedIndex >= 0) {
            this.selectedValue = this.dropdownList.filter(item => {
              return item[this.itemAttr.ddlvaluetext] === this.selectedValueText;
            })[0][this.itemAttr.ddlvalue];
          }
        }
      }
    }

  }

  private selectfavouriteValueBasedOnSelectedIndex() {
    // For Multiple Select
    // if (this.selectedValueText === undefined) {
    //     this.selectedValueText = [];
    // }
    // this.selectedValueText.push(this.values[this.selectedIndex]);
    // if (this.dropdownList && this.selectedIndex >= 0) {
    //   this.selectedValueText = this.dropdownList[this.selectedIndex][this.itemAttr.ddlvaluetext];
    // }
    // window.setTimeout(() => {
    //     this.group.element.nativeElement.focus();
    // }, 1000);
  }

  getValue() {
    const _this = this;
    if (this.itemAttr.multiselect) {
      let selectedValuesText = [];
      if (this.selectFormControl.value.toString() === '-1'
        || (Array.isArray(this.selectFormControl.value) && this.selectFormControl.value.length > 0
          && this.selectFormControl.value[0].toString() === this.itemAttr.addselecttext)) {
        selectedValuesText = this.params.itemAttr.addselecttext;
      } else {
        this.selectFormControl.value.forEach(e => {
          let filteredData = this.dropdownList.filter(x => x[_this.itemAttr.ddlvalue] === e);
          if (filteredData && filteredData.length > 0) {
            selectedValuesText.push(filteredData[0][_this.itemAttr.ddlvaluetext]);
          } else {
            filteredData = this.dropdownList.filter(x => x[_this.itemAttr.ddlvaluetext] === e);
            if (filteredData && filteredData.length > 0) {
              selectedValuesText.push(filteredData[0][_this.itemAttr.ddlvaluetext]);
            }
          }
        });
      }

      return selectedValuesText;
    } else {
      let selectedValueText;
      if (this.selectFormControl.value !== undefined && typeof (this.selectFormControl.value) !== 'object') {
        // this.selectFormControl.value is the selected value not valuetext
        selectedValueText = this.selectFormControl.value;
        if (selectedValueText) {
          if (selectedValueText.toString() === '-1') {
            selectedValueText = 'NONE';
          } else {
            let filteredData = this.dropdownList.filter(e => e[_this.itemAttr.ddlvalue] === selectedValueText);
            if (filteredData && filteredData.length > 0) {
              selectedValueText = filteredData[0][_this.itemAttr.ddlvaluetext];
            } else {
              filteredData = this.dropdownList.filter(e => e[_this.itemAttr.ddlvaluetext] === selectedValueText);
              if (filteredData && filteredData.length > 0) {
                selectedValueText = filteredData[0][_this.itemAttr.ddlvaluetext];
              }
            }
          }
        }
      } else {
        selectedValueText = this.params.value;
      }
      return selectedValueText;
    }
  }

  openedChange(e) {
    // if Open then e true
    this.params.context.componentParent.ddlOpenedChange(e, this.params);
    this.isOpen = e;
    if (!this.itemAttr.autobindddl && (!this.dropdownList || (this.dropdownList && this.dropdownList.length <= 0))) {
      this.isLoading = false;
    } else {
      this.isLoading = true;
    }
    if (this.dropdownList && this.dropdownList.length > 0) {
      this.isLoading = false;
    }
    if (this.itemAttr.multiselect) {
      this.toggleAllSelection();
    }
    if (!e) {
      this.params.stopEditing();
    }
  }

  selectOnChange(e?) {
    if (!this.itemAttr.multiselect) {
      this.cmf.updateDDLValueInJson(this.params, e.value); // I need to get this value in next column value formatter and value updates after the params.stopediting.
      this.params.stopEditing();
      this.cmf.updateDDLValueOnDDLChanged(this.params, undefined, this.dropdownList);
    } else {
      // When pressing enter then this function executes.
      this.toggleAllSelection();
    }
    // const fn = this.params.context.componentParent.gridProperties.id + '_OnCellValueChanged';
    // if (typeof this.params.context.mainParentComponent[fn] === 'function') {
    //   const colID = this.params.colDef.colId || this.params.colDef.field;
    //   this.params.context.mainParentComponent[fn](colID, this.params);
    // }
  }

  isPopup(): boolean {
    return true;
  }

  /*
   * A little over complicated for what it is, but the idea is to illustrate how you might navigate through the radio
   * buttons with up & down keys (instead of finishing editing)
   */
  onKeyDown(event): void {
    const key = event.which || event.keyCode;
    if (key === 13) {
      this.select.open();
      this.preventDefaultAndPropagation(event);
    } else if (key === 38 || key === 40) {
      this.preventDefaultAndPropagation(event);

      // if (key === 38) {
      //   // up
      //   this.selectedIndex = this.selectedIndex === 0 ? this.dropdownList.length - 1 : this.selectedIndex - 1;
      // } else if (key === 40) {
      //   // down
      //   this.selectedIndex = this.selectedIndex === this.dropdownList.length - 1 ? 0 : this.selectedIndex + 1;
      // }
      // this.selectfavouriteValueBasedOnSelectedIndex();
    }
  }

  private preventDefaultAndPropagation(event) {
    event.preventDefault();
    event.stopPropagation();
  }

  protected setInitialValue() {
    const _this = this;
    this.optionList
      .pipe(take(1), takeUntil(this.OnDestroy))
      .subscribe(() => {
        this.select.compareWith = (a: any, b: any) => _this.compareFn(a, b, this);
      });
  }

  protected filterRecordsMulti() {
    if (!this.dropdownList) {
      return;
    }
    let search = this.selectFilterFormControl.value;
    if (!search) {
      this.optionList.next(this.dropdownList.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    this.optionList.next(
      this.dropdownList.filter(e => this.returnfilteredRecords(e, search))
    );
  }

  returnfilteredRecords(e, search) {
    return e[this.itemAttr.ddlvaluetext].toLowerCase().indexOf(search) > -1;
  }

  toggleAllSelection() {
    if (this.itemAttr.multiselect) {
      if (((this.allSelected && this.allSelected.selected) || (!this.allSelected
        && this.params.value === this.itemAttr.addselecttext)) && this.select && this.select.options) {
        this.select.options.forEach(item => {
          if (item.value && item.value.toString() !== '-1') {
            item.deselect();
          }
        });
        // this.params.value wala case first time load k liye h bcz tb tk allSelected init nhi hota
        this.disableOptions = true;
      } else {
        this.disableOptions = false;
      }
    }

    // if (this.allSelected.selected) {
    //   this.selectFormControl
    //     .patchValue([...this.dropdownList.map(item => item.Code), 0]);
    // } else {
    //   this.selectFormControl.patchValue([]);
    // }
  }
}
