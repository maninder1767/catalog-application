/*
WFXControls v1.0.0 (2019-April)
Copyright (c) 2019-2019 WFX.
License: https://www.wfxcloud.com/contact.html
*/
/* eslint-disable */

import {
    Component, Input, Output, EventEmitter, ElementRef, OnChanges, SimpleChanges, OnInit,
    ChangeDetectorRef, AfterViewChecked
} from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { WfxComponentAttributes } from '../wfxsharedmodels/wfxcommonmodel';
import { WfxCommonFunctions } from '../wfxsharedfunction/wfxcommonfunction';


@Component({
    selector: 'app-wfxradiobutton',
    template: `
    <form [hidden]="!itemJsonDef.visible">
    <div class="sectionRadioButtonDiv">
        <mat-radio-group [id]="itemJsonDef.itemid" [labelPosition]="itemJsonDef.labelposition"
        [value]="value" (input)="value = $event.value" (change)="radioButtonOnChange($event)"
        [formControl]="formControl" [matTooltip] = "tooltipValue" [matTooltipPosition] = "itemJsonDef.tooltip.tooltipposition"
        matTooltipClass = "sectiontooltipClass">
            <mat-radio-button *ngFor="let rb of itemJsonDef.rbList" [value]="rb">
                {{rb}}
            </mat-radio-button>
        </mat-radio-group>
    </div>
    </form>`
})

export class WfxRadioButtonComponent implements OnInit, OnChanges, AfterViewChecked {
    constructor(containerElement: ElementRef, private cdr: ChangeDetectorRef, public cmf: WfxCommonFunctions) {
        this.elementRef = containerElement;
    }

    @Input() ParentComponent: Component;
    @Input() itemJsonDef: WfxComponentAttributes;
    @Input() sectionJsonData: any;
    @Output() sectionJsonDataChange = new EventEmitter<any>();
    @Output() OnChange = new EventEmitter<any>();

    formControl = new FormControl();
    elementRef: ElementRef;

    get value() {
        return this.cmf.getItemValue(this.itemJsonDef, this.sectionJsonData);
    }

    set value(val) {
    }

    get tooltipValue() {
        return this.cmf.getToolTipValue(this.itemJsonDef, this.sectionJsonData);
    }

    ngOnInit() {
        if (this.itemJsonDef) {
            this.elementRef = this.cmf.addComponentCSSClass(this.elementRef);
            this.itemJsonDef = this.cmf.setItemDefaultProperty(this.itemJsonDef);
            this.formControl = new FormControl({ value: this.value, disabled: this.itemJsonDef.disable });
            let validators;
            if (this.itemJsonDef.mandatory) {
                validators = [Validators.required];
            }
            this.formControl.setValidators(validators);
        }
    }

    ngOnChanges(changes: SimpleChanges) {
    }

    ngAfterViewChecked(): void {
        this.cdr.detectChanges();
    }

    radioButtonOnChange(e) {
        if (this.itemJsonDef.valuetype === 'dbfield' && this.sectionJsonData) {
            this.sectionJsonData[this.itemJsonDef.value] = this.formControl.value;
        } else {
            this.itemJsonDef.value = this.formControl.value;
        }
        this.sectionJsonDataChange.emit(this.sectionJsonData);
        this.OnChange.emit(e);
    }
}
