/*
WFXControls v1.0.0 (2019-April)
Copyright (c) 2019-2019 WFX.
License: https://www.wfxcloud.com/contact.html
*/
/* eslint-disable */

import { Component, ElementRef, OnChanges, SimpleChanges, OnInit, ChangeDetectorRef, AfterViewChecked } from '@angular/core';

@Component({
    selector: 'app-wfxlayout',
    template: `
    <form class="container-fluid grid-Wrapper">
    <div class="row">
    <div class="col-lg-12 row">
        <ng-content></ng-content>
    </div>
    </div>
    </form>`
})

export class WfxLayoutComponent implements OnInit, OnChanges, AfterViewChecked {
    constructor(containerElement: ElementRef, private cdr: ChangeDetectorRef) {
    }

    ngOnInit() {
    }

    ngOnChanges(changes: SimpleChanges) {
    }

    ngAfterViewChecked(): void {
        this.cdr.detectChanges();
    }
}
