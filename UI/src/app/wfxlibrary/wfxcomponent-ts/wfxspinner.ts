/*
WFXControls v1.0.0 (2019-April)
Copyright (c) 2019-2019 WFX.
License: https://www.wfxcloud.com/contact.html
*/
/* eslint-disable */

import { Component, OnChanges, SimpleChanges, OnInit, AfterViewChecked } from '@angular/core';


@Component({
  selector: 'app-wfxspinner',
  template: `
    <div class="spinnerDiv">
      <img src="./assets/100w.gif">
    </div>
    <!--<img src="./app/wfxlibrary/wfxsharedimages/100w.gif">-->
    `

})

export class WfxSpinnerComponent implements OnInit, OnChanges, AfterViewChecked {
  constructor() { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
  }

  ngAfterViewChecked(): void {
  }
}
