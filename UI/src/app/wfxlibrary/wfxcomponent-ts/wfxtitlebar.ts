/*
WFXControls v1.0.0 (2019-April)
Copyright (c) 2019-2019 WFX.
License: https://www.wfxcloud.com/contact.html
*/
/* eslint-disable */

import {
    Component, Input, Output, EventEmitter, ElementRef, OnChanges, SimpleChanges, OnInit,
    ChangeDetectorRef, AfterViewChecked
} from '@angular/core';
import { WfxToolAction, WfxTitleBar } from '../wfxsharedmodels/wfxcommonmodel';
import { WfxCommonFunctions } from '../wfxsharedfunction/wfxcommonfunction';

@Component({
    selector: 'app-wfxtitlebar',
    template: `
    <form>
    <div [id]="itemJsonDef.titlebarid"
        [class]="itemJsonDef.titlebartype === 'pagetitlebar' ? 'PageTitleBarDiv' : 'SectionTitleBarDiv'">
    <div class = "TitleBarDiv">
        <mat-label [matTooltipPosition] = "itemJsonDef.tooltip.tooltipposition" [matTooltip]="tooltipValue"
            matTooltipClass = "sectiontooltipClass"> {{itemJsonDef.caption}} </mat-label>
        <div class="titleBarButtonDiv">
        <button *ngFor="let tool of itemJsonDef.toollist" [attr.disabled]="tool.disable ? 'disabled' : null"
            [class] = "!tool.visible ? 'hideTitleBarButton' : tool.disable ? 'disableTitleBarButton' : ''"
            [style.display]="tool.visible?'block':'none'" (click)="toolOnClick(tool,$event)"
            [matTooltipPosition] = "tool.tooltip && tool.tooltip.tooltipposition &&
            tool.tooltip.tooltipposition !== undefined ? tool.tooltip.tooltipposition : 'below'"
            [matTooltip]="tool.tooltip && tool.tooltip.tooltipvalue !== ''
                && tool.tooltip.tooltipvalue !== undefined ? tool.tooltip.tooltipvalue : tool.toolname"
             matTooltipClass = "sectiontooltipClass">
            {{tool.toolname}}
        </button>
        </div>
    </div>
    </div>
    </form>`
})

export class WfxTitleBarComponent implements OnInit, OnChanges, AfterViewChecked {
    constructor(containerElement: ElementRef, private cdr: ChangeDetectorRef, public cmf: WfxCommonFunctions) {
        this.elementRef = containerElement;
    }

    @Input() ParentComponent: Component;
    @Input() itemJsonDef: WfxTitleBar;
    @Output() OnClick = new EventEmitter<object>();
    @Input() colLgClass = 'col-lg-12';

    elementRef: ElementRef;

    get tooltipValue() {
        return this.cmf.getToolTipValue(this.itemJsonDef);
    }

    ngOnInit() {
        if (this.itemJsonDef) {
            this.itemJsonDef = this.cmf.setItemDefaultProperty(this.itemJsonDef);
            this.itemJsonDef.titlebarid = this.cmf.getItemID(undefined, this.itemJsonDef);
            this.itemJsonDef.tooltip.tooltipposition = this.cmf.getToolTipPosition(this.itemJsonDef);
            this.elementRef = this.cmf.addComponentCSSClass(this.elementRef, this.colLgClass);
        }
    }

    ngOnChanges(changes: SimpleChanges) {
    }

    ngAfterViewChecked(): void {
        this.cdr.detectChanges();
    }

    toolOnClick(action, e) {
        const toolObj = new WfxToolAction();
        toolObj.actionName = action.toolname;
        toolObj.toolObj = action;
        toolObj.event = e;
        this.OnClick.emit(toolObj);
    }
}
