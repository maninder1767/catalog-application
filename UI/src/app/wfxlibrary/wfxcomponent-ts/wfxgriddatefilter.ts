import { Component, OnInit, ViewChild, AfterViewInit, ElementRef } from '@angular/core';
import { IDateParams } from 'ag-grid-community';
import { AgEditorComponent, AgFilterComponent } from 'ag-grid-angular';
import { MatDatepicker, MatDatepickerInputEvent } from '@angular/material/datepicker';

@Component({
  selector: 'app-wfxgriddatefilter',
  template: `
      <div class="gridDateFilter" class="ag-custom-component-popup ag-input-text-wrapper custom-date-filter">
        <mat-form-field>
            <input matInput #dtPicker [matDatepicker]="picker" [(ngModel)]="value"
            (dateInput)="addEvent('input', $event)" (dateChange)="dateOnChange($event)"
            (click)="picker.open()"> <!--placeholder="Filter..."-->
            <!--<mat-datepicker-toggle matSuffix [for]="picker"></mat-datepicker-toggle>-->
            <mat-datepicker panelClass="ag-custom-component-popup" #picker (closed) = "datePickerClosed($event)"
              (selectedChanged)="onSelectChange($event)"></mat-datepicker>
        </mat-form-field><!--appearance="outline"-->
      </div>
    `,
  styles: [`
  .custom-date-filter a {
    position: absolute;
    right: 20px;
    color: rgba(0, 0, 0, 0.54);
    cursor: pointer;
  }

  .custom-date-filter:after {
    position: absolute;
    content: '\f073';
    display: block;
    font-weight: 400;
    font-family: 'Font Awesome 5 Free';
    right: 5px;
    pointer-events: none;
    color: rgba(0, 0, 0, 0.54);
  }
    `]
})
export class WFXGridDateFilterComponent implements OnInit, AgEditorComponent, AfterViewInit {
  columnWidth: string;
  params: IDateParams;
  value: Date;
  private date: Date;
  @ViewChild('picker', { read: MatDatepicker, static: true }) picker: MatDatepicker<Date>;
  @ViewChild('dtPicker', { static: true }) dtPickerInput: ElementRef;

  constructor() {
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
  }


  isPopup(): boolean {
    return false;
  }

  isCancelBeforeStart(): boolean {
    return false;
  }

  isCancelAfterEnd(): boolean {
    return false;
  }

  agInit(params: any): void {
    this.params = params;
    this.value = params.value;
  }

  getValue(): Date {
    return this.value;
  }

  onSelectChange(e): void {
    this.params.onDateChanged();
    // setTimeout(function () {
    //   this.params.stopEditing();
    // }.bind(this));
  }

  getDate(): Date {
    return this.date;
  }

  setDate(date: Date): void {
    // this.value = null;
    // this.date = null;
    this.date = date || null;
    this.value = this.date || null;
  }
  addEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    this.dateOnChange(event);
  }

  dateOnChange(e: MatDatepickerInputEvent<Date>) {
    this.date = e.value;
    this.params.onDateChanged();
  }

  datePickerClosed(e) {
    this.dtPickerInput.nativeElement.blur();
  }

}
