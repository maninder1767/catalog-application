/*
WFXControls v1.0.0 (2019-April)
Copyright (c) 2019-2019 WFX.
License: https://www.wfxcloud.com/contact.html
*/
/* eslint-disable */

import {
    Component, Input, Output, EventEmitter, ElementRef, OnChanges, SimpleChanges, OnInit,
    ChangeDetectorRef, AfterViewChecked
} from '@angular/core';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { WfxComponentAttributes } from '../wfxsharedmodels/wfxcommonmodel';
import { WfxCommonFunctions } from '../wfxsharedfunction/wfxcommonfunction';

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
    isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
        const isSubmitted = form && form.submitted;
        return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
    }
}

@Component({
    selector: 'app-wfxtextarea',
    template: `
    <form [hidden]="!itemJsonDef.visible">
    <div class="sectionTextAreaDiv">
    <mat-form-field [floatLabel]="itemJsonDef.floatlabel">
        <textarea matInput matTextareaAutosize [id]="itemJsonDef.itemid"
        [style.width]="itemJsonDef.width" [required]="itemJsonDef.mandatory"
        [readonly] = "itemJsonDef.readonly" [value]="value" (input)="value = $event.target.value"
        [maxlength]="itemJsonDef.maxlength" [placeholder]="itemJsonDef.caption"
        (change)="txtAreaOnChange($event)" (keypress)="txtAreaOnKeyPress($event)" [errorStateMatcher]="matcher" [formControl]="formControl"
        [matTooltip] = "tooltipValue" [matTooltipPosition] = "itemJsonDef.tooltip.tooltipposition" matTooltipClass = "sectiontooltipClass">
        </textarea>
        <mat-error *ngIf="formControl.hasError('required')">
            {{itemJsonDef.caption}} is <strong>required</strong>
        </mat-error>
    </mat-form-field>
</div>
</form>`
})

export class WfxTextareaComponent implements OnInit, OnChanges, AfterViewChecked {
    constructor(containerElement: ElementRef, private cdr: ChangeDetectorRef, public cmf: WfxCommonFunctions) {
        this.elementRef = containerElement;
    }

    @Input() ParentComponent: Component;
    @Input() itemJsonDef: WfxComponentAttributes;
    @Input() sectionJsonData: any;
    @Output() sectionJsonDataChange = new EventEmitter<any>();
    @Output() OnChange = new EventEmitter<any>();
    @Output() OnKeyPress = new EventEmitter<any>();

    matcher = new MyErrorStateMatcher();
    formControl = new FormControl();
    elementRef: ElementRef;

    get value() {
        return this.cmf.getItemValue(this.itemJsonDef, this.sectionJsonData);
    }

    set value(val) {
        if (this.itemJsonDef.valuetype === 'dbfield' && this.sectionJsonData) {
            this.sectionJsonData[this.itemJsonDef.value] = val;
        } else {
            this.itemJsonDef.value = val;
        }
    }

    get tooltipValue() {
        return this.cmf.getToolTipValue(this.itemJsonDef, this.sectionJsonData);
    }

    ngOnInit() {
        if (this.itemJsonDef) {
            this.elementRef = this.cmf.addComponentCSSClass(this.elementRef);
            this.itemJsonDef = this.cmf.setItemDefaultProperty(this.itemJsonDef);
            this.formControl = new FormControl({ value: this.value, disabled: this.itemJsonDef.disable });
            let validators;
            if (this.itemJsonDef.mandatory) {
                validators = [Validators.required];
            }
            this.formControl.setValidators(validators);
        }
    }

    ngOnChanges(changes: SimpleChanges) {
    }

    ngAfterViewChecked(): void {
        this.cdr.detectChanges();
    }

    txtAreaOnChange(e) {
        this.sectionJsonDataChange.emit(this.sectionJsonData);
        this.OnChange.emit(e);
    }

    txtAreaOnKeyPress(e) {
      this.OnKeyPress.emit(e);
    }
}
