/*
WFXControls v1.0.0 (2019-April)
Copyright (c) 2019-2019 WFX.
License: https://www.wfxcloud.com/contact.html
*/
/* eslint-disable */

import {
    Component, Input, Output, EventEmitter, ElementRef, OnChanges, SimpleChanges, OnInit,
    ChangeDetectorRef, AfterViewChecked
} from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { WfxComponentAttributes } from '../wfxsharedmodels/wfxcommonmodel';
import { WfxCommonFunctions } from '../wfxsharedfunction/wfxcommonfunction';


@Component({
    selector: 'app-wfxcheckbox',
    template: `
    <form [hidden]="!itemJsonDef.visible">
    <div class="sectionCheckBoxDiv">
    <mat-checkbox [id]="itemJsonDef.itemid" [style.width]="itemJsonDef.width" [checked]="value"
        (input)="value = $event.checked" [labelPosition]="itemJsonDef.labelposition"
        (change)="checkBoxOnChange($event)" [formControl]="formControl"
        [matTooltip] = "tooltipValue" [matTooltipPosition] = "itemJsonDef.tooltip.tooltipposition" matTooltipClass = "sectiontooltipClass">
    {{itemJsonDef.caption}} </mat-checkbox>
</div>
</form>`
})

export class WfxCheckboxComponent implements OnInit, OnChanges, AfterViewChecked {
    constructor(containerElement: ElementRef, private cdr: ChangeDetectorRef, public cmf: WfxCommonFunctions) {
        this.elementRef = containerElement;
    }

    @Input() ParentComponent: Component;
    @Input() itemJsonDef: WfxComponentAttributes;
    @Input() sectionJsonData: any;
    @Output() sectionJsonDataChange = new EventEmitter<any>();
    @Output() OnChange = new EventEmitter<any>();

    formControl: FormControl;
    elementRef: ElementRef;

    get value() {
        return this.cmf.getItemValue(this.itemJsonDef, this.sectionJsonData);
    }

    set value(val) {
    }

    get tooltipValue() {
        return this.cmf.getToolTipValue(this.itemJsonDef, this.sectionJsonData);
    }

    ngOnInit() {
        if (this.itemJsonDef) {
            this.elementRef = this.cmf.addComponentCSSClass(this.elementRef, this.itemJsonDef.classname);
            this.itemJsonDef = this.cmf.setItemDefaultProperty(this.itemJsonDef);
            this.formControl = new FormControl({ value: this.value, disabled: this.itemJsonDef.disable });
            let validators;
            if (this.itemJsonDef.mandatory) {
                validators = [Validators.required];
            }
            this.formControl.setValidators(validators);
        }
    }

    ngOnChanges(changes: SimpleChanges) {
    }

    ngAfterViewChecked(): void {
        this.cdr.detectChanges();
    }

    checkBoxOnChange(e) {
        if (this.itemJsonDef.valuetype === 'dbfield' && this.sectionJsonData) {
            this.sectionJsonData[this.itemJsonDef.value] = this.formControl.value;
        } else {
            this.itemJsonDef.value = this.formControl.value;
        }
        this.sectionJsonDataChange.emit(this.sectionJsonData);
        this.OnChange.emit(e);
    }
}
