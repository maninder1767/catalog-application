/*
WFXControls v1.0.0 (2019-April)
Copyright (c) 2019-2019 WFX.
License: https://www.wfxcloud.com/contact.html
*/
/* eslint-disable */

import {
  Component, Input, Output, EventEmitter, OnChanges, SimpleChanges, OnInit,
  ChangeDetectorRef, AfterViewChecked, HostListener, ElementRef, AfterViewInit
} from '@angular/core';
import { WfxGridTextBoxComponent } from './wfxgridtextbox';
import { WfxGridCheckBoxComponent } from './wfxgridcheckbox';
import { WfxGridSelectComponent } from './wfxgridselect';
import { WfxGridDatePickerComponent } from './wfxgriddatepicker';
import { WfxGridLinkButtonComponent } from './wfxgridlinkbutton';
import { WfxGridTextAreaComponent } from './wfxgridtextarea';
import { WfxCommonFunctions } from '../wfxsharedfunction/wfxcommonfunction';
import { WFXGridDateFilterComponent } from './wfxgriddatefilter';
import { WfxGridPinnedRowComponent } from './wfxgridpinnedrow';
import { WFXGridImageRendererComponent } from './wfxgridimagerenderer';
import 'ag-grid-enterprise';
import { WfxShowMessageservice } from '../wfxsharedservices/wfxshowmessage.service';
import { WFXGridCellAttributes, WFXAttribute } from '../wfxsharedmodels/wfxcommonmodel';
import { WfxGridNumberTextBoxComponent } from './wfxgridnumbertextbox';
import 'ag-grid-enterprise/chartsModule';
import { WfxGlobal } from '../../../app/wfxlibrary/wfxsharedscript/wfxglobal';
import { PageParam } from '../wfxsharedmodels/wfxqueryparam';

declare var $: any;

@Component({
  selector: 'app-wfxgrid',
  template: `
  <div style="display: flex; flex-direction: row">
    <div *ngIf="columnDefs" style="overflow: hidden; flex-grow: 1">
    <div class="test-header" *ngIf="quickFilter">
      <input type="text" id="filter-text-box" placeholder="Filter..." (input)="onFilterTextBoxChanged($event)"/>
    <div *ngIf="pagination" style="display:inline;">
      Page Size:
      <select (change)="onPageSizeChanged($event)" id="page-size">
        <option value="10" selected="">10</option>
        <option value="50">50</option>
        <option value="100">100</option>
        <option value="200">200</option>
      </select>
    </div>
    </div>
    <div *ngIf="rowModelType !== 'infinite'">
        <ag-grid-angular style="width: 100%;" [style.height]="gridHeight + 'px'"
            class="wfxgrid ag-theme-balham" #commonGrid id="commonGrid"
            [rowData]="rowData" [columnDefs]="columnDefs"
            (gridReady)="onGridReady($event)" [suppressRowClickSelection]="suppressRowClickSelection"
            [rowSelection]="rowSelection" [animateRows]="animateRows" [stopEditingWhenGridLosesFocus]="true"
            (cellEditingStopped)="onCellEditingStopped($event)" [defaultColDef]="defaultColDef"
            [getRowNodeId]="getRowNodeId" [rowDragManaged]="rowDragManaged" [suppressDragLeaveHidesColumns]="suppressDragLeaveHidesColumns"
            (sortChanged)="OnSortChanged($event)" [frameworkComponents]="frameworkComponents"
            (rowDragEnd)="onRowDragEnd($event)" (filterChanged)="OnFilterChanged($event)"
            (filterModified)="OnFilterModified($event)" (filterOpened)="OnFilterOpened($event)"
            [suppressLoadingOverlay]="suppressLoadingOverlay" [suppressNoRowsOverlay]="suppressNoRowsOverlay"
            [overlayLoadingTemplate]="overlayLoadingTemplate" [overlayNoRowsTemplate]="overlayNoRowsTemplate"
            [context]="context" [floatingFilter]="floatingFilter" (cellKeyDown)="onCellKeyDown($event)"
            (cellKeyPress)="onCellKeyPress($event)" [suppressKeyboardEvent]="suppressKeyboardEvent"
            [tabToNextCell]="tabToNextCell" [pinnedBottomRowData]="pinnedBottomRowData" [pinnedTopRowData]="pinnedTopRowData"
            (firstDataRendered)="onFirstDataRendered($event)" [pagination]="pagination"
            [paginationPageSize]="paginationPageSize" [sideBar]="customSideBar" [enableCellChangeFlash]="enableCellChangeFlash"
            [enableRangeSelection]="enableRangeSelection" [rowGroupPanelShow]="rowGroupPanelShow" [rowHeight]="36"
            [floatingFiltersHeight]="36" (rowDragEnter)="onRowDragEnter($event)" (rowDragMove)="onRowDragMove($event)"
            (rowDragLeave)="onRowDragLeave($event)" enableCellExpressions="true" [getContextMenuItems]="getContextMenuItems"
            [autoGroupColumnDef]="autoGroupColumnDef" [groupIncludeFooter]="groupIncludeFooter" [enableCharts]="enableCharts"
            [valueCache]="valueCache" (cellValueChanged)="onEveryCellValueChanged($event)" [groupIncludeTotalFooter]="groupIncludeTotalFooter"
            [deltaColumnMode] = "deltaColumnMode" [getRowStyle]="getRowStyle" (pasteStart)="onPasteStart($event)"
            [processCellFromClipboard]="processCellFromClipboard">
            <!--
            [enableCellTextSelection]="true" [headerHeight]="56" [rowHeight]="47" [pivotMode]="true" [domLayout]="autoHeight"
            [rowBuffer]="9999" [floatingFiltersHeight]="30"
            [components]="components" [enableServerSideSorting]="true" [enableServerSideFilter]="true"
            [rowModelType]="rowModelType" [cacheOverflowSize]="cacheOverflowSize"
            [maxConcurrentDatasourceRequests]="maxConcurrentDatasourceRequests" [infiniteInitialRowCount]="infiniteInitialRowCount"
            [maxBlocksInCache]="maxBlocksInCache"
            [cacheBlockSize]="cacheBlockSize" (paginationChanged)="onPaginationChanged($event)">
            [paginationAutoPageSize]="true" [singleClickEdit]="true" [suppressColumnVirtualisation]="true" -->
        </ag-grid-angular>
        </div>
        <div *ngIf="rowModelType === 'infinite'">
          <ag-grid-angular style="width: 100%;" [style.height]="gridHeight + 'px'"
          id="commonGrid" class="wfxgrid ag-theme-balham"
          [columnDefs]="columnDefs" [defaultColDef]="defaultColDef"
          [frameworkComponents]="frameworkComponents"
          [rowModelType]="rowModelType" [animateRows]="animateRows"
          [rowData]="rowData"
          (gridReady)="onGridReady($event)" [rowSelection]="rowSelection"
          (sortChanged)="OnSortChanged($event)"
          (filterModified)="OnFilterModified($event)"
          [deltaColumnMode]="deltaColumnMode" (selectionChanged)="onSelectionChanged($event)"
          [overlayNoRowsTemplate]="overlayNoRowsTemplate"
          [floatingFilter]="floatingFilter"
          [sideBar]="customSideBar"
          [context]="context"
          [getMainMenuItems]="getMainMenuItems"
          [getContextMenuItems]="getContextMenuItems">
          </ag-grid-angular>
        </div>

    </div>
    </div>`,
  styles: [`
    .test-header {
      padding: 4px;
      font-family: Verdana, Geneva, Tahoma, sans-serif;
      font-size: 13px;
    }
    .test-container {
      height: 95% !important;
      display: flex;
      flex-direction: column;
    }
    `]
})

export class WfxGridComponent implements OnInit, OnChanges, AfterViewChecked, AfterViewInit {
  constructor(containerElement: ElementRef, private cdr: ChangeDetectorRef, public cmf: WfxCommonFunctions,
    private msgsvc: WfxShowMessageservice, private global: WfxGlobal) {
    this.elementRef = containerElement;
  }

  @Input() colDef: WFXGridCellAttributes[];
  @Input() gridProperties: any;
  @Input() ParentComponent: Component;
  @Input() gridHeight?: number;
  @Input() enableSorting = true;
  @Input() enableFilter = true;
  @Input() rowData?: any;
  @Input() columnDefs: any;
  @Input() enableColResize = true;
  @Input() suppressRowClickSelection = true;
  @Input() rowSelection = 'multiple';
  @Input() animateRows = true;
  @Input() dataKeyField: string;
  @Input() rowDragManaged = true;
  @Input() suppressDragLeaveHidesColumns = true;
  @Input() gridApi: any;
  @Input() gridColumnApi: any;
  @Input() autoSortAfterRowDragEnd = false;
  @Input() showCustomOverlayLoadingTemplate = false;
  @Input() showCustomOverlayNoRowsTemplate = false;
  @Input() suppressLoadingOverlay = true;
  @Input() suppressNoRowsOverlay = false;
  @Input() ddlLoading = true;
  @Input() colLgClass = 'col-lg-12';
  @Input() floatingFilter = false;
  @Input() pinnedBottomRowData: any;
  @Input() pinnedTopRowData: any;
  @Input() pagination = false;
  @Input() paginationPageSize = 10;
  @Input() sideBar = true;
  @Input() enableCellChangeFlash = true;
  @Input() enableRangeSelection = true;
  @Input() rowGroupPanelShow: any = false;
  @Input() quickFilter: any = false;
  @Input() sizeColumnToFitAfterOpenCloseSideBar = false;
  @Input() autoGroupColumnDef: any;
  @Input() autoSizeAllColumn = true;
  @Input() applyCustomWidthToAllColumns: number;
  @Input() groupIncludeFooter = false;
  @Input() enableCharts = false;
  @Input() valueCache = false;
  @Input() enablePivot = true;
  @Input() enableValue = true;
  @Input() enableRowGroup = true;
  @Input() groupIncludeTotalFooter = false;
  @Input() deltaColumnMode = false;
  @Input() rowModelType?: any;

  @Output() gridReady = new EventEmitter<any>();
  @Output() cellEditingStopped = new EventEmitter<any>();
  @Output() sortChanged = new EventEmitter<any>();
  @Output() rowDragEnd = new EventEmitter<any>();
  @Output() filterChanged = new EventEmitter<any>();
  @Output() filterModified = new EventEmitter<any>();
  @Output() filterOpened = new EventEmitter<any>();
  @Output() openedChange = new EventEmitter<any>();
  @Output() cellValueChanged = new EventEmitter<any>();
  @Output() selectionChanged = new EventEmitter<any>();

  defaultColDef: any;
  frameworkComponents: any;
  overlayLoadingTemplate = '';
  overlayNoRowsTemplate: string;
  context: any;
  elementRef: ElementRef;
  // suppressKeyboardEvent: any;
  tabToNextCell: any;
  customSideBar: any;

  tempGridIds = ['gridEmployee', 'gridWFXPLMSetCosting1', 'gridWFXPLMCostSheet']; // , 'gridWFXPLMCostSheet'
  processCellFromClipboard: any;
  @HostListener('window:resize') onResize() {
    this.setGridHeight();
  }

  getRowNodeId = (data) => {
    if (data && this.dataKeyField !== '' && this.dataKeyField !== undefined) {
      return data[this.dataKeyField];
    }
  }

  ngOnInit() {
    this.elementRef = this.cmf.addComponentCSSClass(this.elementRef, this.colLgClass);
    this.setGridHeight();
    if (this.rowSelection !== 'multiple' && this.rowSelection !== 'single') {
      this.rowSelection = 'multiple';
    }
    if (this.showCustomOverlayLoadingTemplate) {
      this.overlayLoadingTemplate =
        '<span class="ag-overlay-loading-center">Please wait while your rows are loading</span>';
    }
    if (this.showCustomOverlayNoRowsTemplate) {
      this.overlayNoRowsTemplate = `<span style=\"padding: 10px; border: 1px solid #444;\">
      No Record Found
      </span>`;
    }
    this.frameworkComponents = {
      wfxTextBoxEditor: WfxGridTextBoxComponent,
      wfxSelectEditor: WfxGridSelectComponent,
      wfxCheckboxRenderer: WfxGridCheckBoxComponent,
      wfxDatePickerEditor: WfxGridDatePickerComponent,
      wfxLinkButtonRenderer: WfxGridLinkButtonComponent,
      wfxTextAreaEditor: WfxGridTextAreaComponent,
      agDateInput: WFXGridDateFilterComponent, // custom date picker filter
      customPinnedRowRenderer: WfxGridPinnedRowComponent,
      wfxImageRenderer: WFXGridImageRendererComponent,
      wfxNumericTextboxEditor: WfxGridNumberTextBoxComponent
    };
    if (this.rowModelType  !== 'infinite') {
      this.setGridJsonDef();
      this.handleGridCallBackFunction();
    }

  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.rowModelType !== 'infinite') {
      if (changes['colDef'] && !changes['colDef'].firstChange) {
        this.setGridJsonDef();
      }
      this.setWFXGridData();
    }
    if (changes['colDef'] && !changes['colDef'].firstChange) {
      this.setGridJsonDef();
    }
  }

  setGridJsonDef() {
    // if (this.gridProperties && this.tempGridIds.includes(this.gridProperties.id)) {
      this.createGridJsonDef();
    // }
    if (this.rowModelType !== 'infinite') {
      this.updateGridJsonDef();
    }
    // set all column properties in this which you want to enable for all of the columns
    this.defaultColDef = {
      // set for all cols
      filter: this.enableFilter,
      resizable: this.enableColResize,
      sortable: this.enableSorting,
      enablePivot: this.enablePivot,
      enableValue: this.enableValue,
      enableRowGroup: this.enableRowGroup,
      width: this.applyCustomWidthToAllColumns ? this.applyCustomWidthToAllColumns : null
    };
    this.setSideBarDef();
  }

  setSideBarDef() {
    if (this.sideBar) {
      this.customSideBar = {
        toolPanels: ['columns', 'filters'],
        defaultToolPanel: 'none'
      };
    } else {
      this.customSideBar = false;
    }
  }

  createGridJsonDef() {
    const colDefinition = [];
    let col: any = {};
    let childCol: any = {};
    if (this.colDef) {
      for (const coldf of this.colDef) {
        if (this.validateColDef(coldf)) {
          col = {};
          col['headerName'] = coldf.colname;
          if (coldf.children) {
            col['children'] = [];
            col['marryChildren'] = true;
            for (const childColDf of coldf.children) {
              childCol = {};
              childCol['headerName'] = childColDf.colname;
              childCol['columnGroupShow'] = childColDf.columngroupshow;
              childCol = this.returnColProperties(childCol, childColDf);
              col['children'].push(childCol);
            }
          } else {
            col = this.returnColProperties(col, coldf);
          }
          colDefinition.push(col);
        }
      }
    } else {
      this.msgsvc.showSnackBar('Column definition not found!', '', 'Error');
    }
    if (this.gridProperties && this.tempGridIds.includes(this.gridProperties.id)) {
      this.columnDefs = colDefinition;
    }
  }

  returnColProperties(col, coldf: WFXGridCellAttributes) {
    col['colId'] = coldf.colid;
    col['field'] = coldf.valuetext;
    col['editable'] = coldf.editable;
    col['headerTooltip'] = coldf.headertooltip;
    col['tooltipValueGetter'] = coldf.valuetooltip;
    col['suppressKeyboardEvent'] = this.suppressGridColKeyboardEvent;
    col['rowGroup'] = coldf.rowgroup;
    col['pinned'] = coldf.pinned;
    col['hide'] = coldf.hide;
    col['pivot'] = coldf.enablepivot;

    if (coldf.rowdrag) {
      col['rowDrag'] = coldf.rowdrag;
      col['cellClass'] = 'rowDragClass';
      col['width'] = 40;
    }
    if (coldf.headercheckboxselection) {
      col['headerCheckboxSelection'] = true;
      col['headerCheckboxSelectionFilteredOnly'] = true;
      col['width'] = 40;
      if (coldf.checkboxselection) {
        col['checkboxSelection'] = coldf.checkboxselection;
      }
      col['checkboxSelection'] = true;
    }
    if (coldf.rowdrag || coldf.headercheckboxselection) {
      col['resizable'] = false;
      col['filter'] = false;
      col['suppressSizeToFit'] = true;
      col['lockPosition'] = true;
    } else {
      col['resizable'] = coldf.resizable;
      col['filter'] = coldf.filter;
      col['sortable'] = coldf.sortable;
    }

    // add column header Mandatory class
    col = this.addHeadercellHeaderMandatoryClass(col, coldf);

    // set column editable and renderer
    col = this.setCellEditorNRenderer(col, coldf);

    // set filter params
    col = this.setCellFilterParams(col, coldf);

    // set cellEditor Params
    col = this.setCellEditorParams(col, coldf);

    // set cellRenderer Params
    col = this.setCellRendererParams(col, coldf);

    // set Column Default Property
    col = this.setColumnDefaultProperty(col, coldf);

    // set Column Property if sent
    col = this.setIfColumnPropertySent(col, coldf);

    // set column filter
    col = this.setColumnFilter(col, coldf);

    // register functions
    col = this.registerFunction(col, coldf);

    return col;
  }

  setCellEditorNRenderer(col, coldf: WFXGridCellAttributes) {
    if (coldf.editable) {
      if (coldf.inputtype === 'textbox' && (!coldf.inputsubtype || (coldf.inputsubtype !== 'number' && coldf.inputsubtype !== 'money'))) {
        col['cellEditor'] = 'wfxTextBoxEditor';
      } else if (coldf.inputtype === 'select') {
        col['cellEditor'] = 'wfxSelectEditor';
      } else if (coldf.inputtype === 'date') {
        col['cellEditor'] = 'wfxDatePickerEditor';
      } else if (coldf.inputtype === 'textarea') {
        col['cellEditor'] = 'wfxTextAreaEditor';
      } else if (coldf.inputsubtype === 'number' || coldf.inputsubtype === 'money') {
        col['cellEditor'] = 'wfxNumericTextboxEditor';
      }
    } else {
      if (coldf.inputtype === 'checkbox') {
        col['cellRenderer'] = 'wfxCheckboxRenderer';
      } else if (coldf.inputtype === 'linkbutton') {
        col['cellRenderer'] = 'wfxLinkButtonRenderer';
      } else if (coldf.inputtype === 'image') {
        col['cellRenderer'] = 'wfxImageRenderer';
      }
    }
    return col;
  }

  setCellFilterParams(col, coldf: WFXGridCellAttributes) {
    if (coldf.inputtype === 'date') {
      col['filterParams'] = {
        clearButton: true,
        inRangeInclusive: true,
        comparator: this.cmf.myDateComparator
      };
    } else {
      col['filterParams'] = { clearButton: true };
    }
    return col;
  }

  getMainMenuItems(params) {
    if (params.column.getId()) {
      // const fn = params.context.mainParentComponent.tmpMethod;
      // fn();
      const defaultMenuItems = [];
      const items = ['separator', 'pinSubMenu', 'valueAggSubMenu'];
      params.defaultItems.forEach(function(item) {
        if (items.indexOf(item) < 0) {
          defaultMenuItems.push(item);
        }
      });
      return defaultMenuItems;
    }
  }

  setIfColumnPropertySent(col, coldf: WFXGridCellAttributes) {
    if (coldf.width && coldf.width > 0) {
      col['width'] = coldf.width;
    }
    if (coldf.aggfunc) {
      col['aggFunc'] = coldf.aggfunc;
    }
    if (coldf.formula && typeof coldf.formula === 'string') {
      col['valueGetter'] = this.setValueGetter(coldf);
    }
    if (typeof coldf.valuesetter === 'string') {
      col['valueSetter'] = coldf.valuesetter;
    }
    if ((coldf.valueprefix || coldf.valuesuffix) ||
      (coldf.inputsubtype === 'money' && (coldf.currency || coldf.currencyfield)) ||
      (coldf.inputsubtype === 'money' || coldf.inputsubtype === 'number')) {
      const valueprefix = coldf.valueprefix ? '"' + coldf.valueprefix + ' "' : '';
      const valuesuffix = coldf.valuesuffix ? '\" ' + coldf.valuesuffix + '"' : '';
      if (coldf.inputsubtype === 'number' || coldf.inputsubtype === 'money') {
        col['valueFormatter'] = (params) => {
          if (params.colDef.aggFunc) {
            if (coldf.inputsubtype === 'money' && (coldf.currency || coldf.currencyfield)) {
              return this.setMoneyTypeSuffixFormatter(coldf, params);
            } else {
              return (coldf.valueprefix ? coldf.valueprefix + ' ' : '') + (params.value ? Number(params.value).toFixed(coldf.decimalplaces) : Number(0).toFixed(coldf.decimalplaces))
                + (coldf.valuesuffix ? ' ' + coldf.valuesuffix : '');
            }
          } else {
            if (params.node.group) {
              if (params.value) {
                if (coldf.inputsubtype === 'money' && (coldf.currency || coldf.currencyfield)) {
                  return this.setMoneyTypeSuffixFormatter(coldf, params);
                } else {
                  return params.value ? (coldf.valueprefix ? coldf.valueprefix + ' ' : '') +
                    (Number(params.value).toFixed(coldf.decimalplaces)) + (coldf.valuesuffix ? ' ' + coldf.valuesuffix : '') : '';
                }
              }
            } else {
              if (coldf.inputsubtype === 'money' && (coldf.currency || coldf.currencyfield)) {
                return this.setMoneyTypeSuffixFormatter(coldf, params);
              } else {
                return (coldf.valueprefix ? coldf.valueprefix + ' ' : '') + (params.value ? Number(params.value).toFixed(coldf.decimalplaces) : Number(0).toFixed(coldf.decimalplaces))
                  + (coldf.valuesuffix ? ' ' + coldf.valuesuffix : '');
              }
            }
          }
        };
      } else {
        col['valueFormatter'] = (valueprefix ? valueprefix + ' + ' : '') + 'value.toString()' + (valuesuffix ? ' + ' + valuesuffix : '');
      }
    }
    if (typeof coldf.valueparser === 'string') {
      col['valueParser'] = coldf.valueparser;
    }
    if (typeof coldf.cellstyle === 'object' || typeof coldf.cellstyle === 'function') {
      col['cellStyle'] = coldf.cellstyle;
      // col['cellStyle'] = (params) => {
      //   if (params && !params.node.group) {
      //     let result;
      //     // tslint:disable-next-line: quotemark , max-line-length
      //     const str = "if (params.value > params.data.TargetWSMarginPercEUR) { result = { color: 'green' }; } else { result = { color: 'red' }; }";
      //     // tslint:disable-next-line: no-eval
      //     return eval(str);
      //   }
      // };
    }
    if (typeof coldf.stoppaste === 'boolean' && coldf.stoppaste) {
      col['suppressPaste'] = coldf.stoppaste;
    }
    // for now we are not getting the pasted value in params in suppressPaste callback function so wwaiting for that issue to be fixed
    // after that this function will work. bug locked. https://ag-grid.zendesk.com/hc/en-us/requests/9608
    // else if ((typeof coldf.stoppaste === 'boolean' && !coldf.stoppaste) || !coldf.stoppaste) {
    //   col['suppressPaste'] = (params) => {
    //     if (this.cmf.isGridCellNumeric(params)) {
    //       return !this.cmf.isValueIsNumber(params);
    //     } else if (this.cmf.isGridCellOfDropdownType(params)) {
    //       const key = this.cmf.createKeyFromParams(params);
    //       if (key) {
    //         return !(this.global.gObjDDLHashData[key].filter(e => {
    //           return e.text === params.value;
    //         }).length > 0);
    //       }
    //     } else {
    //       return coldf.stoppaste;
    //     }
    //   };
    // }
    return col;
  }

  setMoneyTypeSuffixFormatter(coldf: WFXGridCellAttributes, params) {
    let currencyCode;
    if (coldf.currencyfield && params.data && params.data[coldf.currencyfield]) {
      currencyCode = params.data[coldf.currencyfield];
    } else {
      currencyCode = coldf.currency;
    }
    // create pageparams for Filter data
    // let pageParam: PageParam[] = [];
    // if (coldf.attributelist && coldf.attributelist.length > 0) {
    //   pageParam = this.cmf.createPageParamsFromAttributeList(coldf.attributelist, params);
    // } else {
    //   const fn = params.context.componentParent.gridProperties.id + '_getDDLPageParams';
    //   if (typeof params.context.mainParentComponent[fn] === 'function') {
    //     pageParam = params.context.mainParentComponent[fn](params);
    //   }
    // }
    // const key = this.cmf.createKeyFromPageParams(pageParam);
    // const key = 'MetaDataFor|AllCurrency~objectName|MetaData~';
    const key = 'CurrencySymbolList';
    let valuesuffix = '';
    if (currencyCode && this.global.gObjDDLHashData && this.global.gObjDDLHashData[key]) {
      valuesuffix = this.cmf.getFilterJsonData(this.global.gObjDDLHashData[key],
        'value', currencyCode, 'symbol');
    }
    return (coldf.valueprefix ? coldf.valueprefix + ' ' : '') + (params.value ? Number(params.value).toFixed(coldf.decimalplaces) : Number(0).toFixed(coldf.decimalplaces))
      + (valuesuffix ? ' ' + valuesuffix : '');
  }

  setValueGetter(coldf: WFXGridCellAttributes) {
    const valueGetter = coldf.formula.trim();
    const operatorArray = this.getArrayOfOperatorOnlyFromString(valueGetter);
    let newValueGetter = `node.group ? '' : `;
    const arrFormula = this.getArrayIrrespectiveOfOperatorsFromString(valueGetter);
    if (arrFormula) {
      for (let i = 0; i < arrFormula.length; i++) {
        const prvString = arrFormula[i].trim();
        const fieldName = prvString.replace(/[()]/g, ''); // for replacing ( and )
        const regex = /^[0-9]+(\.)?[0-9]*$/;
        const isNumericOnly = regex.test(fieldName); // https://stackoverflow.com/questions/12467542/how-can-i-check-if-a-string-is-a-float
        if (!isNumericOnly) {
          let isCalulatedField = false;
          // isCalulatedField = this.colDef.filter(e => {
          //   if (e.children) {
          //     e.children.filter(ce => {
          //       return (ce.valuetext === fieldName && ce.formula);
          //     });
          //   } else {
          //     return (e.valuetext === fieldName && e.formula);
          //   }
          // }).length > 0;

          for (const perCol of this.colDef) {
            if (perCol.children) {
              for (const perColChild of perCol.children) {
                if (perColChild.valuetext === fieldName && perColChild.formula) {
                  isCalulatedField = true;
                  break;
                }
              }
            } else {
              if (perCol.valuetext === fieldName && perCol.formula) {
                isCalulatedField = true;
                break;
              }
            }
          }
          if (isCalulatedField) {
            newValueGetter += prvString.replace(fieldName, 'Number(getValue("' + fieldName + '"))');
          } else {
            newValueGetter += prvString.replace(fieldName, '(data.' + fieldName + ' ? Number(data.' + fieldName + ')' + ' : Number(0))');
          }
        } else {
          newValueGetter += prvString;
        }
        if (i < arrFormula.length - 1) {
          newValueGetter = newValueGetter + operatorArray[i];
        }
      }
    }
    return newValueGetter;
  }

  registerFunction(col, coldf: WFXGridCellAttributes) {
    if (typeof coldf.onchange === 'function') {
      col['onCellValueChanged'] = coldf.onchange;
      // col['onCellValueChanged'] = (params) => {
      //   coldf.onchange(params, params.context.mainParentComponent);
      // };
    }
    if (typeof coldf.onchange === 'object') {
      col['onCellValueChanged'] = (params) => {
        const CellEditorParams = this.cmf.getCellEditorParams(params);
        const onChangeArrObj = CellEditorParams.onchange;
        onChangeArrObj.forEach(onChangeObj => {
          const fn = onChangeObj.fnname;
          const updateField = onChangeObj.updatefield;
          const paramListData: any = [];
          const paramList: WFXAttribute[] = onChangeObj.paramlist;
          for (const param of paramList) {
            paramListData.push(this.cmf.getAttributeParamValue(param, params));


          }
          this.fnCall(params, fn, updateField, paramList, ...paramListData);
        });
      };
      // col['onCellValueChanged'] = (params) => {
      //   coldf.onchange(params, params.context.mainParentComponent);
      // };
    }
    return col;
  }

  // Method 1
  // fnCall(params, fn, ...args) {
  //   const func = (typeof fn === 'string') ? params.context.mainParentComponent[fn] : fn;
  //   if (typeof func === 'function') {
  //     func(...args); // from this method we can not get *this* in the child component so we are calling directly from Method2.
  //   } else {
  //     console.error(`${fn} is Not a function!`);
  //   }
  // }

  // Method 2
  fnCall(params, fn, updateField, paramList, ...paramListData) {
    const func = (typeof fn === 'string') ? params.context.mainParentComponent[fn] : fn;
    if (typeof func === 'function') {
      params.context.mainParentComponent[fn](params, updateField, paramList, ...paramListData);
    } else {
      console.error(`${fn} is Not a function!`);
    }
  }

  setColumnDefaultProperty(col, coldf: WFXGridCellAttributes) {
    if (col.resizable === undefined) {
      col.resizable = true;
    }
    if (col.sortable === undefined) {
      col.sortable = true;
    }
    if (col.headerTooltip === undefined) {
      col.headerTooltip = col.headerName;
    }
    if (col.hide === undefined) {
      col.hide = col.false;
    }
    if (col.valueParser === undefined && (coldf.inputsubtype === 'number' || coldf.inputsubtype === 'money')) {
      col.valueParser = this.numberParser;
    }
    if (coldf.valueprefix === undefined) {
      coldf.valueprefix = '';
    }
    if (coldf.valuesuffix === undefined) {
      coldf.valuesuffix = '';
    }
    if (coldf.decimalplaces === undefined) {
      coldf.decimalplaces = 0;
    }
    if (coldf.editable === undefined) {
      coldf.editable = false;
    }
    if (coldf.editableproperties === undefined) {
      coldf.editableproperties = {};
    }
    if (coldf.metadatavaluefieldname === undefined) {
      coldf.metadatavaluefieldname = 'Code';
    }
    if (col.tooltipValueGetter === undefined || !col.tooltipValueGetter) {
      col.tooltipValueGetter = (params) => {
        return params.value;
      };
    }
    return col;
  }

  setColumnFilter(col, coldf: WFXGridCellAttributes) {
    if (col.filter === undefined || col.filter === '' || col.filter) {
      if ((coldf.inputtype === 'textbox' || coldf.inputtype === 'select' || coldf.inputtype === 'checkbox'
        || coldf.filter === 'text' || !col.editable) && coldf.inputsubtype !== 'number' && coldf.inputtype !== 'image') {
        col.filter = 'agTextColumnFilter';
      } else if (coldf.inputtype === 'date' || coldf.filter === 'date') {
        col.filter = 'agDateColumnFilter';
      } else if (coldf.inputsubtype === 'number' || coldf.inputsubtype === 'money') {
        col.filter = 'agNumberColumnFilter';
      }
    }
    return col;
  }

  setCellEditorParams(col, coldf: WFXGridCellAttributes) {
    col['cellEditorParams'] = (params, propertyName, propertyValue) => {
      if (propertyName && propertyValue) {
        coldf.editableproperties[propertyName] = propertyValue;
      }
      return {
        itemAttr: {
          mandatory: coldf.mandatory,
          values: coldf.ddlvalues ? params.context.mainParentComponent[coldf.ddlvalues] : undefined,
          value: coldf.value,
          inputsubtype: coldf.inputsubtype,
          decimalplaces: coldf.decimalplaces,
          multiselect: coldf.multiselect,
          addselect: coldf.mandatory && !coldf.multiselect ? false : coldf.addselect,
          addselecttext: coldf.addselecttext,
          editableproperties: coldf.editableproperties,
          dependentsuffixvaluetext: coldf.dependentsuffixvaluetext,
          onchange: coldf.onchange,
          attributelist: coldf.attributelist,
          ddlvalue: coldf.ddlvalue,
          ddlvaluetext: coldf.ddlvaluetext
        }
      };
    };
    return col;
  }

  setCellRendererParams(col, coldf: WFXGridCellAttributes) {
    if (col.cellRenderer) {
      col['cellRendererParams'] = (params) => {
        return {
          itemAttr: {
            action: coldf.onclick,
          }
        };
      };
    }
    return col;
  }

  validateColDef(colDef: WFXGridCellAttributes) {
    if (colDef.rowdrag && colDef.headercheckboxselection) {
      this.msgsvc.showSnackBar('Row Drag and header check box selection property can not be true on one column!', '', 'Error');
      return false;
    } else if ((colDef.rowdrag || colDef.headercheckboxselection) && colDef.colname) {
      this.msgsvc.showSnackBar('If Row Drag or header check box selection property is true ' +
        'then column name should be blank for that column!', '', 'Error');
      return false;
    } else if (colDef.editable && (colDef.inputtype === 'image' || colDef.inputtype === 'checkbox' || colDef.inputtype === 'linkbutton')) {
      this.msgsvc.showSnackBar('Editable can not be true for Input Type checkbox/linkbutton/Image!', '', 'Error');
      return false;
    }
    return true;
  }

  setGridHeight() {
    if (this.gridHeight === undefined) {
      this.gridHeight = window.innerHeight - 245;
    }
  }

  ngAfterViewChecked(): void {
    this.cdr.detectChanges();
  }

  ngAfterViewInit(): void {
    // params.api.sizeColumnsToFit();
    // window.addEventListener('resize', function() {
    //   setTimeout(function() {
    //     params.api.sizeColumnsToFit();
    //   });
    // });
  }

  onGridReady(params) {
    this.context = { componentParent: this, mainParentComponent: this.ParentComponent };
    // params.api.sizeColumnsToFit(); if there are more columns then it collapses all columns
    // and tries to set the grid without horizontal scroll
    if (this.dataKeyField) {
      params.api.WFXGridProperties = {
        dataKeyField: this.dataKeyField
      };
    }
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.gridReady.emit(params);
    this.autoAdjustColumnAfterSideBarOpenClose(params);

    // $('.ag-body-viewport').scroll(() => {
    //   this.cmf.autoSizeAllGridColumn(this.gridColumnApi);
    // });
  }

  autoAdjustColumnAfterSideBarOpenClose(params) {
    if (this.sizeColumnToFitAfterOpenCloseSideBar) {
      const _this = this;
      const sideButtons = document.querySelector('.ag-side-buttons');
      sideButtons.addEventListener('click', () => {
        // _this.cmf.autoSizeAllGridColumn(params.columnApi);
        params.api.sizeColumnsToFit();
      });
    }
  }

  onFirstDataRendered(params) {
    if (this.autoSizeAllColumn) {
      this.cmf.autoSizeAllGridColumn(params.columnApi);
    }
    // $('.ag-header').css('height', parseInt($('.ag-header').css('min-height'), 10) + 27);
  }

  setWFXGridData() {
    if (this.rowData && this.gridApi) {
      this.gridApi['WFXData'] = new Object();
      this.gridApi['WFXData']['WFXGridData'] = new Object();
      this.gridApi['WFXData']['WFXGridData'] = this.rowData;
    }
  }

  onCellEditingStopped(e) {
    if (e && e.data) {
      e.data.Edited = true;
    }
  }
  OnSortChanged(e) {
    this.sortChanged.emit(e);
  }
  onRowDragEnd(e) {
    if (this.gridApi && e.node.data && this.autoSortAfterRowDragEnd) {
      const movedRowSortOrder = e.overIndex + 1;
      if (e.node.data.SortOrder !== movedRowSortOrder) {
        let SortOrder = 1;
        this.gridApi.forEachNode((node) => {
          node.data.Edited = true;
          node.data.SortOrder = SortOrder;
          SortOrder = SortOrder + 1;
        });
      }
    }

    this.rowDragEnd.emit(e);
  }
  OnFilterChanged(e) {
    // this.gridApi.onFilterChanged();
    this.filterChanged.emit(e);
  }
  OnFilterModified(e) {
    const _this = this;
    $('#filterConditionText').keydown((event) => {
      _this.dontGoFurtherWhenPressingEnterInFilter(event);
    });
    this.filterModified.emit(e);
  }
  OnFilterOpened(e) {
    const _this = this;
    if ($('#clearButton') && $('#clearButton')[0]) {
      $('#clearButton')[0].innerText = 'CLEAR';
    }
    $('#filterText').keydown((event) => {
      _this.dontGoFurtherWhenPressingEnterInFilter(event);
    });
    $('#filterDateFromPanel').keydown((event) => {
      _this.dontGoFurtherWhenPressingEnterInFilter(event);
    });
    this.filterOpened.emit(e);
  }
  methodFromParent(e) {
    alert('came');
  }

  dontGoFurtherWhenPressingEnterInFilter(event) {
    // When i search something in filer and press enter then it fire the click event of link button
    // becuase of cellRendererParams
    if (event.keyCode === 13) {
      this.preventDefaultAndPropagation(event);
    }
  }

  preventDefaultAndPropagation(event) {
    event.preventDefault();
    event.stopPropagation();
  }

  ddlOpenedChange(e, params) {
    this.openedChange.emit({ opened: e, headerName: params.colDef.headerName, params });
  }

  onCellKeyDown(event) {
    const key = event.which || event.keyCode;
    // if (key === 9 && this.gridApi.getEditingCells().length > 0) {
    //   this.gridApi.stopEditing();
    //   this.gridApi.tabToNextCell();
    //   this.cmf.preventDefaultAndPropagation(event);
    // }
    // this.gridApi.stopEditing();
    // console.log('onCellKeyDown', e);
  }

  onCellKeyPress(e) {
    // console.log('onCellKeyPress', e);
    // const keyPressed = e.event.key;
    // console.log('Key Pressed = ' + keyPressed);
  }

  // stop keyboard event from this URL - https://www.ag-grid.com/javascript-grid-keyboard-navigation/
  suppressKeyboardEvent = (params) => {
    // const KEY_A = 65;
    // const KEY_C = 67;
    // const KEY_V = 86;
    // const KEY_D = 68;
    // const KEY_PAGE_UP = 33;
    // const KEY_PAGE_DOWN = 34;
    // const KEY_TAB = 9;
    // const KEY_LEFT = 37;
    // const KEY_UP = 38;
    // const KEY_RIGHT = 39;
    // const KEY_DOWN = 40;
    // const KEY_F2 = 113;
    // const KEY_BACKSPACE = 8;
    // const KEY_ESCAPE = 27;
    const KEY_SPACE = 32;
    // const KEY_DELETE = 46;
    // const KEY_PAGE_HOME = 36;
    // const KEY_PAGE_END = 35;
    const event = params.event;
    const key = event.which;
    const keysToSuppress = []; // KEY_PAGE_UP, KEY_PAGE_DOWN, KEY_TAB, KEY_F2, KEY_ESCAPE
    const editingKeys = [
      // KEY_LEFT,
      // KEY_RIGHT,
      // KEY_UP,
      // KEY_DOWN,
      // KEY_BACKSPACE,
      // KEY_DELETE,
      KEY_SPACE,
      // KEY_PAGE_HOME,
      // KEY_PAGE_END
    ];
    // if (event.ctrlKey || event.metaKey) {
    //   keysToSuppress.push(KEY_A);
    //   keysToSuppress.push(KEY_V);
    //   keysToSuppress.push(KEY_C);
    //   keysToSuppress.push(KEY_D);
    // }

    // if (!params.editing) {
    //   keysToSuppress = keysToSuppress.concat(editingKeys);
    // }
    const suppress = keysToSuppress.indexOf(key) >= 0;
    return suppress;
  }

  // tabToNextCell(params) {
  //   console.log(params);
  // }

  getRowStyle = (params) => {
    if (params.node.rowPinned || params.node.group) {
      return { 'font-weight': 'bold' };
    }
  }

  updateGridJsonDef() {
    for (const col of this.columnDefs) {
      this.addColFunctions(col);
      if (col.children && col.marryChildren) {
        for (const childCol of col.children) {
          this.addColFunctions(childCol);
        }
      }
    }
  }

  addColFunctions(col) {
    // adding class rules on each column
    this.addCellClassRuleOnColumn(col);
    // add callback function isCellEditable function
    this.cellEditable(col);
  }

  addCellClassRuleOnColumn(col) {
    if (!col.cellClassRules) {
      col.cellClassRules = {};
    }
    // if column is editable then adding class rule.
    this.addCellEditableClass(col);
    // if column is numeric then adding class rule.
    this.addCellNumericClass(col);
    // if column is invalid then this class will add
    this.addCellInvalidClass(col);
    // if column is mandatory then adding class rule.
    // this.addCellMandatoryClass(col);
    // this.addHeadercellHeaderMandatoryClass(col);
  }

  cellEditable(col) {
    if (col.headerName !== '' && typeof col.editable === 'boolean' && col.editable) {
      col.editable = this.isCellEditable;
    } else if (col.children && col.marryChildren) {
      for (const childCol of col.children) {
        if (childCol.headerName !== ''
          && typeof childCol.editable === 'boolean' && childCol.editable) {
          childCol.editable = this.isCellEditable;
        }
      }
    }
  }

  addCellEditableClass(col) {
    if (!col.cellClassRules.editableCell) {
      col.cellClassRules.editableCell = (params) => {
        if (typeof params.colDef.editable === 'function' && params.colDef.editable(params)) {
          return true;
        } else if (typeof params.colDef.editable === 'boolean' && params.colDef.editable) {
          return true;
        } else {
          return false;
        }
      };
    }
  }

  addCellNumericClass(col) {
    if (!col.cellClassRules.numericCell) {
      col.cellClassRules.numericCell = (params) => {
        const CellEditorParams = this.cmf.getCellEditorParams(params);
        if (CellEditorParams && (CellEditorParams.inputsubtype === 'number' || CellEditorParams.inputsubtype === 'money')) {
          return true;
        } else {
          return false;
        }
      };
    }
  }

  addCellInvalidClass(col) {
    if (!col.cellClassRules.invalidCell) {
      col.cellClassRules.invalidCell = (params) => {
        if (this.cmf.getExtraDataObjectPropertyValue(params, 'invalidcell')) {
          return true;
        } else {
          return false;
        }
      };
    }
  }

  addCellMandatoryClass(col) {
    if (!col.cellClassRules.cellHeaderMandatory) {
      col.cellClassRules.cellHeaderMandatory = (params) => {
        const CellEditorParams = this.cmf.getCellEditorParams(params);
        if (CellEditorParams && typeof CellEditorParams.mandatory === 'function' && CellEditorParams.mandatory(params)) {
          return true;
        } else if (CellEditorParams && typeof CellEditorParams.mandatory === 'boolean' && CellEditorParams.mandatory) {
          return true;
        } else {
          return false;
        }
      };
    }
  }

  addHeadercellHeaderMandatoryClass(col, coldf?) {
    if (coldf && coldf.mandatory) {
      col['headerClass'] = 'cellHeaderMandatory';
    }
    return col;
  }

  isCellEditable(params) {
    if (params.node.rowPinned) {
      return false;
    } else if (params.context.componentParent.gridProperties && params.context.componentParent.gridProperties.id) {
      const fn = params.context.componentParent.gridProperties.id + '_IsCellEditable';
      if (typeof params.context.mainParentComponent[fn] === 'function') {
        // I have to return the value in this so that i can not emit output event
        return params.context.mainParentComponent[fn](params, params.context.mainParentComponent);
      } else {
        return true;
      }
    } else {
      return true;
    }
  }

  suppressGridColKeyboardEvent(params) {
    if (params.colDef.cellRenderer === 'wfxCheckboxRenderer') {
      params.data.Edited = true;
      if (Number(params.event.keyCode) === 32) {
        params.node.setDataValue(params.colDef, params.data.InActive ? false : true);
        params.context.componentParent.cmf.preventDefaultAndPropagation(params.event);
      }
    } else if (params.colDef.cellRenderer === 'wfxLinkButtonRenderer') {
      if (Number(params.event.keyCode) === 32 && !params.editing) {
        params.context.mainParentComponent.linkButtonOnClick(params, params.context.mainParentComponent);
      }
      params.context.componentParent.cmf.stopSelectingRow(params);
    } else {
      if (!params.colDef.headerCheckboxSelection) {
        params.context.componentParent.cmf.stopSelectingRow(params);
      }
    }
    if (params.editing) {
      params.context.componentParent.cmf.stopEditingGrid(params, params.api);
    }
    if (params.context.componentParent.gridProperties && params.context.componentParent.gridProperties.id) {
      const fn = params.context.componentParent.gridProperties.id + '_gridColKeyDown';
      if (typeof params.context.mainParentComponent[fn] === 'function') {
        return params.context.mainParentComponent[fn](params, params.context.mainParentComponent);
      }
    }
  }

  onPageSizeChanged(e) {
    const value = e.target.value;
    // const value = document.getElementById('page-size').value;
    this.gridApi.paginationSetPageSize(Number(value));
  }

  onFilterTextBoxChanged(e) {
    this.gridApi.setQuickFilter(e.target.value);
  }

  onRowDragEnter(e) {
    // console.log('Entered');
  }
  onRowDragMove(e) {
    // console.log('Moved');
  }
  onRowDragLeave(e) {
    // console.log('Left');
  }

  getContextMenuItems(params) {
    let result = [];
    const fn = params.context.componentParent.gridProperties.id + '_getContextMenuItems';
    if (typeof params.context.mainParentComponent[fn] === 'function') {
      result = params.context.mainParentComponent[fn](params, params.context.mainParentComponent);
      if (result && Array.isArray(result) && result.length > 0) {
        return result;
      } else {
        return params.context.componentParent.returnDefaultContextMenuItems(params);
      }
    } else {
      return params.context.componentParent.returnDefaultContextMenuItems(params);
    }
  }

  returnDefaultContextMenuItems(params) {
    const result = [];
    result.push(...params.defaultItems);
    return result;
  }

  getArrayIrrespectiveOfOperatorsFromString(source) {
    const splitBy: any = '-+*/';
    const splitter = splitBy.split('');
    splitter.push([source]); // Push initial value

    return splitter.reduceRight((accumulator, curValue) => {
      let k = [];
      accumulator.forEach(v => k = [...k, ...v.split(curValue)]);
      return k;
    });
  }

  getArrayOfOperatorOnlyFromString(str: any) {
    const arrStr = str.split('');
    const strRegExp = /[-+*/]/g;
    const arrNonWord = [];
    arrStr.forEach((x) => {
      const result = x.match(strRegExp);
      if (result) {
        arrNonWord.push(result[0]);
      }
    });
    return arrNonWord;
  }

  onEveryCellValueChanged(params) {
    const colID = params.colDef.colId || params.colDef.field;
    const opt = { colID, node: params };
    this.cellValueChanged.emit(opt);
  }

  // ways when ngonchange dont fire when you change nested array.
  // one way
  // this.columnDefs[2].editable = true;
  // this.columnDefs = this.columnDefs.slice();

  // 2nd way
  // this.columnDefs[2].editable = true;
  // this.columnDefs = [...replicaColDef];

  // 3rd way
  // this.columnDefs[2].editable = true;
  // this.columnDefs = Object.assign({}, this.columnDefs);

  updateRule(params) {
    // this.rowData[0].IsRowEditable = true;
    // this.cmf.updateCellFromRowNode(this.cmf.getRowNode(this.gridApi, 1), 'Theme', this.rowData[0].Theme);
  }

  numberParser(params) {
    return Number(params.newValue);
  }

  onPasteStart(e) {
  }
  handleGridCallBackFunction() {
    this.processCellFromClipboard = (params) => {
      let newCellValue;
      // This case will never occur now because we will stop pasting invalid value in suppressPaste. (Bug is locked for this)
      // If cell is numeric and pasted value is not number type then set the previous value.
      if (this.cmf.isGridCellNumeric(params)) {
        if (this.cmf.isValueIsNumber(params)) {
          newCellValue = params.value;
        } else {
          newCellValue = this.cmf.getGridCellValue(this.gridApi, this.cmf.getColKey(params), params.node);
        }
      } else if (this.cmf.isGridCellOfDropdownType(params)) {
        const key = this.cmf.createKeyFromParams(params);
        if (key) {
          if ((this.global.gObjDDLHashData[key].filter(e => {
            return e.text === params.value;
          }).length > 0)) {
            newCellValue = params.value;
          } else {
            newCellValue = this.cmf.getGridCellValue(this.gridApi, this.cmf.getColKey(params), params.node);
          }
        }
      } else {
        newCellValue = params.value;
      }
      return newCellValue;
    };
  }
  onSelectionChanged($event) {
    this.selectionChanged.emit($event);
  }
}




