import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';

@Component({
  selector: 'app-wfxgridpinnedrow',
  template: `<span [ngStyle]="style">{{params.value}}</span>`
})
export class WfxGridPinnedRowComponent implements ICellRendererAngularComp {
  public params: any;
  public style: any;

  agInit(params: any): void {
    this.params = params;
    this.style = this.params.style;
  }

  refresh(): boolean {
    return false;
  }
}
