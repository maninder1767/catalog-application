import { Component } from '@angular/core';

@Component({
  selector: 'app-wfxgridimagerenderer',
  template: `<img *ngIf="params.value" border="0" width="36" height="36" src=\"{{ params.value }}\">`
})

export class WFXGridImageRendererComponent {
  params: any;
  agInit(params: any): void {
    this.params = params;
  }
}
