/*
WFXControls v1.0.0 (2019-April)
Copyright (c) 2019-2019 WFX.
License: https://www.wfxcloud.com/contact.html
*/
/* eslint-disable */

import {
  Component, Input, Output, EventEmitter, ElementRef, OnChanges, SimpleChanges, OnInit,
  ChangeDetectorRef, AfterContentChecked
} from '@angular/core';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher, DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { WfxComponentAttributes } from '../wfxsharedmodels/wfxcommonmodel';
import { WfxCommonFunctions } from '../wfxsharedfunction/wfxcommonfunction';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
// Depending on whether rollup is used, moment needs to be imported differently.
// Since Moment.js doesn't have a default export, we normally need to import using the `* as`
// syntax. However, rollup creates a synthetic default module and we thus need to import it using
// the `default as` syntax.
import * as _moment from 'moment';
// tslint:disable-next-line:no-duplicate-imports
import { default as _rollupMoment } from 'moment';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
const moment = _rollupMoment || _moment;

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

export const MY_FORMATS = {
  parse: {
    dateInput: 'll',
  },
  display: {
    dateInput: 'll', // It will show the display of calender
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'llll',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

moment.updateLocale('en', {
  longDateFormat: {
    LT: 'h:mm A',
    LTS: 'h:mm:ss A',
    L: 'MM/DD/YYYY',
    l: 'M/D/YYYY',
    LL: 'MMMM Do YYYY',
    ll: 'MMM D YYYY',
    LLL: 'MMMM Do YYYY LT',
    lll: 'MMM D YYYY LT',
    LLLL: 'dddd, MMMM Do YYYY LT',
    llll: 'ddd, MMM D YYYY'
  }
});

@Component({
  selector: 'app-wfxdatepicker',
  template: `
    <form [hidden]="!itemJsonDef.visible">
    <div class="sectionDatePickerDiv">
    <mat-form-field>
        <input matInput [id]="itemJsonDef.itemid" [matDatepicker]="dp" [placeholder]="itemJsonDef.caption" [min]="minDate"
            [max]="maxDate" [formControl]="formControl" [required]="itemJsonDef.mandatory" [value]="value"
            [errorStateMatcher]="matcher" (dateInput)="addEvent('input', $event)" (dateChange)="dateOnChange($event)"
            [matTooltip] = "tooltipValue" [matTooltipPosition] = "itemJsonDef.tooltip.tooltipposition"
            matTooltipClass = "sectiontooltipClass">
        <mat-datepicker-toggle matSuffix [for]="dp"></mat-datepicker-toggle>
        <mat-datepicker #dp [disabled]="inputdatedisable"></mat-datepicker>
        <mat-error *ngIf="formControl.hasError('required')">
            {{itemJsonDef.caption}} is <strong>required</strong>
        </mat-error>
        <mat-error *ngIf="!formControl.hasError('required') && formControl.invalid">
            <strong>{{getErrorMessage()}}</strong>
        </mat-error>
    </mat-form-field>
    </div>
    </form>`,
  providers: [
    // The locale would typically be provided on the root module of your application. We do it at
    // the component level here, due to limitations of our example generation script.
    // { provide: MAT_DATE_LOCALE, useValue: 'en-US' },
    // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
    // application's root module. We provide it at the component level here, due to limitations of
    // our example generation script.
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})

export class WfxDatePickerComponent implements OnInit, OnChanges, AfterContentChecked {
  constructor(
    containerElement: ElementRef, private cdr: ChangeDetectorRef, public cmf: WfxCommonFunctions) {
    this.elementRef = containerElement;
  }

  @Input() ParentComponent: Component;
  @Input() itemJsonDef: WfxComponentAttributes;
  @Input() sectionJsonData: any;
  @Output() sectionJsonDataChange = new EventEmitter<any>();
  @Output() OnChange = new EventEmitter<any>();

  matcher = new MyErrorStateMatcher();
  formControl = new FormControl();
  elementRef: ElementRef;
  minDate: any;
  maxDate: any;
  inputdatedisable = false;

  get value() {
    return this.cmf.getItemValue(this.itemJsonDef, this.sectionJsonData);
  }

  set value(val) {
  }

  get tooltipValue() {
    return this.cmf.getToolTipValue(this.itemJsonDef, this.sectionJsonData);
  }

  ngOnInit() {
    if (this.itemJsonDef) {
      // private adapter: DateAdapter<any> in constructor. change Locale dynamically
      // console.log(this.adapter);
      this.elementRef = this.cmf.addComponentCSSClass(this.elementRef);
      this.itemJsonDef = this.cmf.setItemDefaultProperty(this.itemJsonDef);
      this.formControl = new FormControl({
        value: this.value ? moment(new Date(this.value)).format() : null,
        disabled: this.itemJsonDef.disable ? true : this.itemJsonDef.inputdatedisable
      });
      this.setMinMaxDate();
      if (this.itemJsonDef.disable) {
        this.inputdatedisable = true;
      } else if (!this.itemJsonDef.disable && this.inputdatedisable) {
        this.inputdatedisable = false;
      }
      let validators;
      if (this.itemJsonDef.mandatory) {
        validators = [Validators.required];
      }
      this.formControl.setValidators(validators);
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    this.setMinMaxDate();
  }

  setMinMaxDate() {
    this.minDate = this.cmf.getMinValue(this.itemJsonDef, this.sectionJsonData);
    this.maxDate = this.cmf.getMaxValue(this.itemJsonDef, this.sectionJsonData);
  }

  ngAfterContentChecked(): void {
    this.cdr.detectChanges();
  }

  addEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    this.dateOnChange(event);
  }

  dateOnChange(e: MatDatepickerInputEvent<Date>) {
    if (this.formControl.value === null && e.value !== null) {
      this.formControl.setValue(moment(new Date(e.value)).format(this.itemJsonDef.dateformat));
    }
    if (this.sectionJsonData && this.formControl.value === null) {
      this.sectionJsonData[this.itemJsonDef.value] = null;
    } else if (this.formControl.value !== null && this.sectionJsonData) {
      this.sectionJsonData[this.itemJsonDef.value] = moment(new Date(this.formControl.value)).format();
    }
    this.sectionJsonDataChange.emit(this.sectionJsonData);
    this.OnChange.emit(e);
  }
  // getErrors(ctrl) {
  //     return Object.keys(ctrl.errors);
  // }

  getErrorMessage() {
    // console.log(Object.keys(formControl.errors));
    // <strong _ngcontent-c7="">required</strong>
    return this.formControl.hasError('matDatepickerMin') ? 'Min Date is: '
      + moment(this.minDate).format(this.itemJsonDef.dateformat) :
      this.formControl.hasError('matDatepickerMax') ? 'Max date is: '
        + moment(this.maxDate).format(this.itemJsonDef.dateformat) : '';
  }
}
