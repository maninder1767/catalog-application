import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CommonModule } from '@angular/common';

// Mat components
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatChipsModule } from '@angular/material/chips';
import { MatNativeDateModule, MatOptionModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';

// WFX Components
import { WfxTextboxComponent } from './wfxcomponent-ts/wfxtextbox';
import { WfxTextareaComponent } from './wfxcomponent-ts/wfxtextarea';
import { WfxCheckboxComponent } from './wfxcomponent-ts/wfxcheckbox';
import { WfxRadioButtonComponent } from './wfxcomponent-ts/wfxradiobutton';
import { WfxSelectComponent } from './wfxcomponent-ts/wfxselect';
import { WfxSlideToggleComponent } from './wfxcomponent-ts/wfxslidetoggle';
import { WfxDatePickerComponent } from './wfxcomponent-ts/wfxdatepicker';
import { WfxTitleBarComponent } from './wfxcomponent-ts/wfxtitlebar';
import { ConfirmationdialogComponent } from './wfxcomponent-ts/wfxconfirmationdialog';
import { WfxGridComponent } from './wfxcomponent-ts/wfxgrid';
import { WfxTabComponent } from './wfxcomponent-ts/wfxtab';
import { WfxSectionComponent } from './wfxcomponent-ts/wfxsection';
import { WfxGridTextBoxComponent } from './wfxcomponent-ts/wfxgridtextbox';
import { WfxGridCheckBoxComponent } from './wfxcomponent-ts/wfxgridcheckbox';
import { WfxGridSelectComponent } from './wfxcomponent-ts/wfxgridselect';
import { WfxLinkButtonComponent } from './wfxcomponent-ts/wfxlinkbutton';
import { WfxGridDatePickerComponent } from './wfxcomponent-ts/wfxgriddatepicker';
import { WfxGridLinkButtonComponent } from './wfxcomponent-ts/wfxgridlinkbutton';
import { WfxGridTextAreaComponent } from './wfxcomponent-ts/wfxgridtextarea';
import { WfxPageNotFoundComponent } from './wfxcomponent-ts/wfxpagenotfound';
import { WfxButtonComponent } from './wfxcomponent-ts/wfxbutton';
import { WfxLayoutComponent } from './wfxcomponent-ts/wfxlayout';
import { WfxLabelComponent } from './wfxcomponent-ts/wfxlabel';
import { WfxSpinnerComponent } from './wfxcomponent-ts/wfxspinner';
import { WFXGridDateFilterComponent } from './wfxcomponent-ts/wfxgriddatefilter';
import { WfxGridPinnedRowComponent } from './wfxcomponent-ts/wfxgridpinnedrow';
import { WFXGridImageRendererComponent } from './wfxcomponent-ts/wfxgridimagerenderer';
import { WfxGridNumberTextBoxComponent } from './wfxcomponent-ts/wfxgridnumbertextbox';
import { WfxListComponent, FilterListPipe } from './wfxcomponent-ts/wfxlist';

// WFX Function
import { WfxCommonFunctions } from './wfxsharedfunction/wfxcommonfunction';

// WFX Global Ts For Variables
// import { WfxGlobal } from './wfxsharedscript/wfxglobal';

// WFX Services
import { WfxShowMessageservice } from './wfxsharedservices/wfxshowmessage.service';

// Ag Grid
import { AgGridModule } from 'ag-grid-angular';
import 'ag-grid-enterprise';

// WFX Interceptors
import { WfxAuthInterceptor } from './wfxinterceptor/wfxauthinterceptor';

// WFX Directives
import { WfxnumbercdirectiveDirective } from './wfxshareddirectives/wfxnumbercdirective.directive';

@NgModule({
  declarations: [
    WfxGridLinkButtonComponent, WfxTextboxComponent, WfxTextareaComponent, WfxCheckboxComponent,
    WfxRadioButtonComponent, WfxSelectComponent, WfxSlideToggleComponent, WfxDatePickerComponent,
    WfxTitleBarComponent, ConfirmationdialogComponent, WfxGridComponent, WfxTabComponent, WfxSectionComponent,
    WfxGridTextBoxComponent, WfxGridCheckBoxComponent, WfxGridSelectComponent, WfxLinkButtonComponent,
    WfxGridDatePickerComponent, WfxGridTextAreaComponent, WfxPageNotFoundComponent,
    WfxButtonComponent, WfxLayoutComponent, WfxLabelComponent, WfxSpinnerComponent, WFXGridDateFilterComponent, WfxGridPinnedRowComponent,
    WFXGridImageRendererComponent, WfxGridNumberTextBoxComponent, WfxnumbercdirectiveDirective, WfxListComponent, FilterListPipe
  ],
  imports: [
    FormsModule, ReactiveFormsModule, HttpClientModule, MatInputModule, MatFormFieldModule, CommonModule,
    MatCardModule, MatSelectModule, MatTabsModule, MatListModule, MatSidenavModule,
    MatToolbarModule, MatIconModule, MatDialogModule, MatAutocompleteModule, MatInputModule,
    MatFormFieldModule, MatMenuModule, MatDatepickerModule, MatNativeDateModule, MatTooltipModule,
    MatCheckboxModule, MatProgressBarModule, MatChipsModule, MatSnackBarModule, MatTableModule,
    MatSortModule, MatPaginatorModule, MatOptionModule, MatRadioModule, NgxMatSelectSearchModule,
    MatSlideToggleModule, MatButtonModule, MatProgressSpinnerModule,
    AgGridModule.withComponents([
      WfxGridLinkButtonComponent, WfxGridTextBoxComponent, WfxGridCheckBoxComponent, WfxGridSelectComponent, WfxGridDatePickerComponent,
      WfxGridTextAreaComponent, WFXGridDateFilterComponent, WfxGridPinnedRowComponent, WFXGridImageRendererComponent, WfxGridNumberTextBoxComponent
    ])
  ],
  providers: [WfxShowMessageservice, WfxCommonFunctions,
    { provide: HTTP_INTERCEPTORS, useClass: WfxAuthInterceptor, multi: true }
  ],
  bootstrap: [],
  entryComponents: [ConfirmationdialogComponent],
  exports: [WfxGridLinkButtonComponent, WfxTextboxComponent, WfxTextareaComponent, WfxCheckboxComponent,
    WfxRadioButtonComponent, WfxSelectComponent, WfxSlideToggleComponent, WfxDatePickerComponent,
    WfxTitleBarComponent, ConfirmationdialogComponent, WfxGridComponent, WfxTabComponent, WfxSectionComponent,
    WfxGridTextBoxComponent, WfxGridCheckBoxComponent, WfxGridSelectComponent, WfxLinkButtonComponent,
    WfxGridDatePickerComponent, WfxGridTextAreaComponent, WfxPageNotFoundComponent, WfxButtonComponent,
    WfxLayoutComponent, WfxLabelComponent, WfxSpinnerComponent, WFXGridDateFilterComponent, WfxGridPinnedRowComponent,
    WFXGridImageRendererComponent, WfxGridNumberTextBoxComponent, WfxnumbercdirectiveDirective, WfxListComponent, FilterListPipe]
})
export class WfxSharedModule { }
