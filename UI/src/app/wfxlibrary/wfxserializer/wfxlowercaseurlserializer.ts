import { DefaultUrlSerializer, UrlTree } from '@angular/router';

export class WfxLowerCaseUrlSerializer extends DefaultUrlSerializer {
    parse(url: string): UrlTree {
        return super.parse(url.toLowerCase());
    }
}