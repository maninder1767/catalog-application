import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor } from '@angular/common/http';
import { HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';

@Injectable()
export class WfxAuthInterceptor implements HttpInterceptor {

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const clonedRequest = req.clone({
      // headers: req.headers.set('X-CustomAuthHeader', 'some-auth-token')
      headers: req.headers.set('Authorization', 'V0ZYQWRtaW46cXJzdEAxMjM0Lg==')
    });
    // console.log('new headers', clonedRequest.headers.keys());
    return next.handle(clonedRequest);
  }

}
