import { Injectable, OnInit } from '@angular/core';
import { WfxCommonFunctions } from '../wfxsharedfunction/wfxcommonfunction';

@Injectable({
  providedIn: 'root'
})
export class WfxGlobal {
  constructor() { }

  gObjDDLHashData: any = {};
  isPLM = false;
  sessionVariables: {
    applicationurl: any;
    companycode: any;
    companydivisioncode: any;
    guid: any;
    membercompanycode: any;
    username: any;
  };
}
