import { Directive, Input, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appWfxnumbercdirective]'
})
export class WfxnumbercdirectiveDirective {
  private decimalCounter = 0;
  private navigationKeys = [
    'Backspace',
    'Delete',
    'Tab',
    'Escape',
    'Enter',
    'Home',
    'End',
    'ArrowLeft',
    'ArrowRight',
    'Clear',
    'Copy',
    'Paste'
  ];
  @Input() decimal = false;
  @Input() decimalplaces = 2;

  inputElement: HTMLInputElement;

  constructor(public el: ElementRef) {
    this.inputElement = el.nativeElement;
  }

  @HostListener('keydown', ['$event'])
  onKeyDown(e: KeyboardEvent) {
    if (
      this.navigationKeys.indexOf(e.key) > -1 || // Allow: navigation keys: backspace, delete, arrows etc.
      (e.key === 'a' && e.ctrlKey === true) || // Allow: Ctrl+A
      (e.key === 'c' && e.ctrlKey === true) || // Allow: Ctrl+C
      (e.key === 'v' && e.ctrlKey === true) || // Allow: Ctrl+V
      (e.key === 'x' && e.ctrlKey === true) || // Allow: Ctrl+X
      (e.key === 'a' && e.metaKey === true) || // Allow: Cmd+A (Mac)
      (e.key === 'c' && e.metaKey === true) || // Allow: Cmd+C (Mac)
      (e.key === 'v' && e.metaKey === true) || // Allow: Cmd+V (Mac)
      (e.key === 'x' && e.metaKey === true) || // Allow: Cmd+X (Mac)
      (this.decimal && e.key === '.' && this.decimalCounter < 1) // Allow: only one decimal point
    ) {
      // let it happen, don't do anything
      return;
    }
    // Ensure that it is a number and stop the keypress
    if (e.key === ' ' || isNaN(Number(e.key))) {
      e.preventDefault();
    } else {
      const value = this.el.nativeElement.value;
      // this.el.nativeElement.value = (value.indexOf('.') >= 0) ?
      //   (value.substr(0, value.indexOf('.')) + value.substr(value.indexOf('.'), 4)) : value;
      // decimal places - 1 done because here in this.el.nativeElement.value is coming before the keypress.
      const cursurPosition = e.target['selectionStart']; // for 10.45 cursurPosition will come 2 and value.indexOf('.') will come 2
      if (value.indexOf('.') >= 0 && cursurPosition > value.indexOf('.') && value.toString().split('.')[1].length > this.decimalplaces - 1) {
        e.preventDefault();
      }
    }
  }

  @HostListener('keyup', ['$event'])
  onKeyUp(e: KeyboardEvent, a) {
    if (!this.decimal) {
      return;
    } else {
      this.decimalCounter = this.el.nativeElement.value.split('.').length - 1;
    }
  }

  @HostListener('paste', ['$event'])
  onPaste(event: ClipboardEvent) {
    let pastedInput: string = event.clipboardData.getData('text/plain');
    let pasted = false;
    if (!this.decimal) {
      pasted = document.execCommand('insertText', false, pastedInput.replace(/[^0-9]/g, ''));
    } else if (this.isValidDecimal(pastedInput)) {
      pastedInput = this.returnValueUptoOneDotOnly(pastedInput);
      pastedInput = this.returnUpToDecimalPlaces(pastedInput);
      pasted = document.execCommand('insertText', false, pastedInput);
    }
    if (pasted) {
      event.preventDefault();
    } else {
      if (navigator.clipboard) {
        pastedInput = this.returnValueUptoOneDotOnly(pastedInput);
        pastedInput = this.returnUpToDecimalPlaces(pastedInput);
        navigator.clipboard.writeText(pastedInput);
        document.execCommand('insertText', false, pastedInput);
        event.preventDefault();
        // document.execCommand('paste');
      }
    }
  }

  @HostListener('drop', ['$event'])
  onDrop(event: DragEvent) {
    const textData = event.dataTransfer.getData('text');
    this.inputElement.focus();

    let pasted = false;
    if (!this.decimal) {
      pasted = document.execCommand(
        'insertText',
        false,
        textData.replace(/[^0-9]/g, '')
      );
    } else if (this.isValidDecimal(textData)) {
      pasted = document.execCommand(
        'insertText',
        false,
        textData.replace(/[^0-9.]/g, '')
      );
    }
    if (pasted) {
      event.preventDefault();
    } else {
      if (navigator.clipboard) {
        navigator.clipboard.writeText(textData);
        document.execCommand('paste');
      }
    }
  }

  isValidDecimal(string: string): boolean {
    return string.split('.').length <= 2;
  }

  returnUpToDecimalPlaces(value) {
    return Number(value).toFixed(this.decimalplaces);
  }
  returnValueUptoOneDotOnly(value) {
    let counter = 0;
    return value.replace(/[^0-9.]|\./g, ($0) => {
      if ($0 === '.' && !(counter++)) { // dot found and counter is not incremented
        return '.';
      } // that means we met first dot and we want to keep it
      return ''; // if we find anything else, let's erase it
    });
  }
}
