export class WFXResultModel {
    ResponseID: number;
    ResponseData: any;
    ErrorMsg: string;
    Status: string;
}
