export class PageParam {
    ParamName: string; ParamValue: string;
    constructor(paramName: string, paramValue: string) {
        this.ParamName = paramName;
        this.ParamValue = paramValue;
    }
}

export class SearchParam {
    ParamName: string; ParamValue: string;
    constructor(paramName: string, paramValue: string) {
        this.ParamName = paramName;
        this.ParamValue = paramValue;
    }
}

export class SortParam {
    SortParam: string;
    constructor(sortParam: string) {
        this.SortParam = sortParam;
    }
}

export class PagingParam {
    PageIndex: number; PageSize: number;
    constructor(pageIndex: number, pageSize: number) {
        this.PageIndex = pageIndex; this.PageSize = pageSize;
    }
}
