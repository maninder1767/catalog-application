import { TooltipPosition, FloatLabelType } from '@angular/material';

export class WfxComponentAttributes {
  itemid?: string;
  disable?: boolean;
  readonly?: boolean;
  mandatory?: boolean;
  visible?: boolean;
  caption?: string;
  valuetype?: string;
  value?: string;
  valuetext?: string;
  maxlength?: string;
  minlength?: string;
  inputtype?: string;
  inputsubtype?: string;
  width?: string;
  height?: string;
  labelposition?: any; // before or after
  rbList?: any;
  multiselect?: boolean;
  dateformat?: string; // LT: 'h:mm A', LTS: 'h:mm:ss A', L: 'MM/DD/YYYY', l: 'M/D/YYYY',
  // LL: 'MMMM Do YYYY', ll: 'MMM D YYYY', LLL: 'MMMM Do YYYY LT', lll: 'MMM D YYYY LT',
  // LLLL: 'dddd, MMMM Do YYYY LT', llll: 'ddd, MMM D YYYY' (pass only first digits like 'LT')
  tooltip?: WfxToolTip;
  minvaluetype?: string; // text, dbfield
  maxvaluetype?: string; // text, dbfield
  minvalue?: string;
  maxvalue?: string;
  onchange?: any;
  ddlvalue?: string;
  ddlvaluetext?: string;
  addselect?: boolean;
  addselecttext?: string;
  inputdatedisable?: boolean; // stop user to input the date
  decimalplaces?: number;
  addclearicon?: boolean; // clear action in textbox & select
  addimginddl?: boolean; // image in select
  addsearchoptioninddl?: boolean; // conditional searchable in select
  floatlabel?: FloatLabelType; // "auto" (when user will type then it will float) ,
  // "always" (alwats will be floated) , "never" for hiding the placeholder when user type.
  classname?: any;
  placeholderlabel?: string;
}

export class WfxTitleBar {
  titlebarid?: string;
  caption?: string;
  titlebartype?: string; // pagetitlebar,sectiontitlebar
  tooltip?: WfxToolTip;
  toollist?: WfxTool[];
}

export class WfxSectionJsonDef {
  sectionproperties?: WfxSectionProperties;
  titlebarproperties?: WfxTitleBar;
  Itemjsondef?: WfxComponentAttributes[];
}

export class WfxSectionProperties {
  sectionid?: string;
  itemtype?: string;
}

export class WfxToolTip {
  tooltipvalue?: string;
  tooltipvaluetype?: string; // dbfield, text
  tooltipposition?: TooltipPosition; // ['after', 'before', 'above', 'below', 'left', 'right'];
}

export class WfxTool {
  toolname?: string;
  visible?: boolean;
  disable?: boolean;
  tooltip?: WfxToolTip;
}

export class WfxTab {
  code?: string;
  text?: string;
  visible?: boolean;
  disable?: boolean;
  tooltip?: WfxToolTip;
}

export class WfxConfirmationDialog {
  dialogtitle?: string;
  dialogmessage?: string;
  dialogfalsebuttonname?: string;
  dialogtruebuttonname?: string;
}

export class WfxToolAction {
  actionName?: string;
  toolObj?: any;
  event?: any;
}


export class WFXResultModel {
  ResponseID: number;
  ResponseData: any;
  ErrorMsg: string;
  Status: string;
}

export class WFXSessionVariableModel {
  CompanyCode: string;
  MemberCompanyCode: string;
  CompanyDivisionCode: string;
  UserName: string;
  ApplicationURL: string;
}

export class WFXSessionVariableResultModel {
  ResponseID: number;
  ResponseData: WFXSessionVariableModel;
  ErrorMsg: string;
  Status: string;
}


export class WFXGridCellAttributes {
  colid?: any;
  headercheckboxselection?: any;
  colname?: any;
  valuetext?: any;
  filter?: any;
  resizable?: any;
  sortable?: any;
  filterparams?: WFXGridFilterParams;
  headertooltip?: any;
  pivot?: any;
  enablepivot?: any;
  valuetooltip?: any;
  editable?: any;
  suppresskeyboardevent?: any;
  inputtype?: any;
  columngroupshow?: any;
  rowdrag?: any;
  checkboxselection?: any;
  mandatory?: any;
  inputsubtype?: any;
  maxvalue?: any;
  maxvaluetype?: any;
  decimalplaces?: any;
  ddlvalues?: any;
  value?: any;
  minvaluetype?: string; // text, dbfield
  minvalue?: any;
  ddlvalue?: any;
  ddlvaluetext?: any;
  children?: WFXGridCellAttributes[];
  onclick?: WFXFunctionParams[];
  onchange?: WFXFunctionParams[];
  hide ? = false;
  rowgroup ? = false;
  width?: number;
  pinned?: string;
  aggfunc?: string;
  formula?: any;
  valuesetter?: any;
  valueformatter?: any;
  valueparser?: any;
  cellstyle?: any;
  valueprefix?: string;
  valuesuffix?: string;
  addselect?: boolean;
  addselecttext?: string;
  disable?: boolean;
  multiselect?: boolean;
  autobindddl?: boolean; // auto bind ddl when mouse down event occur mainly i am trying for Grid.
  action?: any; // using for link button action
  maxlength?: string;
  editableproperties?: any = {};
  dependentsuffixvaluetext?: string;
  dependentsuffixfiltervalue?: string;
  metadatavaluefieldname ? = 'Code';
  attributelist?: WFXAttribute[];
  currency?: string; // It will be hardcoded currency from this currency we will filter the symbol and we will add that symbol as suffix in the cell value.
                    // It will be working in money type field only.
  currencyfield?: string; // It will be data driven currency field name from this currency value we will filter the symbol and we will add that symbol as suffix in the cell value.
                    // It will be working in money type field only.
  stoppaste?: any;
}

export class WFXAttribute {
  attrname?: string;
  valuetype?: string;
  attrvalue?: string;
  colid?: string;
}

export class WFXGridFilterParams {
  clearButton = true;
  applyButton = false;
  debounceMs?: number;
}

export class WFXFunctionParams {
  fnname?: string;
  paramlist?: WFXAttribute[];
  updatefield?: string;
}
