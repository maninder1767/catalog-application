import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class Globals {
  productCatCode  = '';
  guid = '';
  companyCode = '';
  memberCompanyCode= '';
  companyDivisionCode= '';
  userName= '';
  languageCode= '';
  menuBar= '';
  sessionID= '';
  isAdvancedSeachFilterClicked = false;
  menuType = '';
  costingApproval: false;
}
