import { Item } from './../../wfxplmitemlibrary/modals/item';
import { data } from './../components/wfx-tableview/db.json';
import { DataService } from './../../shared/services/data.service';
import { Injectable } from '@angular/core';
import { SearchParams, SortParams, FilterCriteria, ApplyFilter } from './../modals/filter';
import { HttpClient } from '@angular/common/http';
import { ItemLibraryGlobals } from './../../wfxplmitemlibrary/itemLibrary.global';
import { Globals } from './../../globals';
import { map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { ViewType } from '../constants/enum';
import { of } from 'rxjs';
import { WfxGlobal } from '../../wfxlibrary/wfxsharedscript/wfxglobal';
@Injectable(
  {
    providedIn: 'root'
  }
)
export class ItemLibraryDataService {
  url = environment.apiUrl;
  aUrl=environment.aUrl;
  nUrl=environment.nUrl;
  headers: { guid?: string, 'Content-Type'?: string } = {};

  constructor(private global: Globals,
    private itemGlobal: ItemLibraryGlobals,
    private httpClient: HttpClient,
    public wfxGlobal: WfxGlobal,
    private dataSvc: DataService) {
  }

  // To get selected View Data
  getList(type?: ViewType, loadmore?: boolean) {
    if (!loadmore) {this.itemGlobal.arrSelectedItemId = []; }
    switch (this.itemGlobal.currentView) {
      case ViewType.GALLERY:
        this.galleryGetList(loadmore);
        break;
        // case ViewType.TABLE:
        //   if (this.itemGlobal.selectedTableSortParam.length > 0) {
        //     // console.log(this.itemGlobal.selectedTableSortParam);
        //     this.getSelectedTableViewData().pipe(map((row: any) => {
        //       this.itemGlobal.arrTableRowData = row.data.item;
        //       this.itemGlobal.recordCount = +row.data.count;
        //       return 1;
        //     })).subscribe();
        //     break;
        //   }
        //     const tableDef =  this.getSelectedTableViewTableDefinition(this.itemGlobal.tableSelectedView.Code)
        //     .pipe(map((colField: any) => {
        //       const colDef: any[] = [];
        //       colDef.push({
        //         headerName: '', width: 40,
        //         lockPosition: true, suppressSizeToFit: true, resizable: false, filter: false,
        //         checkboxSelection: () => {
        //           return true;
        //         },
        //         // pinnedRowCellRenderer: 'customPinnedRowRenderer',
        //         // pinnedRowCellRendererParams: { style: { color: 'blue' } }
        //       });
        //       colField.data.forEach(val => {
        //         const filterType = 'agTextColumnFilter';
        //         let sortValue: any = '';
        //         // switch (val.fieldType) {
        //         //   case 'text': {
        //         //     filterType = 'agTextColumnFilter';
        //         //     break;
        //         //   }
        //         //   case 'date':
        //         //     filterType = 'agTextColumnFilter';
        //         //     break;
        //         //   default:
        //         //     filterType = 'agTextColumnFilter';
        //         //     break;
        //         // }

        //         this.itemGlobal.selectedTableSortParam.forEach(sort => {
        //           if (sort.fieldId === val.fieldId) {
        //             sortValue = sort.sortDirection;
        //           } else {
        //             sortValue = '';
        //           }
        //         });
        //         // console.log(val.fieldId);
        //         colDef.push({headerName: val.caption, width: +val.width, field: val.fieldId,
        //           lockPosition: false, suppressSizeToFit: true, resizable: true, sortable: true,
        //           filter: filterType, rowDrag: false,
        //           cellRenderer: function(params) {
        //             if (params.colDef.field === 'article_image') {
        //               return '<img style="margin-left: 50%; width:20px; height:20px; border-radius:50%;" src="' + params.value + '">';
        //             } else if ( params.value !== undefined) {
        //               return params.value;
        //             }
        //         }});
        //       });
        //       this.itemGlobal.arrTableColumnDefinition = colDef;
        //       return 1;
        //     }));
        //     const rowData =   this.getSelectedTableViewData().pipe(map((row: any) => {
        //       this.itemGlobal.arrTableRowData = row.data.item;
        //       this.itemGlobal.recordCount = +row.data.count;
        //       return 1;
        //     }));
        //     concat(tableDef, rowData).subscribe();
        //   break;
      default:
        this.galleryGetList(loadmore);
        break;
    }
  }

  galleryGetList(loadmore?: boolean) {
    this.setCommonFilterModel();
    console.log(this.itemGlobal.commonFilter)
    this.itemGlobal.commonFilter.sortparams = [this.itemGlobal.defaultSortParams];//this.url + 'Item/Gallery'
    return this.dataSvc.post(this.aUrl+'Gallery'
    , this.itemGlobal.commonFilter, { ProductCatCode: this.global.productCatCode }).pipe(map((val: any) => {     
    if (val.data.item) {
      this.itemGlobal.isSpinnerLoadingGalleryView = false;
      if (loadmore) {
        // this.itemGlobal.arrGalleryViewItemData['checkbox'] = true;
        this.itemGlobal.arrGalleryViewItemData.forEach(obj => {
        obj.checkbox = false;
                 });
        this.itemGlobal.arrGalleryViewItemData = this.itemGlobal.arrGalleryViewItemData.concat(val.data.item);
        this.itemGlobal.arrGalleryViewData.next( this.itemGlobal.arrGalleryViewItemData);
      }else {
        this.itemGlobal.arrGalleryViewItemData = val.data.item;
        // this.itemGlobal.arrGalleryViewItemData['checkbox'] = true;
        this.itemGlobal.arrGalleryViewItemData.forEach(obj => {
                obj.checkbox = false;
        });
        this.itemGlobal.arrGalleryViewData.next(this.itemGlobal.arrGalleryViewItemData);
      }
      this.itemGlobal.recordCount = val.data.count;
      return val.data;
     }
    })).subscribe();
  }

  search(text: string = '') {
    if (text === '') {
      this.itemGlobal.commonFilter.pagingParams.PageIndex = 1;
      this.itemGlobal.quickSearchFilterText = text;
      this.itemGlobal.commonFilter.searchparams.filterText = '';
      this.getList();
    } else {
      this.itemGlobal.commonFilter.pagingParams.PageIndex = 1;
      this.itemGlobal.quickSearchFilterText = text;
      this.itemGlobal.commonFilter.searchparams.filterText = text;
      this.getList();
    }
  }

  getItem(id: string) {
    return this.dataSvc.get(this.url + 'Item/Gallery/' + id);
  }
  updateLike(id: string) {
    return this.dataSvc.put(this.url + 'Item/Like', { ID: id });
  }
  getLikeDetail(id: string) {
    return this.dataSvc.get(this.url + 'Item/Like/' + id);
  }
  postComment(commentData) {
    console.log(commentData)
    return this.dataSvc.post(this.nUrl + 'Comment/', commentData);//(this.url + 'Item/Comment', commentData);//this.nUrl + 'Comment/'
  }
  postCommentReply(commentData: { id: string, comment: string, parentCommentId: string }) {
    return this.dataSvc.post(this.url + 'Item/Comment', commentData);
  }
  getComment(id: string) {
    return this.dataSvc.get(this.nUrl + 'Comment/?id=' + id)//(this.url + 'Item/Comment/' + id);//(this.nUrl + 'Comment/' + id);//
  }

  updateCommentLike(commentData) {
    return this.dataSvc.put(this.url + 'Item/Comment/Like', commentData);
  }

  updateCommentReplyLike(data: { id: string, commentId: string, replyId: string }) {
    return this.dataSvc.put(this.url + 'Item/Comment/Like', data);
  }

  getTabList() {
    return this.dataSvc.get(this.nUrl + 'Tab');
  }

  getAdvanceSearchDefinitionList() {
    return this.dataSvc.get(this.nUrl+"Search/Definition");//(this.url + 'Item/Search/Definition');
  }

  getSortByList() {
    return this.dataSvc.get(this.aUrl+'sort', { ProductCatCode: this.global.productCatCode });//this.url + 'Item/SortBy'
  }

  getSavedSearchDropdownList() {
    return this.dataSvc.get(this.nUrl+"Search");//(this.url + 'Item/Search', { ProductCatCode: this.global.productCatCode });
  }

  postSavedSearch(data: {}) {
    return this.dataSvc.post(this.nUrl+"Search", data, { ProductCatCode: this.global.productCatCode });//this.url + 'Item/Search'
  }

  getSelectedSavedSearch(id: string) {
    return this.dataSvc.get(this.nUrl+"Search/ID/?id="+id);//(this.url + 'Item/Search/' + id);
  }

  deleteSavedSearch(id: string) {
    return this.dataSvc.delete(this.nUrl+'Search/?id='+id);//(this.url + 'Item/Search/' + id);
  }

  getFixedFieldAdvanceFilterData(objectType: string, dataSource: string) {
    return this.dataSvc.get(this.nUrl + 'Master/Data/Custom',
    { objectType: objectType, dataSource: dataSource, ProductCatCode: this.global.productCatCode });
  }

  getCustomFieldAdvanceFilterData(objectType: string, dataSource: string) {
    return this.dataSvc.get(this.nUrl + 'Master/Data/Custom',
    { objectType: objectType, dataSource: dataSource, ProductCatCode: this.global.productCatCode });
  }

  getTableViewDropdownList() {
    return this.dataSvc.get(this.aUrl + 'TableLayout', { productCatCode: this.global.productCatCode });
  }

  getSelectedTableViewData() {
    this.itemGlobal.commonFilter.params = [{ParamName: 'ListId',
    ParamValue: this.itemGlobal.tableSelectedView.Code}];
    // if (this.itemGlobal.selectedTableSortParam.length > 0) {
    //   this.itemGlobal.commonFilter.sortparams = this.itemGlobal.selectedTableSortParam;
    // } else {
    //   this.itemGlobal.commonFilter.sortparams = [];
    // }
    console.log(this.itemGlobal.commonFilter)
    this.setCommonFilterModel();
    return this.dataSvc.post(this.aUrl+"table/", this.itemGlobal.commonFilter,
    { ProductCatCode: this.global.productCatCode });
    //this.url + 'Item/Table', this.itemGlobal.commonFilter,{ ProductCatCode: this.global.productCatCode }
    //.get('http://192.168.2.252:1400/api/?table=2750000&limit=100');
  }

  getContextMenuList(id: string) {
    return this.dataSvc.get(this.aUrl + 'ContextMenu/' + id, { productCatCode: this.global.productCatCode });
  }

  updateItemStatus(data: any) {
    return this.dataSvc.put(this.aUrl + 'Active', data);
  }

  getCommentLikeDetail(id: string, commentID: number) {
    return this.dataSvc.get(this.url + 'Item/Comment/Like/' + id + '/' + commentID);
  }

  updateItemBookmark(data: { id: string }) {
    return this.dataSvc.put(this.aUrl + 'BookMark', data);
  }

  getSelectedItemPropertyData(id: string) {
    return this.dataSvc.get(this.aUrl + 'Property/' + id,
    { ProductCatCode: this.global.productCatCode });
}

  getSelectedTableViewTableDefinition(id: string) {
    return this.dataSvc.get(this.aUrl+'tablecolumn');//(this.url + 'Item/TableDefinition/' + id, {ProductCatCode: this.global.productCatCode});
  }

  getRecordCount() {
    return this.itemGlobal.recordCount;
  }
  sortbyItemDropdownField(value: SortParams[] = []) {
    if (value.length === 0) {
      this.itemGlobal.commonFilter.pagingParams.PageIndex = 1;
      this.getList();
    } else {
      this.itemGlobal.commonFilter.pagingParams.PageIndex = 1;
      this.itemGlobal.commonFilter.sortparams = value;
      this.getList();
    }
  }

  advanceSearchFilter(filterCriteria: FilterCriteria[] = []) {
    if (filterCriteria.length === 0) {
      this.itemGlobal.commonFilter.pagingParams.PageIndex = 1;
      this.getList();
    } else {
      // this.itemGlobal.arrFilterCriteria = filterCriteria;
      this.itemGlobal.arrGalleryFilterCriteria = filterCriteria;
      this.itemGlobal.commonFilter.pagingParams.PageIndex = 1;
      this.getList();
    }
  }

  getCommonData() {
    return this.dataSvc.get(this.url + 'Common');
  }

  activeItemToggle(currentValue: string = '') {
    if (currentValue === '') {
      this.itemGlobal.commonFilter.pagingParams.PageIndex = 1;
      this.getList();
    } else {
      this.itemGlobal.commonFilter.pagingParams.PageIndex = 1;
      this.itemGlobal.itemActiveToggleModel.filterData.forEach((obj => {
        obj.values.forEach(val => {
          val.value = currentValue;
        });
      }));
      this.getList();
    }
  }

  getBottomListData(selectedItem: number) {
    return this.dataSvc.get(this.aUrl + 'BottomPanel/',
    {
      itemcount: selectedItem.toString(),
      view: this.itemGlobal.currentView.toString(),
      active: this.itemGlobal.isActiveItem.toString(),
       ListId: '0',
       ProductCatCode : this.global.productCatCode
    });
  }

  updateSessionVariable(param: [{ParamName: string, ParamValue: string}]) {
    return this.dataSvc.put(this.url +  'Session/' + this.global.sessionID, param);
  }

  getQuickSearchPlaceholder() {
    return this.dataSvc.get(this.url + 'Item/QuickSearchPlaceHolder', {ProductCatCode: this.global.productCatCode});
  }

  private setCommonFilterModel() {
    if (this.itemGlobal.currentView === ViewType.GALLERY) {
      this.itemGlobal.commonFilter =  new ApplyFilter(
        this.itemGlobal.commonFilter.pagingParams,
        new SearchParams(
          this.itemGlobal.commonFilter.searchparams.filterText = this.itemGlobal.quickSearchFilterText,
          [this.itemGlobal.itemActiveToggleModel,
          ...this.itemGlobal.arrGalleryFilterCriteria]),
          this.itemGlobal.commonFilter.sortparams,
          this.itemGlobal.commonFilter.params);
    } else {
      this.itemGlobal.commonFilter =  new ApplyFilter(
        this.itemGlobal.commonFilter.pagingParams,
        new SearchParams(
          this.itemGlobal.commonFilter.searchparams.filterText = '',
          [this.itemGlobal.itemActiveToggleModel,
          ...this.itemGlobal.arrFilterCriteria]),
          this.itemGlobal.commonFilter.sortparams,
          this.itemGlobal.commonFilter.params);
    }
    }


  getMenuType() {
    if (this.global.menuType === 'mnuItemProduct') {
      return of({
        data: 'Products',
        message: '',
        status: 'Success'
     });
    }
    if (this.global.menuType === 'mnuItemComponent') {
      return of({
        data: 'Components',
        message: '',
        status: 'Success'
      });
    }
    return of({
      data: 'Product',
      message: '',
      status: 'Success'
    });
  }

  itemGenerateSKU(data: any[]) {
     return this.dataSvc.post(this.url + 'Item/GenerateSKU', data);
  }
  itemGenerateBarcode(data: any[]) {
    return this.dataSvc.post(this.url + 'Item/GenerateBarcode', data);
  }

  deleteItemTrack(data: any[]) {
    return this.dataSvc.post(this.url + 'Item/DeleteArticleTrack', data);
  }
  deleteDeliveryTrack(data: any[]) {
    return this.httpClient.post(this.url + 'Item/DeleteDeliveryTrack', data,
    {headers: { guid: this.global.guid, 'Content-Type': 'application/json' } });
  }

  bindDDL(URL, key, params?) {
    if (!params) {
      params = {};
    }
    if (this.wfxGlobal.gObjDDLHashData && this.wfxGlobal.gObjDDLHashData[key]) {
      const res = {};
      res['data'] = this.wfxGlobal.gObjDDLHashData[key];
      return of(res);
    } else {
      return this.getRequest(URL, params, key);
    }
  }

  getRequest(URL, params?, key?) {
    if (!params) {
      params = {};
    }
    return this.httpClient.get(this.url + URL,
      {
        params: { params },
        headers: { guid: this.global.guid, ProductCatCode: this.global.productCatCode, 'Content-Type': 'application/json' }
      }).pipe(
        map((res: any) => {
          if (key && key !== '') {
            this.wfxGlobal.gObjDDLHashData[key] = res.data;
          }
          return res;
        }));
  }

  postRequest(URL, params?, reqData?) {
    if (!params) {
      params = {};
    }
    return this.httpClient.post(this.url + URL, reqData,
      {
        params: { params },
        headers: { guid: this.global.guid, ProductCatCode: this.global.productCatCode, 'Content-Type': 'application/json' }
      });
  }

  putRequest(URL, params?, reqData?) {
    if (!params) {
      params = {};
    }
    return this.httpClient.put(this.url + URL, reqData,
      {
        params: { params },
        headers: { guid: this.global.guid, ProductCatCode: this.global.productCatCode, 'Content-Type': 'application/json' }
      });
  }

  deleteRequest(URL, params?) {
    if (!params) {
      params = {};
    }
    return this.httpClient.delete(this.url + 'Item/' + URL,
      {
        params: { params },
        headers: { guid: this.global.guid, ProductCatCode: this.global.productCatCode, 'Content-Type': 'application/json' }
      });
  }
}

