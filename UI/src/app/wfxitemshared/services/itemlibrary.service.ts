import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject } from 'rxjs';
import { ItemFilter, Filter, ApplyFilter } from '../modals/filter';

@Injectable(
  {
    providedIn: 'root'
  }
)
export class ItemLibraryService {
  serviceURL = '';
  constructor() { }

  private itemFilterSource = new Subject<{ itemFilter: ItemFilter }>();
  private showComment = new Subject<{ showCommentIcon: boolean, commentCount: string }>();
  private showCommentDetail = new Subject<{ ID: string }>();
  private commentClick = new Subject<any>();
  private filterSource = new Subject<{ filter: Filter }>();
  private filterChip = new Subject<{ filterChip: Filter }>();
  private appliedFilter = new Subject<{ appliedFilter: ApplyFilter }>();
  private propertyViewClick = new Subject<{ id: string }>();
  private selectedPanel = new Subject<any>();
  private costSheetDefSelectedValue = new Subject<{ selectedValueEvent: any}>();
  private bottompPanelMakeInactive = new Subject<boolean>();
  private costSheetDDlValue = new Subject<boolean>();

  itemFilter = this.itemFilterSource.asObservable();
  showCommentSubscription = this.showComment.asObservable();
  showCommentDetailSubscription = this.showCommentDetail.asObservable();
  showCommentClickSubscription = this.commentClick.asObservable();
  filter = this.filterSource.asObservable();
  filterChipSubscription = this.filterChip.asObservable();
  appliedFilterSubscription = this.appliedFilter.asObservable();
  showPropertyViewClickSubscription = this.propertyViewClick.asObservable();
  selectedPanelSubscription = this.selectedPanel.asObservable();
  costSheetDefSelectedValueSubscription = this.costSheetDefSelectedValue.asObservable();
  makeInactiveFromBottomPanelSubscription = this.bottompPanelMakeInactive.asObservable();
  costSheetDDlValueSubscription = this.costSheetDDlValue.asObservable();

  changeItemFilter(message: { itemFilter: ItemFilter }) {
    this.itemFilterSource.next(message);
  }

  invokeshowComment(message: { showCommentIcon: boolean, commentCount: string }) {
    this.showComment.next(message)
  }

  invokeshowCommentDetail(message: { ID: string }) {
    this.showCommentDetail.next(message)
  }
  invokeCommentClick(commentClick: boolean) {
    this.commentClick.next(commentClick)
  }
  changeFilter(message: { filter: Filter }) {
    this.filterSource.next(message);
  }
  invokesFilterChip(message: { filterChip: Filter }) {
    this.filterChip.next(message);
  }
  invokesAppliedFilter(message: { appliedFilter: ApplyFilter }) {
    this.appliedFilter.next(message);
  }
  invokesSortTypeChange(message: { appliedFilter: ApplyFilter }) {
    this.appliedFilter.next(message);
  }
  invokeProperViewClick(message: { id: string }) {
    this.propertyViewClick.next(message);
  }

  invokeshowSelectedRightPanel(rightpanelstate) {
    this.selectedPanel.next(rightpanelstate);
  }
  invokecostSheetDefSelectedValueChange(selectedValueEvent) {
    this.costSheetDefSelectedValue.next(selectedValueEvent);
  }

  invokeMakeInactiveFromBottomPanel(value) {
    this.bottompPanelMakeInactive.next(value);
  }

  invokeCostSheetDDlValue(data) {
    this.costSheetDDlValue.next(data);
  }

}
