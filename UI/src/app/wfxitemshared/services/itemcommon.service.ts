import { ItemLibraryGlobals } from './../../wfxplmitemlibrary/itemLibrary.global';
import { Injectable } from '@angular/core';
import { SearchParams, SortParams, FilterCriteria, Values } from './../modals/filter';
import { environment } from '../../../environments/environment';

@Injectable(
  {
    providedIn: 'root'
  }
)
export class ItemLibraryCommonService {

  constructor(private itemGlobal: ItemLibraryGlobals) {
    // this.updateQuickSearchText('hello World');
    // this.updateSortByItemFieldDropdown([{fieldId: '', sortOrder: 10, sortDirection: 'asc'}]);
  }

  updateQuickSearchText(value: string) {
    this.itemGlobal.commonFilter.searchparams.filterText = value;
  }

  updateSortByItemDropdownField(value: SortParams[]) {
    this.itemGlobal.commonFilter.sortparams = value;
  }

  updateAdvanceSearchFilterCriteria(filterCriteria: FilterCriteria[]) {
   // this.itemGlobal.arrFilterCriteria = filterCriteria;
   this.itemGlobal.arrGalleryFilterCriteria = filterCriteria;
  }

  updateItemActiveToggleFilterCriteria(value: string) {
    this.itemGlobal.itemActiveToggleModel.filterData.forEach((obj => {
      obj.values.forEach(val => {
        val.value = value;
      });
    }));
  }

  updateItemGlobalsPageParams(Value: number) {
      this.itemGlobal.defaultPageParams.PageIndex = Value;
  }

}

