import { TestBed, inject } from '@angular/core/testing';

import { ItemLibraryService } from './itemlibrary.service';

describe('DamService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ItemLibraryService]
    });
  });

  it('should be created', inject([ItemLibraryService], (service: ItemLibraryService) => {
    expect(service).toBeTruthy();
  }));
});
