import { WfxpopupheaderComponent } from './../wfxlibrary/wfxcomponent-ts/wfxpopupheader';
import { WfxContextmenuComponent } from './components/wfx-contextmenu/wfx-contextmenu.component';

// import { MatCardModule } from '@angular/material/card';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { WfxAvatarComponent } from './components/wfx-avatar/wfx-avatar.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WfxSharedModule } from '../wfxlibrary/wfxshared.module';
import { WfxQuicksearchComponent } from './components/wfx-quick-search/wfx-quick-search.component';
import { MatSelectModule, MatInputModule, MatCheckboxModule, MatListModule, MatDialogModule,
  MatCardModule, MatChipsModule, MatProgressBarModule, MatOptionModule, MatTabsModule,
  MatProgressSpinnerModule, MatRadioModule, MatFormFieldModule, MatTooltipModule, MatSliderModule, MatSidenavModule,
  MatButtonModule,
  MatGridListModule
  } from '@angular/material';
  import { FormsModule, ReactiveFormsModule } from '@angular/forms';
  import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { ItemLibraryService } from './services/itemlibrary.service';
import { WfxViewToolbarComponent } from './components/wfx-view-toolbar/wfx-view-toolbar.component';
import { WfxToolbarComponent } from './components/wfx-toolbar/wfx-toolbar.component';
import { WfxTitlebarComponent } from './components/wfx-titlebar/wfx-titlebar.component';
import { WfxAddButtonComponent } from './components/wfx-add-button/wfx-add-button.component';
import { WfxSliderComponent } from './components/wfx-slider/wfx-slider.component';
import { WfxViewoptionsComponent } from './components/wfx-viewoptions/wfx-viewoptions.component';
import { WfxLikeComponent } from './components/wfx-like/wfx-like.component';
import { LikeDialogComponent } from './components/wfx-like/wfx-like.component';
import { WfxBookmarkComponent } from './components/wfx-bookmark/wfx-bookmark.component';
import { WfxCardComponent } from './components/wfx-card/wfx-card.component';
import { WfxCommentComponent } from './components/wfx-comment/wfx-comment.component';
 import { WfxSortComponent } from './components/wfx-sort/wfx-sort.component';
 import { WfxTimeagoComponent } from './components/wfx-timeago/wfx-timeago.component';
 import { WfxfilterchipComponent } from './components/wfx-filterchip/wfx-filterchip.component';
 import { WfxFilterComponent, FilterSubTypePipe } from './components/wfx-filter/wfx-filter.component';
import { jqxCalendarComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxcalendar';
import { jqxDateTimeInputComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxdatetimeinput';
import {  ContextMenuModule, ContextMenuService } from 'ngx-contextmenu';
import { WfxExtendedmenuComponent } from './components/wfx-extendedmenu/wfx-extendedmenu.component';
import { WfxCommentListComponent } from './components/wfx-comment-list/wfx-comment-list.component';
import { WfxRecordCountComponent } from './components/wfx-record-count/wfx-record-count.component';
import { WfxTableviewComponent } from './components/wfx-tableview/wfx-tableview.component';
import { WfxFilterListComponent } from './components/wfx-filterlist/wfx-filterlist.component';
 import { WfxImageButtonComponent } from './components/wfx-imagebutton';
import { WfxSaveSearchComponent } from './components/wfx-save-search/wfx-save-search.component';
import { WfxPropertyViewComponent } from './components/wfx-property-view/wfx-property-view.component';
import { WfxBreadcrumbComponent } from './components/wfx-breadcrumb/wfx-breadcrumb.component';
import { WfxcolorcardComponent } from './components/wfx-colorcard';
import { TimeAgoPipe } from './pipes/time-ago.pipe';
import { LocalTimePipe } from './pipes/local.pipe';
import { FromUtcPipe } from './pipes/from-utc.pipe';
import { AutofocusDirective } from './directives/autofocus/autofocus.directive';
import { jqxTreeComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxtree';
import { jqxMenuComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxmenu';
import { AgGridModule } from 'ag-grid-angular';
import { ShrinkingHeaderDirective } from '../wfxplmitemlibrary/directives/shrinkingheader/shrinkingheader.directive'; 
import { ScrollingModule } from '@angular/cdk/scrolling';

@NgModule({
  declarations: [
    WfxAvatarComponent, WfxQuicksearchComponent, WfxViewToolbarComponent, WfxToolbarComponent,
    WfxTitlebarComponent, WfxAddButtonComponent, WfxSliderComponent, WfxViewoptionsComponent,
    WfxBookmarkComponent, LikeDialogComponent, WfxLikeComponent, jqxCalendarComponent, jqxDateTimeInputComponent,
    WfxCardComponent, WfxCommentComponent, WfxSortComponent, WfxExtendedmenuComponent, WfxContextmenuComponent,
    WfxTimeagoComponent,WfxCommentListComponent, WfxfilterchipComponent,
    WfxFilterComponent,WfxRecordCountComponent, WfxTableviewComponent, WfxFilterListComponent,
    WfxImageButtonComponent,WfxSaveSearchComponent,WfxPropertyViewComponent ,WfxBreadcrumbComponent,WfxcolorcardComponent,
    TimeAgoPipe,FilterSubTypePipe,LocalTimePipe, FromUtcPipe,AutofocusDirective,jqxTreeComponent, jqxMenuComponent,ShrinkingHeaderDirective,
    WfxpopupheaderComponent
   ],

  imports: [
    CommonModule,ScrollingModule,
    MatTooltipModule, MatFormFieldModule, MatInputModule, FormsModule, ReactiveFormsModule, NgxMatSelectSearchModule,
    MatSelectModule, MatProgressSpinnerModule, MatSliderModule, MatCheckboxModule, MatListModule, MatDialogModule,
    WfxSharedModule, LazyLoadImageModule, MatCardModule, MatChipsModule, MatRadioModule,
    MatProgressBarModule, MatOptionModule, MatTabsModule, MatSidenavModule,MatButtonModule,MatGridListModule,
    ContextMenuModule.forRoot(),  AgGridModule.withComponents([])
  ],

  exports: [
    WfxAvatarComponent, WfxViewToolbarComponent, WfxTitlebarComponent, WfxBookmarkComponent, LikeDialogComponent,
    WfxLikeComponent, LazyLoadImageModule, WfxCardComponent, MatCardModule, MatChipsModule,
    MatCheckboxModule, MatListModule, MatDialogModule, MatRadioModule, MatProgressBarModule,
    MatOptionModule, MatTabsModule, MatSliderModule, MatSidenavModule, jqxCalendarComponent,
     jqxDateTimeInputComponent, WfxSortComponent, WfxExtendedmenuComponent, WfxContextmenuComponent,
     WfxTimeagoComponent,WfxCommentListComponent, WfxfilterchipComponent, WfxToolbarComponent,
     WfxFilterComponent,WfxRecordCountComponent, WfxTableviewComponent, WfxSliderComponent, WfxCommentComponent
    ,MatButtonModule, WfxFilterListComponent, WfxImageButtonComponent,WfxSaveSearchComponent,ScrollingModule,
    WfxPropertyViewComponent ,WfxBreadcrumbComponent,WfxcolorcardComponent,ShrinkingHeaderDirective,WfxpopupheaderComponent
        ],
        providers: [ContextMenuService],
  entryComponents: [WfxSaveSearchComponent,LikeDialogComponent]
})
export class WfxItemSharedModule { }







