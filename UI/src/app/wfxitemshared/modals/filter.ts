export class ItemFilter {
    constructor() { }
    // SearchKeyword?: string;
    Keyword: '';
    isFilterApplied?= false;
    arrItemFilterType: ItemFilterType[] = [];
}
export class ItemFilterType {
    constructor() { }

    // SearchKeyword?: string;
    FilterType: string;
    arrFilterSubType: ItemFilterSubType[] = [];
    ControlType: string;
    FilterValue?= '';
    DateFrom?= '';
    DateTo?= '';
}
export class ItemFilterSubType {
    constructor() { }

    FilterSubType: string;
    IsSelected: boolean;
    ID?: number;
    FilterFileSubType?: ItemFilterSubType[] = [];
}


////////////////////////////////////////


export class Filter {
    constructor() { }
    // SearchKeyword?: string;
    searchId: number;
    listId: number;
    filterText: '';
    isFilterApplied = false;
    arrFilterType: FilterType[] = [];
}
export class FilterType {
    constructor() { }
    fieldId = '';
    caption= '';
    dataSource = '';
    controlType = '';
    fieldType = '';
    sortOrder = 0;
    arrFilterSubType?: FilterSubType[] = [];
    filterValue?= '';
    dateFrom?= '';
    dateTo?= '';
}

export class FilterSubType {
    constructor() { }
    id: number;
    value: string;
    text: string;
    IsSelected: Boolean = false;
}

export class FilterResultModel {
    data: FilterType[];
    message: string;
    status: string;
}


//////////// Apply   Filter ////////////

export class ApplyFilter {
    constructor(_pagingParams?: PageParams, _searchparams?: SearchParams,
      _sortparams?: Array<SortParams>, _params?: Array<Params>) {

        this.searchparams = _searchparams;
        this.sortparams = _sortparams;
        this.params = _params;
        this.pagingParams = _pagingParams;
    }
    pagingParams?: PageParams;
    searchparams?: SearchParams;
    sortparams?: Array<SortParams> = [];
    params?: Array<Params> = [];
}

export class SearchParams {
    constructor(_filterText?: string, _filterCriteria?: Array<FilterCriteria>) {
        this.filterText = _filterText;
        this.filterCriteria = _filterCriteria;

    }
    filterText = '';
    filterCriteria?: Array<FilterCriteria> = [];

}


export class FilterCriteria {
    constructor(_fieldId?: string, _caption?: string, _controlType?: string,
      _fieldType?: string, _operator?: string, _filterData?: Array<FilterData>) {
        this.fieldId = _fieldId;
        this.controlType = _controlType;
        this.caption = _caption;
        this.fieldType = _fieldType;
        this.operator = _operator;
        this.filterData = _filterData;
    }
    fieldId?: string;
    controlType?: string;
    caption = '';
    fieldType?: string;
    operator = '';
    filterData?: Array<FilterData> = [];
}



export class FilterData {
    constructor(_values?: Array<Values>, _filterType?: string) {
        this.values = _values;
        this.filterType = _filterType;
    }
    values?: Array<Values> = [];
    filterType = '';
}
export class Values {
    constructor(_id?: number, _value?: string, _text?: string) {
        this.id = _id;
        this.value = _value;
        this.text = _text;
    }
    id?: number;
    value?: string;
    text?: string;

}

export class SortParams {
    constructor(_fieldId?: string, _sortOrder?: number, _sortDirection?: string) {
        this.fieldId = _fieldId;
        this.sortOrder = _sortOrder;
        this.sortDirection = _sortDirection;
    }
    fieldId?: string;
    sortOrder?: number;
    sortDirection?: string;
}

export class PageParams {
    constructor(_pageIndex?: number, _pageSize?: number) {
        this.PageIndex = _pageIndex;
        this.PageSize = _pageSize;
    }
    PageIndex?: number;
    PageSize?: number;
}

export class Params {
    constructor(_paramName?: string, _paramValue?: string) {
        this.ParamName = _paramName;
        this.ParamValue = _paramValue;
    }
    ParamName?: string;
    ParamValue?: string;
}
//////////////////// Saved Search///////////////////
export class SavedSearchesValue {
    id: number;
    value: string;
    text: string;
}

export class SavedSearchesDdlResult {
    data: SavedSearchesValue[];
    message: string;
    status: string;
}

///////////// Saved Search Result/////////////////
export class SavedSearchesResult {
    data: FilterCriteria[];
    message: string;
    status: string;
}
