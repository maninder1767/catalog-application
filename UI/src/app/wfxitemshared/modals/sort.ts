export class Sort {
    constructor() { }
    id : number;
    value: string;
    text: string;
    isSelected?: boolean;
    sortOrder: number;
    sortDirection: string;
}
export class SortResult {
    data: Sort[];
    message: string;
    status: string;
}