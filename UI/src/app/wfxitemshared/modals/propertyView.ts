
export class Property {
    agent?: string;
    agentRef?: string;
    buyerRef?: string;
    description?: string;
    itemCode?: string;
    itemColors?:  any;
    itemID?: number;
    itemName?: string;
    itemSizes?: any;
    lifeCycleStage?: string;
    productGroup?: string;
    subCategory?: string;
    supplier?: string;
    supplierRef?: string;
}

export class PropertyData {
    data: Property[];
}

export class PropertyResultModel {
    item: Property[];
    ErrorMsg: string;
    Status: string;
}