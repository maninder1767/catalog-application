export enum ControlType {
    MULTISELECT = 'MULTISELECT',
    SELECT = 'SELECT',
    DATE = 'DATE',
    TEXT = 'TEXT',
    CHECKBOX = 'CHECKBOX'
}
export enum FieldType {
    FIXED = 'FIXED',
    CUSTOM = 'CUSTOM',
    FLEXI = 'FLEXI',
}
export enum ViewType {
    GALLERY = 'GALLERY',
    LIST = 'LIST',
    TABLE = 'TABLE',
}
