export const defaultDef= [
  {
    headerName: "",
    width: 40,
    field: "Name",
    lockPosition: true,
    suppressSizeToFit: true,
    resizable: false,
    cellClass: "rowDragClass",
    filter: false,
    rowDrag: true
  },
  {
    headerName: "Rendered Value",
    field: "imgUrl",
    width: 100,
    cellRenderer: function(params) {
      // put the value in bold
      // return '<b>' + params.value + '</b>';
      return `<div style="border-radius: 50%; height: 100%; width: 100%;">
      <img style="height: 100%; width: 100%;" src="` + params.value + `"/>
      </div>`;
  }
  },
  {
    headerName: 'Full Name', width: 100, field: 'Name', filter: 'agTextColumnFilter',
    sortable: true, filterParams: { clearButton: true }, columnGroupShow: 'closed',
    headerTooltip: 'Full Name',
    tooltipValueGetter: (params) => {
      return params.value;
    },
    editable: true,
    cellEditor: 'wfxTextBoxEditor',
    cellEditorParams: () => {
      return {
        itemAttr: {
          mandatory: true,
        }
      };
    },
  },
  {
    headerName: "",
    width: 60,
    headerCheckboxSelection: true,
    headerCheckboxSelectionFilteredOnly: true,
    lockPosition: true,
    suppressSizeToFit: true,
    resizable: false,
    filter: false,
    checkboxSelection: (params: any) => {
      return true;
    },
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    pinnedRowCellRendererParams: { style: { color: "blue" } }
  },
  {
    headerName: "Joining Date",
    editable: true,
    field: "JoiningDate",
    filter: "agDateColumnFilter",
    headerTooltip: "Joining Date",
    sortable: true,
    tooltipValueGetter: params => {
      return params.value;
    },
    cellEditor: "wfxDatePickerEditor",
    cellEditorParams: () => {
      return {
        itemAttr: {
          mandatory: true
        }
      };
    }
  },
  {
    headerName: "Telephone No",
    editable: true,
    field: "TelephoneNo",
    filter: "agNumberColumnFilter",
    filterParams: { clearButton: true },
    headerTooltip: "Telephone No",
    sortable: true,
    tooltipValueGetter: params => {
      return params.value;
    },
    cellEditor: "wfxTextBoxEditor",
    cellEditorParams: () => {
      return {
        itemAttr: {
          inputsubtype: "number",
          mandatory: true
        }
      };
    }
  },
  {
    headerName: "InActive",
    editable: false,
    field: "InActive",
    filter: "agTextColumnFilter",
    filterParams: { clearButton: true },
    headerTooltip: "InActive",
    sortable: true,
    tooltipValueGetter: params => {
      return params.value;
    },
    cellRenderer: "wfxCheckboxRenderer",
    suppressKeyboardEvent: function(params: any) {
      params.data.Edited = true;
    }
  }
];

export const premimumDef= [
  {
    headerName: "",
    width: 40,
    field: "Name",
    lockPosition: true,
    suppressSizeToFit: true,
    resizable: false,
    cellClass: "rowDragClass",
    filter: false,
    rowDrag: true
  },
  {
    headerName: "Rendered Value",
    field: "imgUrl",
    width: 100,
    cellRenderer: function(params) {
      // put the value in bold
      // return '<b>' + params.value + '</b>';
      return `<div style="border-radius: 50%; height: 100%; width: 100%;">
      <img style="height: 100%; width: 100%;" src="` + params.value + `"/>
      </div>`;
  }
  },
  {
    headerName: 'Full Name', width: 100, field: 'Name', filter: 'agTextColumnFilter',
    sortable: true, filterParams: { clearButton: true }, columnGroupShow: 'closed',
    headerTooltip: 'Full Name',
    tooltipValueGetter: (params) => {
      return params.value;
    },
    editable: true,
    cellEditor: 'wfxTextBoxEditor',
    cellEditorParams: () => {
      return {
        itemAttr: {
          mandatory: true,
        }
      };
    },
  },
  {
    headerName: "InActive",
    editable: false,
    field: "InActive",
    filter: "agTextColumnFilter",
    filterParams: { clearButton: true },
    headerTooltip: "InActive",
    sortable: true,
    tooltipValueGetter: params => {
      return params.value;
    },
    cellRenderer: "wfxCheckboxRenderer",
    suppressKeyboardEvent: function(params: any) {
      params.data.Edited = true;
    }
  },
  {
    headerName: "Employee Address",
    editable: true,
    field: "EmployeeAddress",
    filter: "agTextColumnFilter",
    headerTooltip: "Employee Address",
    sortable: true,
    tooltipValueGetter: params => {
      return params.value;
    },
    cellEditor: "wfxTextAreaEditor", // agLargeTextCellEditor
    cellEditorParams: () => {
      return {
        itemAttr: {
          mandatory: true
        }
      };
    }
  },
  // {
  //   headerName: "Link Button",
  //   editable: false,
  //   field: "",
  //   filter: "",
  //   colId: "lnk",
  //   filterParams: { clearButton: true },
  //   headerTooltip: "Link Button",
  //   sortable: true,
  //   tooltipValueGetter: params => {
  //     return params.value;
  //   },
  //   cellRenderer: "wfxLinkButtonRenderer"
  // },
  {
    headerName: "Created By",
    editable: false,
    field: "CreatedBy",
    filter: "agTextColumnFilter",
    filterParams: { clearButton: true },
    headerTooltip: "Created By",
    sortable: true,
    tooltipValueGetter: params => {
      return params.value;
    }
  },
  {
    headerName: "Created On",
    editable: false,
    field: "CreatedOn",
    filter: "agTextColumnFilter",
    filterParams: { clearButton: true },
    headerTooltip: "Created On",
    sortable: true,
    tooltipValueGetter: params => {
      return params.value;
    }
  }
];

export const simpleDef= [
  {
    headerName: "",
    width: 40,
    field: "Name",
    lockPosition: true,
    suppressSizeToFit: true,
    resizable: false,
    cellClass: "rowDragClass",
    filter: false,
    rowDrag: true
  },
  {
    headerName: "Rendered Value",
    field: "imgUrl",
    width: 100,
    cellRenderer: function(params) {
      // put the value in bold
      // return '<b>' + params.value + '</b>';
      return `<div style="border-radius: 50%; height: 100%; width: 100%;">
      <img style="height: 100%; width: 100%;" src="` + params.value + `"/>
      </div>`;
  }
  },
  {
    headerName: 'Full Name', width: 100, field: 'Name', filter: 'agTextColumnFilter',
    sortable: true, filterParams: { clearButton: true }, columnGroupShow: 'closed',
    headerTooltip: 'Full Name',
    tooltipValueGetter: (params) => {
      return params.value;
    },
    editable: true,
    cellEditor: 'wfxTextBoxEditor',
    cellEditorParams: () => {
      return {
        itemAttr: {
          mandatory: true,
        }
      };
    },
  },
  {
    headerName: "",
    width: 60,
    headerCheckboxSelection: true,
    headerCheckboxSelectionFilteredOnly: true,
    lockPosition: true,
    suppressSizeToFit: true,
    resizable: false,
    filter: false,
    checkboxSelection: (params: any) => {
      return true;
    },
    pinnedRowCellRenderer: "customPinnedRowRenderer",
    pinnedRowCellRendererParams: { style: { color: "blue" } }
  },
  {
    headerName: "City",
    editable: true,
    field: "City",
    filter: "agTextColumnFilter",
    filterParams: { clearButton: true },
    headerTooltip: "City",
    sortable: true,
    tooltipValueGetter: params => {
      return params.value;
    },
    cellEditor: "wfxSelectEditor"
  },
  {
    headerName: "Joining Date",
    editable: true,
    field: "JoiningDate",
    filter: "agDateColumnFilter",
    headerTooltip: "Joining Date",
    sortable: true,
    tooltipValueGetter: params => {
      return params.value;
    },
    cellEditor: "wfxDatePickerEditor",
    cellEditorParams: () => {
      return {
        itemAttr: {
          mandatory: true
        }
      };
    }
  },
  {
    headerName: "Telephone No",
    editable: true,
    field: "TelephoneNo",
    filter: "agNumberColumnFilter",
    filterParams: { clearButton: true },
    headerTooltip: "Telephone No",
    sortable: true,
    tooltipValueGetter: params => {
      return params.value;
    },
    cellEditor: "wfxTextBoxEditor",
    cellEditorParams: () => {
      return {
        itemAttr: {
          inputsubtype: "number",
          mandatory: true
        }
      };
    }
  },
  {
    headerName: "InActive",
    editable: false,
    field: "InActive",
    filter: "agTextColumnFilter",
    filterParams: { clearButton: true },
    headerTooltip: "InActive",
    sortable: true,
    tooltipValueGetter: params => {
      return params.value;
    },
    cellRenderer: "wfxCheckboxRenderer",
    suppressKeyboardEvent: function(params: any) {
      params.data.Edited = true;
    }
  },
  {
    headerName: "Employee Address",
    editable: true,
    field: "EmployeeAddress",
    filter: "agTextColumnFilter",
    headerTooltip: "Employee Address",
    sortable: true,
    tooltipValueGetter: params => {
      return params.value;
    },
    cellEditor: "wfxTextAreaEditor", // agLargeTextCellEditor
    cellEditorParams: () => {
      return {
        itemAttr: {
          mandatory: true
        }
      };
    }
  },
  // {
  //   headerName: "Link Button",
  //   editable: false,
  //   field: "",
  //   filter: "",
  //   colId: "lnk",
  //   filterParams: { clearButton: true },
  //   headerTooltip: "Link Button",
  //   sortable: true,
  //   tooltipValueGetter: params => {
  //     return params.value;
  //   },
  //   cellRenderer: "wfxLinkButtonRenderer"
  // },
  {
    headerName: "Created By",
    editable: false,
    field: "CreatedBy",
    filter: "agTextColumnFilter",
    filterParams: { clearButton: true },
    headerTooltip: "Created By",
    sortable: true,
    tooltipValueGetter: params => {
      return params.value;
    }
  },
  {
    headerName: "Created On",
    editable: false,
    field: "CreatedOn",
    filter: "agTextColumnFilter",
    filterParams: { clearButton: true },
    headerTooltip: "Created On",
    sortable: true,
    tooltipValueGetter: params => {
      return params.value;
    }
  }
];




export function getColumnDef(type) {
  console.log(type);
  switch (type) {
    case 'WFX Premimum':
      return premimumDef;
    case 'WFX Default':
      return defaultDef;
    default:
      return simpleDef;
  }
}





// export const columnDef =
// [
//   {
//     headerName: '', width: 40, field: 'Name', lockPosition: true, suppressSizeToFit: true, resizable: false,
//     cellClass: 'rowDragClass', filter: false,
//     rowDrag: true,
//     suppressKeyboardEvent: (params: any) => {
//       _this.cmf.stopSelectingRow(params);
//     },
//   },
//   {
//     headerName: '', width: 60, headerCheckboxSelection: true, headerCheckboxSelectionFilteredOnly: true,
//     lockPosition: true, suppressSizeToFit: true, resizable: false, filter: false,
//     checkboxSelection: (params: any) => {
//       return true;
//     },
//     pinnedRowCellRenderer: 'customPinnedRowRenderer',
//     pinnedRowCellRendererParams: { style: { color: 'blue' } }
//   },
//   {
//     headerName: 'Name', marryChildren: true,
//     children: [
//       {
//         headerName: 'Full Name', field: 'Name', filter: 'agTextColumnFilter',
//         sortable: true, filterParams: { clearButton: true }, columnGroupShow: 'closed',
//         headerTooltip: 'Full Name',
//         tooltipValueGetter: (params) => {
//           return params.value;
//         },
//         editable: true,
//         suppressKeyboardEvent: function (params: any) {
//           _this.cmf.stopSelectingRow(params);
//           _this.cmf.stopEditingGrid(params, _this.gridApi);
//         },
//         cellEditor: 'wfxTextBoxEditor',
//         cellEditorParams: () => {
//           return {
//             itemAttr: {
//               mandatory: true,
//             }
//           };
//         },
//       },
//       {
//         headerName: 'First Name', field: 'FirstName', filter: 'agTextColumnFilter',
//         sortable: true, filterParams: { clearButton: true }, columnGroupShow: 'open',
//         headerTooltip: 'First Name',
//         tooltipValueGetter: (params) => {
//           return params.value;
//         },
//         editable: true,
//         suppressKeyboardEvent: function (params: any) {
//           _this.cmf.stopSelectingRow(params);
//           _this.cmf.stopEditingGrid(params, _this.gridApi);
//         },
//         cellEditor: 'wfxTextBoxEditor',
//         cellEditorParams: () => {
//           return {
//             itemAttr: {
//               mandatory: true,
//             }
//           };
//         },
//       },
//       {
//         headerName: 'Last Name', field: 'LastName', filter: 'agTextColumnFilter',
//         sortable: true, filterParams: { clearButton: true }, columnGroupShow: 'open',
//         headerTooltip: 'Last Name',
//         tooltipValueGetter: (params) => {
//           return params.value;
//         },
//         editable: true,
//         suppressKeyboardEvent: function (params: any) {
//           _this.cmf.stopSelectingRow(params);
//           _this.cmf.stopEditingGrid(params, _this.gridApi);
//         },
//         cellEditor: 'wfxTextBoxEditor',
//         cellEditorParams: () => {
//           return {
//             itemAttr: {
//               mandatory: true,
//             }
//           };
//         },
//       },
//     ]
//   },
//   {
//     headerName: 'City', editable: true, field: 'City', filter: 'agTextColumnFilter',
//     filterParams: { clearButton: true }, headerTooltip: 'City', sortable: true,
//     tooltipValueGetter: (params) => {
//       return params.value;
//     },
//     cellEditor: 'wfxSelectEditor',
//     cellEditorParams: () => {
//       return {
//         itemAttr: {
//           mandatory: true,
//           values: _this.arrCityValues,
//           value: 'City',
//         }
//       };
//     },
//     suppressKeyboardEvent: function (params: any) {
//       _this.cmf.stopSelectingRow(params);
//       _this.cmf.stopEditingGrid(params, _this.gridApi);
//     },
//   },
//   {
//     headerName: 'Joining Date', editable: true, field: 'JoiningDate', filter: 'agDateColumnFilter',
//     headerTooltip: 'Joining Date', sortable: true,
//     tooltipValueGetter: (params) => {
//       return params.value;
//     },
//     filterParams: {
//       clearButton: true,
//       inRangeInclusive: true,
//       comparator: _this.cmf.myDateComparator
//     },
//     cellEditor: 'wfxDatePickerEditor',
//     cellEditorParams: () => {
//       return {
//         itemAttr: {
//           mandatory: true,
//         }
//       };
//     },
//     suppressKeyboardEvent: function (params: any) {
//       _this.cmf.stopSelectingRow(params);
//       _this.cmf.stopEditingGrid(params, _this.gridApi);
//     },
//   },
//   {
//     headerName: 'Telephone No', editable: true, field: 'TelephoneNo', filter: 'agNumberColumnFilter',
//     filterParams: { clearButton: true }, headerTooltip: 'Telephone No', sortable: true,
//     tooltipValueGetter: (params) => {
//       return params.value;
//     },
//     cellEditor: 'wfxTextBoxEditor',
//     cellEditorParams: () => {
//       return {
//         itemAttr: {
//           inputsubtype: 'number',
//           mandatory: true,
//         }
//       };
//     },
//     suppressKeyboardEvent: function (params: any) {
//       _this.cmf.stopSelectingRow(params);
//       _this.cmf.stopEditingGrid(params, _this.gridApi);
//     },
//   },
//   {
//     headerName: 'InActive', editable: false, field: 'InActive', filter: 'agTextColumnFilter',
//     filterParams: { clearButton: true }, headerTooltip: 'InActive', sortable: true,
//     tooltipValueGetter: (params) => {
//       return params.value;
//     },
//     cellRenderer: 'wfxCheckboxRenderer',
//     suppressKeyboardEvent: function (params: any) {
//       params.data.Edited = true;
//       if (Number(params.event.keyCode) === 32) {
//         params.node.setDataValue(params.colDef, params.data.InActive ? false : true);
//         _this.cmf.preventDefaultAndPropagation(params.event);
//       }
//     },
//   },
//   {
//     headerName: 'Employee Address', editable: true, field: 'EmployeeAddress', filter: 'agTextColumnFilter',
//     headerTooltip: 'Employee Address', sortable: true,
//     tooltipValueGetter: (params) => {
//       return params.value;
//     },
//     cellEditor: 'wfxTextAreaEditor', // agLargeTextCellEditor
//     cellEditorParams: () => {
//       return {
//         itemAttr: {
//           mandatory: true,
//         }
//       };
//     },
//     suppressKeyboardEvent: function (params: any) {
//       _this.cmf.stopSelectingRow(params);
//       _this.cmf.stopEditingGrid(params, _this.gridApi);
//     },
//   },
//   {
//     headerName: 'Link Button', editable: false, field: '', filter: '', colId: 'lnk',
//     filterParams: { clearButton: true }, headerTooltip: 'Link Button', sortable: true,
//     tooltipValueGetter: (params) => {
//       return params.value;
//     },
//     cellRenderer: 'wfxLinkButtonRenderer',
//     cellRendererParams: {
//       itemAttr: {
//         action: _this.linkButtonOnClick
//       }
//     },
//     suppressKeyboardEvent: function (params: any) {
//       if (Number(params.event.keyCode) === 32 && !params.editing) {
//         _this.linkButtonOnClick(params);
//       }
//       _this.cmf.stopSelectingRow(params);
//     },
//   },
//   {
//     headerName: 'Created By', editable: false, field: 'CreatedBy', filter: 'agTextColumnFilter',
//     filterParams: { clearButton: true }, headerTooltip: 'Created By', sortable: true,
//     tooltipValueGetter: (params) => {
//       return params.value;
//     },
//     suppressKeyboardEvent: function (params: any) {
//       _this.cmf.stopSelectingRow(params);
//     },
//   },
//   {
//     headerName: 'Created On', editable: false, field: 'CreatedOn', filter: 'agTextColumnFilter',
//     filterParams: { clearButton: true }, headerTooltip: 'Created On', sortable: true,
//     tooltipValueGetter: (params) => {
//       return params.value;
//     },
//     suppressKeyboardEvent: function (params: any) {
//       _this.cmf.stopSelectingRow(params);
//     },
//   }
// ];
