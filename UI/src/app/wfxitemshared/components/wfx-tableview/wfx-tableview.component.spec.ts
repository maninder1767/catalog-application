import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WfxTableviewComponent } from './wfx-tableview.component';

describe('WfxTableviewComponent', () => {
  let component: WfxTableviewComponent;
  let fixture: ComponentFixture<WfxTableviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WfxTableviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WfxTableviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
