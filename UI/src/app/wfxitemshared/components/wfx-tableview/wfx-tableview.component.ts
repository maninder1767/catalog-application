import { Item } from './../../../wfxplmitemlibrary/modals/item';
import { ItemLibraryService } from './../../services/itemlibrary.service';
import { FilterCriteria } from './../../modals/filter';
import { Observable, concat } from 'rxjs';
import 'rxjs/add/observable/of';
import { debug } from 'util';
import { ItemLibraryDataService } from './../../services/itemdata.service';
import { ItemLibraryGlobals, RightSelectedPanel } from './../../../wfxplmitemlibrary/itemLibrary.global';
import { WfxShowMessageservice } from './../../../wfxlibrary/wfxsharedservices/wfxshowmessage.service';
import { WfxGlobal } from './../../../wfxlibrary/wfxsharedscript/wfxglobal';
import { WfxcommonService } from './../../../wfxlibrary/wfxsharedservices/wfxcommon.service';
import { WfxCommonFunctions } from './../../../wfxlibrary/wfxsharedfunction/wfxcommonfunction';
import {
  Component, OnInit, OnDestroy, isDevMode, Input, OnChanges, HostListener,
  AfterViewInit, SimpleChange, SimpleChanges, ViewChild, AfterViewChecked
} from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { IDatasource, IGetRowsParams, IServerSideGetRowsRequest, IServerSideGetRowsParams, IServerSideDatasource } from 'ag-grid-community';
import { SortParams } from '../../modals/filter';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { AgGridAngular } from 'ag-grid-angular';
import { WfxBottompanelService } from '../../../wfxplmitemlibrary/components/wfx-bottompanel/wfx-bottompanel.service';
declare var $: any;
@Component({
  selector: 'app-wfx-tableview',
  templateUrl: './wfx-tableview.component.html',
  styleUrls: ['./wfx-tableview.component.css']
})
export class WfxTableviewComponent implements OnInit, OnChanges, AfterViewInit, AfterViewChecked {
  @Input() optionType: any;
  gridApi;
  gridColumnApi;
  columnDefs;
  defaultColDef;
  rowModelType;
  cacheBlockSize;
  maxBlocksInCache;
  rowData = [];
  viewInnerHeight = '';
  gridOptions;
  rowSelection = 'multiple';
  isViewChange = false;
  pageIndex = 0;
  pageSize = 100;
  params;
  tmpDef;
  selectAllItem = false;
  overlayNoRowsTemplate;
  floatingFilter = false;
  sideBar = true;
  componentObject;
  isSpinnerLoading = true;
  dataSource: IServerSideDatasource = {
    getRows: (params: IServerSideGetRowsParams) => {
      const filterModel = params['filterModel'];
      const arrFilter: FilterCriteria[] = [];
      const sortModel: [] = params['sortModel'];
      const arrSortModel: SortParams[] = [];
      sortModel.forEach((sort: any) => {
        arrSortModel.push({ fieldId: sort.colId, sortDirection: sort.sort, sortOrder: 0 });
      });
      // console.log(filterModel, this.tmpDef);
      this.itemGlobal.tableViewFilterModel = filterModel;
      if (Object.keys(filterModel).length > 0) {
        for (const filter in filterModel) {
          if (filter !== '') {
            this.tmpDef.forEach((def: any) => {
              if (def.field === filter) {
                // console.log(def);
                // arrFilter.push({
                //   fieldId: filter,
                //   caption: '',
                //   controlType: '',
                //   operator: '',
                //   fieldType: def.extraFieldType,
                //   filterData: [
                //     {
                //       values: [
                //         {
                //           value: filterModel[filter].filter
                //         }
                //       ],
                //       filterType: filterModel[filter].type
                //     }
                //   ]
                // });

                if (Object.keys(filterModel[filter]).length > 3) {
                  const arrMulitFilter = [];
                  const operator = filterModel[filter].operator;
                  Object.keys(filterModel[filter]).forEach(o => {
                    if (o.startsWith('condition')) {
                      arrMulitFilter.push({
                        values: [{value: filterModel[filter][o].filter}],
                        filterType: filterModel[filter][o].type
                      });
                    }
                  });
                  arrFilter.push({
                    fieldId: filter,
                    caption: '',
                    controlType: '',
                    operator: operator,
                    fieldType: def.extraFieldType,
                    filterData: [
                      ...arrMulitFilter
                    ]
                  });
                } else {
                  arrFilter.push({
                    fieldId: filter,
                    caption: '',
                    controlType: '',
                    operator: '',
                    fieldType: def.extraFieldType,
                    filterData: [
                      {
                        values: [
                          {
                            value: filterModel[filter].filter
                          }
                        ],
                        filterType: filterModel[filter].type
                      }
                    ]
                  });
                }
              }
            });
          }
        }
      }
      this.itemGlobal.arrFilterCriteria = arrFilter;
      this.pageIndex = this.pageIndex + 1;
      this.itemGlobal.commonFilter.pagingParams = { PageIndex: this.pageIndex, PageSize: 100 };
      this.itemGlobal.commonFilter.sortparams = arrSortModel;
      this.itemDataService.getSelectedTableViewData().pipe(map((row: any) => {
        console.log(row);
        // this.itemGlobal.arrTableRowData = this.itemGlobal.arrTableRowData.concat(row.data.item);
        // this.itemGlobal.arrTableRowData = [this.itemGlobal.arrTableRowData, ...row.data.item];
        // this.rowData.push(row.data.item);
        let isLoadingData = true;
        if (this.pageIndex > 1 && (row.data.item.length === 0)) {
          isLoadingData = false;
        } else {
          this.itemGlobal.arrTableRowData = row.data.item;
          this.itemGlobal.recordCounttableview += row.data.item.length;
        }
        // console.log(this.itemGlobal.recordCounttableview);
        let pageSize = 0;
        this.gridApi.hideOverlay();
        // console.log(this.itemGlobal.arrTableRowData);
        if (isLoadingData) {
          if (row.data.item.length === 0) {
            this.gridApi.showNoRowsOverlay();
            pageSize = 0;
          } else if (this.itemGlobal.arrTableRowData.length === 100) {
            pageSize = (this.pageIndex * row.data.item.length) + 1;
          } else {
            pageSize = +row.data.count;
          }
        } else {
          pageSize = +row.data.count;
        }
        this.itemGlobal.recordCount = +row.data.count;
        params.successCallback(
          this.itemGlobal.arrTableRowData,
          pageSize
        );
        this.isSpinnerLoading = false;
        if (this.selectAllItem === true) {
          // this.gridOptions.api.forEachNode(function(singlerow) {
          //   singlerow.setSelected(true);
          // });
          this.selectAllItem = false;
          this.bottamService.setSelectedItemIds({
            itemIds: this.itemGlobal.arrSelectedItemId,
            allItems: this.selectAllItem
          });
        }
      })).subscribe();
    }
  };

  @HostListener('window:resize') onResize() {
    this.viewInnerHeight = (window.innerHeight - $('#tblItem').offset().top).toString();
  }
  constructor(private http: HttpClient, private cmf: WfxCommonFunctions, public commonsvc: WfxcommonService, private router: Router,
    private global: WfxGlobal, private msgsvc: WfxShowMessageservice,
    public itemGlobal: ItemLibraryGlobals, private itemDataService: ItemLibraryDataService,
    private bottamService: WfxBottompanelService, private itemLibraryService: ItemLibraryService) {
    this.defaultColDef = {
      width: 120,
      resizable: true,
      filter: true
    };
    this.rowModelType = 'infinite';
  }

  tmpMethod = () => {
    console.log('temp Method');
  }

  ngOnInit() {
    this.componentObject = this;
    this.itemGlobal.recordCounttableview = 0;
    this.overlayNoRowsTemplate =
      `<span style=\"padding: 10px; border: 1px solid #444;\">
      No Record Found
      </span>`;
    this.bottamService.selectAllItemsSubsciption
      .subscribe(message => {
        this.selectAllRows(message.allItems);
      });

    this.itemLibraryService.makeInactiveFromBottomPanelSubscription.subscribe(isActive => {
      this.pageIndex = 0;
      this.selectAllItem = false;
      this.itemGlobal.arrSelectedItemId = [];
      this.gridApi.setDatasource(this.dataSource);
    });
  }

  ngOnChanges(change: SimpleChanges) {
    this.pageIndex = 0;
    this.itemGlobal.recordCounttableview = 0;
    this.selectAllItem = false;
    this.itemGlobal.arrSelectedItemId = [];
    const tableDef = this.itemDataService.getSelectedTableViewTableDefinition(this.itemGlobal.tableSelectedView.Code)
      .pipe(map((colField: any) => {
         console.log(colField);
        //this.tmpDef = [...colField.data];        
        const colDef: any[] = [];
        colDef.push({
          headerName: '', width: 40,
          lockPosition: true, suppressSizeToFit: true, resizable: false, filter: false,
          checkboxSelection: () => {
            return true;
          },
        });
       // colField.data.forEach((val, index) => {
          for(var colname in colField.data[0])
        {
          console.log(colname);
         // console.log(val.fieldId);
          const filterType = 'agTextColumnFilter';
          colDef.push({
            headerName: colname//val.caption
            , field: colname,//val.fieldId,
            lockPosition: false, suppressSizeToFit: true, resizable: true, sortable: true,
            colId: colname, //val.fieldId
             filter: filterType, rowDrag: false,
            cellRenderer: function (params) {
              if (params.colDef.field ==='PicFileName') {
                return '<img style="margin-left: 50%; width:20px; height:20px; border-radius:50%;" src="' + params.value + '">';//http://192.168.2.167/Company/10006/Pictures///http://192.168.2.167/Company/10006/Pictures/10006_17669831_thumb.jpg' + params.value + '
              } else if (params.value !== undefined) {
                return params.value;
              }
            }
            // ,
            // cellRendererParams: {
            //   itemAttr: {
            //     action: _this.linkButtonOnClick
            //   }
            // }
          });
        }
       // });
       this.tmpDef = [...colDef];  
        this.columnDefs = colDef;
        // this.itemGlobal.arrTableColumnDefinition = colDef;
        // console.log(this.itemGlobal.arrTableColumnDefinition);
      })).subscribe();
    if (this.gridApi) {
      this.gridApi.setDatasource(this.dataSource);
    }

  }

  ngAfterViewInit() {
    // console.log('AGGRID', this.agGrid);
    // console.log('Checked');
    setTimeout(() => {
      this.viewInnerHeight = (window.innerHeight - $('#tblItem').offset().top).toString();
    }, 1);
  }

  ngAfterViewChecked() {
    // console.log('Checked');
  }

  onGridReady(params) {
    this.params = params;
    this.gridApi = params.api;
    this.gridOptions = params;
    this.gridColumnApi = params.columnApi;
    this.gridApi.setFilterModel(this.itemGlobal.tableViewFilterModel);
    // this.gridApi.setFilterModel({
    //   article_code: {
    //     filter: '473',
    //     filterType: 'text',
    //     type: 'contains'
    //   },
    //   article_name: {
    //     filter: 'neo',
    //     filterType: 'text',
    //     type: 'contains'
    //   }
    // });
    // console.log(this.gridColumnApi);
    // this.gridColumnApi.resetColumnState();

    this.gridApi.setDatasource(this.dataSource);
  }

  OnSortChanged($event) {
    this.itemGlobal.arrSelectedItemId = [];
    this.selectAllItem = false;
    this.pageIndex = 0;
    this.itemGlobal.recordCounttableview = 0;
  }

  myGrid_getContextMenuItems(params, _this) {
    console.log(params, _this);
  }

  OnFilterModified($event) {
    this.itemGlobal.arrSelectedItemId = [];
    this.selectAllItem = false;
    this.pageIndex = 0;
    this.itemGlobal.recordCounttableview = 0;

  }
  onSelectionChanged(Item) {
    if (this.itemGlobal.arrSelectedItemId.length > 0) {
      this.itemGlobal.activePanel = false;
      this.itemLibraryService.invokeshowSelectedRightPanel(true);
      this.itemGlobal.currentRightSelectedPanel =  RightSelectedPanel.CommentDetail;
    }
    const selectedRows = this.gridApi.getSelectedRows();
    const selectedIds = [];
    this.itemGlobal.tableViewRowID = 0;
    selectedRows.forEach(function (selectedrow) {
      selectedIds.push(selectedrow.ArticleCode.toString());//article_id
    });
    this.itemGlobal.arrSelectedItemId = selectedIds;
    if (this.itemGlobal.arrSelectedItemId.length === 1) {
      this.itemGlobal.tableViewRowID = selectedIds[0];
    }
    this.bottamService.setSelectedItemIds({
      itemIds: selectedIds,
      allItems: this.selectAllItem
    });
  }
  /* selectAllRows method is called by bottom service when we set checbox true on bottom panel*/
  selectAllRows(isAllItemSelect) {
    this.selectAllItem = isAllItemSelect;
    // if (isAllItemSelect === true) {
    //   this.gridApi.api.selectAll();
    // } else {
    //   this.gridApi.api.deselectAll();
    // }
    this.gridOptions.api.forEachNode(function (singlerow) {
      singlerow.setSelected(isAllItemSelect);
    });
    /* When we select rows then onSelectionChanged event is fired autometically for each row  */
  }

  @HostListener('window:keydown', ['$event'])
  onkeydown(event) {
    if (event.key === 'Escape') {
      this.selectAllItem = false;
      this.gridOptions.api.forEachNode(function (singlerow) {
        singlerow.setSelected(false);
      });
    }
    return true;
  }

  @HostListener('window:keydown.control.a', ['$event'])
  keyEvent(event: KeyboardEvent) {
    this.selectAllItem = true;
    this.gridOptions.api.forEachNode(function (singlerow) {
      singlerow.setSelected(true);
    });
  }
}
