import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WfxQuicksearchComponent } from './wfx-quick-search.component';

describe('WfxQuicksearchComponent', () => {
  let component: WfxQuicksearchComponent;
  let fixture: ComponentFixture<WfxQuicksearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WfxQuicksearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WfxQuicksearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
