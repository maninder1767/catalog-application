/*
  * Created By : Urvashi Sharma
  * Created On : 9/Aug/2019
  * Purpose : This component is for quick search and saved search
  * Last Modified Date : 9/Aug/2019
  * Last Modified By : Urvashi Sharma
  */
import { ItemLibraryGlobals, RightSelectedPanel } from './../../../wfxplmitemlibrary/itemLibrary.global';
import { Component, OnInit, ViewChild, ElementRef, OnChanges, HostListener, AfterViewInit } from '@angular/core';
import { ItemLibraryDataService } from '../../services/itemdata.service';
import { ItemLibraryService } from '../../services/itemlibrary.service';

@Component({
  selector: 'app-wfx-quick-search',
  templateUrl: './wfx-quick-search.component.html',
  styleUrls: ['./wfx-quick-search.component.css']
})

export class WfxQuicksearchComponent implements OnInit, AfterViewInit {

  filterName;
  placeholder;
  @ViewChild('search', { static: true, read: ElementRef }) search: any;

  constructor(private eRef: ElementRef, private itemLibraryDataService: ItemLibraryDataService,
    private itemLibraryService: ItemLibraryService, private itemGlobals: ItemLibraryGlobals) { }

  ngOnInit() {
    this.itemLibraryDataService.getQuickSearchPlaceholder()
      .subscribe((message: any) => {
        this.placeholder = 'Search by ' + message.data;
      });
  }

  ngAfterViewInit() {
    if (this.itemGlobals.quickSearchFilterText && this.itemGlobals.quickSearchFilterText !== '') {
      this.search.nativeElement.value = this.itemGlobals.quickSearchFilterText;
      if (this.search && this.search.nativeElement.value !== '') {
        this.search.nativeElement.classList.add('active-search');
      }
    }
  }

  @HostListener('document:click', ['$event'])
  clickout(event) {
    if (this.eRef.nativeElement.contains(event.target)) {
    } else {
      if (this.search && this.search.nativeElement.value === '') {
        this.search.nativeElement.classList.remove('active-search');
      } else {
        this.search.nativeElement.classList.add('active-search');
      }
    }
  }

  /*** Event Section Starts */
  /**
 * Created By : Urvashi Sharma
 * Created On : 9/Aug/2019
 * Purpose : This event is for hide and show the search div when mouse over on search icon.
 */
  onMouseOverSearch() {
    this.search.nativeElement.focus();
  }

  /**
  * Created By : Urvashi Sharma
  * Created On : 9/Aug/2019
  * Purpose : This event is for hide and show the search div when mouse leave on search icon.
  */
  onMouseLeaveSearch() {
    if (this.search && this.search.nativeElement.value === '') {
      this.search.nativeElement.classList.remove('active-search');
    }
  }
  /**
  * Created By :  Urvashi
  * Created On :  9/Aug/2019
  * Purpose : This event is call when any text search in search textbox
  */
  onTextSearch($event): void {
    if (this.itemGlobals.currentRightSelectedPanel !== RightSelectedPanel.Filter) {
      this.itemGlobals.activePanel = false;
    }
    let value = $event.target.value;
    if ($event.target.value === undefined) {
      value = '';
      this.search.nativeElement.classList.remove('active-search');
    } else {
      this.search.nativeElement.classList.add('active-search');
    }
    this.itemLibraryDataService.search(value);
    this.itemLibraryService.invokeshowSelectedRightPanel(true);

  }
  /*** Event Section Ends */
}
