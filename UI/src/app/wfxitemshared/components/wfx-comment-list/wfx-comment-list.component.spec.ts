import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WfxCommentListComponent } from './wfx-comment-list.component';

describe('WfxCommentListComponent', () => {
  let component: WfxCommentListComponent;
  let fixture: ComponentFixture<WfxCommentListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WfxCommentListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WfxCommentListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
