import { ItemLibraryGlobals } from './../../../wfxplmitemlibrary/itemLibrary.global';
import { ItemLibraryDataService } from './../../services/itemdata.service';
import { ItemLibraryService } from './../../services/itemlibrary.service';
import { Comment, CommentResultModel } from './../../../wfxplmitemlibrary/modals/comment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { WfxTextareaComponent } from './../../../wfxlibrary/wfxcomponent-ts/wfxtextarea';
import { WfxLikeComponent } from './../wfx-like/wfx-like.component';
import { Component, OnInit, Inject, EventEmitter, ViewChild, ElementRef,
  ChangeDetectorRef, AfterViewInit, Output, Input, OnChanges,HostListener, OnDestroy } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { WfxComponentAttributes } from '../../../wfxlibrary/wfxsharedmodels/wfxcommonmodel';
import { RichSelectCellEditor } from 'ag-grid-enterprise';
import { iif, Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';

declare var $: any;

@Component({
  selector: 'app-comment-list',
  templateUrl: './wfx-comment-list.component.html',
  styleUrls: ['./wfx-comment-list.component.css'],
  providers: []
})

export class WfxCommentListComponent implements OnInit, AfterViewInit, OnChanges, OnDestroy {
  public txtReplyCommentEventEmitter = new EventEmitter<boolean>();
  @ViewChild('commentContainer', { static: true }) commentContainer: ElementRef;
  @ViewChild('txtReplyComment', { static: false }) txtReplyComment: ElementRef;
  @ViewChild('likeRef', { static: false }) likeComponent: WfxLikeComponent;
  @ViewChild('textarea', { static: false }) TextareaComponent: WfxTextareaComponent;


  likeJsonDef;
  likeJsonData;
  commentValue;
  commentData: Comment[];
  eventValue: any;
  wfxLinkButtonMoreJsonDef;
  wfxLinkButtonLessJsonDef;
  viewInnerHeight: number;
  textareavalue;

    /** Subject that emits when the component has been destroyed. */
    protected onDestroy = new Subject<void>();


  @Input() jsonDef: {
    attribute?: WfxComponentAttributes,
    placeholder?: string;
  };

  @Input() jsonData: {
    ID: string;
  };

  defaultValue =
    {
      width: '25px',
      height: '420px',
      visible: true,
      tooltip: '',
      color: '',
      placeholder: 'Write a comment',
      disableButton: false
    };

  avatarDef = {
    attribute: {
      height: '40px',
      width: '40px',
      tooltip: {
        tooltipvalue: '',
        tooltipvaluetype: '',
        tooltipposition: 'below'
      }
    },
    top: '0px'
  };



  textAreaJsonDef =
    {
      readonly: false, mandatory: false, caption: 'comment', disable: false,
      value: '', valuetype: 'dbfield',
      tooltip: { tooltipvalue: '', tooltipvaluetype: '', tooltipposition: 'below' },
      itemid: 'txtarea', addclearicon: true, floatlabel: 'never'
    };

    @HostListener('window:resize') onResize() {
      this.viewInnerHeight = (window.innerHeight - $('#commentList').offset().top);
      this.viewInnerHeight = this.viewInnerHeight + (-157);
      //this.itemJsonDef[0].height = this.viewInnerHeight + (- 45) + 'px';
    }
  constructor(public dialogRef: MatDialogRef<WfxCommentListComponent>,
    @Inject(MAT_DIALOG_DATA) public option: any,
    private cd: ChangeDetectorRef,
     private itemLibraryService: ItemLibraryService, private itemLibraryDataService: ItemLibraryDataService,
  ) {

  }

  ngOnInit() {
    this.textareavalue = true;
    this.viewInnerHeight = (window.innerHeight - $('#commentList').offset().top);
    this.viewInnerHeight = this.viewInnerHeight + (-157);
    this.likeJsonDef = {
      dailog: {
        attribute: {
          width: '278px',
          height: '140px',
          color: '',
        },
        image: {
          attribute: {
            height: '20px',
            width: '20px',
          },
        }

      }
    };

    this.wfxLinkButtonMoreJsonDef = {
      readonly: false, mandatory: false, caption: 'ReadMore', disable: false,
      value: 'ReadMore', valuetype: 'dbfield',
      tooltip: { tooltipvalue: 'ReadMore', tooltipvaluetype: '', tooltipposition: 'below' },
    };

    this.wfxLinkButtonLessJsonDef = {
      readonly: false, mandatory: false, caption: 'ReadLess', disable: false,
      value: 'ReadLess', valuetype: 'dbfield',
      tooltip: { tooltipvalue: 'ReadLess', tooltipvaluetype: '', tooltipposition: 'below' },
    };

  }

  ngOnDestroy() {
    this.onDestroy.next();
    this.onDestroy.complete();
  }

  getLikeJson(comment) {
    if (comment && comment.hasOwnProperty('count')) {
      return {
        likeCount: comment.count,
        isLike: comment.isLiked
      };
    }else {
      return {
        likeCount: 0,
        isLike: false,
      };
    }

  }

  
 /**
* Created By :Priya
* Created On :  12/sep/2019
* Purpose : This method is call while getting comments data  from database
* @param {any} data
*/



  onCommentListData(id) {
    this.itemLibraryDataService.getComment(id)
    .pipe(take(1), takeUntil(this.onDestroy)).subscribe((result: CommentResultModel) => {
      console.log(result)
      if (!result || result.message === 'No Record Found') {
        this.commentData = [];
      }else {
        for (const element of result.data[0].comment) {
          //if (element.comment.length > 35) { element['ReadMore'] = 1; }
        }
        this.commentData = result.data[0].comment;
      }
    });

  }


  /*** Method Section Ends */

  ngOnChanges() {
    this.onCommentListData(this.jsonData.ID);
  }

  /**
  * Created By :Priya
  * Created On :  12/sep/2019
  * Purpose : This method is call while getting avatar data
  * @param {any} user
  */

  avatarData(user) {
    return {
      imageUrl: user.createdBy.userImageURL==undefined?user[0].createdBy.userImageURL:user.createdBy.userImageURL,
      profileName: user.createdBy.fullName==undefined?user.createdBy[0].fullName:user.createdBy.fullName
    };
  }

  /*** Method Section Ends */


  ngAfterViewInit() {
    setTimeout(() => {
      this.viewInnerHeight = (window.innerHeight - $('#commentList').offset().top);
      this.viewInnerHeight = this.viewInnerHeight + (-157);
      }, 1);
    this.cd.detectChanges();
  }


  onReadMoreClick(comment: Comment) {
    comment.ReadMore = 0;
      // const commentText = document.getElementById('comment_text-id_' + comment.commentID.toString());
      // $(commentText).addClass('comment_line_ellipsis');
    //return false;
  }

  onReadLessClick(comment: Comment) {
    comment.ReadMore = 1;
    //      const commentText = document.getElementById('comment_text-id_' + comment.commentID.toString());
    //   $(commentText).removeClass('comment_line_ellipsis');
    // return false;
  }





  /**
* Created By :Priya
* Created On :  09/sep/2019
* Purpose : This event is called for emiting comment data
* @param {any} comment*/


  onTextValue($event) {
    if($event.key || $event.target.value == ' '){
      this.textareavalue = false;
    }
    else{
      this.textareavalue = true;
    }
    this.eventValue = $event;
    if (this.commentValue !== undefined && this.textAreaJsonDef.value =='') {
      $event.target.value = '';
    }
  }



   onSendClick($event, txtcomment, CommentID) {
    const CommentData = new Comment();
    CommentData.id = this.jsonData.ID;
    CommentData.comment = txtcomment;
    CommentData.parentCommentID = CommentID;
    this.itemLibraryDataService.postComment(CommentData).pipe(take(1), takeUntil(this.onDestroy)).subscribe(res => {
      this.onCommentListData(this.jsonData.ID);
    },
    );
    this.commentValue = txtcomment;
    this.textAreaJsonDef.value = '';
    this.onTextValue(this.eventValue);
    setTimeout(() => {
      this.itemLibraryDataService.getItem(this.jsonData.ID).pipe(take(1), takeUntil(this.onDestroy)).subscribe((result: any) =>{
        this.itemLibraryService.invokeshowComment({showCommentIcon: true, commentCount: result.data.item[0].commentCount});
      });
 }, 500);

 this.textareavalue = true;
  }



  /*** Event Section Ends */

  /**
* Created By :Priya
* Created On :  09/sep/2019
* Purpose : This event is called for send comments
* @param {any} comment
*/



  /*** Event Section Ends */

  onLikeClick(likeRef, comment, commentReplyID) {
    const CommentData = new Comment();
    if (commentReplyID === undefined) {
      CommentData.id = this.jsonData.ID;
      CommentData.commentID = comment.commentID;
    }else {
      CommentData.id = this.jsonData.ID;
      CommentData.commentID = commentReplyID;
      CommentData.ReplyID = comment.commentID;
    }
    this.itemLibraryDataService.updateCommentLike(CommentData).pipe(take(1), takeUntil(this.onDestroy)).subscribe();
    comment.like = {
      count: likeRef.jsonData.likeCount,
      isLiked: likeRef.jsonData.isLike
    };
  }

  onShowLikeDetails($event, commentID) {
    this.itemLibraryDataService.getCommentLikeDetail(this.jsonData.ID, commentID)
    .pipe(take(1), takeUntil(this.onDestroy)).subscribe((val: any) => {
      this.likeComponent.onUserData(val.data);
    });
  }


  /*** Property Section Starts */

  get height() {
    if (this.jsonDef && this.jsonDef.attribute && this.jsonDef.attribute.height) {
      return this.jsonDef.attribute.height;
    } else {
      return this.defaultValue.height;
    }
  }

  get width() {
    if (this.jsonDef && this.jsonDef.attribute && this.jsonDef.attribute.width) {
      return this.jsonDef.attribute.width;
    } else {
      return this.defaultValue.width;
    }
  }

  get tooltip() {
    if (this.jsonDef && this.jsonDef.attribute && this.jsonDef.attribute.tooltip) {
      return this.jsonDef.attribute.tooltip;
    } else {
      return this.defaultValue.tooltip;
    }
  }

  get visible() {
    if (this.jsonDef && this.jsonDef.attribute && this.jsonDef.attribute.visible) {
      return this.jsonDef.attribute.visible;
    } else {
      return this.defaultValue.visible;
    }
  }


  get placeholder() {
    if (this.jsonDef && this.jsonDef.attribute && this.jsonDef.placeholder) {
      return this.jsonDef.placeholder;
    } else {
      return this.defaultValue.placeholder;
    }
  }

  get disable() {
    if (this.jsonDef && this.jsonDef.attribute && this.jsonDef.attribute.disable) {
      return this.jsonDef.attribute.disable;
    } else {
      return this.defaultValue.disableButton;
    }
  }

  /*** Property Section Ends */



}
