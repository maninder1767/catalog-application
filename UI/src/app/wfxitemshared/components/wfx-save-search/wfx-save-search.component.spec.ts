import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WfxSaveSearchComponent } from './wfx-save-search.component';

describe('WfxSaveSearchComponent', () => {
  let component: WfxSaveSearchComponent;
  let fixture: ComponentFixture<WfxSaveSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WfxSaveSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WfxSaveSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
