import { WfxShowMessageservice } from './../../../wfxlibrary/wfxsharedservices/wfxshowmessage.service';
import { WfxCommonFunctions } from './../../../wfxlibrary/wfxsharedfunction/wfxcommonfunction';
import { MatDialog } from '@angular/material';
import { ItemLibraryDataService } from './../../services/itemdata.service';
import { ItemLibraryGlobals } from './../../../wfxplmitemlibrary/itemLibrary.global';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ElementRef, ViewChild, ViewContainerRef, AfterViewInit } from '@angular/core';
import {
  FilterResultModel, Filter, FilterSubType, SavedSearchesDdlResult, SavedSearchesValue
  , SavedSearchesResult, FilterCriteria, SearchParams, FilterData, ApplyFilter
} from '../../modals/filter';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-wfx-save-search',
  templateUrl: './wfx-save-search.component.html',
  styleUrls: ['./wfx-save-search.component.css']
})
export class WfxSaveSearchComponent implements OnInit, AfterViewInit {

  constructor(private httpClient: HttpClient, private itemGlobal: ItemLibraryGlobals,
    private ItemLibraryDataService: ItemLibraryDataService, public dialog: MatDialog,
    private msgsvc: WfxShowMessageservice) { }
  wfxLabelJsonDef;
  wfxLabeljsonData;
  wfxTextJsonDef;
  wfxTextJsonData;
  wfxLabelValidationJsonDef;
  wfxLabelValidationjsonData;
  FilterCriteria: FilterCriteria[];
  SearchParams: SearchParams;
  errormsgtxt = false;
  applyFilter: ApplyFilter = new ApplyFilter({}, new SearchParams('', []), [], []);
  textValue;
  @ViewChild('search', { static: true, read: ElementRef }) input: ElementRef;
  // @ViewChild('search', { read: ElementRef, static: false }) public input;

  ngOnInit() {
    this.wfxLabelJsonDef = {
      readonly: false, mandatory: false, caption: 'Save Search', disable: false,
      value: 'Label', valuetype: 'dbfield',
      tooltip: { tooltipvalue: 'Save Search', tooltipvaluetype: 'dbfield', tooltipposition: 'below' },
      inputtype: 'label', itemid: 'txtcount'
    };

    this.wfxLabeljsonData = {
      Label: 'Save Search'
    };

    this.wfxLabelValidationJsonDef = {
      readonly: false, mandatory: false, caption: 'Save Search', disable: false,
      value: 'Label', valuetype: 'dbfield',
      tooltip: { tooltipvalue: '', tooltipvaluetype: 'dbfield', tooltipposition: 'below' },
      inputtype: 'label', itemid: 'lblvalidtn'
    };


    this.wfxTextJsonDef = {
      readonly: false, mandatory: true, caption: 'Name', disable: false,
      value: 'textBoxValue', valuetype: 'dbfield', required: 'true',
      tooltip: { tooltipvalue: 'Search', tooltipvaluetype: '', tooltipposition: 'below' },
      inputtype: 'textbox', itemid: 'txtsearch', floatlabel: 'never',
    };
    this.wfxTextJsonData = {
      textBoxValue: ''
    };

  }

  ngAfterViewInit() {
    console.log(this.input);
  }

  onTextSearch($event) {
    this.textValue = $event.target.value;

  }

  onSaveClick() {
    this.wfxTextJsonData.textBoxValue = this.wfxTextJsonData.textBoxValue.trim();
    if (!this.wfxTextJsonData.textBoxValue || this.wfxTextJsonData.textBoxValue === '') {
      this.input.nativeElement.childNodes[0][0].focus();
      return false;
    }


   // this.applyFilter.searchparams.filterCriteria = this.itemGlobal.arrFilterCriteria;
    this.applyFilter.searchparams.filterCriteria = this.itemGlobal.arrGalleryFilterCriteria;
    this.applyFilter.searchparams['filterName'] = this.textValue;
    this.ItemLibraryDataService.postSavedSearch(this.applyFilter.searchparams).subscribe((res: any) => {
      if (res.status === 'Success') {
        this.msgsvc.openSnackBar('save successfully', '', {
          duration: 2000,
        });
        this.dialog.closeAll();
      }
    }, errormsg => {
      if (errormsg.error.message === 'Search Name Already Exists') {
        this.errormsgtxt = true;
      }

    });
  }

  onClearClick() {
    this.dialog.closeAll();
  }

}
