import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WfxViewToolbarComponent } from './wfx-view-toolbar.component';

describe('WfxViewToolbarComponent', () => {
  let component: WfxViewToolbarComponent;
  let fixture: ComponentFixture<WfxViewToolbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WfxViewToolbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WfxViewToolbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
