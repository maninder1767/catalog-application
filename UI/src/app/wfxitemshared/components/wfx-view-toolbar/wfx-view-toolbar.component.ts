import { PageParam } from './../../../wfxlibrary/wfxsharedmodels/wfxqueryparam';
import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { ItemLibraryService } from '../../services/itemlibrary.service';
import { ItemFilter, ItemFilterType, ItemFilterSubType,
  FilterResultModel, Filter, FilterSubType, ApplyFilter, SearchParams, SortParams } from '../../modals/filter';
import { SortJsonDef } from '../wfx-sort/wfx-sort.component';
import { HttpClient } from '@angular/common/http';
import { SortResult, Sort } from '../../modals/sort';
import { ItemLibraryDataService } from '../../services/itemdata.service';
import { ItemLibraryGlobals, RightSelectedPanel } from '../../../../app/wfxplmitemlibrary/itemLibrary.global';
import { TableDropdownViewModel } from '../../modals/table';
import { WfxFilterService } from '../wfx-filter/wfx-filter.service';
import { Globals } from '../../../globals';
import { WfxComponentAttributes } from '../../../wfxlibrary/wfxsharedmodels/wfxcommonmodel';
import { ViewType } from '../../constants/enum';
import { take, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-wfx-view-toolbar',
  templateUrl: './wfx-view-toolbar.component.html',
  styleUrls: ['./wfx-view-toolbar.component.css']
})

export class WfxViewToolbarComponent implements OnInit, OnDestroy {
  constructor(private itemLibraryService: ItemLibraryService, private http: HttpClient,
    private itemLibraryDataService: ItemLibraryDataService,
    public itemGlobal: ItemLibraryGlobals, private wfxFilterService: WfxFilterService, private globals: Globals) { }

  @Input() filter: Filter = new Filter();
  @Input() selectedAssetSortType: string;
  @Output() advanceSearchClick = new EventEmitter();
  @Output() viewOptionClick = new EventEmitter<string>();
  @Output() viewDdlChange = new EventEmitter<string>();
  arrFilterSubType: FilterSubType[] = [];
  sortParams = new SortParams();
  arrAssetSortOptionData: any[] = [];
  viewoptionsJsonDef;
  viewoptionsjsondata;
  sortJsonData;
  defaultSortParams: SortParams;
  sortData: Sort[];
  isAdvanceSearchOpened = false;
  sortJsonDef = new SortJsonDef();
  filterChip: Filter = new Filter();
  appliedFilter = new ApplyFilter();
  componentObject: any;
  arrCoshSheetDefJsonData: any[] = [];
  // itemJsonDef: WfxComponentAttributes[] = [
  //   {
  //     itemid: 'ddlCostSheetDef', caption: '', valuetype: 'dbfield', value: 'CostSheetDefinitionID', valuetext: 'CostSheetDefinitionName',
  //     mandatory: false, addselect: false, width: '100px',  addsearchoptioninddl: false,
  //   },
  // ];
  itemJsonDef: WfxComponentAttributes[] = [
    {
      itemid: 'ddlCostSheetDef', caption: '', valuetype: 'dbfield',
      value: 'value', valuetext: 'text',
      mandatory: false, addselect: false, width: '100px'
    },
  ];
  // headerJosnData: any;
  headerJsonData = {
    value: 0,
    text: ''
  };
  wfxLabelJsonDef: WfxComponentAttributes = {
    readonly: false, mandatory: false, caption: 'Definition', disable: false,
    value: 'Label', valuetype: 'dbfield',
    tooltip: { tooltipvalue: '', tooltipvaluetype: 'dbfield' },
    inputtype: 'label', itemid: 'txtDefinition', classname: 'wfxlabeldiv'
  };

  wfxLabelJsonData = {
    Label: 'Definition'
  };
  protected onDestroy = new Subject<void>();

  ngOnInit() {
    
    this.itemLibraryDataService.getAdvanceSearchDefinitionList().subscribe((result: FilterResultModel) => {
      this.filter.arrFilterType = result.data;
      this.filter.arrFilterType.forEach(val => {
        val.arrFilterSubType = [];
        val.filterValue = '';
        val.dateFrom = '';
        val.dateTo = '';
      });
      
      this.itemLibraryService.changeFilter({
        filter: this.filter
      });
    });

    this.componentObject = this;
    this.defaultSortParams = this.itemGlobal.defaultSortParams;
    this.sortJsonData = {
      defaultSortParams: this.itemGlobal.defaultSortParams,
      sortOptions: this.sortData
    }
    // this.filter.isFilterApplied = false;
    this.getSortOptions();
    if (this.itemGlobal.currentView === ViewType.GALLERY) {
      this.viewoptionsJsonDef = {
        attribute: {
          visible: true,
          disable: false,
        },
        slider: {
          attribute: {
            visible: true,
            disable: false
          },
        },
      };
    } else {
      this.viewoptionsJsonDef = {
        attribute: {
          visible: true,
          disable: false,
        },
        slider: {
          attribute: {
            visible: false,
            disable: false
          },
        },
      };
    }
    // this.viewoptionsjsondata = {
    //   sliderDefaultvalue: this.itemGlobal.sliderRange,
    // };
    this.viewoptionsjsondata = {
      sliderDefaultvalue: this.itemGlobal.sliderRange,
      selectedItemView: this.itemGlobal.tableSelectedView.Code,
      selectedItemText : this.itemGlobal.tableSelectedView.Text,
      arrItemsViewOptionData: this.itemGlobal.arrtableDropdownViewData,
    };
    this.itemLibraryService.filterChipSubscription
      .subscribe(message => {
        this.filterChip = message.filterChip;
      });
    this.itemLibraryService.appliedFilterSubscription
      .subscribe(message => {
        this.appliedFilter = message.appliedFilter;
      });
      this.itemLibraryService.filter
      .subscribe(message => {
        this.filter = message.filter;
        // this.filterJsonData = {
        //   filter: this.filter,
        // };
     });
      if (this.itemGlobal.currentSelectedTab.Code === 'COSTING') {
        this.itemLibraryDataService.getRequest('Master/Data?objectType=ItemCosting&DataSource=COSTINGDEFINITION')
          .pipe(take(1), takeUntil(this.onDestroy))
          .subscribe((res: any) => {
              this.headerJsonData = {
                value: res.data[0].value,
                text: res.data[0].text
              };
              this.arrCoshSheetDefJsonData = res.data;
              this.itemGlobal.costingDefaultSelectedView = {
                value : this.headerJsonData.value,
                text: this.headerJsonData.text
              };
              this.itemLibraryService.invokeCostSheetDDlValue(true);
          });
        }
  }

  ngOnDestroy() {
    this.onDestroy.next();
    this.onDestroy.complete();
  }

  /*** Event Section Starts */
  OnClickThumbNailView() {
    console.log('Hello I am ThumbNail from Output Event');
    this.viewOptionClick.emit('galleryview');
    // this.selectedAssetView.emit('ThumbnailView');
  }

  OnClickListView() {
    console.log('Hello I am ListView from Output Event');
    // this.selectedAssetView.emit("ListView");
    this.viewOptionClick.emit('listview');
  }
  OnClickTableView() {
    this.itemLibraryDataService.getTableViewDropdownList().subscribe((res: any) => {
      const ddlTableViewData: TableDropdownViewModel[] = [];

      res.data.forEach(ddlType => {
        ddlTableViewData.push({ Text: ddlType.text, Code: ddlType.id });
      });
      this.itemGlobal.arrtableDropdownViewData = ddlTableViewData;
      if (this.itemGlobal.tableSelectedView.Code === '') {
        this.itemGlobal.defaultTableSelectedView = ddlTableViewData[0];
        this.itemGlobal.tableSelectedView = ddlTableViewData[0];
      }
      this.viewoptionsjsondata = {
        sliderDefaultvalue: this.itemGlobal.sliderRange,
        selectedItemView: this.itemGlobal.tableSelectedView.Code,
        selectedItemText: this.itemGlobal.tableSelectedView.Text,
        arrItemsViewOptionData: ddlTableViewData,
      };
      this.viewOptionClick.emit('tableview');
    });
    // this.selectedAssetView.emit('ListView');
  }
  OnSliderChange($event) {
    console.log('Hello I am Slider Change from Ourtput Event' + $event);
  }
  onViewTypeChange($event): void {
    console.log('Hello I am from on view type change' + $event);
    this.viewDdlChange.emit($event);
    /*this.assetSortTypeChange.emit($event);*/
    // this.damService.setSortParam({ SortParams: new SortParam($event) });
 }
  /*** Event Section Ends */


  /*** Event Section Starts */
  /**
 * Created By : MAnoj Udayan
 * Created On : 22/Sep/2019
 * Purpose : This event is for sort
 */

  getSortOptions(): void {
    this.itemLibraryDataService.getSortByList().subscribe((result: SortResult) => {
      this.sortData = result.data;
      this.sortJsonData = {
        defaultSortParams: this.defaultSortParams,
        sortOptions: this.sortData
      };
    });
  }
  onSortTypeChange(event): void {
    this.sortParams.fieldId = event.selectFieldId;
    this.sortParams.sortOrder = event.sortOrder;
    this.sortParams.sortDirection = event.sortDirection;
    this.appliedFilter.sortparams = [this.sortParams];
    this.itemGlobal.defaultSortParams = this.sortParams;
    this.sortJsonData = {
      defaultSortParams: this.itemGlobal.defaultSortParams,
      sortOptions: this.sortData
    }
    this.itemLibraryDataService.sortbyItemDropdownField([this.sortParams]);
  }

  openAdvanceSearch(): void {
    this.globals.isAdvancedSeachFilterClicked = true;
    if (this.filter.arrFilterType.length === 0) {
      // this.itemLibraryDataService.getAdvanceSearchDefinitionList().subscribe((result: FilterResultModel) => {
      //   this.filter.arrFilterType = result.data;
      //   this.filter.arrFilterType.forEach(val => {
      //     val.arrFilterSubType = [];
      //     val.filterValue = '';
      //     val.dateFrom = '';
      //     val.dateTo = '';
      //   });

      //   if (this.wfxFilterService.SavedfiterCriteriaData.length > 0) {
      //     this.wfxFilterService.rebindingSavedFilters(this.filter, this.wfxFilterService.SavedfiterCriteriaData).subscribe(filterData => {
      //       this.filter = filterData;
      //       // this.advanceSearchClick.emit(this.filter);
      //       this.itemGlobal.currentRightSelectedPanel =  RightSelectedPanel.Filter;
      //       this.itemGlobal.activePanel = true;
      //       this.itemLibraryService.invokeshowSelectedRightPanel(true);
      //       this.itemLibraryService.changeFilter({
      //         filter: this.filter
      //       });
      //     });
      //   } else {
      //     this.itemGlobal.currentRightSelectedPanel =  RightSelectedPanel.Filter;
      //     this.itemGlobal.activePanel = true;
      //     this.itemLibraryService.invokeshowSelectedRightPanel(true);
      //     this.itemLibraryService.changeFilter({
      //       filter: this.filter
      //     });
      //   }
      // });
    } else {
      if (this.itemGlobal.currentRightSelectedPanel ===  RightSelectedPanel.Filter) {
        this.itemGlobal.currentRightSelectedPanel = undefined;
        this.itemGlobal.activePanel = false;
        this.itemLibraryService.invokeshowSelectedRightPanel(false);
      } else {
        this.itemGlobal.currentRightSelectedPanel =  RightSelectedPanel.Filter;
        this.itemGlobal.activePanel = true;
        this.itemLibraryService.invokeshowSelectedRightPanel(true);
      }
    }
  }

  applyChipFilter(chipFilter: Filter, filter: Filter) {
    this.filter.isFilterApplied = true;
    chipFilter.arrFilterType.forEach(ft => {
      const index = this.filter.arrFilterType.findIndex(x => x.fieldId === ft.fieldId);
      if (index > 0) {
        this.filter.arrFilterType[index].arrFilterSubType = ft.arrFilterSubType;
      }
    });
    this.advanceSearchClick.emit(this.filter);
    this.itemLibraryService.changeFilter({
      filter: this.filter
    });
  }
  /*** Event Section Ends */
}
