import { ItemLibraryGlobals, RightSelectedPanel } from './../../../wfxplmitemlibrary/itemLibrary.global';

import { Component, OnInit, Input, EventEmitter, Output, OnChanges, AfterContentChecked, AfterViewChecked } from '@angular/core';
import { WfxComponentAttributes } from '../../../wfxlibrary/wfxsharedmodels/wfxcommonmodel';
import { WfxviewoptionService } from './wfxviewoption.service';
import { ViewType } from '../../constants/enum';
import { ItemLibraryDataService } from '../../services/itemdata.service';
import { WfxFilterService } from '../wfx-filter/wfx-filter.service';
import { Filter } from '../../modals/filter';
import { ItemLibraryService } from '../../services/itemlibrary.service';
@Component({
  selector: 'app-wfx-viewoptions',
  templateUrl: './wfx-viewoptions.component.html',
  styleUrls: ['./wfx-viewoptions.component.css']
})
export class WfxViewoptionsComponent implements OnInit, OnChanges, AfterViewChecked {
  @Input() jsonDef: {
    attribute: WfxComponentAttributes
    backgroundcolor?: string,
    slider: {
      attribute: WfxComponentAttributes
    }
  };
  @Input() jsonData: {
    sliderDefaultvalue: number,
    selectedItemView: string,
    selectedItemText: string,
    arrItemsViewOptionData: [],
  };

  @Output() sliderChange = new EventEmitter<string>();
  @Output() clickGalleryView = new EventEmitter();
  @Output() clickListView = new EventEmitter();
  @Output() clickTableView = new EventEmitter();
  @Output() optionTypeChange = new EventEmitter();
  selectedOptionType: string;
  arrViewOptionData = [];
  sliderVisible: boolean;
  viewOptionsVisible: boolean;
  sliderJsonDef: any;
  sliderJsonData: any;
  wfxSelectJsonDef: WfxComponentAttributes;
  wfxSelectJsonData;
  wfxLabelitemJsonDef: WfxComponentAttributes;
  wfxlabeljsonData;
  componentObject: any;
  galleyviewActive = true;
  wfxSavedSearchSelectJsonDef: WfxComponentAttributes;
  filterName;
  filter: Filter;
  ddlOptionData;
  arrTableData;
  defaultValue =
    {
      backgroundcolor: 'white',
      visible: true,
      disable: false,
      Slidervisible: true,
      Sliderdisable: false,
      sliderDefaultvalue: 50,
    };

  constructor(private sliderService: WfxviewoptionService, private globals: ItemLibraryGlobals,
    private itemLibraryDataService: ItemLibraryDataService, private itemGlobals: ItemLibraryGlobals,
    private wfxFilterService: WfxFilterService, private itemLibraryService: ItemLibraryService) { }

  ngOnInit() {
    this.componentObject = this;
    this.viewOptionsVisible = false;
    this.sliderJsonDef = {
      attribute: {
        visible: this.SliderVisible,
        disable: this.SliderDisable
      }
    };
    this.sliderJsonData = {
      sliderDefaultvalue: this.globals.sliderRange,
      step: 20,
      maxValue: 120
    };
    /*** Wfx Select Json Data and Json Def Start***/

    /*** Wfx Select Json Data and Json Def End***/
    /*** Wfx Lable Json Data and Json Def Start***/
    this.wfxLabelitemJsonDef = {
      readonly: false, mandatory: false, caption: 'First Name', disable: false,
      value: 'fname', valuetype: 'dbfield',
      tooltip: { tooltipvalue: 'First Name', tooltipvaluetype: 'dbfield', tooltipposition: 'below' },
      inputtype: 'textbox', itemid: 'txtFirstName'
    };

    this.wfxlabeljsonData = {
      fname: 'View ' // ['A', 'B', 'C'],
      // ['Bank A (Switzerland)', 'Bank B (Switzerland)', 'Bank C (France)']
    };
    this.itemLibraryService.filter
    .subscribe(message => {
      this.filter = message.filter;
      if (!message.filter.isFilterApplied) {
        this.itemGlobals.selectedSortFieldValue = '';
        this.loadData();
      }
    }); 
    this.loadData();
    /*** Wfx Lable Json Data and Json Def End***/
  }
  /*** Event Section Starts ***/
  onChange($event) {
    this.sliderChange.emit($event);
    this.globals.sliderRange = $event;
    this.sliderService.onSliderValueChange({
      sliderValue: $event
    });
  }

  ngOnChanges() {
    switch (this.globals.currentView) {
      case ViewType.GALLERY:
        this.galleyviewActive = true;
        break;
      case ViewType.TABLE:
        this.galleyviewActive = false;
        break;
    }
    this.loadData();
  }
  // ngAfterContentChecked() {
  //   if (this.itemGlobals.selectedSortFieldValue === '') {
  //     console.log('ngAfterContentChecked');
  //     this.wfxSelectJsonData = {
  //       fname: '', savedSearch: this.itemGlobals.selectedSortFieldValue, // ['A', 'B', 'C'],
  //       optionname: this.itemGlobals.selectedSortFieldValue,  // ['Bank A (Switzerland)', 'Bank B (Switzerland)', 'Bank C (France)']
  //     };
  //   }
  // }

  ngAfterViewChecked(){
  //   switch (this.globals.currentView) {
  //     case ViewType.GALLERY:
  // console.log('ngAfterViewChecked',this.itemGlobals.selectedSortFieldValue);
  //   if (this.itemGlobals.selectedSortFieldValue === '') {
  //     console.log('ngAfterViewChecked',this.itemGlobals.selectedSortFieldValue);
  //     this.wfxSelectJsonData = {
  //       fname: '', savedSearch: this.itemGlobals.selectedSortFieldValue, // ['A', 'B', 'C'],
  //       optionname: this.itemGlobals.selectedSortFieldValue,  // ['Bank A (Switzerland)', 'Bank B (Switzerland)', 'Bank C (France)']
  //     };
  //   }
  //   break;
  //   case ViewType.LIST:
  //   break;
  // }
  }
  galleryViewClick(sliderRef) {
    // sliderRef.visible(true);
    this.globals.currentView = ViewType.GALLERY;
    this.sliderJsonDef = {
      attribute: {
        visible: true,
        disable: this.SliderDisable,
      }
    };
    this.sliderJsonData = {
      sliderDefaultvalue: this.globals.sliderRange,
      step: 20,
      maxValue: 120
    };
    this.viewOptionsVisible = false;
    this.galleyviewActive = true;
    this.loadData();
    this.clickGalleryView.emit();
  }
  listViewClick() {

    this.globals.currentView = ViewType.LIST;
    this.clickListView.emit();
    this.sliderJsonData = {
      sliderDefaultvalue: this.globals.sliderRange,
      step: 20,
      maxValue: 120
    };
  }
  tableViewClick(sliderRef) {
    // console.log('arrtable data listview click',this.arrTableData);
    this.globals.currentView = ViewType.TABLE;
    this.sliderJsonDef = {
      attribute: {
        visible: false,
        disable: this.SliderDisable
      }
    };
    this.sliderJsonData = {
      sliderDefaultvalue: this.globals.sliderRange,
      step: 20,
      maxValue: 120
    };
    // sliderRef.visible(false);
    this.viewOptionsVisible = true;
    this.galleyviewActive = false;
    this.loadData();
    this.clickTableView.emit();
  }
  onViewTypeChange(event) {
    switch (this.globals.currentView) {
      case ViewType.GALLERY:
        break;
      case ViewType.TABLE:
        this.optionTypeChange.emit(event);
        break;
    }
  }

  onDdlSelect(id) {
    switch (this.globals.currentView) {
      case ViewType.GALLERY:
          if (this.itemGlobals.currentRightSelectedPanel !== RightSelectedPanel.Filter) {
            this.itemGlobals.activePanel = false;
          }
          this.itemLibraryDataService.getSelectedSavedSearch(id.value)
            .subscribe((result: any) => {
              this.itemGlobals.isFilterApplied = true;
              this.itemLibraryDataService.advanceSearchFilter(result.data);
              this.wfxFilterService.rebindingSavedFilters(this.filter, result.data).subscribe(res => {
                this.filter = res;
                this.itemLibraryService.changeFilter({
                  filter: res
                });
                this.itemLibraryService.invokesFilterChip({
                  filterChip: res
                });
              });
            },
              error => {
                return console.log(error);
              });
          this.itemLibraryService.invokeshowSelectedRightPanel(true);
          this.itemGlobals.selectedSortFieldValue = id.value.toString();
          this.wfxSelectJsonData = {
            fname: '', savedSearch: this.itemGlobals.selectedSortFieldValue, // ['A', 'B', 'C'],
            optionname: this.itemGlobals.selectedSortFieldValue,  // ['Bank A (Switzerland)', 'Bank B (Switzerland)', 'Bank C (France)']
          };
        break;
      case ViewType.TABLE:
        break;
    }
  }
  onRemove(id) {
    this.itemLibraryDataService.deleteSavedSearch(id).subscribe(res => {
      this.loadData();
    });
  }

  onOpenList(event){
    this.loadData();
  }


  /*** Event Section Ends ***/
  /*** Method Section Starts ***/
  loadData() {

    switch (this.globals.currentView) {
      case ViewType.GALLERY:

          this.itemLibraryDataService.getSavedSearchDropdownList().subscribe
          ((result: any) => {
            if(result.message === 'No Record Found'){
              //this.wfxSelectJsonDef.caption='Default'
              this.ddlOptionData = [];
              this.filterName = null;
            }else{
              this.filterName = result.data;
              this.ddlOptionData = this.filterName;
            }
            // this.filterName = result.data;
            // this.ddlOptionData = this.filterName;
          });
        this.wfxSelectJsonDef = {
          readonly: false, itemid: 'ddlViewOption', mandatory: false, caption: 'Default',
          valuetype: 'dbfield', value: 'savedSearch', valuetext: 'optionname', 
          multiselect: false, disable: false, inputtype: 'select',
          ddlvalue: 'id', visible: true, ddlvaluetext: 'text', 
          addsearchoptioninddl: true, addclearicon: true,
          addimginddl: false, floatlabel: 'never', inputsubtype: 'text', placeholderlabel :'Search',
          tooltip: {
            tooltipvalue: '', tooltipvaluetype: '',
          }
        }
        this.wfxSelectJsonData = {
          fname: '', savedSearch: this.itemGlobals.selectedSortFieldValue, // ['A', 'B', 'C'],
                optionname: this.itemGlobals.selectedSortFieldValue,
        };
        // this.wfxSelectJsonData = {
        //   savedSearch: '', // ['A', 'B', 'C'],
        //   optionname: '',  // ['Bank A (Switzerland)', 'Bank B (Switzerland)', 'Bank C (France)']
        // };
        break;
      case ViewType.TABLE:
        //  this.ddlOptionData = this.jsonData.arrItemsViewOptionData;
        // this.wfxSelectJsonDef = {
        //   readonly: false, itemid: 'ddlViewOption', mandatory: false, caption: '', valuetype: 'dbfield', value: 'Code1',
        //   valuetext: 'optionname', multiselect: false, disable: false, inputtype: 'select', 
        // ddlvalue: 'Code',
        //   ddlvaluetext: 'Text', tooltip: { tooltipvalue: 'Option Value', tooltipvaluetype: 'dbfield', tooltipposition: 'below' },
        //   addclearicon: false, addimginddl: false, addsearchoptioninddl: true
        // };
        this.ddlOptionData = this.jsonData.arrItemsViewOptionData;
        this.wfxSelectJsonDef = {
          readonly: false, itemid: 'ddlViewOption', mandatory: false, caption: '',
          valuetype: 'dbfield', value: 'Code1', valuetext: 'optionname', multiselect: false, disable: false, inputtype: 'select',
          ddlvalue: 'Code', visible: true, ddlvaluetext: 'Text', addsearchoptioninddl: true, addclearicon: false,
          addimginddl: false, floatlabel: 'never', inputsubtype: 'text', placeholderlabel : 'Search',
          tooltip: {
            tooltipvalue: '', tooltipvaluetype: '',
          }
        }
        this.wfxSelectJsonData = {
          Code1: this.jsonData.selectedItemView, // ['A', 'B', 'C'],
          optionname: this.jsonData.selectedItemText // ['Bank A (Switzerland)', 'Bank B (Switzerland)', 'Bank C (France)']
        };
        break;
    }
  }

  /*** Method Section Ends ***/
  /*** Property Section Starts ***/
  get SliderVisible() {
    if (this.jsonDef && this.jsonDef.slider && this.jsonDef.slider.attribute &&
      this.jsonDef.slider.attribute.visible === false) {
      return false;
    }
    return this.defaultValue.Slidervisible;
  }
  get SliderDisable() {
    if (this.jsonDef && this.jsonDef.slider.attribute.disable) {
      return this.jsonDef.slider.attribute.disable;
    }
    return this.defaultValue.Sliderdisable;
  }
  /*** Property Section Ends ***/
}
