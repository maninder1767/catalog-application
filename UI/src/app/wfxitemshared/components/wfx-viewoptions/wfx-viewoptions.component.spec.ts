import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WfxViewoptionsComponent } from './wfx-viewoptions.component';

describe('WfxViewoptionsComponent', () => {
  let component: WfxViewoptionsComponent;
  let fixture: ComponentFixture<WfxViewoptionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WfxViewoptionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WfxViewoptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
