import { TestBed } from '@angular/core/testing';

import { WfxviewoptionService } from './wfxviewoption.service';

describe('WfxviewoptionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WfxviewoptionService = TestBed.get(WfxviewoptionService);
    expect(service).toBeTruthy();
  });
});
