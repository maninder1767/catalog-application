import { Injectable } from '@angular/core';
import { AsyncSubject, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WfxviewoptionService {

  constructor() { }
  private sliderValue = new Subject<{sliderValue: number}>();
  sliderValueSubsciption = this.sliderValue.asObservable();
   /*Send Slider Range value on gallery view */
  onSliderValueChange(message: { sliderValue: number}) {
    this.sliderValue.next(message);
  }
}
