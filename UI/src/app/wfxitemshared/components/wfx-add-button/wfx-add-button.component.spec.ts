import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WfxAddButtonComponent } from './wfx-add-button.component';

describe('WfxAddButtonComponent', () => {
  let component: WfxAddButtonComponent;
  let fixture: ComponentFixture<WfxAddButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WfxAddButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WfxAddButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
