import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { WfxComponentAttributes } from '../../../wfxlibrary/wfxsharedmodels/wfxcommonmodel';

@Component({
  selector: 'app-wfx-add-button',
  templateUrl: './wfx-add-button.component.html',
  styleUrls: ['./wfx-add-button.component.css']
})
export class WfxAddButtonComponent implements OnInit {

  constructor() { }
  @Input() jsonDef: {
    attribute?: WfxComponentAttributes,
    addBtn: {
      isShowAddBtn: Boolean
    },
  };
  @Output() addButtonClick = new EventEmitter();

  defaultJsonDef =
    {
      tooltip: 'Combined Shape',
      hidden: false,
      disabled: false,
      placeholder: 'Add',
      visible: true,
      label: 'Add',
      addBtn: {
        isShowAddBtn: true
      },
    };

    ngOnInit() {
    }

  /*** Event Section Starts */
  /**
 * Created By : Urvashi Sharma
 * Created On : 10/Aug/2019
 * Purpose : This event is for call parent component with some perameter.
 */
  onAddButtonClick(action) {
    this.addButtonClick.emit(action);
  }
  /*** Event Section Ends */

  /*** Property Section Starts */
   get tooltip() {
    if (this.jsonDef && this.jsonDef.attribute && this.jsonDef.attribute.tooltip) {
      return this.jsonDef.attribute.tooltip;
    } else {
      return this.defaultJsonDef.tooltip;
    }
  }

  get disabled() {
    if (this.jsonDef && this.jsonDef.attribute && this.jsonDef.attribute.disable) {
      return this.jsonDef.attribute.disable;
    } else {
      return this.defaultJsonDef.disabled;
    }
  }

  get visible() {
    if (this.jsonDef && this.jsonDef.addBtn && this.jsonDef.addBtn.isShowAddBtn===false) {
      return this.jsonDef.addBtn.isShowAddBtn;
    } else {
      return this.defaultJsonDef.addBtn.isShowAddBtn;
    }
  }
  /*** Property Section Ends */
}
