import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WfxfilterchipComponent } from './wfx-filterchip.component';

describe('WfxfilterchipComponent', () => {
  let component: WfxfilterchipComponent;
  let fixture: ComponentFixture<WfxfilterchipComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WfxfilterchipComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WfxfilterchipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
