import { MatDialog } from '@angular/material';
import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { Filter, FilterType } from '../../modals/filter';
import { WfxFilterComponent } from '../wfx-filter/wfx-filter.component';
import { WfxComponentAttributes } from '../../../wfxlibrary/wfxsharedmodels/wfxcommonmodel';
import { WfxSaveSearchComponent } from '../wfx-save-search/wfx-save-search.component';
import { WfxFilterService } from '../wfx-filter/wfx-filter.service';
import { ControlType } from '../../constants/enum';
import { ItemLibraryService } from '../../services/itemlibrary.service';
import { ItemLibraryGlobals } from '../../../wfxplmitemlibrary/itemLibrary.global';
@Component({
  selector: 'app-wfx-filterchip',
  templateUrl: './wfx-filterchip.component.html',
  styleUrls: ['./wfx-filterchip.component.css']
})
export class WfxfilterchipComponent implements OnInit {
  @Input() filter: Filter;
  @Output() chipFilterChange = new EventEmitter<any>();
  @ViewChild('filter', { static: false }) wfxFilterComponent: WfxFilterComponent;
  visible = true;
  filterChip: Filter;
  selectable = true;
  removable = true;

  constructor(public dialog: MatDialog, private wfxFilterService: WfxFilterService, public itemGlobal: ItemLibraryGlobals,
    private itemLibraryService: ItemLibraryService) { }

    ngOnInit() {
      this.itemLibraryService.filterChipSubscription
      .subscribe(message => {
        if (message) {
          this.filterChip = Object.assign({}, message.filterChip);
          this.createLocalFilter(message.filterChip);
        }
      });
    }
  itemJsonDef(filtertype) {
    return {
      readonly: false, mandatory: false, caption: '', disable: false,
      value: 'fname', valuetype: 'dbfield', inputsubtype: '', visible: true,
      tooltip: { tooltipvalue: '', tooltipvaluetype: 'dbfield', tooltipposition: 'below' },
      inputtype: 'textbox', itemid: ''
    };
  }

  jsonData(filtertype) {
    return {
      fname: filtertype.caption + ': ' + this.getSelectedFilter(filtertype)
    };
  }


  remove(filtertype: any): void {
    if (this.wfxFilterService.SavedfiterCriteriaData.length > 0) {
      const savedIndex = this.wfxFilterService.SavedfiterCriteriaData.findIndex(x => x.fieldId === filtertype.fieldId);
      if (savedIndex >= 0) {
        this.wfxFilterService.SavedfiterCriteriaData.splice(savedIndex, 1);
      }
    }
    if (this.itemGlobal.appliedFilterChips) {
      const localIndex = this.itemGlobal.appliedFilterChips.arrFilterType.findIndex(x => x.fieldId === filtertype.fieldId);
      if (localIndex >= 0) {
        this.itemGlobal.appliedFilterChips.arrFilterType.splice(localIndex, 1);
      }

    }
    const index = this.filter.arrFilterType.findIndex(x => x.fieldId === filtertype.fieldId);
    filtertype.filterValue = '';
    this.filter.isFilterApplied = false;
    if (filtertype.controlType === ControlType.DATE || filtertype.controlType === ControlType.TEXT) {
      filtertype.filterValue = '';
      filtertype.dateFrom = '';
      filtertype.dateTo = '';
    }
    if (index >= 0) {
      this.filter.arrFilterType[index].filterValue = '';
      this.filter.arrFilterType[index].dateFrom = '';
      this.filter.arrFilterType[index].dateTo = '';
      this.filter.arrFilterType[index].arrFilterSubType.forEach(
        function (filtersubtype, indexsubtype) {
          filtersubtype.IsSelected = false;
        });
    }
    this.filter.isFilterApplied = false;
    for (let ind = 0; ind < this.filter.arrFilterType.length; ind++) {
      const ft = this.filter.arrFilterType[ind];
      if (this.isFilterSelected(ft)) {
        this.filter.isFilterApplied = true;
      }
    }
    this.chipFilterChange.emit(this.filter);
  }
  onClearFilter() {
    if (this.wfxFilterService.SavedfiterCriteriaData.length > 0) {
      this.wfxFilterService.SavedfiterCriteriaData = [];
    }
    this.filter.filterText = '';
    this.filter.isFilterApplied = false;
    this.itemGlobal.appliedFilterChips = null;
    this.filter.arrFilterType.forEach(function (filter, index) {
      if (filter.controlType === ControlType.DATE || filter.controlType === ControlType.TEXT) {
        filter.filterValue = '';
        filter.dateFrom = '';
        filter.dateTo = '';
      } else {
        filter.filterValue = '';
        filter.arrFilterSubType.forEach(
          function (filtersubtype, indexsubtype) {
            filtersubtype.IsSelected = false;
          });
      }
    });
    this.chipFilterChange.emit(this.filter);
    this.itemGlobal.activePanel = false;
    this.itemLibraryService.invokeshowSelectedRightPanel(true);
  }

  // getSelectedFilter(filtertype: FilterType) {
  //   return this.wfxFilterService.getSelectedFilter(this.filter, filtertype);
  // }
  getSelectedFilter(filtertype?: FilterType) {
    if (filtertype && this.itemGlobal.isFilterApplied) {
       const value = this.wfxFilterService.getSelectedFilter(this.itemGlobal.appliedFilterChips, filtertype);
       if (value !== '' && value !== null && value !== undefined) {
        return value;
      }
    } else {
      const value = this.wfxFilterService.getSelectedFilter(this.itemGlobal.appliedFilterChips, filtertype);
      if (value !== '' && value !== null && value !== undefined) {
        return  value;
      }
    }
  }

  onSaveSearchClick() {
    const dialogRef = this.dialog.open(WfxSaveSearchComponent, {
      panelClass: '',
    });
    dialogRef.afterClosed().subscribe(result => { });
  }
  isFilterSelected(filtertype: FilterType) {
    let bReturn = false;
    if ((filtertype.controlType === ControlType.DATE && filtertype.dateFrom !== '' && filtertype.dateTo !== '') ||
      (filtertype.controlType === ControlType.TEXT && filtertype.filterValue !== '')) {
      return bReturn = true;
    } else if (filtertype.controlType === ControlType.MULTISELECT || filtertype.controlType === ControlType.CHECKBOX) {
      const arrFilterSubType = this.filter.arrFilterType
        .find(f => f.caption === filtertype.caption)
        .arrFilterSubType.find(f => f.IsSelected === true);
      if (arrFilterSubType) {
        return bReturn = true;
      }
    } else {
      return bReturn;
    }
  }
  createLocalFilter(filter) {
    this.wfxFilterService.filterJson(filter).subscribe((resp) => {
     this.wfxFilterService.rebindingSavedFilters(null, resp.searchparams.filterCriteria).subscribe((objFilter: Filter) => {
      // this.localFilterData = objFilter;
       this.itemGlobal.appliedFilterChips = objFilter;
        resp.searchparams.filterCriteria = [];
     });
     });
   }
}
