import { WfxComponentAttributes } from './../../../wfxlibrary/wfxsharedmodels/wfxcommonmodel';
import { Component, OnInit, Input, OnChanges } from '@angular/core';


@Component({
  selector: 'app-avatar',
  templateUrl: './wfx-avatar.component.html',
  styleUrls: ['./wfx-avatar.component.css']
})
export class WfxAvatarComponent implements OnChanges, OnInit {
  @Input() jsonDef: {
    attribute: WfxComponentAttributes,
  };

  @Input() jsonData: {
    imageUrl: string,
    profileName: string
  };

  defaultDef =  {
    attribute: {
      tooltip: {
       tooltipvalue: '',
       tooltipvaluetype : '',
       tooltipposition: 'below'
       }
     }
  };
  getWidth;
  getHeight;
  setwidth = this.width;
  setheight = this.height;

  componentObject: any;

  labelJsonDef = {
    readonly: false, mandatory: true, caption: 'Profile', disable: false,
    value: 'fname', valuetype: 'dbfield',
    tooltip: { tooltipvalue: 'First Name', tooltipvaluetype: 'dbfield', tooltipposition: 'below' },
    onchange: this.callDynamicfunction, inputtype: 'textbox', itemid: 'txtFirstName'
  };

  labelJsonData =  {
    fname: '', checkBoxValue: false, selectedRadioValue: 'Winter', SwitchOn: false,
    DocDate: 'Wed, Apr 24 2019', idtesting1: 'B',
    optionname: 'Bank B (Switzerland)'
  };

  constructor() { }

  ngOnInit() {
    this.componentObject = this;
  }

   ngOnChanges() {
     this.labelJsonData.fname = this.getInitial();
  }

  callDynamicfunction(e?: any) {
    alert('awd');
  }

  getInitial() {
      if (this.profileName === '' || this.profileName === null || this.profileName === undefined) {
        return '';
      } else {
        const arrProfile = this.profileName.trim().split(' ');
        let initial = '';
        arrProfile.forEach((char) => {
           initial += char[0];
        });
        return initial;
      }
  }

  get imageUrl() {
    if (this.jsonData && this.jsonData.imageUrl) {
      return this.jsonData.imageUrl;
    }
    return '';
  }
   get tooltip() {
    if ( this.jsonDef && this.jsonDef.attribute && this.jsonDef.attribute.tooltip && this.jsonDef.attribute.tooltip.tooltipvalue) {
      return this.jsonDef.attribute.tooltip.tooltipvalue ;
    } else {
      return this.getInitial();
    }
  }

  get profileName() {
    if (this.jsonData && this.jsonData.profileName) {
      return this.jsonData.profileName;
    }
    return '';
  }

  height(height: string) {
    if (height !== '') {
      this.getHeight = height;
      return height;
    }
  }

  width(width: string) {
    if (width !== '') {
      this.getWidth = width;
      return width;
    }
  }

}
