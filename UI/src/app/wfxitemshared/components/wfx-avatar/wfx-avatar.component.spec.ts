import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { WfxAvatarComponent } from './wfx-avatar.component';



describe('AvatarComponent', () => {
  let component: WfxAvatarComponent;
  let fixture: ComponentFixture<WfxAvatarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WfxAvatarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WfxAvatarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
