import { ItemLibraryGlobals, RightSelectedPanel } from './../../../wfxplmitemlibrary/itemLibrary.global';
import { ItemLibraryService } from './../../services/itemlibrary.service';

import { Component, OnInit, Input } from '@angular/core';
import { WfxComponentAttributes } from '../../../wfxlibrary/wfxsharedmodels/wfxcommonmodel';
import { ItemLibraryDataService } from '../../services/itemdata.service';


@Component({
  selector: 'app-wfx-toolbar',
  templateUrl: './wfx-toolbar.component.html',
  styleUrls: ['./wfx-toolbar.component.css']
})
export class WfxToolbarComponent implements OnInit {
@Input() jsonDef: any;
@Input() jsonData: any;
wfxslidetoggleJsonDef: WfxComponentAttributes;
wfxslidetogglejsonData;
commentCount: string;

  constructor(private itemLibraryService: ItemLibraryService,
    private itemGlobal: ItemLibraryGlobals) { }



   commentJsonDef=
   {
     attribute: {
     width: '25px',
     height: '15px',
     tooltip: 'Comment Here',
     hidden: false,
     disable: true,
     },
     comment:
     {
       showComment: false
     }
   };



  ngOnInit() {
    this.itemLibraryService.showCommentSubscription.subscribe(message => {
      if (message.showCommentIcon === true) {
      this.commentJsonDef.comment.showComment = true;
      if (message.commentCount === undefined) {
        this.commentCount = '0';
      }else {
        this.commentCount = message.commentCount;
      }

      }else{
        this.commentJsonDef.comment.showComment = false;
      }
    });

  }

  onCommentClick(){
    console.log('priyaa');
    if (this.itemGlobal.currentView === 'GALLERY') {
      const commentdata = this.itemGlobal.arrGalleryViewData.getValue().filter(_x => _x.checkbox === true);
      this.itemLibraryService.invokeshowCommentDetail({ ID: commentdata[0].itemId});
    } else {
      this.itemLibraryService.invokeshowCommentDetail({ ID: this.itemGlobal.arrSelectedItemId.toString()});
    }
    this.itemGlobal.activePanel = true;
    this.itemLibraryService.invokeshowSelectedRightPanel(true);
    this.itemGlobal.currentRightSelectedPanel =  RightSelectedPanel.CommentDetail;
  }

}

