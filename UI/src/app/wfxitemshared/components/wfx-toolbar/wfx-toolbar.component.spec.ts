import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WfxToolbarComponent } from './wfx-toolbar.component';

describe('WfxToolbarComponent', () => {
  let component: WfxToolbarComponent;
  let fixture: ComponentFixture<WfxToolbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WfxToolbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WfxToolbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
