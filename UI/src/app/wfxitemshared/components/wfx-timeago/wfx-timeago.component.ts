/*
  * Created By : Ankit Gupta
  * Created On : 16/Aug/2019
  * Purpose : This component is for show Time
  * Last Modified Date :16/Aug/2019
  * Last Modified By : Ankit Gupta
  */

import { WfxComponentAttributes } from './../../../wfxlibrary/wfxsharedmodels/wfxcommonmodel';
import { Component, OnInit, Input, OnChanges } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'app-timeago',
  templateUrl: './wfx-timeago.component.html',
  styleUrls: ['./wfx-timeago.component.css']
})
export class WfxTimeagoComponent implements OnInit, OnChanges {
  @Input() jsonDef: {
    attribute?: WfxComponentAttributes,
  };
  @Input() jsonData: {
    date: string;
  };
  componentObject;
  labelJsonData;


  constructor() { }
  ngOnInit() {
    this.componentObject = this;
    if (this.jsonData && this.jsonData.date) {
      this.labelJsonData =  {
        // 2019-09-13 13:07:27.510
        fname: this.timeAgoDifferenc(this.jsonData.date)
      };
    }
  }

  ngOnChanges() {
    if (this.jsonDef === undefined) {
      this.jsonDef = {
        attribute: {
        readonly: false, mandatory: true, caption: '', disable: false,
        value: 'fname', valuetype: 'dbfield',
        tooltip: { tooltipvalue: '', tooltipvaluetype: 'dbfield', tooltipposition: 'below' },
        inputtype: 'textbox', itemid: 'txtFirstName'
      }};
    }
  }

  callDynamicfunction(e?: any) {
    alert('awd');
  }


 /*** Event Section Starts */
 /**
 * Created By : Ankit Gupta
 * Created On : 16/Aug/2019
 * Purpose : This event is for call from html.
 */

  timeAgoDifferenc(value: string) {
    if (value === '' || value === undefined || value === null) {
      return '';
    }
    let time = moment.utc(value).toNow();
    const isTextStartToIn = time.substr(0, 2);

    // to remove 'in' from the starting of the string
    if (isTextStartToIn === 'in') {
      time = time.substr(3);
    }
    const length = time.length;
    const isTextEndToAgo = time.substr(length - 3, length);

    // to add 'ago' at the end of the string
    if (isTextEndToAgo !== 'ago') {
      time = time + ' ago';
    }
    return time;
    // return moment.utc(value).toNow();
  }
}
