import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { WfxTimeagoComponent } from './wfx-timeago.component';


describe('TimeagoComponent', () => {
  let component: WfxTimeagoComponent;
  let fixture: ComponentFixture<WfxTimeagoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WfxTimeagoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WfxTimeagoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
