import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-wfx-extendedmenu',
  templateUrl: './wfx-extendedmenu.component.html',
  styleUrls: ['./wfx-extendedmenu.component.css']
})
export class WfxExtendedmenuComponent implements OnInit {

  @Input() MouseOutColor: string;
  @Input() MouseOverColor: string;
  @Output() extendedMenuClick = new EventEmitter();
  @Output() moreClick = new EventEmitter();

  Color;
  constructor() { }
  ngOnInit() { this.Color = this.MouseOutColor; }
  OnExtendedMenuClick($event) {
    this.extendedMenuClick.emit($event);
    this.moreClick.emit($event);
  }
  onMouseOver() { this.Color = this.MouseOverColor; }
  onMouseOut() { this.Color = this.MouseOutColor; }


}
