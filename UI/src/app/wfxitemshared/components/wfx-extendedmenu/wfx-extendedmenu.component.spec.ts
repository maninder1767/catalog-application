import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WfxExtendedmenuComponent } from './wfx-extendedmenu.component';

describe('WfxExtendedmenuComponent', () => {
  let component: WfxExtendedmenuComponent;
  let fixture: ComponentFixture<WfxExtendedmenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WfxExtendedmenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WfxExtendedmenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
