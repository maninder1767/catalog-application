/*
WFXControls v1.0.0 (2019-April)
Copyright (c) 2019-2019 WFX.
License: https://www.wfxcloud.com/contact.html
*/
/* eslint-disable */

import {
    Component, Input, Output, EventEmitter, ElementRef, OnChanges, SimpleChanges, OnInit,
    ChangeDetectorRef, AfterViewChecked
} from '@angular/core';
import { WfxCommonFunctions } from '../../wfxlibrary/wfxsharedfunction/wfxcommonfunction';
import { WfxComponentAttributes } from '../../wfxlibrary/wfxsharedmodels/wfxcommonmodel';


@Component({
    selector: 'app-wfximagebutton',
    template: `
    <div>
    <a class="add-folder" (click)="ButtonOnClick($event)"></a>
    </div> `
})

export class WfxImageButtonComponent implements OnInit {
    constructor(
        containerElement: ElementRef, private cdr: ChangeDetectorRef,
        public cmf: WfxCommonFunctions) {
    }

    @Output() OnClick = new EventEmitter<any>();



    ngOnInit() {
    }



    ButtonOnClick(e) {
        this.OnClick.emit(e);
    }

}
