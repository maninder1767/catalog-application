import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WfxCommentComponent } from './wfx-comment.component';

describe('WfxCommentComponent', () => {
  let component: WfxCommentComponent;
  let fixture: ComponentFixture<WfxCommentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WfxCommentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WfxCommentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
