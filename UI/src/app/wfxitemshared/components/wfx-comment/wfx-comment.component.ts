import { ItemLibraryGlobals } from './../../../wfxplmitemlibrary/itemLibrary.global';
import { Component, OnInit, Input, Output, EventEmitter, OnChanges, ɵConsole } from '@angular/core';
import { WfxComponentAttributes } from '../../../wfxlibrary/wfxsharedmodels/wfxcommonmodel';
//import { WfxComponentAttributes } from '../../wfxlibrary/wfxsharedmodels/wfxcommonmodel';

@Component({
  selector: 'app-wfx-comment',
  templateUrl: './wfx-comment.component.html',
  styleUrls: ['./wfx-comment.component.css']
})
export class WfxCommentComponent implements OnInit, OnChanges {
  
  wfxLinkButtonJsonDef;
  wfxLinkButtonjsonData;
  @Output() commentClick = new EventEmitter<string>();
  @Input() jsonData

  @Input() jsonDef: {
    attribute?: WfxComponentAttributes,
    comment: { showComment?: boolean,}

  };



  defaultValue =
    {
      width: '25px',
      height: '15px',
      tooltip: 'Comment Here',
      visible: false,
      disable: true,
      showComment: false
    };

  constructor(public itemGlobal:ItemLibraryGlobals) { }

  ngOnInit() {

    this.wfxLinkButtonJsonDef =  {
      readonly: false, mandatory: false, caption: this.jsonData, disable: false,
      value: 'commentCount', valuetype: 'dbfield',
      tooltip: { tooltipvalue: 'Comment Count', tooltipvaluetype: '', tooltipposition: 'below' },
    };

    this.wfxLinkButtonjsonData = {
      commentCount: this.jsonData
    };
  }

  ngOnChanges() {

    this.wfxLinkButtonJsonDef =  {
      readonly: false, mandatory: false, caption: this.jsonData, disable: false,
      value: 'commentCount', valuetype: 'dbfield',
      tooltip: { tooltipvalue: 'Comment Count', tooltipvaluetype: '', tooltipposition: 'below' },
    };

    this.wfxLinkButtonjsonData = {
      commentCount: this.jsonData
    };
  }



  /**
* Created By :Priya
* Created On :  23/Aug/2019
* Purpose : This method is call for open comment slider
*/

onCommentClick() { 
    this.commentClick.emit();
  }

   /*** Method Section Ends */

     /*** Property Section Starts */

  get height() {
    if (this.jsonDef && this.jsonDef.attribute.height) {
      return this.jsonDef.attribute.height;
    } else {
      return this.defaultValue.height;
    }
  }

  get width() {
    if (this.jsonDef && this.jsonDef.attribute.width) {
      return this.jsonDef.attribute.width;
    } else {
      return this.defaultValue.width;
    }
  }

  get tooltip() {
    if (this.jsonDef && this.jsonDef.attribute.tooltip) {
      return this.jsonDef.attribute.tooltip;
    } else {
      return this.defaultValue.tooltip;
    }
  }

  get visible() {
    if (this.jsonDef && this.jsonDef.attribute.visible) {
      return this.jsonDef.attribute.visible;
    } else {
      return this.defaultValue.visible;
    }
  }

  get showComment() {
    if (this.jsonDef && this.jsonDef.comment.showComment) {
      return this.jsonDef.comment.showComment;
    } else {
      return this.defaultValue.showComment;
    }
  }

     /*** Property Section Ends */
}
