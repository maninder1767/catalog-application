import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FilterType } from '../../modals/filter';

@Component({
  selector: 'app-wfx-filterlist',
  templateUrl: './wfx-filterlist.component.html',
  styleUrls: ['./wfx-filterlist.component.css']
})
export class WfxFilterListComponent implements OnInit {
@Input() filtertype = new FilterType();
@Output() textAreaInput = new EventEmitter<{event:any,filtertype: any}>();
  constructor() { }

  ngOnInit() {

  }
  getTextAreaJsonDef(filtertype: FilterType) {
    return  {
      readonly: this.filtertype.controlType !='TEXT' ? true: false , mandatory: false, caption: this.filtertype.caption, disable: false, 
      value: 'fname', valuetype: 'dbfield',  inputsubtype: 'textbox', visible: true,  maxlength: 500,
      tooltip: { tooltipvalue: this.filtertype.caption, tooltipvaluetype: 'dbfield', tooltipposition: 'below' },
      inputtype: 'textbox', itemid: 'txtTextbox',
    }
  }
  getTextAreaJsonData(filtertype) {
    return  {
      fname: this.filtertype.filterValue, checkBoxValue: false, selectedRadioValue: 'Winter', SwitchOn: false, 
      DocDate: 'Wed, Apr 24 2019', idtesting1: 'B', // ['A', 'B', 'C'],
      optionname: 'Bank B (Switzerland)', // ['Bank A (Switzerland)', 'Bank B (Switzerland)', 'Bank C (France)']
      }
  }
  onTexAreaInput(event) {
   this.textAreaInput.emit({event: event.target.value, filtertype: this.filtertype});
  }
}
