import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WfxFilterListComponent } from './wfx-filterlist.component';

describe('WfxFilterListComponent', () => {
  let component: WfxFilterListComponent;
  let fixture: ComponentFixture<WfxFilterListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WfxFilterListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WfxFilterListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
