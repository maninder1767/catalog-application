
/*
  * Created By : MAnoj Udayan
  * Created On : 13 Sep 2019
  * Purpose : Sorting of Data
  * Last Modified Date : 13 Sep 2019l
  * Last Modified By : Manoj Udayan
  */

import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { WfxComponentAttributes } from '../../../../app/wfxlibrary/wfxsharedmodels/wfxcommonmodel';
import { Sort } from '../../modals/sort';
import { SortParams } from '../../modals/filter';

@Component({
  selector: 'app-wfxsort',
  templateUrl: './wfx-sort.component.html',
  styleUrls: ['./wfx-sort.component.css']
})
export class WfxSortComponent implements OnInit, OnChanges {

  @Input() jsonDef = new SortJsonDef();
  // @Input() sortOptions;
  @Input() jsonData: {
    defaultSortParams: SortParams,
    sortOptions: Sort[];
  };
  @Output() sortTypeChange = new EventEmitter();
  @Output() orderTypeChange = new EventEmitter<any>();
  ascendingArrow = 'Ascending';
  descendingArrow = 'Descending';
  ascending = 'Asc';
  descending = 'Desc';
  selectedSortDirection: string;
  selectFieldId = '';
  selectFieldText = '';
  selectedSortFieldValue = '';
  sortOrder = 0;
  selectedSortTooltip = 'Created On';
  itemJsonDef: WfxComponentAttributes[];
  wfxSelectJsonData;
  DefaultjsonDef = {
    width: '15px', height: '15px', color: '#3CBBC9', tooltip: 'Ascending', label: 'Sort By', placeholder: 'Sort By',
    sortDirectionEnable: true, isSvgAsc: true, isSortVisible: true, visible: true, readonly: false, disable: false
  };

  constructor() { }

  ngOnInit() {
    console.log('this.jsonData', this.jsonData.defaultSortParams);
    this.selectedSortFieldValue = this.jsonData.defaultSortParams.fieldId;
    this.selectFieldId = this.jsonData.defaultSortParams.fieldId;
    this.selectedSortDirection = this.jsonData.defaultSortParams.sortDirection;
    this.sortOrder = this.jsonData.defaultSortParams.sortOrder;
    this.itemJsonDef = [
      {
        readonly: false, itemid: 'ddlsavedSearch', mandatory: false, caption: '', valuetype: 'dbfield', value: 'value1',
        valuetext: 'optionname', disable: false, inputtype: 'select', ddlvalue: 'value',
        ddlvaluetext: 'text', multiselect: false, addsearchoptioninddl: false,
        tooltip: { tooltipvalue: this.selectedSortTooltip, tooltipvaluetype: 'text', tooltipposition: 'below' }
      },
    ];

    this.wfxSelectJsonData = {
      fname: '', value1: this.selectedSortFieldValue, // ['A', 'B', 'C'],
      optionname: this.selectedSortFieldValue,  // ['Bank A (Switzerland)', 'Bank B (Switzerland)', 'Bank C (France)']
    };
  }
  ngOnChanges() {
  }
  /**
  * Created By : MAnoj Udayan
  * Created On : 13 Sep 2019
  * Purpose : Sorting of Data
  * Last Modified Date : 13 Sep 2019
  * Last Modified By : Manoj Udayan
* Change Summary:
* @param {string} event     // Selected Option like Most Recent
*/

  onSortTypeChange(event): void {
    if (this.jsonData && this.jsonData.sortOptions) {
      const sectedOpt = this.jsonData.sortOptions.find(x => x.value === event);
      this.selectFieldId = sectedOpt.value;
      this.sortOrder = sectedOpt.sortOrder;
      this.selectFieldText = sectedOpt.text;
      this.selectedSortFieldValue = event;
      this.selectedSortTooltip = sectedOpt.text;
      this.itemJsonDef[0].tooltip.tooltipvalue = this.selectedSortTooltip;
      const eventData = {
        selectFieldId: this.selectFieldId,
        sortDirection: this.selectedSortDirection,
        sortOrder: this.sortOrder,
      };
      this.sortTypeChange.emit(eventData);
    }
  }
  /**
  * Created By : MAnoj Udayan
  * Created On : 13 Sep 2019
  * Purpose : Sorting of Data
  * Last Modified Date : 13 Sep 2019
  * Last Modified By : Manoj Udayan
* Change Summary: 
* @param {string} event  // Ascending or Descending
*/
  onOrderTypeChange(event): void {
    if (this.jsonDef && this.jsonDef.wfxComponentAttributes && this.jsonDef.wfxComponentAttributes.readonly ||
        this.DefaultjsonDef.readonly) {
      return;
    }
    if (event) {
      if (this.jsonDef && this.jsonDef.isSvgAsc) {
        this.jsonDef.isSvgAsc = !this.jsonDef.isSvgAsc;
      }
      this.DefaultjsonDef.isSvgAsc = !this.DefaultjsonDef.isSvgAsc;
    }
    this.selectedSortDirection = event;
    const eventData = {
      selectFieldId: this.selectFieldId,
      sortDirection: this.selectedSortDirection,
      sortOrder: this.sortOrder,
    }
    this.sortTypeChange.emit(eventData);
  }
  get width() {
    if (this.jsonDef && this.jsonDef.wfxComponentAttributes && this.jsonDef.wfxComponentAttributes.width) {
      return this.jsonDef.wfxComponentAttributes.width;
    }
    return this.DefaultjsonDef.width;
  }
  get height() {
    if (this.jsonDef && this.jsonDef.wfxComponentAttributes && this.jsonDef.wfxComponentAttributes.height) {
      return this.jsonDef.wfxComponentAttributes.height;
    }
    return this.DefaultjsonDef.height;
  }
  get visible() {
    if (this.jsonDef && this.jsonDef.wfxComponentAttributes && this.jsonDef.wfxComponentAttributes.visible) {
      return this.jsonDef.wfxComponentAttributes.visible;
    }
    return this.DefaultjsonDef.visible;
  }
  get color() {
    // if (this.jsonDef && this.jsonDef.wfxComponentAttributes && this.jsonDef.wfxComponentAttributes.color) {
    //   return this.jsonDef.wfxComponentAttributes.color;
    // }
    return this.DefaultjsonDef.color;
  }
  get tooltip() {
    if (this.jsonDef && this.jsonDef.wfxComponentAttributes && this.jsonDef.wfxComponentAttributes.tooltip) {
      return this.jsonDef.wfxComponentAttributes.tooltip;
    }
    return this.DefaultjsonDef.tooltip;
  }
  get label() {
    if (this.jsonDef && this.jsonDef.label) {
      return this.jsonDef.label;
    }
    return this.DefaultjsonDef.label;
  }
  get placeholder() {
    if (this.jsonDef && this.jsonDef.placeholder) {
      return this.jsonDef.placeholder;
    }
    return this.DefaultjsonDef.placeholder;
  }
  get sortDirectionEnable() {
    if (this.jsonDef && this.jsonDef.sortDirectionEnable) {
      return this.jsonDef.sortDirectionEnable;
    }
    return this.DefaultjsonDef.sortDirectionEnable;
  }
  get isSvgAsc() {
    if (this.jsonDef && this.jsonDef.isSvgAsc) {
      return this.jsonDef.isSvgAsc;
    }
    return this.DefaultjsonDef.isSvgAsc;
  }
  get isSortVisible() {
    if (this.jsonDef && this.jsonDef.isSortVisible) {
      return this.jsonDef.isSortVisible;
    }
    return this.DefaultjsonDef.isSortVisible;
  }
  get disable() {
    if (this.jsonDef && this.jsonDef.wfxComponentAttributes && this.jsonDef.wfxComponentAttributes.disable) {
      return this.jsonDef.wfxComponentAttributes.disable;
    }
    return this.DefaultjsonDef.disable;
  }
}

export class SortJsonDef {
  wfxComponentAttributes: WfxComponentAttributes;
  label: string;
  placeholder: string;
  sortDirectionEnable: Boolean;
  isSvgAsc: Boolean;
  isSortVisible: Boolean;
}


