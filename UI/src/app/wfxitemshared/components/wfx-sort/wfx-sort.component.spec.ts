import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WfxSortComponent } from './wfx-sort.component';

describe('AssetSortComponent', () => {
  let component: WfxSortComponent;
  let fixture: ComponentFixture<WfxSortComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WfxSortComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WfxSortComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
