import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WfxBookmarkComponent } from './wfx-bookmark.component';

describe('WfxBookmarkComponent', () => {
  let component: WfxBookmarkComponent;
  let fixture: ComponentFixture<WfxBookmarkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WfxBookmarkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WfxBookmarkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
