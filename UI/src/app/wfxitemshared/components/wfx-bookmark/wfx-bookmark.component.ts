import { WfxComponentAttributes } from './../../../wfxlibrary/wfxsharedmodels/wfxcommonmodel';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
/*
  * Created By : Ashutosh Rai
  * Created On : 11/09/2018
  * Purpose : This is use to bookmark the styles
  * Last Modified Date : This will be getting updated whenever we make changes. This will show only  last modified details
  * Last Modified By : This will be getting updated whenever we make changes. This will show only  last modified details
  */
@Component({
  selector: 'app-wfx-bookmark',
  templateUrl: './wfx-bookmark.component.html',
  styleUrls: ['./wfx-bookmark.component.css']
})
export class WfxBookmarkComponent implements OnInit {
  constructor() { }
/*** Property Section Starts ***/
@Input() jsonDef: {attribute: WfxComponentAttributes};
@Input() jsonData: {isBookMark: boolean};
/*** Property Section Ends ***/
/*** Event Section Starts ***/
@Output() bookmarkclick = new EventEmitter();
/*** Event Section Ends ***/
/*** Variable Section Starts ***/
  getWidth;
  getHeight;
  setwidth = this.width;
  setheight = this.height;
defaultjsonDef: WfxComponentAttributes;
defaultjsondata = { isBookMark : false };
/*** Variable Section Ends ***/
ngOnInit() {
  this.defaultjsonDef = {
    tooltip: {
      tooltipvalue: '',
    },
    visible: false,
    disable: false,
  };
}
/*** Method Section Starts ***/

/**
* Created By :  Ashutosh Rai
* Created On : 11/09/2018
* Purpose : It is  use to invoke bookmarkclick
*/

onBookmarkClick() {
  if (this.jsonDef.attribute.disable) { return; }
   this.jsonData.isBookMark = !this.jsonData.isBookMark;
   this.bookmarkclick.emit(this.jsonData);
}

height(height: string) {
  if (height !== '') {
    this.getHeight = height;
    return height;
  }
}

width(width: string) {
  if (width !== '') {
    this.getWidth = width;
    return width;
  }
}
/*** Method Section Ends ***/
  get isBookmarked() {
    return this.jsonData.isBookMark || this.defaultjsondata.isBookMark;
  }
  get visible() {
    if (this.jsonDef && this.jsonDef.attribute && this.jsonDef.attribute.visible === false) {
      return false;
    }
    return true;
  }

  get title() {
    if (this.jsonDef && this.jsonDef.attribute && this.jsonDef.attribute.tooltip) {
      return this.jsonDef.attribute.tooltip;
    }
    return this.defaultjsonDef.tooltip.tooltipvalue;
  }

}
