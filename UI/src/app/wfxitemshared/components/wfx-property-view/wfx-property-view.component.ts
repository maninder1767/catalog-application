import { Component, OnInit, Input, ElementRef, ViewChild, OnChanges, HostListener } from '@angular/core';
import { ItemLibraryDataService } from '../../services/itemdata.service';
import { Property, PropertyData } from '../../modals/propertyView';
import { WfxComponentAttributes } from '../../../../app/wfxlibrary/wfxsharedmodels/wfxcommonmodel';
import { ItemLibraryService } from '../../services/itemlibrary.service';
import { Observable, merge, fromEvent, Subject } from 'rxjs';

@Component({
  selector: 'app-wfx-property-view',
  templateUrl: './wfx-property-view.component.html',
  styleUrls: ['./wfx-property-view.component.css']
})
export class WfxPropertyViewComponent implements OnInit, OnChanges {
  @Input() jsonData;
  @Input() jsonDef;
  propertyViewData;
  propertyViewColorData;
  itemImg;
  propViewData;
  articleCode;
  articleName;
  showColorSection = true;
  lazyscrollObservable: Observable<{} | Event>;
  lazyScrollSub = new Subject();
  itemJsonData = [];
  viewInnerHeight: number;
  viewInnerdivHeight: number;
  itemJsonDef: WfxComponentAttributes[] = [
    {
      readonly: false, mandatory: false, caption: 'Save Search', disable: false,
      value: 'labelValue', valuetype: 'dbfield',
      tooltip: { tooltipvalue: 'Save Search', tooltipvaluetype: 'dbfield', tooltipposition: 'below' },
      inputtype: 'label', itemid: 'txtcount'
    },
    {
      readonly: false, mandatory: false, caption: '', disable: false,
      value: 'Label', valuetype: 'dbfield',
      tooltip: { tooltipvalue: '', tooltipvaluetype: 'dbfield', tooltipposition: 'below' },
      inputtype: 'textbox', itemid: 'txtcount'
    },
    {
      readonly: false, mandatory: false, caption: 'Save Search', disable: false,
      value: 'Label', valuetype: 'dbfield',
      tooltip: { tooltipvalue: 'Save Search', tooltipvaluetype: 'dbfield', tooltipposition: 'below' },
      inputtype: 'textbox', itemid: 'txtcount'
    },
    {
      readonly: false, mandatory: false, caption: 'Save Search', disable: false,
      value: 'labelValue', valuetype: 'dbfield',
      tooltip: { tooltipvalue: 'Save Search', tooltipvaluetype: 'dbfield', tooltipposition: 'below' },
      inputtype: 'textbox', itemid: 'txtcount'
    },
  ];

  arrayLength: number;

  avatarDef = {
    attribute: {
      height: '40px',
      width: '40px',
      tooltip: {
        tooltipvalue: '',
        tooltipvaluetype: '',
        tooltipposition: 'below'
      }
    },
    top: '0px'
  };

  constructor(private itemLibraryDataService: ItemLibraryDataService, private itemLibraryService: ItemLibraryService) { }

  ngOnChanges() {
    this.itemJsonData = [];
    this.itemLibraryDataService.getSelectedItemPropertyData(this.jsonData.id).subscribe((result: any) => {
      this.propertyViewData = result.data.item.filter((val: any) => val.fieldId !== 'article_image' && val.fieldId !== 'article_name' &&
      val.fieldId !== 'article_code');
      this.propertyViewColorData = result.data.color;
      this.propViewData = result.data.item.filter((val: any) => val.fieldId === 'article_image');
      this.itemImg = this.propViewData[0].value;
      this.propViewData = result.data.item.filter((val: any) => val.fieldId === 'article_code');
      this.articleCode = this.propViewData[0].value;
      this.propViewData = result.data.item.filter((val: any) => val.fieldId === 'article_name');
      this.articleName = this.propViewData[0].value;

      if (result.data.color !== undefined && result.data.color.length !== 0) {
        this.viewInnerHeight = window.innerHeight - (165 + 50);
    this.viewInnerHeight = this.viewInnerHeight + (163);
    this.viewInnerdivHeight = this.viewInnerHeight + (-127);
        for (this.arrayLength = 0; this.arrayLength < 3; this.arrayLength++) {
          this.itemJsonData.push(this.propertyViewColorData[this.arrayLength]);
        }
        this.showColorSection = true;
        if (result.data.color.length > 3) {
          document.getElementById('ancShowMore').style.display = 'block';
          document.getElementById('ancShowLess').style.display = 'none';
        } else {
          document.getElementById('ancShowMore').style.display = 'none';
          document.getElementById('ancShowLess').style.display = 'none';

        }
      } else {
        this.showColorSection = false;
        this.viewInnerHeight = window.innerHeight - (165 + 50);
        this.viewInnerHeight = this.viewInnerHeight + (163);
        this.viewInnerdivHeight = this.viewInnerHeight;
      }
    });
    this.itemLibraryService.showPropertyViewClickSubscription.subscribe(message => {
      console.log(message.id);
    });
    //document.getElementById('divItemImageContainer').classList.remove('shrink')
  }
  ngOnInit() {
    const scrobj = document.getElementById('divItemImageContainer');
    this.lazyscrollObservable = merge(
      fromEvent(scrobj, 'scroll'),
      this.lazyScrollSub
    );
    this.viewInnerHeight = window.innerHeight - (165 + 50);
    this.viewInnerHeight = this.viewInnerHeight + (163);
    this.viewInnerdivHeight = this.viewInnerHeight + (-127);
  }

  avatarData(user) {
    return {
      imageUrl: user.userImageURL,
      profileName: user.fullName
    };
  }

  @HostListener('window:resize') onResize() {
    this.viewInnerHeight = window.innerHeight - (165 + 50);
    this.viewInnerHeight = this.viewInnerHeight + (163);
    this.viewInnerdivHeight = this.viewInnerHeight + (-127);
  }

  onShowMoreClick() {
    document.getElementById('ancShowLess').style.display = 'block';
    document.getElementById('ancShowMore').style.display = 'none';
    this.itemJsonData = [];
    this.itemJsonData = this.propertyViewColorData;
  }

  onShowLessClick() {
    document.getElementById('ancShowLess').style.display = 'none';
    document.getElementById('ancShowMore').style.display = 'block';
    this.itemJsonData = [];
    for (this.arrayLength = 0; this.arrayLength < 3; this.arrayLength++) {
      this.itemJsonData.push(this.propertyViewColorData[this.arrayLength]);
    }
  }
}

