import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WfxPropertyViewComponent } from './wfx-property-view.component';

describe('WfxPropertyViewComponent', () => {
  let component: WfxPropertyViewComponent;
  let fixture: ComponentFixture<WfxPropertyViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WfxPropertyViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WfxPropertyViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
