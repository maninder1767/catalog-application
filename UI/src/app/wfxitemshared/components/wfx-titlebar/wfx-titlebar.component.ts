import { ItemLibraryGlobals } from './../../../wfxplmitemlibrary/itemLibrary.global';
import { WfxComponentAttributes } from './../../../wfxlibrary/wfxsharedmodels/wfxcommonmodel';
import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ItemLibraryDataService } from '../../services/itemdata.service';
import { ItemLibraryService } from '../../services/itemlibrary.service';
import { ViewType } from '../../constants/enum';

@Component({
  selector: 'app-wfx-titlebar',
  templateUrl: './wfx-titlebar.component.html',
  styleUrls: ['./wfx-titlebar.component.css']
})
export class WfxTitlebarComponent implements OnInit {

  constructor(private httpClient: HttpClient, private itemdata: ItemLibraryDataService,
  public itemGlobal: ItemLibraryGlobals, private itemLibraryService: ItemLibraryService,) { }
  tabJsonDef;
  arrayBreadCrumb: any[];
  titleName;
  @Input() addBtnJsonDef: {
    visible?: WfxComponentAttributes,
  };

  addBtnDefaultValue =
  {
      visible: true
    };
  itemJsonDef: WfxComponentAttributes[] = [
    {
              readonly: false, mandatory: true, caption: '+', disable: false,
              value: 'fname', valuetype: 'dbfield', inputsubtype:'text',
              tooltip: { tooltipvalue: 'Add Button', tooltipvaluetype: 'dbfield', tooltipposition: 'below' },
              onchange: this.onAddButtonClick, inputtype: 'textbox', itemid: 'txtFirstName', visible: true
    }
  ];
  arrBreadCrumb;
  ngOnInit() {
    this.arrayBreadCrumb = [];
    this.arrayBreadCrumb.push({NodeName: 'Item Library', isClickable : false});
    this.itemdata.getMenuType().subscribe(val => {
      this.arrayBreadCrumb.push({NodeName: val.data, isClickable : false});
      this.titleName = val.data;
    });
    this.itemdata.getTabList().subscribe((res: any) => {
      this.tabJsonDef = res.data;
    });
  }

onAddButtonClick(action) {

    const url = '..//wfx/wfx_ArticleDetail.aspx'
    + '?ID=0&GroupID=10006425&ComboCode=01&CurrentPage=1'
    + '&CatalogCompany=&ArticleSelection=&FPerm=3&CatalogType=';
    alert("This Functionality is temporarily blocked  Redirected to : "+  url);
    //window.open(url);


   // window.open(url);
}

get visible() {
  if (this.addBtnJsonDef  && this.addBtnJsonDef.visible) {
    return true;
  } else {
    return false;
  }
}

onTabViewChangeEvent(e: any) {
  this.itemGlobal.arrSelectedItemId = [];
  this.itemGlobal.currentSelectedTab = {Code: e.selectedTabCode, Text: e.selectedTabCode, Tooltip: e.selectedTabName};
  if (e.selectedTabCode === 'COSTING') {
    this.itemGlobal.currentRightSelectedPanel = undefined;
    this.itemLibraryService.invokeshowSelectedRightPanel(true);
  }
  // if (e.selectedTabCode === "LIBRARY")  {
  //   this.itemGlobal.currentView = ViewType.GALLERY;
  // }
}
//  this.tabJsonDef: WfxTab[] = [{
//     text: 'LIBRARY', visible: false, disable: false, code: 'LIBRARY',
//     tooltip: { tooltipvalue: 'LIBRARY', tooltipposition: 'below' }
//   },
//   {
//     text: 'COASTING', visible: true, disable: false, code: 'COASTING',
//     tooltip: { tooltipvalue: 'PropertiesView', tooltipposition: 'below' }
//   },
//   {
//     text: 'T&A COCKPIT', visible: true, disable: false, code: 'ListView',
//     tooltip: { tooltipvalue: 'ListView', tooltipposition: 'below' }
//   }
// ];

}