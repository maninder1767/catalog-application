import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WfxTitlebarComponent } from './wfx-titlebar.component';

describe('WfxTitlebarComponent', () => {
  let component: WfxTitlebarComponent;
  let fixture: ComponentFixture<WfxTitlebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WfxTitlebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WfxTitlebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
