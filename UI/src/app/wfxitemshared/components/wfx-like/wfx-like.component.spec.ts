import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WfxLikeComponent } from './wfx-like.component';

describe('WfxLikeComponent', () => {
  let component: WfxLikeComponent;
  let fixture: ComponentFixture<WfxLikeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WfxLikeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WfxLikeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
