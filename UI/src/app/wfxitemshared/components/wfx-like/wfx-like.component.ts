import { WfxComponentAttributes } from './../../../wfxlibrary/wfxsharedmodels/wfxcommonmodel';
import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-like',
  templateUrl: './wfx-like.component.html',
  styleUrls: ['./wfx-like.component.css']
})
export class WfxLikeComponent implements OnInit, OnChanges {
  @Input() jsonDef: {
    attribute?: WfxComponentAttributes
    dailog?: {
      attribute?: WfxComponentAttributes,
      image?: {
        attribute?: WfxComponentAttributes
      }
    }
  };
  @Input() jsonData: {
    likeCount?: number,
    isLike: boolean,
    userData: any[]
  };

  @Output() onLike = new EventEmitter();
  @Output() onShowDetails = new EventEmitter();

  showSpiner: boolean;
  defaultDef: WfxComponentAttributes;
  defaultDialogDef: WfxComponentAttributes;
  isShowDetail: boolean;
 // dialog2Ref;
  constructor(public dialog: MatDialog) { }

  ngOnInit() {
    this.defaultDef = {
      tooltip: {
        tooltipvalue: '',
      },
      disable: false,
      visible: true
    };
  }

  ngOnChanges() {
  }

 /*** Event Section Starts */
 /**
 * Created By : Ankit Gupta
 * Created On : 16/Aug/2019
 * Purpose : This event is for call from html.
 */
  get dialogwidth() {
    if (this.jsonDef.dailog && this.jsonDef.dailog.attribute.width) {
      return this.jsonDef.dailog.attribute.width;
    }
    return this.defaultDialogDef.width;
  }
  get dialogHeight() {
    if (this.jsonDef.dailog && this.jsonDef.dailog.attribute.height) {
      return this.jsonDef.dailog.attribute.height;
    }
    return this.defaultDialogDef.height;
  }

  get likeCount() {
    return  this.jsonData.likeCount;
  }

  get isLike() {
    return this.jsonData.isLike;
  }

  get dailogJsonDef() {
    if (this.jsonDef && this.jsonDef.dailog) {
      return [{...this.defaultDialogDef}, {...this.jsonDef.dailog}];
    }
    return [{...this.defaultDialogDef}];
  }

  get userData() {
      return this.jsonData.userData;
  }

  get visible() {
    if (this.jsonDef && this.jsonDef.attribute && this.jsonDef.attribute.visible === false) {
      return false;
    }
    return true;
  }

  get width() {
    if (this.jsonDef && this.jsonDef.attribute && this.jsonDef.attribute.width) {
      return this.jsonDef.attribute.width;
    }
    return this.defaultDef.width;
  }

  get height() {
    if (this.jsonDef && this.jsonDef.attribute && this.jsonDef.attribute.height) {
    return this.jsonDef.attribute.height;
    }
    return this.defaultDef.height;
  }

  get title() {
    if (this.jsonDef && this.jsonDef.attribute && this.jsonDef.attribute.tooltip) {
      return this.jsonDef.attribute.tooltip;
    }
    return this.defaultDef.tooltip.tooltipvalue;
  }

  // get fillColor() {
  //   if (this.jsonDef && this.jsonDef.attribute && this.jsonDef.attribute.color) {
  //     return this.jsonDef.attribute.color;
  //   }
  //   return this.defaultDef.color;
  // }

  get dialogData() {
    //this.dialog2Ref.close();
    return {
      likedByUsers: [...this.jsonData.userData],
      noOfLikes: this.jsonData.likeCount,
      itemJsonDef: this.dailogJsonDef,
      avatar: this.jsonDef.dailog.image
    };
  }

 /*** Method Section Starts */
 /**
 * Created By : Urvashi Sharma
 * Created On : 10/Aug/2019
 * Purpose : This event is for call parent component with some perameter.
 */
  onUserData(data) {
    this.jsonData.userData = data;
    if (this.jsonData && this.jsonData.userData && this.jsonData.userData.length > 0) {
      this.showSpiner = false;
      this.displayDialog();
    }
  }

/*** Method Section Ends */

 /*** Method Section Starts */
 /**
 * Created By : Ankit Gupta
 * Created On : 16/Aug/2019
 * Purpose : Internal Implemention of the component.
 */
  onLikeClick() {
    if (this.jsonDef && this.jsonDef.attribute && this.jsonDef.attribute.disable) {
      return;
    }
    this.jsonData.isLike = !this.jsonData.isLike;
    if (this.jsonData.isLike) {
      this.jsonData.likeCount++;
    } else {
      this.jsonData.likeCount--;
    }
    this.onLike.emit(this.jsonData.isLike);
  }

  onShowDetailsClick() {
    if (this.jsonDef && this.jsonDef.attribute && this.jsonDef.attribute.disable) {
      return;
    }
    if (this.jsonData.likeCount > 0) {
      this.isShowDetail = true;
      this.showSpiner = true;
     // this.displaySpinnerDialog();
      this.onShowDetails.emit(this.likeCount);
   }
  }
  private displayDialog() {
      const dialogRef = this.dialog.open(LikeDialogComponent, {
        // height: this.dialogHeight,
        // width: this.dialogwidth,
        height: '80',
        width: '80',
        data: this.dialogData,
        panelClass: 'widgetcont'
    });
    dialogRef.afterClosed().subscribe(() => {
      this.jsonData.userData = [];
      this.isShowDetail = false;
    });
  }

  // private displaySpinnerDialog() {
  //   this.dialog2Ref = this.dialog.open(LikeSpinnerDialogComponent, {
  //     // height: this.dialogHeight,
  //     // width: this.dialogwidth
  //     height: '80',
  //     width: '80',
  // });
  // this.dialog2Ref.afterClosed().subscribe(() => {
  //  this.isShowDetail = false;
  // });
  // }
/*** Method Section Ends */
}


@Component({
  selector: 'app-wfx-likedialog',
  templateUrl: './wfx-likedialog.component.html',
  styleUrls: ['./wfx-like.component.css']
})
export class LikeDialogComponent implements OnInit {
  likedByUsers: any[];
  noOfLikes = 0;
  avatarDef;
  // jsonDef: any;
  // defaultValue: any;

  constructor(public dialogRef: MatDialogRef<LikeDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { likedByUsers: any[], noOfLikes: number, itemJsonDef: [] , avatar: any}) { }

  ngOnInit() {
    this.avatarDef = this.data.avatar;
  }

 /*** Event Section Starts */
 /**
 * Created By : Ankit Gupta
 * Created On : 16/Aug/2019
 * Purpose : This event is for call from html.
 */
  get likeCount() {
    return this.data.noOfLikes || this.noOfLikes;
  }

  get users() {
    return this.data.likedByUsers || this.likedByUsers;
  }
 /*** Event Section End */

 /*** Method Section Starts */
 /**
 * Created By : Ankit Gupta
 * Created On : 16/Aug/2019
 * Purpose : Internal Implemention of the component.
 */
  avatarData(user) {
    return {
      imageUrl: user.userImageURL,
      profileName: user.fullName
    };
  }
   /*** Method Section Ends */

}


// @Component({
//   selector: 'app-wfx-likespinnerdialog',
//   template: `<div>
//   <img src="./assets/100w.gif" style="border:0px;" alt="">
//   </div>`,
//   styleUrls: ['./wfx-like.component.css']
// })
// export class LikeSpinnerDialogComponent implements OnInit {
//   constructor(public dialogRef: MatDialogRef<LikeSpinnerDialogComponent>,
//     @Inject(MAT_DIALOG_DATA) public data) { }

//   ngOnInit() {
//   }
// }
