import { Component, OnInit, OnChanges, AfterViewChecked, Input, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { WfxComponentAttributes } from '../../../wfxlibrary/wfxsharedmodels/wfxcommonmodel';
@Component({
  selector: 'app-wfx-slider',
  templateUrl: './wfx-slider.component.html',
  styleUrls: ['./wfx-slider.component.css']
})
/*
  * Created By : Kanhaiya Sharma
  * Created On : 10-09-2019
  * Purpose : slider
  * Last Modified Date : 13-09-2019
  * Last Modified By : Kanhaiya Sharma G
  */
 export class WfxSliderComponent implements OnInit, OnChanges, AfterViewChecked {

  @Input() jsonData: {
    sliderDefaultvalue: number,
    step: number,
    maxValue: number
  };
  @Input() jsonDef: {
    attribute: WfxComponentAttributes
  };
  @Output() SliderValueChange = new EventEmitter<string>();

  defaultValue =
  {
    visible: true,
    disable: false,
    sliderDefaultValue: 50,
    step : 1,
    maxValue: 100
  };
  constructor(private cdr: ChangeDetectorRef) { }

  ngOnInit() {
  }
  ngOnChanges() {
  }
  ngAfterViewChecked() {
    this.cdr.detectChanges();
  }
  get hideSlider(){
    if (this.jsonDef && this.jsonDef.attribute && this.jsonDef.attribute.visible === false) {
      return false;
    }
    return true;
    }
  get enableSlider() {
    if (this.jsonDef && this.jsonDef.attribute.disable) {
      return this.jsonDef.attribute.disable;
    }
    return this.defaultValue.disable;
   }
   get sliderDefaultValue() {
    if (this.jsonData && this.jsonData.sliderDefaultvalue) {
      return this.jsonData.sliderDefaultvalue;
    }
    return this.defaultValue.sliderDefaultValue;
   }
   get sliderStep(){
    if (this.jsonData && this.jsonData.step) {
      return this.jsonData.step;
    }
    return this.defaultValue.step;
    }

    get sliderMaxValue(){
      if (this.jsonData && this.jsonData.maxValue) {
        return this.jsonData.maxValue;
      }
      return this.defaultValue.maxValue;
      }
   /*
  * Created By : Kanhaiya Sharma
  * Created On : 10-09-2019
  * Purpose : on change
  * Last Modified Date : 13-09-2019
  * Last Modified By : Kanhaiya Sharma
  */
   onChange(range) {
    this.SliderValueChange.emit(range.value);
  }
  visible(isVisible: boolean) {
    // this.hideSlider = isVisible;
  }
}
