import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WfxSliderComponent } from './wfx-slider.component';

describe('WfxSliderComponent', () => {
  let component: WfxSliderComponent;
  let fixture: ComponentFixture<WfxSliderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WfxSliderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WfxSliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
