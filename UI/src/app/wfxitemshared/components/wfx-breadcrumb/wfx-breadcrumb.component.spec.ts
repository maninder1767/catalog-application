import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WfxBeadcrumbComponent } from './wfx-beadcrumb.component';

describe('WfxBeadcrumbComponent', () => {
  let component: WfxBeadcrumbComponent;
  let fixture: ComponentFixture<WfxBeadcrumbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WfxBeadcrumbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WfxBeadcrumbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
