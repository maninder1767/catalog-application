import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';

declare var $: any;
@Component({
  selector: 'app-wfx-breadcrumb',
  templateUrl: './wfx-breadcrumb.component.html',
  styleUrls: ['./wfx-breadcrumb.component.css']
})
export class WfxBreadcrumbComponent implements OnInit, OnChanges {
  @Input() arrBreadCrumb: any[];
  @Output() clickBreadCrumb = new EventEmitter<any>();
  arrayBreadCrumb: any[];
  constructor() { }
  ngOnInit() {
   this.arrayBreadCrumb = this.arrBreadCrumb;
  }
  ngOnChanges() {
    // this.arrayBreadCrumb = this.arrayBreadCrumb;

  }
  onClickBreadCrumb(node) {
      this.clickBreadCrumb.emit(node);
      console.log('onclick node breadcrumb', node);
      return false;
  }
}
