/*
WFXControls v1.0.0 (2019-April)
Copyright (c) 2019-2019 WFX.
License: https://www.wfxcloud.com/contact.html
*/
/* eslint-disable */

import {
    Component, Input, Output, EventEmitter, ElementRef, OnChanges, SimpleChanges, OnInit,
    ChangeDetectorRef, AfterViewChecked
} from '@angular/core';
import { WfxCommonFunctions } from '../../wfxlibrary/wfxsharedfunction/wfxcommonfunction';
import { WfxComponentAttributes } from '../../../app/wfxlibrary/wfxsharedmodels/wfxcommonmodel';


@Component({
    selector: 'app-wfxcolorcard',
    template: `
    <form>
    <div class="properties-color-list"  *ngFor="let color of itemJsonData">
        <div *ngIf="color.articlecolor_image_url === undefined || color.articlecolor_image_url === '' "
         style="display:block" class="productLifeCycleStage product-life-cycle-circle-icon" 
        [style.background-color]=rgbColor(color.articlecolor_firstcolor_rgb)>
        </div>
        <div class="properties-color-img" *ngIf="color.articlecolor_image_url !== undefined && color.articlecolor_image_url !== ''">
        <img class="productLifeCycleStage product-life-cycle-circle-icon"
          [src]=color?.articlecolor_image_url></div>
        <div class="properties-color-name">
        <app-wfxlabel #lbl [ParentComponent]="wfxColorCard" [itemJsonDef]="ItemJsonDef"
        [sectionJsonData]=colorJsonData(color.articlecolor_name)></app-wfxlabel>
        </div>
    </div>
    </form>`
})


export class WfxcolorcardComponent implements OnInit,OnChanges {
    constructor(
        containerElement: ElementRef, private cdr: ChangeDetectorRef,
        public cmf: WfxCommonFunctions) {
    }
    @Input() itemJsonData;
    @Input() itemJsonDef;
    ItemJsonDef = {
        readonly: false, mandatory: false, caption: '', disable: false,
        value: 'Label', valuetype: 'dbfield',
        tooltip: { tooltipvalue: '', tooltipvaluetype: 'dbfield', tooltipposition: 'below' },
        inputtype: 'textbox', itemid: 'txtcount'
    };

    ngOnChanges(){
        this.itemJsonData;
    }

    rgbColor(color) {
        if (color) {
            return 'rgb(' + color.split('-').toString() + ')';
        }
    }

    colorJsonData(colorName) {
        return { Label: colorName };
    }
    ngOnInit() {
    }
}
