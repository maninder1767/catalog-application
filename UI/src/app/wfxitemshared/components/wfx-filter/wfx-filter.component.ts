
import { WfxShowMessageservice } from './../../../wfxlibrary/wfxsharedservices/wfxshowmessage.service';
import { MatDialog } from '@angular/material';
import { WfxSaveSearchComponent } from './../wfx-save-search/wfx-save-search.component';
import { jqxCalendarComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxcalendar';
import { jqxDateTimeInputComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxdatetimeinput';
import {
  Component, OnInit, OnChanges, Input, Output, EventEmitter, HostListener,
  AfterViewInit, ViewChild, ElementRef, PipeTransform, Pipe, ChangeDetectorRef, AfterViewChecked, AfterContentChecked
} from '@angular/core';
import { FilterType, ApplyFilter, FilterSubType, Filter, SearchParams, FilterCriteria} from '../../modals/filter';
import { ItemLibraryService } from '../../services/itemlibrary.service';
import { HttpClient } from '@angular/common/http';
import { ControlType, FieldType } from '../../constants/enum';
import { WfxComponentAttributes } from '../../../wfxlibrary/wfxsharedmodels/wfxcommonmodel';
import { WfxFilterService } from './wfx-filter.service';
import { ItemLibraryDataService } from '../../services/itemdata.service';
import { ItemLibraryGlobals } from '../../../wfxplmitemlibrary/itemLibrary.global';

declare var $: any;
@Component({
  selector: 'app-wfx-filter',
  templateUrl: './wfx-filter.component.html',
  styleUrls: ['./wfx-filter.component.css']
})
export class WfxFilterComponent implements OnInit, OnChanges, AfterViewInit, AfterViewChecked, AfterContentChecked {

  @ViewChild('calendarReference', { static: false }) calendarReference: jqxCalendarComponent;
  @Output() filterChange = new EventEmitter<Filter>();
  @Output() applyFilterChange = new EventEmitter<Filter>();
  @Output() filterApplied = new EventEmitter<ApplyFilter>();
  @Input() filter: Filter = new Filter();
  @Input() jsonDef = new JsonDef();
  @Input()
  set filterPanelStatus(filterPanelStatus: boolean) {
    if (!filterPanelStatus) { this.arrFilterSubType = []; }
  }
 // @Input() jsonData: any;
  @ViewChild('search', { static: false }) search: ElementRef;
  selectedFilterType = new FilterType();
  arrFilterSubType: FilterSubType[] = [];
  viewInnerHeight: number;
  filterDefinitionHeight: number;
  selectedMenuType = '';
  optionFilter: number;
  selectRadioFilter: number;
  advanceSearch: ApplyFilter[];
  applyFilter: ApplyFilter = new ApplyFilter({}, new SearchParams('', []), [], []);
  wfxslidetoggleJsonDef: WfxComponentAttributes;
  wfxslidetogglejsonData;
  actveFilter: string;
  // applyFilter: ApplyFilter = new ApplyFilter();

  filterCriteria: Array<FilterCriteria> = [];
  arrCreatedOnType: any[];
  arrLastChangedOnType: any[];
  SavedFiterCriteriaData: FilterCriteria[] = [];
  filterData = new Filter();
  defaultSVGJsonDef = { width: '5px', height: '7px', opacity: '0.9' };

  itemJsonDef: any[] = [
    {
      readonly: false, itemid: 'ddlFilters', mandatory: false, caption: 'Search', valuetype: '', value: '', height: '',
      valuetext: 'optionname', multiselect: true, disable: false, inputtype: 'select', ddlvalue: 'id', visible: true,
      ddlvaluetext: 'text', addsearchoptioninddl: true, addclearicon: false, addimginddl: false, floatlabel: 'never', inputsubtype: 'text',
      action: true, tooltip: { tooltipvalue: '', tooltipvaluetype: '', tooltipposition: 'below' },
      ckeckBoxJsonDef: {
        itemid: '', readonly: false, mandatory: false, caption: '', inputsubtype: 'text',
        value: 'checkBoxValue', disable: false, labelposition: 'after', valuetype: 'dbfield', visible: true,
        tooltip: { tooltipvalue: '', tooltipvaluetype: '', tooltipposition: 'below' }
      }
    },
  ];

  dateData = { id: -1, value: '', text: '', IsSelected: false };

  @HostListener('window:resize') onResize() {
    this.viewInnerHeight = (window.innerHeight - $('#filterList').offset().top);
    if (this.itemGlobal.arrSelectedItemId.length > 0) {
      this.filterDefinitionHeight = this.viewInnerHeight + (-219);
      this.itemJsonDef[0].height = this.viewInnerHeight + (- 119) + 'px';
    } else {
      this.filterDefinitionHeight = this.viewInnerHeight + (-169);
      this.itemJsonDef[0].height = this.viewInnerHeight + (- 69) + 'px';
    }
  }
  
  @HostListener('window:click') onClick() {
    this.viewInnerHeight = (window.innerHeight - $('#filterList').offset().top);
    setTimeout(() => {
      if (this.itemGlobal.arrSelectedItemId.length > 0) {
        this.filterDefinitionHeight = this.viewInnerHeight + (-219);
        this.itemJsonDef[0].height = this.viewInnerHeight + (- 119) + 'px';
      } else {
        this.filterDefinitionHeight = this.viewInnerHeight + (-169);
        this.itemJsonDef[0].height = this.viewInnerHeight + (- 69) + 'px';
      }
    }, 1);
  }
  constructor(private cd: ChangeDetectorRef, private itemLibraryService: ItemLibraryService, public dialog: MatDialog,
    private http: HttpClient, private wfxFilterService: WfxFilterService,
    private itemLibraryDataService: ItemLibraryDataService,
    private itemGlobal: ItemLibraryGlobals, public msgsvc: WfxShowMessageservice) { }
  ngAfterViewInit() {
    // setTimeout(() => {
    //   if (this.itemGlobal.arrSelectedItemId.length > 0) {
    //     this.filterDefinitionHeight = this.viewInnerHeight + (-272);
    //     this.itemJsonDef[0].height = this.viewInnerHeight + (- 175) + 'px';
    //   } else {
    //     this.filterDefinitionHeight = this.viewInnerHeight + (-222);
    //     this.itemJsonDef[0].height = this.viewInnerHeight + (- 125) + 'px';
    //   }
    // }, 1);
    this.cd.detectChanges();
  }
  ngAfterViewChecked() {
    // setTimeout(() => {
    //   this.viewInnerHeight = (window.innerHeight - $('#filterList').offset().top);
    //   if (this.itemGlobal.arrSelectedItemId.length > 0) {
    //     this.filterDefinitionHeight = this.viewInnerHeight + (-292);
    //     this.itemJsonDef[0].height = this.viewInnerHeight + (- 195) + 'px';
    //   } else {
    //     this.filterDefinitionHeight = this.viewInnerHeight + (-222);
    //     this.itemJsonDef[0].height = this.viewInnerHeight + (- 125) + 'px';
    //   }
    //   }, 1);
  }
  ngAfterContentChecked() {
    // setTimeout(() => {
    //   this.viewInnerHeight = (window.innerHeight - $('#filterList').offset().top);
    //   if (this.itemGlobal.arrSelectedItemId.length > 0) {
    //     this.filterDefinitionHeight = this.viewInnerHeight + (-292);
    //     this.itemJsonDef[0].height = this.viewInnerHeight + (- 195) + 'px';
    //   } else {
    //     this.filterDefinitionHeight = this.viewInnerHeight + (-222);
    //     this.itemJsonDef[0].height = this.viewInnerHeight + (- 125) + 'px';
    //   }
    //   }, 1);
  }
  onTextSearch() { }
  ngOnInit() {
    this.viewInnerHeight = (window.innerHeight - $('#filterList').offset().top);
      if (this.itemGlobal.arrSelectedItemId.length > 0) {
        this.filterDefinitionHeight = this.viewInnerHeight + (-219);
        this.itemJsonDef[0].height = this.viewInnerHeight + (- 119) + 'px';
      } else {
        this.filterDefinitionHeight = this.viewInnerHeight + (-169);
        this.itemJsonDef[0].height = this.viewInnerHeight + (- 69) + 'px';
      }
   
    this.itemLibraryService.filter
      .subscribe(message => {
        this.filter.filterText = message.filter.filterText;
      });
    // this.itemLibraryService.filterChipSubscription
    //   .subscribe(message => {
    //     this.filterChip = message.filterChip;
    //   });
    this.wfxslidetoggleJsonDef = {
      readonly: false, mandatory: false, caption: 'Active', valuetype: 'dbfield',
      value: 'SwitchOn', disable: false,
      tooltip: { tooltipvalue: 'Active', tooltipvaluetype: '', tooltipposition: 'below' },
    };
    this.wfxslidetogglejsonData = {
      SwitchOn: true
    };

    if (this.itemGlobal.itemActiveToggleModel.filterData[0].values[0].value.toString() === 'false') {
      this.wfxslidetogglejsonData.SwitchOn = false;
    } else {
      this.wfxslidetogglejsonData.SwitchOn = true;
    }

  }
  ngOnChanges() {

  //  this.filter = this.jsonData.filter;
  }
  onCheckBoxChanged(event, arrFilterSubType) {
    if (this.arrFilterSubType.length > 0) {
      const index = this.arrFilterSubType.findIndex(x => x.id === event.id);
      if (index >= 0) {
        this.arrFilterSubType[index].IsSelected = !this.arrFilterSubType[index].IsSelected;
        this.setSelectedValues();
      }
    }
  }
  onTextAreaInput(event) {
    if (this.filter.arrFilterType.length > 0) {
      const index = this.filter.arrFilterType.findIndex(x => x.fieldId === event.filtertype.fieldId);
      if (index >= 0) {
        this.filter.arrFilterType[index].filterValue = event.event;
      }
    }
  }

  OnSliderToggleChange(data) {
    // this.slidervalue = data.SwitchOn;
   this.itemGlobal.isActiveItem = data.SwitchOn;
    if (data.SwitchOn === true) {
      this.wfxslidetoggleJsonDef.tooltip.tooltipvalue = 'Active';
      this.itemGlobal.itemActiveToggleModel.filterData[0].values[0].value = 'true'
    } else {
      this.wfxslidetoggleJsonDef.tooltip.tooltipvalue = 'Inactive';
      this.itemGlobal.itemActiveToggleModel.filterData[0].values[0].value = 'false';
    }
    this.itemLibraryDataService.activeItemToggle(data.SwitchOn);
  }
  setSelectedValues() {
    if (this.filter.arrFilterType.length > 0) {
      const index = this.filter.arrFilterType.findIndex(x => x.fieldId === this.selectedFilterType.fieldId);
      if (index >= 0) {
        this.filter.arrFilterType[index].filterValue = this.getSelectedFilter(this.filter.arrFilterType[index]);
      }
    }
  }
  onSelectAction(value) {
    if (this.arrFilterSubType.length > 0) {
      const index = this.arrFilterSubType.findIndex(x => x.id === value);
      if (index >= 0) {
        this.arrFilterSubType[index].IsSelected = !this.arrFilterSubType[index].IsSelected;
        this.setSelectedValues();
      }
    }
  }
  getTextAreaJsonDef(filtertype: FilterType) {
    return {
      readonly: filtertype.controlType !== 'TEXT' ? true : false, mandatory: false, caption: filtertype.caption, disable: false,
      value: 'fname', valuetype: 'dbfield', inputsubtype: 'text', visible: true,
      tooltip: { tooltipvalue: 'First Name', tooltipvaluetype: 'dbfield', tooltipposition: 'below' },
      inputtype: 'textbox', itemid: 'txtTextbox'
    };
  }
  getTextAreaJsonData(filtertype) {
    return {
      fname: filtertype.caption, SwitchOn: false,
    };
  }
  onFilterExpand(filtertype: FilterType) {
    this.selectedFilterType = filtertype;
    this.actveFilter = filtertype.fieldId;
    this.getSubFilters(filtertype);
  }
  setRange(filtertype: any) {
    const filter = this.filter.arrFilterType.find(f => f.caption === filtertype.caption);
    if (filter.dateFrom !== '' && filter.dateTo !== '') {
      const dateFrom = filter.dateFrom.valueOf();
      const dateTo = filter.dateTo.valueOf();
      this.calendarReference.clear();
      this.calendarReference.setRange(new Date(dateFrom), new Date(dateTo));
      this.setSelectedValues();
    } else {
      this.calendarReference.clear();
    }
  }
  onRangeChange($event, filtertype: any) {
    const selection = $event.args.range;
    const filter = this.filter.arrFilterType.find(f => f.caption === filtertype.caption);
    filter.dateFrom = selection.from !== null ? selection.from.toDateString().valueOf() : '';
    filter.dateTo = selection.to !== null ? selection.to.toDateString().valueOf() : '';
    this.setSelectedValues();
  }
  onApplyFilter() {
    let hasFilter = false;
    for (let index = 0; index < this.filter.arrFilterType.length; index++) {
      const filter = this.filter.arrFilterType[index];
      if (filter.controlType === ControlType.DATE && filter.dateFrom !== '' && filter.dateTo !== '') {
        hasFilter = true;
      } else {
        if (filter.arrFilterSubType.findIndex(f => f.IsSelected === true) > -1) {
          hasFilter = true;
        }
        if (filter.controlType === ControlType.TEXT) {
          if (filter.filterValue !== null && filter.filterValue !== undefined && filter.filterValue !== '') {
            if (filter.filterValue.trim()) {
              hasFilter = true;
            }
          }
        }
      }
      this.filter.isFilterApplied = hasFilter;
    }
    this.applyFilterChange.emit(this.filter);
    this.arrFilterSubType = [];
    this.filterJson(this.filter);
  }
  onClearClick() {
    this.filter.isFilterApplied = false;
    this.filter.arrFilterType.forEach(function (filtertype, index) {
      filtertype.dateFrom = '';
      filtertype.dateTo = '';
      filtertype.filterValue = '';
      filtertype.arrFilterSubType.forEach(function (filterSubType, ifIndex) {
        filterSubType.IsSelected = false;
      });
    });
    this.filter.filterText = '';
    this.filterChange.emit(this.filter);
  }
  onFilterSubTypeChange($event) {
    $event.source.options._results.forEach(element => {
      const isSelected = element.selected;
      const optionText: string = element._element.nativeElement.innerText.trim();
      this.arrFilterSubType.find(a => a.value === optionText).IsSelected = isSelected;
    });
  }
  isFilterSelected(filtertype: any) {
    let bReturn = false;
    if (filtertype.ControlType === ControlType.DATE && filtertype.DateFrom !== '' && filtertype.DateTo !== '') {
      bReturn = true;
    } else {
      const arrFilterSubType = this.filter.arrFilterType
        .find(f => f.caption === filtertype.caption)
        .arrFilterSubType.find(f => f.IsSelected === true);
      if (arrFilterSubType) {
        bReturn = true;
      }
    }
    return bReturn;
  }
  getSelectedFilter(filtertype: FilterType) {
    return this.wfxFilterService.getSelectedFilter(this.filter, filtertype);
  }

  openDateFilter(calender: jqxDateTimeInputComponent) {
    calender.open();
  }
  OnSelectFilterSubTypeChange(event) {
    this.arrFilterSubType.forEach(function (item, index) {
      if (item.id === event.value) {
        item.IsSelected = true;
      } else {
        item.IsSelected = false;
      }
    });
  }
  onUserFilterApply(value) {
    this.selectedFilterType.filterValue = value;
  }
  filterJson(filter: Filter) {
    this.wfxFilterService.filterJson(filter).subscribe((appliedFilterResp: ApplyFilter) => {
      this.filterApplied.emit(appliedFilterResp);
    });
  }

  getSubFilters(filtertype: FilterType) {
    this.itemJsonDef[0].caption = 'Search ' + filtertype.caption;
    this.itemJsonDef[0].tooltip.tooltipvaluetype = 'Search ' + filtertype.caption;

    const index = this.wfxFilterService.SavedfiterCriteriaData.findIndex(x => x.fieldId === filtertype.fieldId);
    if ((filtertype.arrFilterSubType.length === 0 || (index >= 0 || filtertype.arrFilterSubType[0].id === -1)) &&
    (filtertype.controlType === ControlType.MULTISELECT || filtertype.controlType === ControlType.SELECT)) {
      if (filtertype.fieldType.toUpperCase() === FieldType.FIXED) {

        this.itemLibraryDataService.getFixedFieldAdvanceFilterData('Item', filtertype.dataSource).subscribe((result: any) => {
         if (result.status === 'Success' ) {
            if (result.data && result.data.length > 0) {
                         filtertype.arrFilterSubType = result.data;
              this.modifySubfilterUsingSavedFilters(filtertype);
            } else {
            const  noReordFound = { id: -1, value: result.message, text: result.message, IsSelected: false };
              // filtertype.arrFilterSubType = [noReordFound];
              filtertype.arrFilterSubType = [noReordFound];
              this.arrFilterSubType = [noReordFound];
            }
          }
        },
        error => {
          const noReordFound = { id: -1, value: '', text: '', IsSelected: false };
          filtertype.arrFilterSubType = [noReordFound];
          this.arrFilterSubType = [noReordFound];
          console.log(error);
        });
      } else if ((filtertype.fieldType.toUpperCase() === FieldType.CUSTOM || filtertype.fieldType.toUpperCase() === FieldType.FLEXI)) {
        this.itemLibraryDataService.getCustomFieldAdvanceFilterData('Item', filtertype.dataSource).subscribe((result: any) => {
          if (result.status === 'Success' ) {
            if (result.data && result.data.length > 0) {
              filtertype.arrFilterSubType = result.data;
              this.modifySubfilterUsingSavedFilters(filtertype);
            } else {
              // filtertype.arrFilterSubType = [];
              const  noReordFound = { id: -1, value: result.message, text: result.message, IsSelected: false };
              filtertype.arrFilterSubType = [noReordFound];
              this.arrFilterSubType = [noReordFound];
            }
          }
        },
        error => {
          const noReordFound = { id: -1, value: '', text: '', IsSelected: false };
          filtertype.arrFilterSubType = [noReordFound];
          this.arrFilterSubType = [noReordFound];
          console.log(error);
        });
      }
    } else if ((filtertype.arrFilterSubType.length === 0 || (index >= 0 || filtertype.arrFilterSubType[0].id === -1)) &&
    filtertype.controlType === ControlType.DATE) {
      filtertype.arrFilterSubType = [this.dateData];
      this.modifySubfilterUsingSavedFilters(filtertype);
    } else if ((filtertype.arrFilterSubType.length === 0 || (index >= 0)) && filtertype.controlType === ControlType.CHECKBOX) {
    const  checkboxData = [{ id: 1, value: 'Yes', text: 'Yes', IsSelected: false },
                           { id: 2, value: 'No', text: 'No', IsSelected: false }];

      filtertype.arrFilterSubType = checkboxData;
      this.modifySubfilterUsingSavedFilters(filtertype);
    } else {
      this.localData(filtertype);
    }
  }

  localData(filtertype: FilterType) {
    this.selectedFilterType = filtertype;
    this.arrFilterSubType = [];
    this.arrFilterSubType = filtertype.arrFilterSubType;
    if (filtertype.controlType === ControlType.SELECT) {
      if (this.arrFilterSubType.find(x => x.IsSelected === true)) {
        this.selectRadioFilter = this.arrFilterSubType.find(x => x.IsSelected === true).id;
      }
    }
    if (filtertype.controlType === ControlType.DATE) {
      if (this.calendarReference) {
        this.setRange(filtertype);
      } else {
        setTimeout(() => {
          this.setRange(filtertype);
        }, 500);
      }
    } else { }
  }

  modifySubfilterUsingSavedFilters(filtertype: FilterType) {
    if (this.wfxFilterService.SavedfiterCriteriaData.length > 0) {
      const savedIndex = this.wfxFilterService.SavedfiterCriteriaData.findIndex(x => x.fieldId === filtertype.fieldId);
      if (savedIndex >= 0 ) {
   this.wfxFilterService.rebindingSavedFilters(this.filter, this.wfxFilterService.SavedfiterCriteriaData, filtertype)
                         .subscribe(response => {
    this.filterData = response;
    this.wfxFilterService.SavedfiterCriteriaData.splice(savedIndex, 1);
    this.localData(filtertype);
   });
  } else {
    this.localData(filtertype);
  }
} else {
  this.localData(filtertype);
}
  }

  get opacity() {
    if (this.jsonDef.opacity) {
      return this.jsonDef.opacity;
    }
    return this.defaultSVGJsonDef.opacity;
  }
}


@Pipe({
  name: 'filter'
})
export class FilterSubTypePipe implements PipeTransform {
  transform(items: any[], filterBy: string): any[] {
    if (!items) { return []; }
    return items.filter(item => item.FilterSubType.toLowerCase().indexOf(filterBy.toLowerCase()) !== -1);
  }
}

export class JsonDef {
  wfxComponentAttributes: WfxComponentAttributes;
  opacity: string;
}
