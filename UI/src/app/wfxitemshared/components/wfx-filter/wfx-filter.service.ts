import { Injectable } from '@angular/core';
import { FilterCriteria, Filter, ApplyFilter, SearchParams, FilterSubType, FilterType, Values, FilterData } from '../../modals/filter';
import { ControlType, FieldType } from '../../constants/enum';
import { of } from 'rxjs';
import { Globals } from '../../../globals';

@Injectable(
  {
    providedIn: 'root'
  }
)
export class WfxFilterService {

  applyFilter: ApplyFilter = new ApplyFilter({}, new SearchParams('', []), [], []);
  filter: Filter = new Filter();
  SavedfiterCriteriaData: FilterCriteria[] = [];
  constructor(private globals: Globals) { }

  rebindingSavedFilters(filter: Filter, SavedfiterCriteria: FilterCriteria[], filtertype?: FilterType) {
    console.log('SavedfiterCriteria Data Error', SavedfiterCriteria);
    this.SavedfiterCriteriaData = SavedfiterCriteria;
    if (filter !== null && filter !== undefined) {
      this.onClearFilter(filter, filtertype).subscribe((RespFilter: Filter) => {
        filter = RespFilter;
        filter.isFilterApplied = true;
        this.applyFilter.searchparams.filterCriteria = SavedfiterCriteria;
        SavedfiterCriteria.forEach(element => {
          const _filterType = filter.arrFilterType.find(x => x.fieldId === element.fieldId);
          _filterType.filterValue = '';
          if (_filterType.arrFilterSubType.length === 0) {
            _filterType.caption = element.caption;
            _filterType.fieldId = element.fieldId;
            _filterType.controlType = element.controlType;
            _filterType.fieldType = element.fieldType;
            if (element.filterData.length > 0) {
              element.filterData.forEach(filterData => {
                if (filterData.values.length > 0) {
                  if (element.controlType === ControlType.MULTISELECT || element.controlType === ControlType.SELECT ||
                    element.controlType === ControlType.CHECKBOX) {
                    filterData.values.forEach((values, index) => {
                      const _subFilterType = new FilterSubType();
                      _subFilterType.IsSelected = true;
                      _subFilterType.text = values.text;
                      _subFilterType.value = values.value;
                      _subFilterType.id = values.id;
                      _filterType.filterValue = _filterType.filterValue + values.text + ', ';
                      _filterType.arrFilterSubType.push(_subFilterType);
                    });
                    _filterType.filterValue = _filterType.filterValue.substring(0, _filterType.filterValue.length - 2);
                  } else if (element.controlType === ControlType.TEXT) {
                    filterData.values.forEach(values => {
                      _filterType.filterValue = values.text;
                    });
                  } else if (element.controlType === ControlType.DATE) {
                    filterData.values.forEach((values, i) => {
                      if (i === 0) {
                        _filterType.dateFrom = values.text;
                      } else if (i === 1) {
                        _filterType.dateTo = values.text;
                      }
                      _filterType.filterValue = _filterType.dateFrom + ' - ' + _filterType.dateTo;
                    });
                  }
                }
              });
            }
          } else {
            if (element.filterData.length > 0) {
              element.filterData.forEach(filterData => {
                if (filterData.values.length > 0) {
                  if (element.controlType === ControlType.MULTISELECT || element.controlType === ControlType.SELECT ||
                    element.controlType === ControlType.CHECKBOX) {
                    filterData.values.forEach(values => {
                      const subIndex = _filterType.arrFilterSubType.findIndex(x => x.id === values.id);
                      if (subIndex >= 0) {
                        _filterType.arrFilterSubType[subIndex].IsSelected = true;
                        _filterType.filterValue = _filterType.filterValue + values.text + ', ';
                      }
                    });
                    _filterType.filterValue = _filterType.filterValue.substring(0, _filterType.filterValue.length - 2);
                  } else if (element.controlType === ControlType.TEXT) {
                    filterData.values.forEach(values => {
                      const subIndex = _filterType.arrFilterSubType.findIndex(x => x.id === values.id);
                      if (subIndex >= 0) {
                        _filterType.filterValue = values.text;
                      }
                    });
                  } else if (element.controlType === ControlType.DATE) {
                    filterData.values.forEach((values, i) => {
                      if (i === 0) {
                        _filterType.dateFrom = values.text;
                      } else if (i === 1) {
                        _filterType.dateTo = values.text;
                      }
                      _filterType.filterValue = _filterType.dateFrom + ' - ' + _filterType.dateTo;
                    });
                  }
                }
              });
            }
          }
        });
      });
      return of(filter);
    } else {
      filter = new Filter();
      filter.isFilterApplied = true;
       this.applyFilter.searchparams.filterCriteria = SavedfiterCriteria;
      SavedfiterCriteria.forEach(element => {
        const _filterType = new FilterType();
        _filterType.caption = element.caption;
        _filterType.fieldId = element.fieldId;
        _filterType.controlType = element.controlType;
        _filterType.fieldType = element.fieldType;
        if (element.filterData.length > 0) {
          element.filterData.forEach(filterData => {
            if (filterData.values.length > 0) {
              if (element.controlType === ControlType.MULTISELECT || element.controlType === ControlType.SELECT ||
                element.controlType === ControlType.CHECKBOX) {
                filterData.values.forEach(values => {
                  const _subFilterType = new FilterSubType();
                  _subFilterType.IsSelected = true;
                  _subFilterType.text = values.text;
                  _subFilterType.value = values.value;
                  _subFilterType.id = values.id;
                  _filterType.filterValue = _filterType.filterValue + values.text + ', ';
                  _filterType.arrFilterSubType.push(_subFilterType);
                });
                _filterType.filterValue = _filterType.filterValue.substring(0, _filterType.filterValue.length - 2);
              } else if (element.controlType === ControlType.TEXT) {
                filterData.values.forEach(values => {
                  _filterType.filterValue = values.text;
                });
              } else if (element.controlType === ControlType.DATE) {
                filterData.values.forEach((values, i) => {
                  // _filterType.filterValue = _filterType.filterValue + values.text + '';
                  if (i === 0) {
                    _filterType.dateFrom = values.text;
                  } else if (i === 1) {
                    _filterType.dateTo = values.text;
                  }
                  _filterType.filterValue = _filterType.dateFrom + ' - ' + _filterType.dateTo;
                });
              }
            }
            filter.arrFilterType.push(_filterType);
          });
        }
      });
      return of(filter);
    }
  }
  filterJson(filter: Filter) {
    this.applyFilter = new ApplyFilter(this.applyFilter.pagingParams, new SearchParams('', []),
      this.applyFilter.sortparams, this.applyFilter.params);
    const applyFilter = this.applyFilter;
    filter.arrFilterType.forEach(function (item) {
      const arrFilterSubType = [];
      const arrSelectedFilterFileSubType = [];
      if (item.controlType === ControlType.DATE && (item.dateFrom !== '' && item.dateFrom !== undefined
        && item.dateTo !== '' && item.dateTo !== undefined)) {

        // arrFilterSubType.push(new Values(0, item.dateFrom.substring(4, item.dateFrom.length), item.dateFrom.substring(4, item.dateFrom.length)));
        // arrFilterSubType.push(new Values(0, item.dateTo.substring(4, item.dateTo.length), item.dateTo.substring(4, item.dateTo.length)));
        arrFilterSubType.push(new Values(1, item.dateFrom, item.dateFrom));
        arrFilterSubType.push(new Values(2, item.dateTo, item.dateTo));

        applyFilter.searchparams.filterCriteria.push(new FilterCriteria(item.fieldId, item.caption, item.controlType, item.fieldType, '',
          [new FilterData(arrFilterSubType, 'equals')]));

      } else if (item.controlType === ControlType.TEXT && (item.filterValue !== '' && item.filterValue !== undefined)) {
        applyFilter.searchparams.filterCriteria.push(new FilterCriteria(item.fieldId, item.caption, item.controlType, item.fieldType, '',
          [new FilterData([new Values(0, item.filterValue, '')], 'contains')]));

      } else {
        item.arrFilterSubType.forEach(function (subtype) {
          if (item.controlType === ControlType.SELECT || item.controlType === ControlType.MULTISELECT ||
            item.controlType === ControlType.CHECKBOX) {
            if (subtype.IsSelected) {
              arrFilterSubType.push(new Values(subtype.id, subtype.value, subtype.text));
            }
          }
        });
        if (arrFilterSubType.length > 0) {
          applyFilter.searchparams.filterCriteria.push(new FilterCriteria(item.fieldId, item.caption, item.controlType, item.fieldType, '',
            [new FilterData(arrFilterSubType, 'equals')]));

        }
        if (arrSelectedFilterFileSubType.length > 0) {
          applyFilter.searchparams.filterCriteria.push(new FilterCriteria(item.fieldId, item.caption, item.controlType, item.fieldType, '',
            [new FilterData(arrFilterSubType, 'equals')]));
        }
      }
    });
    return of(this.applyFilter);
    // console.log('_this.applyFilter on filter', JSON.stringify(this.applyFilter));
  }
  ///// To clear all the applied filter once we click on the dropdown option from quick search list
  onClearFilter(filter: Filter, filtertype?: FilterType) {
    if (!filtertype && filter) {

      filter.filterText = '';
      filter.isFilterApplied = false;
      filter.arrFilterType.forEach(function (filterType, index) {
        if (filterType.controlType === ControlType.DATE || filterType.controlType === ControlType.TEXT) {
          filterType.filterValue = '';
          filterType.dateFrom = '';
          filterType.dateTo = '';
        } else {
          filterType.filterValue = '';
          filterType.arrFilterSubType.forEach(
            function (filtersubtype, indexsubtype) {
              filtersubtype.IsSelected = false;
            });
        }
      });
    }
    return of(filter);
  }

  getSelectedFilter(filter, filtertype: FilterType) {
    let value = '';
    if (filter) {
    if (filtertype.controlType === ControlType.DATE && filtertype.dateFrom !== '' && filtertype.dateTo !== '') {
      value = filtertype.dateFrom + ' - ' + filtertype.dateTo;
    } else if (filtertype.controlType === ControlType.TEXT) {
      value = (filtertype.filterValue && filtertype.filterValue !== 'undefined') ? filtertype.filterValue : '';
    } else {
      const index = filter.arrFilterType.findIndex(f => f.fieldId === filtertype.fieldId);
      if (index >= 0) {
        const arrFilterSubType = filter.arrFilterType.find(f => f.fieldId === filtertype.fieldId).arrFilterSubType;
        if (arrFilterSubType) {
          arrFilterSubType.forEach(function (item, index) {
            value = value + (item.IsSelected ? item.text + ', ' : '');
          });
          if (value !== '') {
            value = value.substring(0, value.length - 2);
          }
        }
      }
    }
  }
    return value;
  }

}
