import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WfxFilterComponent } from './wfx-filter.component';

describe('AssetfilterComponent', () => {
  let component: WfxFilterComponent;
  let fixture: ComponentFixture<WfxFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WfxFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WfxFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
