import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { WfxComponentAttributes } from '../../../wfxlibrary/wfxsharedmodels/wfxcommonmodel';
import { ItemLibraryGlobals } from '../../../../app/wfxplmitemlibrary/itemLibrary.global';
@Component({
  selector: 'app-wfx-record-count',
  templateUrl: './wfx-record-count.component.html',
  styleUrls: ['./wfx-record-count.component.css']
})
export class WfxRecordCountComponent implements OnInit {
  wfxRecordTypeJsonDef;
  wfxRecordTypejsonData;
  wfxRecordCountJsonDef;
  wfxRecordCountjsonData;
  recordcount;

  @Input() jsonDef: {
    attribute?: WfxComponentAttributes
    fontSize?: string
  };

  @Input() jsonData: {
    recordType?: string
    recordCount?: number,
  };



defaultValue =
  {
    fontSize: '11px',
    visible: true
  };
  constructor(public itemGlobal: ItemLibraryGlobals) { }

  ngOnInit() {
    console.log('record',this.itemGlobal.arrGalleryViewData.getValue().length)
    this.wfxRecordCountJsonDef =  {
      readonly: false, mandatory: false, caption: 'Record Count', disable: false,
      value: 'Count', valuetype: 'dbfield',
      tooltip: { tooltipvalue: 'RecordCount', tooltipvaluetype: 'dbfield', tooltipposition: 'below' },
      inputtype: 'textbox', itemid: 'txtcount'
    };



  }

     /*** Property Section Starts */

   get  getRecordCount() {
     if(this.itemGlobal.currentView === 'GALLERY'){
      if (this.itemGlobal.recordCount) {
        this.recordcount = 'Showing' + ' ' +  this.itemGlobal.arrGalleryViewData.getValue().length + ' ' +
        'of' + ' ' + this.itemGlobal.recordCount + ' ' + this.itemGlobal.recordName;
         return this.recordcount;
       } else {
        this.recordcount = 'Showing' + ' ' +  this.itemGlobal.arrGalleryViewData.getValue().length + ' ' +
        'of' + ' ' + '0'
        + ' ' +   this.itemGlobal.recordName;
         return this.recordcount;
       }
     }
     else{
      if (this.itemGlobal.recordCount) {
        this.recordcount = 'showing' + ' ' +  this.itemGlobal.recordCounttableview + ' ' +
        'of' + ' ' + this.itemGlobal.recordCount + ' ' + this.itemGlobal.recordName;
         return this.recordcount;
       } else {
        this.recordcount = 'showing' + ' ' +  this.itemGlobal.recordCounttableview + ' ' +
        'of' + ' ' + '0'
        + ' ' +   this.itemGlobal.recordName;
         return this.recordcount;
       }
     }

    }

  get fontSize() {
    if (this.jsonDef && this.jsonDef.fontSize) {
      return this.jsonDef.fontSize;
    } else {
      return this.defaultValue.fontSize;
    }
  }



  get visible() {
    if (this.jsonDef && this.jsonDef.attribute.visible) {
      return this.jsonDef.attribute.visible;
    } else {
      return this.defaultValue.visible;
    }
  }

     /*** Property Section Ends */

}
