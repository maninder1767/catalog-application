import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WfxRecordCountComponent } from './wfx-record-count.component';

describe('WfxRecordCountComponent', () => {
  let component: WfxRecordCountComponent;
  let fixture: ComponentFixture<WfxRecordCountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WfxRecordCountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WfxRecordCountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
