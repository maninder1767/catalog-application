import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WfxContextmenuComponent } from './wfx-contextmenu.component';

describe('WfxContextmenuComponent', () => {
  let component: WfxContextmenuComponent;
  let fixture: ComponentFixture<WfxContextmenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WfxContextmenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WfxContextmenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
