import { Globals } from './../../../globals';
import { WfxShowMessageservice } from './../../../wfxlibrary/wfxsharedservices/wfxshowmessage.service';
/*
  * Created By : Ashutosh Rai
  * Created On : 13/09/2019
  * Purpose : To open context menu in our application
  * Last Modified Date : This will be getting updated whenever we make changes. This will show only  last modified details
  * Last Modified By : This will be getting updated whenever we make changes. This will show only  last modified details
  */


import { Component, Input, Output, EventEmitter, OnChanges, ViewChild, OnInit } from '@angular/core';
import { ContextMenuComponent } from 'ngx-contextmenu';
import { MatDialog } from '@angular/material';
import { WfxPublishComponent } from '../../../wfxplmitemlibrary/components/wfx-publish/wfx-publish.component';
import { ItemLibraryDataService } from '../../services/itemdata.service';
import { ItemLibraryGlobals } from '../../../wfxplmitemlibrary/itemLibrary.global';


@Component({
  selector: 'app-wfx-contextmenu',
  templateUrl: './wfx-contextmenu.component.html',
  styleUrls: ['./wfx-contextmenu.component.css']
})
export class WfxContextmenuComponent implements OnInit, OnChanges {
  /*** Property Section Starts ***/
  @Input() jsonData: any;
  @Input() contextMenuActions: any;
  contextmenu: any;
  @ViewChild('basicMenu', { static: true }) contextMenu: ContextMenuComponent;
  /*** Property Section Ends ***/

  /*** Event Section Starts ***/
  @Output() contextMenuAction = new EventEmitter<string>();
  @Output() method = new EventEmitter();
  @Output() menusubtype = new EventEmitter();
  /*** Event Section Ends ***/
  productCategory: any;
  constructor(public dialog: MatDialog, private itemdata: ItemLibraryDataService, private globals:Globals,
    private itemGlobal: ItemLibraryGlobals, private msgsvc: WfxShowMessageservice) { }
  ngOnInit() {
    // console.log('Ashu', this.contextMenuActions);
  }
  ngOnChanges() {
    if (this.contextMenuActions) {
      this.contextMenuActions.contextMenu.forEach((val) => {
        val.click = (item) => this.onContextMenuAction(val.code, item);
        if (val.subMenus) {
          const arrSub = val.subMenus;
          arrSub.forEach((subVal) => {
            // console.log('Ashu', subVal);
            subVal.click = (item) => this.openDialog(subVal.code, item, this.contextMenuActions.groupID, this.contextMenuActions.fPerm);
          });
        }
      });
    }
  }
  /*
    * Created By : Ashutosh Rai
    * Created On : 14/09/2019
    * Purpose :   its invoke click of context menu options
    */

  onContextMenuAction(action, itemId) {
    let url = '';
    switch (action) {
      case 'SHARE':
        this.displayPublishDialog();
        break;
      case 'PRINT':
        url = '..//wfx_catalog/wfxcataloggui/wfx_ArticleInfoSummaryNew.aspx?ID=' + itemId +
        '&CatalogType=' + this.globals.companyCode + '&CatalogCompany=' + this.globals.companyCode +
        ' &Version=1&Print=1&Combocode=' + this.globals.productCatCode + '&UserName=' + this.globals.userName +
        '&SessionIDForPrint=' + this.globals.sessionID + '&isIEBrowser=0&FromEmail=&ArticleSelection=';
         //window.open(url, '_blank', 'width=710,height=480,top=30,left=30');
         alert("This Functionality is temporarily blocked  Redirected to : "+  url);
        break;
      case 'ACTIVE':
       this.ActiveUnActive(itemId, 'Active Successfully');
        break;
        case 'INACTIVE':
          this.ActiveUnActive(itemId, 'Inactive Successfully');
           break;
      case 'WHEREUSED':
        break;
      default:
        break;
    }
   // this.method.emit({ action, itemId });
  }

  /*
  * Created By : Ashutosh Rai
  * Created On : 14/09/2019
  * Purpose :   its invoke click of context menu sub options
  */
  openDialog(tabName, itemId, groupID, folderPermission): void {
    let singleFrame;
    let url;
    const productCatCode = this.globals.productCatCode;
    if (tabName === 'Techpacks') {
       url = '..//wfx/wfx_ArticleDetail.aspx?ID=' + itemId + '&ArticleSelection=1&CatalogType= ' + productCatCode + ' ';
      //window.open(url, '_blank', 'width=710,height=480,top=30,left=30');
      alert("This Functionality is temporarily blocked  Redirected to : "+  url);
    } else {
      if (tabName === (('Msg') || ('Approvals') || ('CostSheet'))) {
        singleFrame = '0';
      } else {
        singleFrame = '1';
      }
      if ( tabName === 'Msg') {
        singleFrame = '1';
      }
       if (folderPermission === '0') {
          url = '..//wfx/wfx_ArticleDetail.aspx?ID=' + itemId + '&GroupID=' + groupID + '&ComboCode=' + productCatCode +
                    '&CurrentFolder=' + tabName + '&ArticleSelection=1&CatalogType=1&CatalogCompany=&SingleFrame=' + singleFrame;
        //window.open(url, '_blank', 'width=710,height=480,top=30,left=30,resizable=1,status=1');
        alert("This Functionality is temporarily blocked  Redirected to : "+  url);
      } else {
         url = '..//wfx/wfx_ArticleDetail.aspx?ID=' + itemId + '&GroupID=' + groupID + '&ComboCode=' + productCatCode +
                    '&CurrentFolder=' + tabName + '&ArticleSelection=&CatalogType=' + productCatCode +
                    '&CatalogCompany=&SingleFrame=' + singleFrame ;
       // window.open(url, '_blank', 'width=710,height=480,top=30,left=30,resizable=1,status=1');
       alert("This Functionality is temporarily blocked  Redirected to : "+  url);
      }

    }
  }
  private displayPublishDialog() {
    // const dialogRef = this.dialog.open(WfxPublishComponent, {
    //   height: '511px',
    //   width: '873px',
  // });
  alert("you can't share anything ")

}

private ActiveUnActive(itemid: string, msg: string) {
  this.itemdata.updateItemStatus([{id: itemid.toString()}]).
  subscribe((res: any) => {
    if (res.status === 'Success') {
      this.itemGlobal.arrGalleryViewItemData.splice
      (this.itemGlobal.arrGalleryViewData.getValue().findIndex(x => x.itemId === itemid) , 1);
       this.itemGlobal.arrGalleryViewData.next(this.itemGlobal.arrGalleryViewItemData);
      this.msgsvc.showSnackBar(msg, '', res.status);
    }
   });
}
}
