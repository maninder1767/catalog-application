import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WfxCardComponent } from './wfx-card.component';

describe('WfxCardComponent', () => {
  let component: WfxCardComponent;
  let fixture: ComponentFixture<WfxCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WfxCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WfxCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
