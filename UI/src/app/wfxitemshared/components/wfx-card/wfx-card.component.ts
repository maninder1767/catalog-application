import { Globals } from './../../../globals';
import { ItemLibraryGlobals } from './../../../wfxplmitemlibrary/itemLibrary.global';
import { WfxCheckboxComponent } from './../../../wfxlibrary/wfxcomponent-ts/wfxcheckbox';
import { Item } from './../../../wfxplmitemlibrary/modals/item';
import { WfxContextmenuComponent } from './../wfx-contextmenu/wfx-contextmenu.component';
import { Image404 } from '../../../wfxplmitemlibrary/modals/errors/image-error/404';
import { WfxComponentAttributes } from './../../../wfxlibrary/wfxsharedmodels/wfxcommonmodel';
import { Component, OnInit, Input, Output, EventEmitter, OnChanges, ViewEncapsulation, ViewChild, ElementRef, Renderer2, AfterViewChecked, AfterViewInit, ViewChildren } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { ContextMenuService } from 'ngx-contextmenu';
import { ContextMenuComponent } from 'ngx-contextmenu';
import { ItemLibraryDataService } from '../../services/itemdata.service';
import { WfxLikeComponent } from '../wfx-like/wfx-like.component';

/*
  * Created By : Ashutosh Rai
  * Created On : 13/09/2019
  * Purpose : To display the informationm of the styles
  * Last Modified Date : This will be getting updated whenever we make changes. This will show only  last modified details
  * Last Modified By : This will be getting updated whenever we make changes. This will show only  last modified details
  */
@Component({
  selector: 'app-wfx-card',
  templateUrl: './wfx-card.component.html',
  styleUrls: ['./wfx-card.component.css'],
  // encapsulation: ViewEncapsulation.None
})
export class WfxCardComponent implements OnInit, OnChanges, AfterViewInit {
  /*** Property Section Starts ***/
  @Input() jsonData: Item;
  @Input() scrollObservable: Observable<{} | Event>;
  @ViewChild(WfxContextmenuComponent, { static: true }) wfxContextmenu: WfxContextmenuComponent;
  checked = false;
  @Input() parentElement: ElementRef;
  @Input() jsonDef: {
    card?: WfxComponentAttributes
    isLifeCycleStage?: boolean,
    isAvatar?: boolean,
    cardHeight?: string,
    cardWidth?: string,
    cardImageHeight?: string,
    cardImageWidth?: string,
    cardTitleFontSize?: string,
    cardTitleWidth?: string,
    cardSubTitleFontSize?: string,
    cardSubTitleWidth?: string,
    cardContentFontSize?: string,
    cardContentWidth?: string,
    cardPLSFontSize?: string,
    cardPLSFontWidth?: string,
  };
  defaultCardDef: WfxComponentAttributes;
  defaultCardTitleDef: {
    title?: WfxComponentAttributes,
    fontSize?: string
  };
  defaultCardSubtitleDef: {
    subtitle?: WfxComponentAttributes,
    fontSize?: string
  };
  defaultCardContentDef: {
    cardContent?: WfxComponentAttributes,
    fontSize?: string
  };
  defaultCardAvatarDef: {
    avatar: WfxComponentAttributes,
    top: '-25px'
  };
  /*** Property Section Ends ***/
  /*** Event Section Starts ***/
  @Output() OnCardClick = new EventEmitter<object>();
  @Output() OnCardDoubleClick = new EventEmitter<object>();
  @Output() extendedMenuClick = new EventEmitter();
  @Output() QuickView = new EventEmitter();
  @Output() selectedCard = new EventEmitter<boolean>();
  // @ViewChild('avatar', {static: false})   avatar: WfxAvatarComponent;
  /*** Event Section Ends ***/

  /*** variable Section Starts ***/
  titleContent = '';
  defaultCardLikeDef: WfxComponentAttributes;
  defaultCardBookmarDef: WfxComponentAttributes;
  contextMenuActions: any;
  defaultCardImageDef;
  likeJsonDef;
  userDetailsData;
  offset = 100;
  avatarJsonDef;
  avatarJsonData;
  likeJsonData;
  bookMarkJsonDef;
  bookMarkJsonData;
  isContextMenuVisible = false;
  isCheckboxVisible = false;
  menuType: string;
  defaultImageURL = './assets/100w.gif';
  errorImage = '';
  color = false;
  buyerSupllierRefNoumber;
  checkboxJsonDef = {
    readonly: false, mandatory: true, caption: '',
    value: 'checkbox', disable: false, labelposition: 'after', valuetype: 'dbfield',
    tooltip: { tooltipvalue: '', tooltipvaluetype: '', tooltipposition: 'below' }
  };
  checkboxJsonData ;

  getCardWidth;
  getCardHeight;
  getCardImageWidth;
  getCardImageHeight;

  getCardTitleFontSize;
  getCardTitleWidth;

  getCardSubTitleFontSize;
  getCardSubTitleWidth;

  getCardContentFontSize;
  getCardContentWidth;
  getLifeCycleStageFontSize;
  getLifeCycleStageWidth;
  @ViewChild(ContextMenuComponent, { static: false }) public basicMenu: ContextMenuComponent;
  @ViewChild('cardcheck', { static: false }) cardcheck: ElementRef;
  @ViewChild(WfxLikeComponent, { static: false }) wfxLikeComponent : WfxLikeComponent;
  /*** variable Section ends ***/
  constructor(private contextMenuService: ContextMenuService, private itemGlobal: ItemLibraryGlobals,
    private globals:Globals,
     private itemdata: ItemLibraryDataService) { }
  ngOnInit() {
    // this.itemdata.getContextMenuList(this.jsonData.itemId.toString())
    // .subscribe((res: any) => {
    //   this.contextMenuActions = res.data;
    // });
   // this.jsonData.checkbox = false;
    if (this.jsonData.buyerReference) {
      this.titleContent = this.jsonData.buyerReference + '/';
    }
    if (this.jsonData.supplierReference) {
      this.titleContent = this.titleContent + this.jsonData.supplierReference;
    } else {
      if (this.jsonData.buyerReference) {
        this.titleContent = this.jsonData.buyerReference;
      }
    }


    this.defaultCardDef = {
      disable: false,
      visible: false,
      tooltip: {
        tooltipvalue: '',
      },
    };
    this.avatarJsonDef = {
      attribute: {
        tooltip: {
          tooltipValue: this.jsonData.CreatedByFullName,
          tooltipValueType: '',
          tooltipPosition: 'below'
        }
      }
    };
    this.likeJsonDef = {
      attribute: '',
      dailog: {
        attribute: {
          },
        image: {
          attribute: {
            height: '20px',
            width: '20px',
          },
        }
      }
    };
    this.likeJsonData = {
      likeCount: this.likeCount,
      isLike: this.isCardLike,
    };
    this.bookMarkJsonDef = {
      attribute: ''
    }
    this.bookMarkJsonData = {
      isBookMark: this.jsonData.IsBookmark,
    };
    this.errorImage = new Image404().uploadErrImage();

  }

  ngAfterViewInit() {
    // console.log("cardcheck:", this.cardcheck);
  }

  ngOnChanges() {
  //   console.log('Ashutosh',this.checked);
    this.jsonDef.isLifeCycleStage = true;
    this.jsonDef.isAvatar = false;
    this.getCardImageWidth = this.jsonDef.cardImageWidth;
    this.getCardImageHeight = this.jsonDef.cardImageHeight;

    this.getCardWidth = this.jsonDef.cardWidth;
    this.getCardHeight = this.jsonDef.cardHeight;

    this.getCardTitleFontSize = this.jsonDef.cardTitleFontSize;
    this.getCardTitleWidth = this.jsonDef.cardTitleWidth;

    this.getCardSubTitleFontSize = this.jsonDef.cardSubTitleFontSize;
    this.getCardSubTitleWidth = this.jsonDef.cardSubTitleWidth;

    this.getCardContentFontSize = this.jsonDef.cardContentFontSize;
    this.getCardContentWidth = this.jsonDef.cardContentWidth;

    this.getLifeCycleStageFontSize = this.jsonDef.cardPLSFontSize;
    this.getLifeCycleStageWidth = this.jsonDef.cardPLSFontWidth;
  //  this.jsonData.checkbox = false;
  }
 

  get tooltip() {
    if (this.jsonDef.card && this.jsonDef.card.tooltip) {
      return this.jsonDef.card.tooltip;
    }
    return this.defaultCardDef.tooltip;
  }
  get visible() {
    if (this.jsonDef.card && this.jsonDef.card.visible) {
      return this.jsonDef.card.visible;
    }
    return this.defaultCardDef.visible;
  }
  get disabled() {
    if (this.jsonDef.card && this.jsonDef.card.disable) {
      return this.jsonDef.card.disable;
    }
    return this.defaultCardDef.disable;
  }

  get likeCount() {
    if (this.jsonData && this.jsonData.likeCount) {
      return this.jsonData.likeCount;
    }
    return 0;
  }

  get isCardLike() {
    if (this.jsonData && this.jsonData.isLike) {
      return this.jsonData.isLike;
    }
    return false;
  }

  get rgbProductLifeCycleStage() {
    if (this.jsonData && this.jsonData.plcColor) {
      this.color = true;
     //return  'rgb(' + this.jsonData.plcColor.split('-').toString() + ')';
    }

    return false;
  }
  get checkbox() {
    return this.jsonData.checkbox;
  }
  get checkboxOpacity() {
    if (this.checkbox || this.isCheckboxVisible) {
      return true;
    } else if (this.checkbox === undefined) {
      return true;
    } else {
      return false;
    }
  }
  get supplierOrBuyerRefVisible() {
    if (this.jsonData.buyerReference && this.jsonData.supplierReference) {
      return true;
    }
    return false;
  }
  /*** Property Section End ***/
  //  onRefreshAsset(data: AssetDetailData) {
  //   this.jsonData = data.AssetData;
  //  }
  /*** Method Section Starts ***/

  /**
  * Created By :  Ashutosh Rai
  * Created On :  13/09/2019
  * Purpose :    this is use to hide and show to extendedmenu controle on wfx-card
  * @param {string} id
  */

  OnCardMouseOver(id: string) {
     this.isCheckboxVisible = true;
  }
  OnCardMouseOut() {
    this.isCheckboxVisible = false;
    if (this.jsonDef.card.disable) { return; }
  }

  /**
  * Created By :  Ashutosh Rai
  * Created On :  13/09/2019
  * Purpose :    this is use to invoke clik event on wfx-card
  * @param {any} jsondata
  */
 OnClick(jsondata) {
  if (this.jsonDef.card.disable) { return; }
  let url = '..//wfx/wfx_ArticleDetail.aspx?ID=' + jsondata.itemId + '&ArticleSelection=1&CatalogType=' + this.globals.productCatCode + '';
  alert("This Functionality is temporarily blocked  Redirected to : "+  url);
  //window.open(url, '_blank', 'width=710,height=480,top=30,left=30') ;
}


  /**
  * Created By :  Ashutosh Rai
  * Created On :  13/09/2019
  * Purpose :    this is use to invoke Dubbleclik event on wfx-card
  * @param {any} jsondata
  */
  OnDoubleClick(jsondata) {
   // if (this.jsonDef.card.disable) { return; }
    // this.OnCardDoubleClick.emit(jsondata);
   // window.open('..//wfx/wfx_ArticleDetail.aspx?ID=' + jsondata.itemId + '&ArticleSelection=1&CatalogType=1');
  }
  /**
 * Created By :  Ashutosh Rai
 * Created On :  13/09/2019
 * Purpose :    this is use to invoke quick view event on wfx-card
 * @param {boolean} true/false
 */
  OnQuickViewClick(jsonData) { this.QuickView.emit(jsonData.itemId); }

  /**
  * Created By :  Ashutosh Rai
  * Created On :  13/09/2019
  * Purpose :    this is use to error image on wfx-card
  * @param {boolean} true/false
  */

  imageOnError(event: any) {
    this.jsonData.NoItemImage = true;
    //  new Image404().defaultImage(event, 'asset_image_w_h', '');
  }

  /**
  * Created By :  Ashutosh Rai
  * Created On :  13/09/2019
  * Purpose :    this is use to open context menu on  wfx-card
  * @param {boolean} true/false
  */
 public onContextMenu($event: MouseEvent, item: any): void {
    this.contextMenuService.show.next({
      anchorElement: $event.target,
      contextMenu: this.wfxContextmenu.contextMenu,
      event: $event,
      item: item,
    });
    $event.preventDefault();
    $event.stopPropagation();
}

onmenuClick($event: MouseEvent, item) {
  this.itemdata.getContextMenuList(item)
      .subscribe((res: any) => {
      this.contextMenuActions = res.data;
      setTimeout(() => {
       this.onContextMenu($event, item);
    },1);
    });
}

  /**
 * Created By :  Ashutosh Rai
 * Created On :  13/09/2019
 * Purpose :    this is use to close context menu on  wfx-card
 * @param {any} $event
 */
  closeAllContextMenu($event) {
    this.contextMenuService.closeAllContextMenus($event);
    $event.preventDefault();
    $event.stopPropagation();
  }
  /**
* Created By :  Ashutosh Rai
* Created On :  13/09/2019
* Purpose :    this is used when  context menu option is cliked .
* @param {any} $event
*/
  onContextSelectedType($event) {
    // this.contextmenuService.onContextMenuAction($event);
  }
  /*** Method Section Ends ***/

  onLikeClick($event, like) {
    this.itemdata.updateLike(this.jsonData.itemId.toString()).subscribe(val => {
    });
  }
  onbookmarkclick(data, itmeid: number) {
    console.log(data);
    this.itemdata.updateItemBookmark({id : itmeid.toString()}).subscribe((val: any) => {
      if (val.status === 'Success') {
        this.jsonData.IsBookmark = !this.jsonData.IsBookmark;
      }
    });
  }

  onLoadImage(isImageLoad) {
    // console.log(isImageLoad, this.jsonData.SortOrder);
    if (!isImageLoad) {
      this.defaultImageURL = new Image404().uploadErrImage();
      this.errorImage = new Image404().uploadErrImage();
    }
  }

  onCheckboxChange(e, item: any) {
   this.jsonData.checkbox = e.checked;
    this.OnCardClick.emit(item);
    this.selectedCard.emit(true);
    if (this.checkbox === true) {
      this.isContextMenuVisible = true;
    } else {
      this.isContextMenuVisible = false;
    }
  }
  /**
* Created By :  Urvashi Sharma
* Created On :  14/10/2019
* Purpose :    this is used when click on likedetail
* @param {any} $event
*/
  onShowLikeDetails($event) {
    this.itemdata.getLikeDetail(this.jsonData.itemId.toString()).subscribe((val :any) => {
      this.wfxLikeComponent.onUserData(val.data)
    });
  }
}
