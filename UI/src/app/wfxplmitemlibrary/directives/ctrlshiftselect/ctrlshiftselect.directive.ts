import { data } from './../../../wfxitemshared/components/wfx-tableview/db.json';
import { WfxCardComponent } from './../../../wfxitemshared/components/wfx-card/wfx-card.component';
import { Directive, Input, ElementRef, Renderer2, HostListener, OnInit, Output, EventEmitter, ViewChild, AfterViewInit } from '@angular/core';
import { ItemLibraryGlobals } from '../../itemLibrary.global';
@Directive({
  selector: '[appCtrlshiftselect]'
})
export class CtrlshiftselectDirective implements OnInit, AfterViewInit {
  @Input() isMultiSelection = true;
  @Input() isMaxSelection = false;
  @Input() index: number;
  @Input() seletedid: number;
  @Output() selectedCards = new EventEmitter<boolean>();
  endindex: number;
  constructor(private el: ElementRef, private globals: ItemLibraryGlobals, private renderer: Renderer2,
    private itemGlobal: ItemLibraryGlobals) { }
  ngAfterViewInit() {
  }
  ngOnInit() {
  }
  @HostListener('click', ['$event'])
  onclick(event) {
    if (this.isMultiSelection && (event.shiftKey || event.metaKey)) {
      if (this.seletedid > this.itemGlobal.lastItemSelectedID) {
        let selectedItem = this.itemGlobal.arrGalleryViewItemData.filter(x => Number(x.itemId) >
        this.itemGlobal.lastItemSelectedID && Number(x.itemId) < this.seletedid) ;
         selectedItem.forEach(item =>{item.checkbox = true; });
      } else {
        let selectedItem = this.itemGlobal.arrGalleryViewItemData.filter(x => Number(x.itemId) >
        this.seletedid && Number(x.itemId) < this.itemGlobal.lastItemSelectedID) ;
         selectedItem.forEach(item => {item.checkbox = true; });
      }
      this.selectedCards.emit(true);
    }
    this.globals.seletionitemindex = this.index  ;
  }
}

