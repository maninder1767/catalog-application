import { ItemLibraryGlobals } from './../../itemLibrary.global';
import { Directive, HostListener,  OnInit, EventEmitter, Output } from '@angular/core';
@Directive({
  selector: '[appKeyboardNavigation]'
})
export class KeyboardNavigationDirective implements OnInit {
  @Output() selectedCards = new EventEmitter<boolean>();
  constructor(private globals: ItemLibraryGlobals) {
  }
  ngOnInit() {
  }
  @HostListener('window:keydown', ['$event'])
  onkeydown(event) {
    if (event.key === 'Escape') {
      this.globals.arrGalleryViewItemData.forEach(obj => {
        if (obj.checkbox) {
        }  obj.checkbox = false;
    });
    this.globals.arrGalleryViewData.next(this.globals.arrGalleryViewItemData);
    this.globals.seletionitemindex = -1;
    this.globals.isShiftKeyPressed = false;
    this.selectedCards.emit(false);
       }
        return true;
  }
  @HostListener('window:keydown.control.a', ['$event'])
  keyEvent(event: KeyboardEvent) {
   this.globals.arrGalleryViewItemData.forEach(obj => {
    obj.checkbox = true;
    });
    this.globals.arrGalleryViewData.next(this.globals.arrGalleryViewItemData);
    this.selectedCards.emit(true);
  }

}
