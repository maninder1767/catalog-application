import { Directive, HostListener, ElementRef, Input, ViewChild, Output, EventEmitter } from '@angular/core';

@Directive({
  selector: '[appShrinkingHeader]'
})
export class ShrinkingHeaderDirective {
  constructor(private el: ElementRef) { }
  @Input() controlID: string;
  @Input() maxSize: number;
  @Input() minSize: number;
  @Input() size: string;
  @HostListener('scroll', ['$event']) onListenerTriggered(event: any): void {
    const cardAllDetail = document.getElementById('propertiesAllOptionContainer');
    const cardHeader = document.getElementById('itemNameContainer');
    const distanceY = event.currentTarget.scrollTop;
    let heightvalueall: number;
    const imageContainer = document.getElementById(this.controlID);
    heightvalueall = Math.abs(window.innerHeight - 250 - cardAllDetail.offsetHeight);

    if (document.getElementById(this.controlID)) {
      if (distanceY > 0 && !imageContainer.classList.contains('shrink') ) {
       shrinkAddClass();
      } else if (distanceY === 0 && imageContainer.classList.contains('shrink') ) {
       shrinkRemoveClass();
      }
    }
    function shrinkAddClass() {
      imageContainer.classList.add('shrink');
      //cardAllDetail.style.marginBottom = heightvalueall + 'px';
      cardHeader.style.display = 'inline-block';
      cardHeader.style.marginLeft = '15px';
    }
  function shrinkRemoveClass() {
      imageContainer.classList.remove('shrink');
      cardHeader.style.display = 'block';
      cardHeader.style.marginLeft = '0';
    }
  }
}
