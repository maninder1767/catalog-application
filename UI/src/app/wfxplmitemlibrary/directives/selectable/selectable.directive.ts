
import { Directive, ElementRef, AfterViewInit, OnDestroy, Input,
  Renderer2, Output, EventEmitter } from '@angular/core';

declare var $: any;
@Directive({
  selector: '[appSelectable]',
})
export class SelectableDirective implements AfterViewInit, OnDestroy {
  @Input() isMultiSelection = true;
  @Input() selectableItem = 'li .mat-card';
  @Input() isMaxSelection = false;
  @Output() selectionStart = new EventEmitter();

  constructor(private el: ElementRef, private renderer: Renderer2) {
   // renderer.addClass(el.nativeElement, 'selectable');
  }
  ngAfterViewInit() {
    if (this.isMultiSelection) {
        const obj = this;
      $(this.el.nativeElement).selectable({
         filter: this.selectableItem,
         cancel: 'li,.cancel',
         selecting: function (event, ui) {
          //  console.log(ui.selecting.id);
          //   obj.selectionStart.emit(ui.id);
        },
        selected: function( event, ui ) {
            obj.selectionStart.emit(ui.selected.id);
        },
            });
    }
  }
  ngOnDestroy() {
    if (this.isMultiSelection) {
      $(this.el.nativeElement).selectable('destroy');
    } else if (this.isMaxSelection && this.el.nativeElement.id !== '') {
      $('#' + this.el.nativeElement.id).selectable('destroy');
    }
  }

}
