import { PageParam, PagingParam } from './../wfxlibrary/wfxsharedmodels/wfxqueryparam';
import { FilterCriteria, SortParams, Filter } from './../wfxitemshared/modals/filter';
import { ViewType } from './../wfxitemshared/constants/enum';

import { Injectable } from '@angular/core';
import { ApplyFilter, SearchParams } from '../wfxitemshared/modals/filter';
import { Item } from './modals/item';
import { Cardproperties } from './modals/cardproperties';
import { TableDropdownViewModel } from '../wfxitemshared/modals/table';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ItemLibraryGlobals {
  recordCount = 0;
  recordCounttableview = 0;
  seletionitemindex: number;
  isSpinnerLoadingGalleryView = false;
  lastItemSelectedID = 0;
  arrGalleryViewData= new BehaviorSubject([]);
  isShiftKeyPressed: boolean;
  currentView = ViewType.GALLERY;
  arrGalleryViewItemData: Item[] = [];
  itemActiveToggleModel: FilterCriteria = {
    fieldId: 'article_active',
    caption: '',
    controlType: '',
    fieldType: 'Fixed',
    operator: '',
    filterData: [
      {
        values: [
          {
            value: 'true'
          }
        ],
        filterType: 'equals'
      }
    ]
};
activetoggle: boolean ;
tableViewRowID: number;
defaultSortParams: SortParams = {
  fieldId: 'ART.CreatedOn',
  sortDirection: 'DESC',
  sortOrder:  40,
};

defaultPageParams: PagingParam = {
   PageIndex : 1,
   PageSize : 100
};

  arrFilterCriteria: FilterCriteria[] = [];
  arrGalleryFilterCriteria: FilterCriteria[] = [];
  quickSearchFilterText: string;
  commonFilter = new ApplyFilter(this.defaultPageParams,
    new SearchParams('', [this.itemActiveToggleModel]), [this.defaultSortParams], []);

  arrSelectedItemId: string[] = [];  /*To Hold Selected Item Ids*/
    isAllItemsSelected: Boolean = false;

    cardproperties: Cardproperties = {
      cardHeight_zero : 184.8,
      cardHeight_fifty : 300,
      cardHeight_hundred : 600,

      cardWidth_zero : 115,
      cardWidth_fifty : 175,
      cardWidth_hundred : 350,

      cardImageHeight_zero : 114,
      cardImageHeight_fifty : 184,
      cardImageHeight_hundred : 368,

      cardImageWidth_zero : 105,
      cardImageWidth_fifty : 175,
      cardImageWidth_hundred : 350,

      cardTitleFontSize_zero : 9,
      cardTitleFontSize_fifty : 11,
      cardTitleFontSize_hundred : 15,

      cardTitleWidth_zero : 78,
      cardTitleWidth_fifty : 91,
      cardTitleWidth_hundred : 182,

      cardSubTitleFontSize_zero : 12,
      cardSubTitleFontSize_fifty : 14,
      cardSubTitleFontSize_hundred : 22,

      cardSubTitleWidth_zero : 78,
      cardSubTitleWidth_fifty : 135,
      cardSubTitleWidth_hundred : 270,

      cardContentFontSize_zero : 12,
      cardContentFontSize_fifty : 14,
      cardContentFontSize_hundred : 22,

      cardContentWidth_zero : 78,
      cardContentWidth_fifty : 135,
      cardContentWidth_hundred : 270,

      cardPLSFontSize_zero : 9,
      cardPLSFontSize_fifty : 11,
      cardPLSFontSize_hundred : 14,

      cardPLSWidth_zero : 78,
      cardPLSWidth_fifty : 135,
      cardPLSWidth_hundred : 270,

      cardiconHeight_zero : 20 ,
      cardiconHeight_fifty : 20 ,
      cardiconHeight_hundred : 20,

      cardiconWidth_zero : 15,
      cardiconWidth_fifty : 15,
      cardiconWidth_hundred : 15 ,
    };
    sliderRange = '60';
    defaultsliderRange = 60;
    arrtableDropdownViewData: any[] = [];
    arrTableColumnDefinition: any[] = [];
    arrTableRowData: any[] = [];
    tableSelectedView: TableDropdownViewModel = {Text: '', Code: '', Id: ''};
    defaultTableSelectedView: TableDropdownViewModel = {Text: '', Code: '', Id: ''};

    currentSelectedTab = {
      Code: '',
      Text: '',
      Tooltip: ''
  };
  isActiveItem = true;
  recordName= 'Items';
  activePanel = false;

  currentRightSelectedPanel: RightSelectedPanel;
  selectedTableSortParam: SortParams[] = [];
  appliedFilterChips: Filter;
  isFilterApplied: boolean;
  selectedSortFieldValue = '';
  costingDefaultSelectedView = {
    value: 0,
    text: ''
  };

  tableViewFilterModel = {};
}
export enum RightSelectedPanel {
  Filter,
  PropertyView,
  CommentDetail
}

