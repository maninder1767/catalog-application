import { LoadmoreDirective } from './directives/loadmore/loadmore.directive';
import { KeyboardNavigationDirective } from './directives/keyboard-navigation/keyboard-navigation.directive';
import { SelectableDirective } from './directives/selectable/selectable.directive';
import { CtrlshiftselectDirective } from './directives/ctrlshiftselect/ctrlshiftselect.directive';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WfxSharedModule } from '../wfxlibrary/wfxshared.module';
import { HttpClientModule } from '@angular/common/http';
import { WfxItemLibraryComponent } from './components/wfx-itemlibrary/wfx-itemlibrary.component';
import { WfxTopPanelComponent } from './components/wfx-toppanel/wfx-toppanel.component';
import { WfxRightPanelComponent } from './components/wfx-rightpanel/wfx-rightpanel.component';
import { WfxLeftPanelComponent } from './components/wfx-leftpanel/wfx-leftpanel.component';
import { WfxBottomPanelComponent } from './components/wfx-bottompanel/wfx-bottompanel.component';
import { WfxContentPanelComponent } from './components/wfx-contentpanel/wfx-contentpanel.component';
import { WfxContentTopPanelComponent } from './components/wfx-contenttoppanel/wfx-contenttoppanel.component';
import { WfxContentBottomPanelComponent } from './components/wfx-contentbottompanel/wfx-contentbottompanel.component';
import { WfxContentRightPanelComponent } from './components/wfx-contentrightpanel/wfx-contentrightpanel.component';
import { WfxContentRightNavComponent } from './components/wfx-contentrightnav/wfx-contentrightnav.component';
import { WfxContentLeftNavComponent } from './components/wfx-contentleftnav/wfx-contentleftnav.component';
import { WfxPlmItemLibraryRoutingModule } from './wfxplmitemlibrary-routing.module';
import { WfxItemSharedModule } from '../wfxitemshared/wfxitemshared.module';
import { WfxGalleryViewComponent } from './components/wfx-galleryview/wfx-gallery-view.component';

import { WfxSummaryComponent } from './components/wfx-summary/wfx-summary.component';
import {DemoComponent} from './components/demo/demo.component';
import { WfxPublishComponent } from './components/wfx-publish/wfx-publish.component';
import { WfxplmcostsheetComponent } from './components/wfxplmcostsheet/wfxplmcostsheet.component';

@NgModule({
  declarations: [ WfxGalleryViewComponent , CtrlshiftselectDirective  , SelectableDirective,
     KeyboardNavigationDirective,
    WfxItemLibraryComponent, WfxTopPanelComponent, WfxRightPanelComponent, WfxLeftPanelComponent, WfxBottomPanelComponent,
  WfxContentPanelComponent, WfxContentTopPanelComponent, WfxContentBottomPanelComponent,
    WfxContentRightPanelComponent, WfxContentRightNavComponent, WfxContentLeftNavComponent, WfxSummaryComponent,
    DemoComponent, WfxPublishComponent, LoadmoreDirective, WfxplmcostsheetComponent
  ],
  imports: [
    CommonModule, HttpClientModule,
    WfxPlmItemLibraryRoutingModule, WfxSharedModule, WfxItemSharedModule
  ],
  entryComponents : [WfxPublishComponent]
})
export class WfxPlmItemLibraryModule { }
