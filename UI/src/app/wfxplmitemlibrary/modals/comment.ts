export class Comment {
    commentID: number;
    id: string;
    ReplyID: number;
    comment: string;
    fullName: string;
    CreatedOn: string;
    userImageURL: string;
    commentCount: number;
    IsReplying?: Boolean ;
    parentCommentID: number;
    commentReply?: Comment[];
    ReadMore?: number;
    isLike: boolean;
    count: number;
}
export class CommentDataModel {
    CommentData: Comment;
}

export class CommentResultModel {
    ResponseID: number;
    data: Comment;
    message: string;
    status: string;
}


