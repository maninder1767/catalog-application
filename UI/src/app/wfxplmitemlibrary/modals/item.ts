export class Item {
    itemId: string;
    ObjectID: number;
    PicFileName: string;
    itemCode: string;
    buyerReference: string;
    itemName: string;
    plcName: string;
    supplierReference: string;
    plcColor: string;
    plcColorChip: string;
    commentCount: string;
    CreatedByFullName: string;
    IsBookmark: Boolean;
    likeCount: number;
    NoItemImage: Boolean = false;
    isLike: boolean;
    active: boolean;
    checkbox: boolean = false;
    imageUrl: string;
}
export class ItemData {
    data: Item[];
}
export class ItemResultModel {
    item: Item[];
    ErrorMsg: string;
    Status: string;
}
