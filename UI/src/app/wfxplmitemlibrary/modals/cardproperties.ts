export class Cardproperties {
    cardHeight_zero: number;
    cardHeight_fifty: number;
    cardHeight_hundred: number;

    cardWidth_zero: number;
    cardWidth_fifty: number;
    cardWidth_hundred: number;

    cardImageHeight_zero: number;
    cardImageHeight_fifty: number;
    cardImageHeight_hundred: number;

    cardImageWidth_zero: number;
    cardImageWidth_fifty: number;
    cardImageWidth_hundred: number;

    cardTitleFontSize_zero: number;
    cardTitleFontSize_fifty: number;
    cardTitleFontSize_hundred: number;

    cardTitleWidth_zero: number;
    cardTitleWidth_fifty: number;
    cardTitleWidth_hundred: number;

    cardSubTitleFontSize_zero: number;
    cardSubTitleFontSize_fifty: number;
    cardSubTitleFontSize_hundred: number;

    cardSubTitleWidth_zero: number;
    cardSubTitleWidth_fifty: number;
    cardSubTitleWidth_hundred: number;

    cardContentFontSize_zero: number;
    cardContentFontSize_fifty: number;
    cardContentFontSize_hundred: number;

    cardContentWidth_zero: number;
    cardContentWidth_fifty: number;
    cardContentWidth_hundred: number;

    cardPLSFontSize_zero: number;
    cardPLSFontSize_fifty: number;
    cardPLSFontSize_hundred: number;

    cardPLSWidth_zero: number;
    cardPLSWidth_fifty: number;
    cardPLSWidth_hundred: number;

    cardiconWidth_zero: number;
    cardiconWidth_fifty: number;
    cardiconWidth_hundred: number;

    cardiconHeight_zero: number;
    cardiconHeight_fifty: number;
    cardiconHeight_hundred: number;
}