import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WfxItemLibraryComponent } from './components/wfx-itemlibrary/wfx-itemlibrary.component';
import { DemoComponent } from './components/demo/demo.component';

const routes: Routes = [
  {
    path : '' , component: WfxItemLibraryComponent,
  },
  {
    path : 'itemlibrary' , component: WfxItemLibraryComponent,
  }
  ,
  {
    path : 'wfxDemo' , component: DemoComponent,
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WfxPlmItemLibraryRoutingModule { }
