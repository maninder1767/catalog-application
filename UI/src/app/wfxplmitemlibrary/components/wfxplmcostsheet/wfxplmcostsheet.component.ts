import { Component, OnInit, OnDestroy, Input, OnChanges, SimpleChange, SimpleChanges } from '@angular/core';
import {
  WFXGridCellAttributes, WFXResultModel, WfxTitleBar,
  WfxToolAction, WfxComponentAttributes, WFXAttribute
} from '../../../wfxlibrary/wfxsharedmodels/wfxcommonmodel';
import { Subject } from 'rxjs';
import { PageParam } from '../../../wfxlibrary/wfxsharedmodels/wfxqueryparam';
import { take, takeUntil } from 'rxjs/operators';
import { WfxCommonFunctions } from '../../../wfxlibrary/wfxsharedfunction/wfxcommonfunction';
import { WfxcommonService } from '../../../wfxlibrary/wfxsharedservices/wfxcommon.service';
import { WfxGlobal } from '../../../wfxlibrary/wfxsharedscript/wfxglobal';
import * as _moment from 'moment';
import { default as _rollupMoment } from 'moment';
import { WfxShowMessageservice } from '../../../wfxlibrary/wfxsharedservices/wfxshowmessage.service';
import { ItemLibraryDataService } from '../../../wfxitemshared/services/itemdata.service';
import { Globals } from '../../../../app/globals';
import { ItemLibraryService } from '../../../wfxitemshared/services/itemlibrary.service';
import { ItemLibraryGlobals } from '../../itemLibrary.global';
const moment = _rollupMoment || _moment;

@Component({
  selector: 'app-wfxplmcostsheet',
  templateUrl: './wfxplmcostsheet.component.html',
  styleUrls: ['./wfxplmcostsheet.component.css']
})
export class WfxplmcostsheetComponent implements OnInit, OnChanges, OnDestroy {
  constructor(private cmf: WfxCommonFunctions, public commonsvc: WfxcommonService,
    private global: WfxGlobal, private msgsvc: WfxShowMessageservice,
    private itemsvc: ItemLibraryDataService, private appGlobal: Globals,
    private itemlSVC: ItemLibraryService, private itemGlobal: ItemLibraryGlobals) { }

  componentObject: any;
  colDef: WFXGridCellAttributes[];
  colDef1: WFXGridCellAttributes[];
  colDef2: WFXGridCellAttributes[];
  columnDefs: any;
  gridOptions: any;
  gridApi: any;
  gridColumnApi: any;
  gridJsonData: any[];
  pagination = true;
  headerRowGroup: any = 'always';
  floatingFilter = false;
  sideBar = true;

  gridApi1: any;
  gridColumnApi1: any;

  /** Subject that emits when the component has been destroyed. */
  protected onDestroy = new Subject<void>();

  ddlOption: any;
  ddlLoading = false;
  arrColorValues: any;
  arrSizeValues: any;

  showPageLoading = true;
  titleBarJsonDef: WfxTitleBar = {
    caption: '', titlebartype: 'pagetitlebar', titlebarid: 'titlebarCostSheet',
    tooltip: { tooltipvalue: '', tooltipvaluetype: 'text', tooltipposition: 'below' },
    toollist: [
      {
        toolname: 'Save', visible: true, disable: false, tooltip: { tooltipvalue: 'Save', tooltipposition: 'below' }
      },
    ],
  };

  linkButtonJsonDef = {
    caption: 'Save', value: 'Save', tooltip: { tooltipvalue: 'Save', tooltipvaluetype: '', tooltipposition: 'below' },
  };

  ProductCatCode: '';
  gridHeight;

  arrCoshSheetDefJsonData: any[] = [];
  itemJsonDef: WfxComponentAttributes[] = [
    {
      itemid: 'ddlCostSheetDef', caption: 'Cost Sheet Definition', valuetype: 'dbfield', value: 'CostSheetDefinitionID', valuetext: 'CostSheetDefinitionName',
      mandatory: true, addselect: false, width: '100px'
    },
  ];
  headerJsonData: any = {
    CostSheetDefinitionID: 1,
    CostSheetDefinitionName: 'Def1'
  };

  ngOnInit() {
    // this.global.isPLM = true;
    this.cmf.setPageTitle('PLM CostSheet');
    this.componentObject = this;
    // this.createJsonDef();
    // this.getJsonDefFromDB();
    this.ProductCatCode = this.cmf.getQueryParamValue('ProductCatCode');
    this.BindAllDLL();
    if (this.gridHeight === undefined) {
      this.gridHeight = window.innerHeight - 150;
    }
    this.itemlSVC.costSheetDDlValueSubscription.pipe(take(1), takeUntil(this.onDestroy))
      .subscribe(val => {
        this.getJsonDefFromDB(this.itemGlobal.costingDefaultSelectedView.value);
      });
    this.itemlSVC.costSheetDefSelectedValueSubscription.pipe(take(1), takeUntil(this.onDestroy)).
      subscribe(e =>
        this.ddlCostSheetTypeOnChange(e)
      );
  }

  ngOnChanges(changes: SimpleChanges) {
  }

  ngOnDestroy() {
    this.onDestroy.next();
    this.onDestroy.complete();
  }

  getJsonDefFromDB(jsonDefType?) {
    if (!jsonDefType) {
      jsonDefType = '1';
    }
    this.itemsvc.getRequest('Item/Costing/Definition/' + jsonDefType)
      .pipe(take(1), takeUntil(this.onDestroy))
      .subscribe((res: any) => {
        this.showPageLoading = false;
        this.colDef = res.data;
      });
  }

  createJsonDef() {

  }

  OnGridReady(params) {
    this.gridOptions = params;
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.getRowData();
  }

  getRowData(costSheetDefType?) {
    this.itemsvc.postRequest('Item/Costing/' + (costSheetDefType ? costSheetDefType : this.itemGlobal.costingDefaultSelectedView.value))
      .pipe(take(1), takeUntil(this.onDestroy))
      .subscribe((res: any) => {
        this.showPageLoading = false;
        const articleNCostSheetData = res.data;
        for (const row of articleNCostSheetData) {
          if (row.CostSheetJsonData) {
            for (const key in row.CostSheetJsonData) {
              if (key) {
                row[key] = row.CostSheetJsonData[key];
              }
            }
            delete row.CostSheetJsonData;
          }
        }
        this.gridJsonData = articleNCostSheetData;
        if (!this.gridJsonData.length) {
          this.gridApi.showNoRowsOverlay();
        }
      });
  }

  lnkArticleOnClick(e?) {
    // console.log(e);
    // console.log(this);
  }

  gridWFXPLMCostSheet_IsCellEditable(params, _this) {
    if (params && params.data && params.data.isRowEditable) {
      return true;
    } else {
      return false;
    }
  }

  gridWFXPLMCostSheet_getContextMenuItems(params, _this) {
    const result = [
      {
        name: 'Add Cost Sheet',
        action: () => {
          _this.addRowOrMakeEditable(params);
        },
      },
    ];
    const submitCostSheet = {
      name: 'Submit Cost Sheet',
      action: () => {
        _this.submitCostSheet(params);
      },
    };
    const deleteCostSheet = {
      name: 'Delete Cost Sheet',
      action: () => {
        _this.deleteCostSheet(params);
      },
    };
    const approveCostSheet = {
      name: 'Approve Cost Sheet',
      action: () => {
        _this.approveCostSheet(params);
      },
    };
    const RejectCostSheet = {
      name: 'Reject Cost Sheet',
      action: () => {
        _this.rejectCostSheet(params);
      },
    };
    const reviseCostSheet = {
      name: 'Revise',
      action: () => {
        _this.reviseCostSheet(params);
      },
    };
    if (params.node.data.costing_status) {
      params.node.data.costing_status = params.node.data.costing_status.replace(new RegExp(String.fromCharCode(160), "g"), ' ');
    }
    if ((params.node.data.isRowEditable || Number(params.node.data.costing_id) > 0) && params.node.data.costing_status.toLowerCase() === 'in progress') {
      result.push(deleteCostSheet);
      result.push(submitCostSheet);
    }
    if (this.appGlobal.costingApproval && Number(params.node.data.costing_id) > 0 && params.node.data.costing_status.toLowerCase() === 'ready for review') {
      result.push(approveCostSheet);
      result.push(RejectCostSheet);
    }
    if (Number(params.node.data.costing_id) > 0 && (params.node.data.costing_status.toLowerCase() === 'ready for review' ||
      params.node.data.costing_status === 'Approved' || params.node.data.costing_status === 'Rejected')) {
      result.push(reviseCostSheet);
    }
    result.push(...params.defaultItems);
    return result;
  }

  approveCostSheet(params) {
    this.updateData(params, 'Approve');
  }

  rejectCostSheet(params) {
    this.updateData(params, 'Reject');
  }

  submitCostSheet(params) {
    this.saveCostSheet(params, 'Submit');
  }

  reviseCostSheet(params) {
    // this.updateData(params, 'Revise');
  }

  updateData(params, action, data?) {
    const costing_id = params.node.data.costing_id;
    if (costing_id > 0) {
      this.showPageLoading = true;
      this.itemsvc.putRequest('Item/Costing/' + this.itemGlobal.costingDefaultSelectedView.value + '?action=' + action, {},
        [{ costing_id: costing_id }])
        .pipe(take(1), takeUntil(this.onDestroy))
        .subscribe((res: any) => {
          this.showPageLoading = false;
          if (res.status === 'Success') {
            if (action === 'Approve') {
              action = 'Approved';
              this.cmf.updateCellFromRowNode(params.node, 'costing_status', 'Approved');
            } else if (action === 'Reject') {
              action = 'Rejected';
              this.cmf.updateCellFromRowNode(params.node, 'costing_status', 'Rejected');
            }
            this.msgsvc.showSnackBar(action + ' successfully!', '', 'Success');
          }
        });
    }
  }

  deleteCostSheet(params) {
    const rowData = params.node.data;
    const costing_id = rowData.costing_id;
    const dialogRef = this.cmf.showConfirmationDialog('Delete Confirmation!', 'Are you sure you want to continue?',
      'No', 'Yes');
    dialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        const selectedData: any[] = [rowData];
        if (costing_id > 0) {
          this.showPageLoading = true;
          this.itemsvc.deleteRequest('Costing/' + costing_id.toString())
            .pipe(take(1), takeUntil(this.onDestroy))
            .subscribe((res: WFXResultModel) => {
              this.showPageLoading = false;
              if (!res.ErrorMsg) {
                if (this.checkIfRowExistsOfSameArticle(params)) {
                  this.cmf.deleteParticularRow(this.gridApi, selectedData);
                } else {
                  const pageparams = {
                    'Params': [
                      {
                        'ParamName': 'article_id',
                        'ParamValue': params.node.data.article_id
                      }
                    ]
                  };
                  this.itemsvc.postRequest('Item/Costing/' + this.itemGlobal.costingDefaultSelectedView.value, undefined ,pageparams)
                    .pipe(take(1), takeUntil(this.onDestroy))
                    .subscribe((resp: any) => {
                      this.showPageLoading = false;
                      const articleNCostSheetData = resp.data;
                      for (const row of articleNCostSheetData) {
                        if (row.CostSheetJsonData) {
                          for (const key in row.CostSheetJsonData) {
                            if (key) {
                              row[key] = row.CostSheetJsonData[key];
                            }
                          }
                          delete row.CostSheetJsonData;
                        }
                      }
                      const item = articleNCostSheetData[0];
                      item.CostingID = this.cmf.getMaxRowID(this.gridApi);
                      item.costing_status = 'In Progress';
                      item.isRowEditable = false;
                      this.cmf.updateRowDataFromRowNode(params.node, item);
                    });
                }
                this.msgsvc.showSnackBar('Deleted successfully!', '', 'Success');
              }
            });
        }
      }
    });
  }

  checkIfRowExistsOfSameArticle(params): boolean {
    const ArticleID = params.node.data.article_id;
    const costing_id = params.node.data.costing_id;
    let rowExists = false;
    this.gridApi.forEachNode((eachnode, index) => {
      if (eachnode.data.article_id === ArticleID && eachnode.data.costing_id !== costing_id) {
        rowExists = true;
      }
    });
    return rowExists;
  }

  addRowOrMakeEditable(params) {
    const node = params.node;
    if (node.data.isRowEditable) {
      const pageparams = {
        'Params': [
          {
            'ParamName': 'article_id',
            'ParamValue': params.node.data.article_id
          }
        ]
      };
      this.itemsvc.postRequest('Item/Costing/' + this.itemGlobal.costingDefaultSelectedView.value,undefined ,pageparams)
        .pipe(take(1), takeUntil(this.onDestroy))
        .subscribe((res: any) => {
          this.showPageLoading = false;
          const articleNCostSheetData = res.data;
          for (const row of articleNCostSheetData) {
            if (row.CostSheetJsonData) {
              for (const key in row.CostSheetJsonData) {
                if (key) {
                  row[key] = row.CostSheetJsonData[key];
                }
              }
              delete row.CostSheetJsonData;
            }
          }
          const item = articleNCostSheetData[0];
          item.CostingID = this.cmf.getMaxRowID(this.gridApi);
          item.costing_status = 'In Progress';
          const rowIndex = this.getLastRowIndex(node);
          const result = this.cmf.gridAddRow(this.gridApi, item, rowIndex + 1, false);
          this.setDefaultValues(result.add[0]);
        });
    } else {
      node.data.isRowEditable = true;
      this.setDefaultValues(node);
    }
  }

  getLastRowIndex(node) {
    const article_id = node.data.article_id;
    let rowIndex = 0;
    this.gridApi.forEachNode((eachnode, index) => {
      if (eachnode.data.article_id && article_id && eachnode.data.article_id === article_id) {
        rowIndex = eachnode.rowIndex;
      } else {
        return true;
      }
    });
    return rowIndex;
  }

  setDefaultValues(node) {
    if (!node.data.costing_status) {
      node.data.costing_status = '';
    }
    this.cmf.updateCellFromRowNode(node, 'costing_colors', 'ALL');
    this.cmf.updateCellFromRowNode(node, 'costing_sizes', 'ALL');
    this.cmf.updateCellFromRowNode(node, 'costing_date', moment(new Date()).format('L'));
    this.cmf.updateCellFromRowNode(node, 'costing_status', 'In progress');
  }

  BindAllDLL() {
    this.ddlOption = ['CurrencySymbol', 'CostingCurrency']; // 'CostSheetDefType'
    for (const opt of this.ddlOption) {
      this.getDDLDataNBindData(opt);
    }
  }

  getDDLDataNBindData(MetaDataFor) {
    this.ddlLoading = true;
    const pageParam: PageParam[] = [];
    let url = '';
    let key = '';
    if (MetaDataFor === 'CurrencySymbol') {
      url = 'Master/Data?ObjectType=ItemCosting&DataSource=CurrencySymbol';
      key = 'ObjectType|ItemCosting~DataSource|currencysymbol~';
    } else if (MetaDataFor === 'CostingCurrency') {
      url = 'Master/Data?ObjectType=ItemCosting&DataSource=CostingCurrency';
      key = 'ObjectType|ItemCosting~DataSource|costingcurrency~';
    }
    this.itemsvc.getRequest(url, undefined, key)
      .pipe(take(1), takeUntil(this.onDestroy))
      .subscribe((res: any) => {
        this.ddlLoading = false;
        if (MetaDataFor === 'CurrencySymbol') {
          this.global.gObjDDLHashData['CurrencySymbolList'] = res.data;
        }
      });
  }

  gridWFXPLMCostSheet_getDDLPageParams(params) {
    const pageParam: PageParam[] = [];
    if (params.colDef.field === 'ColorNameList') {
      pageParam.push(new PageParam('MetaDataFor', 'Color'));
    } else if (params.colDef.field === 'SizeNameList') {
      pageParam.push(new PageParam('MetaDataFor', 'Size'));
    } else if (params.colDef.field === 'SupplierCompanyName') {
      pageParam.push(new PageParam('MetaDataFor', 'Supplier'));
    } else if (params.colDef.field === 'CostSheetTypeName') {
      pageParam.push(new PageParam('MetaDataFor', 'CostSheetType'));
    } else if (params.colDef.field === 'DeliveryTermName') {
      pageParam.push(new PageParam('MetaDataFor', 'DeliveryTerm'));
    } else if (params.colDef.field === 'FOBCurrencyName') {
      pageParam.push(new PageParam('MetaDataFor', 'FOBCurrencyCode'));
    }
    pageParam.push(new PageParam('ArticleID', params.node.data.article_id));
    pageParam.push(new PageParam('ArticleCode', params.node.data.article_code));
    pageParam.push(new PageParam('fromPage', 'wfxplmcostsheet'));
    pageParam.push(new PageParam('objectName', 'MetaData'));
    return pageParam;
  }

  costSheetTypeOnChange(e) {
    // console.log(e);
    // console.log(this);
  }
  btnOnClick(e) {
    this.columnDefs[2].editable = true;
    this.gridApi.setColumnDefs(this.columnDefs);
  }

  linkButtonOnClick(e) {
    this.saveCostSheet();
  }

  saveCostSheet(params?, action = 'Save') {
    this.showPageLoading = true;
    if (this.gridJsonData.length > 0) {
      // const data: WFXResultModel = new WFXResultModel();
      const data: any = [];
      if (action === 'Submit') {
        data.push(params.node.data);
      } else {
        this.gridApi.forEachNode(node => {
          if (node.data.isRowEditable) {
            data.push(node.data);
          }
        });
      }
      if (data && data.length <= 0) {
        this.showPageLoading = false;
        this.msgsvc.showSnackBar('No data has been edited!', '', 'Info');
        return;
      }
      if (action === 'Save' || (action === 'Submit' && this.cmf.validateGridJson(this.gridApi, 'isRowEditable', params.node))) {
        // const pageParam: PageParam[] = [];
        // pageParam.push(new PageParam('Action', action));
        // pageParam.push(new PageParam('objectName', 'PLMCostSheet'));
        // this.commonsvc.saveData('WFXCommon/SaveData', data, pageParam)
        this.itemsvc.putRequest('Item/Costing/' + this.itemGlobal.costingDefaultSelectedView.value + '?action=' + action, undefined, data)
          .pipe(take(1), takeUntil(this.onDestroy))
          .subscribe((res: any) => {
            this.showPageLoading = false;
            if (res.status === 'Success') {
              let msg = '';
              if (action === 'Save') {
                msg = 'Saved';
              } else if (action === 'Submit') {
                msg = 'Submitted';
              }
              this.msgsvc.showSnackBar(msg + ' Successfully!', '', 'Success');
              if (action === 'Save') {
                this.getRowData();
              } else if (action === 'Submit') {
                const nodeData = params.node.data;
                nodeData.costing_status = 'Ready for Review';
                nodeData.costing_id = res.data[0].costing_id;
                this.cmf.updateRowDataFromRowNode(params.node, nodeData);
              }
            }
          });
      } else {
        this.showPageLoading = false;
      }
    } else {
      this.showPageLoading = false;
    }
  }

  ddlFOBPriceCurrencyOnChange(params, _this?) {
    const key = 'MetaDataFor|FOBCurrencyCode~ArticleCode|' +
      params.node.data.ArticleCode + '~fromPage|wfxplmcostsheet~objectName|MetaData~';
    let ExchangeRateEUR = this.cmf.getFilterJsonData(this.global.gObjDDLHashData[key], 'Code', 'EUR', 'RateOfExchange1');
    ExchangeRateEUR = isNaN(ExchangeRateEUR) ? 0 : this.cmf.WFXRound(ExchangeRateEUR, 8);
    let ExchangeRateUSD = this.cmf.getFilterJsonData(this.global.gObjDDLHashData[key], 'Code', 'USD', 'RateOfExchange1');
    ExchangeRateUSD = isNaN(ExchangeRateUSD) ? 0 : this.cmf.WFXRound(ExchangeRateUSD, 8);
    this.cmf.updateCellFromRowNode(params.node, 'txtExchangeRateEUR', ExchangeRateEUR);
    this.cmf.updateCellFromRowNode(params.node, 'txtExchangeRateUSD', ExchangeRateUSD);
  }

  gridWFXPLMCostSheet_OnCellValueChanged(colID, params) {
  }

  OnEveryCellValueChanged(opt) {
    const params = opt.node;
    if (opt.colID === 'ddlFOBPriceCurrency' || opt.colID === 'costing_date') {
      this.updateExchangeRate(params);
    }
  }

  ddlCostSheetTypeOnChange(e) {
    this.showPageLoading = true;
    this.getJsonDefFromDB(e.value.toString());
    this.showPageLoading = true;
    this.getRowData(e.value.toString());
  }

  updateExchangeRate(params) {
    // const key = 'MetaDataFor|FOBCurrencyCode~ArticleCode|' + params.node.data.ArticleCode +
    // '~fromPage|wfxplmcostsheet~objectName|MetaData~';
    // let ExchangeRateEUR = this.cmf.getFilterJsonData(this.global.gObjDDLHashData[key], 'Code', 'EUR', 'RateOfExchange1');
    // ExchangeRateEUR = isNaN(ExchangeRateEUR) ? 0 : this.cmf.WFXRound(ExchangeRateEUR, 8);
    // let ExchangeRateUSD = this.cmf.getFilterJsonData(this.global.gObjDDLHashData[key], 'Code', 'USD', 'RateOfExchange1');
    // ExchangeRateUSD = isNaN(ExchangeRateUSD) ? 0 : this.cmf.WFXRound(ExchangeRateUSD, 8);
    // this.cmf.updateCellFromRowNode(params.node, 'txtExchangeRateEUR', ExchangeRateEUR);
    // this.cmf.updateCellFromRowNode(params.node, 'txtExchangeRateUSD', ExchangeRateUSD);
  }
  lookupCostingExchangeRate(params, updateField, paramlist: WFXAttribute[], ...paramlistdata) {
    if (paramlist) {
      const data = this.cmf.returnDataOfJsonDefAttrNameAsParamNameWithData(paramlist, paramlistdata);
      if (data && data.length > 0) {
        this.itemsvc.postRequest('Master/Lookup/lookupCostingExchangeRate', undefined, data)
          .pipe(take(1), takeUntil(this.onDestroy))
          .subscribe((res: any) => {
            if (res.status === 'Success') {
              this.cmf.updateCellFromRowNode(params.node, updateField, res.data);
            }
          });
      }
    }
  }

  lookupSupplierCountry(params, updateField, paramlist: WFXAttribute[], ...paramlistdata) {
    if (paramlist) {
      const data = this.cmf.returnDataOfJsonDefAttrNameAsParamNameWithData(paramlist, paramlistdata);
      this.itemsvc.postRequest('Master/Lookup/lookupSupplierCountry', undefined, data)
        .pipe(take(1), takeUntil(this.onDestroy))
        .subscribe((res: any) => {
          if (res.status === 'Success') {
            this.cmf.updateCellFromRowNode(params.node, updateField, res.data);
          }
        });
    }
  }
}
