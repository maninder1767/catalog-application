import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WfxplmcostsheetComponent } from './wfxplmcostsheet.component';

describe('WfxplmcostsheetComponent', () => {
  let component: WfxplmcostsheetComponent;
  let fixture: ComponentFixture<WfxplmcostsheetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WfxplmcostsheetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WfxplmcostsheetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
