import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WfxContentBottomPanelComponent } from './wfx-contentbottompanel.component';

describe('WfxContentBottomPanelComponent', () => {
  let component: WfxContentBottomPanelComponent;
  let fixture: ComponentFixture<WfxContentBottomPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WfxContentBottomPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WfxContentBottomPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
