import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WfxPublishComponent } from './wfx-publish.component';

describe('WfxPublishComponent', () => {
  let component: WfxPublishComponent;
  let fixture: ComponentFixture<WfxPublishComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WfxPublishComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WfxPublishComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
