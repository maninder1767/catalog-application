import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-wfx-publish',
  templateUrl: './wfx-publish.component.html',
  styleUrls: ['./wfx-publish.component.css']
})
export class WfxPublishComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<WfxPublishComponent>) { }

  ngOnInit() {
  }
  OnbtnCompanySelct() {
    console.log('I am from button click');
  }
  onClosePopup() {
    this.dialogRef.close(true);
  }
}
