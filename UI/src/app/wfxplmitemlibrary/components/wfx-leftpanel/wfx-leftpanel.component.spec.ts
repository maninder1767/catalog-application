import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WfxLeftPanelComponent } from './wfx-leftpanel.component';

describe('WfxLeftPanelComponent', () => {
  let component: WfxLeftPanelComponent;
  let fixture: ComponentFixture<WfxLeftPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WfxLeftPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WfxLeftPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
