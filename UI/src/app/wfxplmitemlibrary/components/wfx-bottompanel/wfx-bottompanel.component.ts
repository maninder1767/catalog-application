import { ItemLibraryService } from './../../../wfxitemshared/services/itemlibrary.service';
import { WfxShowMessageservice } from './../../../wfxlibrary/wfxsharedservices/wfxshowmessage.service';
import { Filter } from './../../../wfxitemshared/modals/filter';
import { data } from './../../../wfxitemshared/components/wfx-tableview/db.json';
import { WfxBottompanelService } from './wfx-bottompanel.service';
import { WfxCheckboxComponent } from './../../../wfxlibrary/wfxcomponent-ts/wfxcheckbox';
import { Component, OnInit, Input, OnChanges, ViewChild, HostListener, ElementRef } from '@angular/core';
import { WfxContextmenuComponent } from '../../../wfxitemshared/components/wfx-contextmenu/wfx-contextmenu.component';
import { ContextMenuService } from 'ngx-contextmenu';
import { WfxComponentAttributes } from '../../../wfxlibrary/wfxsharedmodels/wfxcommonmodel';
import { MatDialog, MatDialogRef } from '@angular/material';
import { WfxPublishComponent } from '../wfx-publish/wfx-publish.component';
import { ItemLibraryDataService } from '../../../wfxitemshared/services/itemdata.service';
import { Globals } from '../../../globals';
import { ItemLibraryGlobals } from '../../itemLibrary.global';
import { ViewType } from '../../../wfxitemshared/constants/enum';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
@Component({
  selector: 'app-wfx-bottompanel',
  templateUrl: './wfx-bottompanel.component.html',
  styleUrls: ['./wfx-bottompanel.component.css']
})

export class WfxBottomPanelComponent implements OnInit, OnChanges {
  @Input() bottomPanelVisible;
  divHeight = '0px';
  selectedItemIds = [];
  selectedItemIdsString = '';
  checkboxJsonData;
  bottomMenusVisible = false;
  wfxLabelitemJsonDef: WfxComponentAttributes;
  wfxlabeljsonData;
  bottamList;
  itemID;
  extendedMenusArry;
  shareVisible = true;
  generateTechpackVisible = true;
  multiarticleUpdateVisible = true;
  checkboxJsonDef;
  componentObject;
  selectedItemLableText;
  selecteditemisGreaterThanTen = false;
  @ViewChild('checkbox', { static: true }) checkbox: WfxCheckboxComponent;
  constructor(private bottamService: WfxBottompanelService,
    private eRef: ElementRef, public dialog: MatDialog,
    private itemdata: ItemLibraryDataService, private global: Globals,
    public itemGlobal: ItemLibraryGlobals, private msgsvc: WfxShowMessageservice,
    private itemLibraryService: ItemLibraryService) { }
  @HostListener('document:click', ['$event'])
  clickout(event) {
    if (this.eRef.nativeElement.contains(event.target)) {
      if (this.bottomMenusVisible === true) {
      }
    } else {
      this.bottomMenusVisible = false;
    }
  }
 
  ngOnInit() {
    this.componentObject = this;
    // this.openbottompanel();
    this.bottamService.SelectedItemIdsSubsciption
      .subscribe(message => {
        this.selectedItemIds = message.itemIds;
        if (this.itemGlobal.arrSelectedItemId.length > 1) {
          this.selectedItemLableText = ' ITEMS SELECTED';
        }else {
          this.selectedItemLableText = ' ITEM SELECTED';
          this.bindBottomList();
        }
        if (this.itemGlobal.arrSelectedItemId.length > 10) {
          this.selecteditemisGreaterThanTen = true;
        }else {
          this.selecteditemisGreaterThanTen = false;
        }
        this.selectedItemIdsString = '';
        for (let i = 0; i < this.itemGlobal.arrSelectedItemId.length; i++) {
          this.selectedItemIdsString += this.itemGlobal.arrSelectedItemId[i];
          if ((this.itemGlobal.arrSelectedItemId.length - (i + 1)) !== 0) {
            this.selectedItemIdsString += ',';
          }
        }
        if (message.itemIds.length !== 0) {
          this.bottomPanelVisible = true;
          this.checkboxJsonData.checkBoxValue = message.allItems;
          this.openbottompanel();
        } else {
          this.bottomPanelVisible = false;
          this.bottomMenusVisible = false;
          this.openbottompanel();
        }
        this.wfxlabeljsonData = { fname: this.selectedItemLableText };
      });
    this.checkboxJsonDef = {
      readonly: false, mandatory: true, caption: '',
      value: 'checkBoxValue', disable: false, labelposition: 'after', valuetype: 'dbfield',
      tooltip: { tooltipvalue: 'Select ALL', tooltipvaluetype: '', tooltipposition: 'below' },
      className : 'bottomPanelwfxcheckbox'
    };
    this.checkboxJsonData = {
      fname: '', checkBoxValue: false, selectedRadioValue: 'Winter', SwitchOn: false
    };

    this.wfxLabelitemJsonDef = {
      readonly: false, mandatory: false, caption: 'First Name', disable: false,
      value: 'fname', valuetype: 'dbfield',
      tooltip: { tooltipvalue: 'First Name', tooltipvaluetype: 'dbfield', tooltipposition: 'below' },
      inputtype: 'textbox', itemid: 'txtFirstName'
    };
    if (this.itemGlobal.arrSelectedItemId.length > 1) {
      this.wfxlabeljsonData = { fname: ' ITEMS SELECTED' };
    }else {
      this.wfxlabeljsonData = { fname: ' ITEM SELECTED' };
    }
  }
  ngOnChanges() {
    this.openbottompanel();
  }
  openbottompanel() {
    if (this.bottomPanelVisible === true) {
      this.divHeight = '40px';
    } else {
      this.divHeight = '0px';
      this.shareVisible = false;
      this.multiarticleUpdateVisible = false;
      this.generateTechpackVisible = false;
    }
  }
 
  OnShareClick() {
    this.displayPublishDialog();
    alert("Sorry you Can't Share AnyThing");
  }
  OnUpdateClick() {
    this.getSession('UPDATE', 'UpadteMultipleStyles');
  }
  OnUpdateClick1(url){
    alert("This Functionality is temporarily blocked  Redirected to : "+  url);
  }
  OnGenerateTechPackClick() {
    this.getSession('PRINT', 'GenerateMultipleTechpack');
   
  }
  OnGenerateTechPackClick1(url) {
    alert("This Functionality is temporarily blocked  Redirected to : "+  url);
    }
  ExtendedMenuClick() {
    console.log('Extended Menu clicked');
  }
  onCheckboxChange(data) {
    let tooltipvalue = 'SELECT ALL';
    this.bottamService.selectAllItems({
      allItems: data.checkBoxValue
    });
    if (data.checkBoxValue === true) {
      tooltipvalue = 'DESELECT ALL';
    }
    this.checkboxJsonDef.tooltip.tooltipvalue = tooltipvalue;
  }
  public onContextMenu($event: MouseEvent, item?: any): void {
    if (this.bottomMenusVisible === false) {
      this.bottomMenusVisible = true;
      this.bindBottomList();
    } else {
      this.bottomMenusVisible = false;
    }
  }
  private displayPublishDialog() {
    if(this.shareVisible === true){
      // const dialogRef = this.dialog.open(WfxPublishComponent, {
      //   height: '511px',
      //   width: '873px',
      // });
    }
  }
  private generateTechPackUrl() {
    if (this.selecteditemisGreaterThanTen === true){ return; }
    if (this.generateTechpackVisible === false){
      return ;
    }
    let url = '';
    switch (this.itemGlobal.currentView) {
      case ViewType.GALLERY:
        url = '../WFXPLM/wfx_PrintTechpack.aspx?SessionIDForPrint=' + this.global.sessionID +
          '&CatalogType=&CatalogCompany=' + this.global.companyCode +
          '&Combocode=' + this.global.productCatCode + '&UserName=' + this.global.userName + '&FromListView=GroupView';
          //console.log(url)
       this.OnGenerateTechPackClick1(url)
        break;
      case ViewType.LIST:
        url = '../WFXPLM/wfx_PrintTechpack.aspx?SessionIDForPrint=' + this.global.sessionID +
          '&CatalogType=1&CatalogCompany=' + this.global.companyCode +
          '&Combocode=' + this.global.productCatCode + '&UserName=' + this.global.userName + '&FromListView=ListView';
          console.log(url)
          this.OnGenerateTechPackClick1(url)
        break;
      default:
        url = '';
        break;
    }
    //window.open(url);
  }

  private updateUrl() {
    if (this.multiarticleUpdateVisible === false){
      return ;
    }
    let url = '';
    switch (this.itemGlobal.currentView) {
      case ViewType.GALLERY:
        url = '../wfx/wfx_BatchUpdateofStyles.aspx?ProductCatCode=' + this.global.productCatCode + '';
        console.log(url)
        alert("This Functionality is temporarily blocked  Redirected to : "+  url);
        break;
      case ViewType.LIST:
        alert("This Functionality is temporarily blocked  Redirected to : "+  url);
        break;
      default:
        url = '';
        break;
    }
   // window.open(url);
  }
  private whereUsedUrl() {
    let url = '';
    switch (this.itemGlobal.currentView) {
      case ViewType.GALLERY:
        url = '../wfxplm/wfx_NewWhereUsed.aspx?ObjectID=' + this.selectedItemIds[0] +
          '&Version=1&sessionID=' + this.global.sessionID + '&ProductCatCode=' + this.global.productCatCode + '&ObjectCode=Article';
         // window.open(url);
         alert("This Functionality is temporarily blocked  Redirected to : "+  url);
       
        break;
      case ViewType.LIST:
          url = '../wfxplm/wfx_NewWhereUsed.aspx?ObjectID=' + this.selectedItemIds[0] +
          '&Version=1&sessionID=' + this.global.sessionID + '&ProductCatCode=' + this.global.productCatCode + '&ObjectCode=Article';
          alert("This Functionality is temporarily blocked  Redirected to : "+  url);
        break;
      default:
        url = '';
        break;
    }
  }

  private createSampleRequest() {
    const url = '../wfx/wfx_BuyerSampleRequestEditNew.aspx?ProductCatCode='
    + this.global.productCatCode + '&SampleType=1&fromPage=wfx_CatalogListNew';
    //window.open(url);
    alert("This Functionality is temporarily blocked  Redirected to : "+  url);
  }

  private createPurchaseOrder() {
    const url = '../wfx/wfx_OrderEdit.aspx?ProductCatCode=' + this.global.productCatCode + '' +
    '&fromPage=wfx_CatalogListNew&ProductCatCode=' + this.global.productCatCode + '&fromPage=wfx_CatalogListNew';
   // window.open(url);
   alert("This Functionality is temporarily blocked  Redirected to : "+  url);
  }

  private createInspection() {
    const url = '../WFXPLM4/WFXQARequest.aspx?' +
    'GUID=' + this.global.guid + '&QARequestID=-1&QARequestType=QualityInspection&' +
    'IsResult=0&ArticleID=' + this.selectedItemIdsString.toString() + '&ProductCatCode=' + this.global.productCatCode + '&FromPage=wfx_CatalogListTop';
    //window.open(url); const url = '../WFXPLM4/WFXQARequest.aspx?' +
    'GUID=' + this.global.guid + '&QARequestID=-1&QARequestType=QualityInspection&' +
    'IsResult=0&ArticleID=' + this.selectedItemIdsString.toString() + '&ProductCatCode=' + this.global.productCatCode + '&FromPage=wfx_CatalogListTop';
    alert("This Functionality is temporarily blocked  Redirected to : "+  url);
  }
  private actTimeAndAction() {
    const url = '../wfx/wfx_NewOrderMain.aspx?ID=&TrackType=3&FromOrderEdit=3&MultipleTrack=1';
    //window.open(url);
    alert("This Functionality is temporarily blocked  Redirected to : "+  url);
  }

  private bindBottomList() {
    // this.extendedMenusArry = this.itemGlobal.arrBottomPanelMenus;
    this.itemdata.getBottomListData(this.itemGlobal.arrSelectedItemId.length)
      .subscribe((res: any) => {
        if (res.status === 'Success') {
          // this.extendedMenusArry = res.data.find(x => x.code === 'menus');
          this.extendedMenusArry = res.data.menus;
          const share = res.data.tools.find(x => x.code === 'SHARE');
          const generatetechpack = res.data.tools.find(x => x.code === 'PRINT');
          const multiarticleupdate = res.data.tools.find(x => x.code === 'UPDATE');
          if (share) { this.shareVisible = true; } else { this.shareVisible = false; }
          if (generatetechpack) { this.generateTechpackVisible = true; } else { this.generateTechpackVisible = false; }
          if (multiarticleupdate) { this.multiarticleUpdateVisible = true; } else { this.multiarticleUpdateVisible = false; }
        }
      });
  }
 
  private itemActiveInactive(a) {
    const paramsId: any[] = [];
    this.itemGlobal.arrSelectedItemId.forEach(element => {
      paramsId.push({ id: element });
    });
    this.itemdata.updateItemStatus(paramsId).
   
    subscribe((res: any) => {
      if (res.status === 'Success') {
        this.msgsvc.showSnackBar('Status Changed Successfully!', '', res.status);
        this.itemGlobal.arrSelectedItemId.forEach(element => {
          // const cur =  this.itemGlobal.arrGalleryViewData.getValue().splice
          // (this.itemGlobal.arrGalleryViewData.getValue().findIndex(x => x.itemId == element) , 1)
          const cur = this.itemGlobal.arrGalleryViewData.getValue().filter((val) => {
            if (val.itemId !== element ) {
              return val;
            }
          });
          this.itemGlobal.arrGalleryViewData.next(cur);
        });
        if (this.itemGlobal.currentView === ViewType.TABLE) {
          this.itemLibraryService.invokeMakeInactiveFromBottomPanel(true);
          // this.selectedItemIds.forEach(selectedRow => {
          //   this.itemGlobal.arrTableRowData.splice(this.itemGlobal.arrTableRowData.findIndex(x => x.article_id === selectedRow), 1);
          // });
          // console.log(this.itemGlobal.arrTableRowData);
        }
        this.selectedItemIds = [];
        this.itemGlobal.arrSelectedItemId = [];
        this.bottomMenusVisible = false;
      }
    });

  }
  private GenerateSKU() {
    const paramsId: any[] = [];
    this.itemGlobal.arrSelectedItemId.forEach(element => {
      paramsId.push({ id: element });
    });
    this.itemdata.itemGenerateSKU(paramsId).
      subscribe((res: any) => {
        if (res.status === 'Success') {
          this.bottomMenusVisible = false;
          this.msgsvc.showSnackBar('Generate SKU Successfully!', '', res.status);
        }
      });

  }
  private GenerateBarcode() {
    const paramsId: any[] = [];
    this.itemGlobal.arrSelectedItemId.forEach(element => {
      paramsId.push({ id: element });
    });
    this.itemdata.itemGenerateBarcode(paramsId).
      subscribe((res: any) => {
        if (res.status === 'Success') {
          this.bottomMenusVisible = false;
          this.msgsvc.showSnackBar('Generate BarCode Successfully!', '', res.status);
        }
      });
  }
  private deleteItemTrack() {
    const paramsId: any[] = [];
    this.itemGlobal.arrSelectedItemId.forEach(element => {
      paramsId.push({ id: element });
    });
    this.itemdata.deleteItemTrack(paramsId).
      subscribe((res: any) => {
        if (res.status === 'Success') {
          // this.bottomMenusVisible = false;
          // this.msgsvc.showSnackBar('Delete Item Track Successfully!', '', res.status);
          alert("Sorry you can't  delete  ");
         }
      });

  }
  private deleteDeliveryTrack() {
    const paramsId: any[] = [];
    this.itemGlobal.arrSelectedItemId.forEach(element => {
      paramsId.push({ id: element });
    });
    this.itemdata.deleteDeliveryTrack(paramsId).
      subscribe((res: any) => {
        if (res.status === 'Success') {
          this.bottomMenusVisible = false;
          this.msgsvc.showSnackBar('Delete Delivery Track Successfully!', '', res.status);
        }
      });

  }
  menuClick(menus: any) {
    switch (menus.code) {
      case 'SHARE':
        this.displayPublishDialog();
        break;
      case 'PRINT':
        this.getSession(menus.code, 'GenerateMultipleTechpack');
        break;
      case 'UPDATE':
        this.getSession(menus.code, 'UpadteMultipleStyles');
        break;
      case 'WHEREUSED':
        this.whereUsedUrl();
        break;
      case 'ACTIVE':
        var a= "Active"
        this.itemActiveInactive(a);
        break;
      case 'INACTIVE':
          var a= "InActive"
          this.itemActiveInactive(a);
          break;
      case 'RAISE_SR':
          this.getSession(menus.code, 'CreatePOSR');
        break;
      case 'RAISE_PO':
          this.getSession(menus.code, 'CreatePOSR');
        this.createPurchaseOrder();
        break;
      case 'ActTnA':
          this.getSession(menus.code, 'AttachMultipleTrack');
        break;
      case 'RAISE_INSPECTION':
          this.createInspection();
          break;
      case 'DelTnA':
        this.deleteItemTrack();
        break;
      case 'SKUCODE':
          this.GenerateSKU();
          break;
      case 'BARCODE':
          this.GenerateBarcode();
            break;
      case 'DelDA':
          this.deleteDeliveryTrack();
            break;
      default:
        break;
    }
    return false;
  }
  private getSession(menuName: string, paramname: string) {
    //  const paramvalue = paramsIds.toString();
    this.itemdata.updateSessionVariable([{ ParamName: paramname, ParamValue: this.selectedItemIdsString.toString() }])
      .subscribe((res: any) => {
        if (res.status === 'Success') {
          switch (menuName) {
            case 'SHARE':
              this.displayPublishDialog();
              break;
            case 'PRINT':
              this.generateTechPackUrl();
              break;
            case 'UPDATE':
              this.updateUrl();
              break;
            case 'WHEREUSED':
              this.whereUsedUrl();
              break;
            case 'RAISE_SR':
                  this.createSampleRequest();
                break;
            case 'RAISE_PO':
                //this.createPurchaseOrder();
                break;
            case 'ActTnA':
                  this.actTimeAndAction();
                  break;
            case 'RAISE_INSPECTION':
                  this.actTimeAndAction();
                  break;
            default:
              break;
          }
        }
      });
  }
}