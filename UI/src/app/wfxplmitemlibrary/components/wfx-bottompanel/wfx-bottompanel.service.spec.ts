import { TestBed } from '@angular/core/testing';

import { WfxBottompanelService } from './wfx-bottompanel.service';

describe('WfxBottompanelService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WfxBottompanelService = TestBed.get(WfxBottompanelService);
    expect(service).toBeTruthy();
  });
});
