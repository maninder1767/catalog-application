import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WfxBottompanelService {

  constructor() { }
  private selectAllItem = new Subject<{allItems: boolean}>();
  selectAllItemsSubsciption = this.selectAllItem.asObservable();

  private SelectedItemIds = new Subject<{itemIds: string[] , allItems: boolean}>();
  SelectedItemIdsSubsciption = this.SelectedItemIds.asObservable();
  /*Set all selected items ids on bottam view */
  setSelectedItemIds(message: { itemIds: string[], allItems: boolean}) {
    this.SelectedItemIds.next(message);
  }
  /*Select or deselect All items on gallery view */
  selectAllItems(message: { allItems: boolean}) {
    this.selectAllItem.next(message);
  }
}
