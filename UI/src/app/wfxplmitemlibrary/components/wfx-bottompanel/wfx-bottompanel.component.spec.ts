import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WfxBottomPanelComponent } from './wfx-bottompanel.component';

describe('WfxBottomPanelComponent', () => {
  let component: WfxBottomPanelComponent;
  let fixture: ComponentFixture<WfxBottomPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WfxBottomPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WfxBottomPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
