import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WfxContentLeftNavComponent } from './wfx-contentleftnav.component';

describe('WfxContentLeftNavComponent', () => {
  let component: WfxContentLeftNavComponent;
  let fixture: ComponentFixture<WfxContentLeftNavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WfxContentLeftNavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WfxContentLeftNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
