import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WfxGalleryViewComponent } from './wfx-gallery-view.component';

describe('WfxGalleryViewComponent', () => {
  let component: WfxGalleryViewComponent;
  let fixture: ComponentFixture<WfxGalleryViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WfxGalleryViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WfxGalleryViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
