
// import { LoadmoreDirective } from './../../directives/loadmore/loadmore.directive';
import { ItemLibraryGlobals, RightSelectedPanel } from './../../itemLibrary.global';
import { ItemLibraryDataService } from './../../../wfxitemshared/services/itemdata.service';
import { WfxBottompanelService } from './../wfx-bottompanel/wfx-bottompanel.service';
import { Component, OnInit, AfterViewInit, OnChanges, Output, EventEmitter, 
  Input, HostListener, ViewChild } from '@angular/core';
import { Observable, fromEvent, merge, Subject } from 'rxjs';
import { WfxviewoptionService } from '../../../wfxitemshared/components/wfx-viewoptions/wfxviewoption.service';
import { ItemLibraryService } from '../../../wfxitemshared/services/itemlibrary.service';
import { Filter, ApplyFilter, SearchParams } from '../../../wfxitemshared/modals/filter';
import { ItemData } from '../../modals/item';
import { WfxFilterService } from '../../../wfxitemshared/components/wfx-filter/wfx-filter.service';
import { ItemLibraryCommonService } from '../../../wfxitemshared/services/itemcommon.service';
import { ViewType } from '../../../../app/wfxitemshared/constants/enum';
import { CdkVirtualScrollViewport, ViewportRuler } from '@angular/cdk/scrolling';
declare var $: any;

@Component({
  selector: 'app-wfx-gallery-view',
  templateUrl: './wfx-gallery-view.component.html',
  styleUrls: ['./wfx-gallery-view.component.css']
})

export class WfxGalleryViewComponent implements OnInit, AfterViewInit, OnChanges {
  isLoading = false;
  @Output() deselect = new EventEmitter();
  constructor( private sliderservice: WfxviewoptionService,private itemLibraryService: ItemLibraryService,
    private globals: ItemLibraryGlobals,
    private bottamService: WfxBottompanelService,public itemGlobal: ItemLibraryGlobals, private wfxFilterService: WfxFilterService,
    private itemLibraryDataService: ItemLibraryDataService, private itemLibraryCommonService: ItemLibraryCommonService) { }
  @Input() filter: Filter;
  @Output() cardClick = new EventEmitter();
  @ViewChild(CdkVirtualScrollViewport,{static :true}) virtualScroll: CdkVirtualScrollViewport;
  arrCardData: ItemData;
  cardJsonDef;
  lazyScrollSub = new Subject();
  viewInnerHeight;
  lazyscrollObservable: Observable<{} | Event>;
  selectedCardData;
  isSelection = false;
  selectedItemIDsArray = [];
  filterChip = new Filter();
  applyFilter = new ApplyFilter({}, new SearchParams('', []), [], []);
  scrollItems: any;
  viewwidth;
  itemsize;
viewPortData: number[] = [];
  @HostListener('window:resize') onResize() {
    this.viewInnerHeight = (window.innerHeight - $('#cardLoadDiv').offset().top).toString();
  }
  @HostListener('window:custom-event', ['$event'])
  updateNodes(event) {
   this.itemLibraryDataService.getList();
 }
  ngOnChanges() {
    setTimeout(() => {
      this.lazyScrollSub.next();
    }, 1);

  }
  ngOnInit() {
    this.itemGlobal.isSpinnerLoadingGalleryView = true;
    this.itemLibraryDataService.getList();
    this.itemLibraryService.filterChipSubscription
      .subscribe(message => {
        this.filterChip = message.filterChip;
      });
    this.itemLibraryService.filter
      .subscribe(message => {
        this.filter = message.filter;
      });
    this.sliderservice.sliderValueSubsciption
      .subscribe(message => {
        this.changeCssPropertiesofAssetcard(message.sliderValue);
      });
    this.bottamService.selectAllItemsSubsciption
      .subscribe(message => {
         this.selectAllItems(message.allItems);
      });

      this.cardJsonDefWrapper();
  }

  changeCssPropertiesofAssetcard(sliderRenge: number) {
     this.cardJsonDefWrapper();
  }
  cardJsonDefWrapper() {
    this.cardJsonDef = {
      card: {
        disable: false,
        visible: true,
        tooltip: ''
      },
      cardHeight: this.calculatecardvalue(this.itemGlobal.cardproperties.cardHeight_zero,
        this.itemGlobal.cardproperties.cardHeight_fifty, this.itemGlobal.cardproperties.cardHeight_hundred) + 'px',

      cardWidth: this.calculatecardvalue(this.itemGlobal.cardproperties.cardWidth_zero,
        this.itemGlobal.cardproperties.cardWidth_fifty, this.itemGlobal.cardproperties.cardWidth_hundred) + 'px',

      cardImageHeight: this.calculatecardvalue(this.itemGlobal.cardproperties.cardImageHeight_zero,
          this.itemGlobal.cardproperties.cardImageHeight_fifty, this.itemGlobal.cardproperties.cardImageHeight_hundred) + 'px',


      cardImageWidth: this.calculatecardvalue(this.itemGlobal.cardproperties.cardImageWidth_zero,
        this.itemGlobal.cardproperties.cardImageWidth_fifty, this.itemGlobal.cardproperties.cardImageWidth_hundred) + 'px',

      cardTitleFontSize: this.calculatecardvalue(this.itemGlobal.cardproperties.cardTitleFontSize_zero,
        this.itemGlobal.cardproperties.cardTitleFontSize_fifty, this.itemGlobal.cardproperties.cardTitleFontSize_hundred) + 'px',

      cardTitleWidth: this.calculatecardvalue(this.itemGlobal.cardproperties.cardTitleWidth_zero,
        this.itemGlobal.cardproperties.cardTitleWidth_fifty, this.itemGlobal.cardproperties.cardTitleWidth_hundred) + 'px',

      cardSubTitleFontSize: this.calculatecardvalue(this.itemGlobal.cardproperties.cardSubTitleFontSize_zero,
        this.itemGlobal.cardproperties.cardSubTitleFontSize_fifty, this.itemGlobal.cardproperties.cardSubTitleFontSize_hundred) + 'px',

      cardSubTitleWidth: this.calculatecardvalue(this.itemGlobal.cardproperties.cardSubTitleWidth_zero,
        this.itemGlobal.cardproperties.cardSubTitleWidth_fifty, this.itemGlobal.cardproperties.cardSubTitleWidth_hundred) + 'px',

      cardContentFontSize: this.calculatecardvalue(this.itemGlobal.cardproperties.cardContentFontSize_zero,
        this.itemGlobal.cardproperties.cardContentFontSize_fifty, this.itemGlobal.cardproperties.cardContentFontSize_hundred) + 'px',

      cardContentWidth: this.calculatecardvalue(this.itemGlobal.cardproperties.cardContentWidth_zero,
        this.itemGlobal.cardproperties.cardContentWidth_fifty, this.itemGlobal.cardproperties.cardContentWidth_hundred) + 'px',

      cardPLSFontSize : this.calculatecardvalue(this.itemGlobal.cardproperties.cardPLSFontSize_zero,
        this.itemGlobal.cardproperties.cardPLSFontSize_fifty, this.itemGlobal.cardproperties.cardPLSFontSize_hundred) + 'px',

      cardPLSFontWidth : this.calculatecardvalue(this.itemGlobal.cardproperties.cardPLSWidth_zero,
        this.itemGlobal.cardproperties.cardPLSWidth_fifty, this.itemGlobal.cardproperties.cardPLSWidth_hundred) + 'px',
    };
  }
  ngAfterViewInit() {
    this.viewwidth = window.innerWidth - 12 - 100; // here 12 is scroll width and 100 is padding from card
    const cardWidth = 177 + 20;
    const cardHeight = 254;
    const cardPerRow = this.viewwidth / cardWidth;
    let itemSizeofCard;
    console.log('viewwidth', this.viewwidth);
    console.log('cardPerRow', cardPerRow);
    setTimeout(() => {
      this.viewInnerHeight = (window.innerHeight - $('#cardLoadDiv').offset().top).toString();
      const rowCount = this.viewInnerHeight / cardHeight;
      console.log('rowCount', rowCount);
      itemSizeofCard = cardPerRow / rowCount;
      this.itemsize = itemSizeofCard;
      console.log('itemsize', this.itemsize);
    }, 1);
    const scrobj = document.getElementById('cardLoadDiv');
    this.lazyscrollObservable = merge(
      fromEvent(scrobj, 'scroll'),
      this.lazyScrollSub
    );
    
      this.virtualScroll.elementScrolled()
      .subscribe(event => {
        if ((this.globals.recordCount >= 100) && (this.globals.arrGalleryViewItemData.length < this.globals.recordCount)) {
        const end = this.virtualScroll.getRenderedRange().end;
        const datalength  = this.virtualScroll.getDataLength();
        const index = this.viewPortData.findIndex(x => x === datalength);
        if (index >= 0) {
        } else {
          if (end === datalength) {
            const index1 = this.viewPortData.findIndex(x => x === datalength);
            if (index1 === -1) {
              if (this.virtualScroll.measureScrollOffset('bottom') === 0 ) {
                this.viewPortData.push(datalength);
                 this.itemGlobal.defaultPageParams.PageIndex = this.itemGlobal.defaultPageParams.PageIndex + 1;
                 this.itemLibraryDataService.getList(ViewType.GALLERY, true);
               }
            }
          
          }
    
        }
    
        // if (end === dtalength) {
        //   console.log('ashutosh' ,  Date());
        //   this.itemGlobal.defaultPageParams.PageIndex = this.itemGlobal.defaultPageParams.PageIndex + 1;
        //    this.itemLibraryDataService.getList(ViewType.GALLERY, true);
        // }
           
      
            }
     });
  }
  onCardClick(item) {
    this.itemGlobal.lastItemSelectedID = item.itemId;
    if (item.checkbox) {
      this.itemGlobal.arrSelectedItemId.push(item.itemId);
      this.selectedItemIDsArray.push(item.itemId);
    } else {
      this.itemGlobal.arrSelectedItemId = this.itemGlobal.arrSelectedItemId.filter(val => val !== item.itemId);
      this.selectedItemIDsArray = this.selectedItemIDsArray.filter(val => val !== item.itemId);
    }
    const commentdata = this.itemGlobal.arrGalleryViewData.getValue().filter(_x => _x.checkbox === true);
    if (commentdata.length === 1 ) {
      this.itemLibraryDataService.getItem(commentdata[0].itemId).subscribe((result:any) =>{
        console.log(result)
        if(commentdata[0].commentCount!=null)
        this.itemLibraryService.invokeshowComment({showCommentIcon: true, commentCount: result.data.item[0].commentCount});
        else{
          this.itemLibraryService.invokeshowComment({ showCommentIcon: false, commentCount: item.commentCount });
          this.itemLibraryService.invokeshowSelectedRightPanel(true);
        }
      });
    } else {
      this.itemLibraryService.invokeshowComment({ showCommentIcon: false, commentCount: item.commentCount });
      this.itemLibraryService.invokeshowSelectedRightPanel(true);
    }
    this.bottamService.setSelectedItemIds({
      itemIds: this.selectedItemIDsArray,
      allItems: false
    });
  }
  onCardDoubleClick(val) {
    this.cardClick.emit(val);
    window.open('http://192.168.1.193/wfx/wfx_ArticleDetail.aspx?ID=' + val.ID + '&ArticleSelection=1&CatalogType=1');
  }
  QuickView(id) {
   this.itemGlobal.activePanel = true;
   this.itemGlobal.currentRightSelectedPanel =  RightSelectedPanel.PropertyView;
   this.itemLibraryService.invokeshowSelectedRightPanel(true)
   this.itemLibraryService.invokeProperViewClick({id: id });

  }
  onSelectionStart($event, isCtrlA = false) {
    this.selectedItemIDsArray = [];
    this.itemGlobal.arrSelectedItemId = [];
    this.itemGlobal.arrGalleryViewItemData.forEach((obj) => {
      if (obj.checkbox) {
        this.itemGlobal.arrSelectedItemId.push(obj.itemId);
        this.selectedItemIDsArray.push(obj.itemId);
      }
    });
    //this.itemLibraryService.invokeshowSelectedRightPanel(true)
    const commentdata = this.itemGlobal.arrGalleryViewItemData.filter(_x => _x.checkbox === true);
    if (commentdata.length === 1) {
      this.itemLibraryService.invokeshowComment({ showCommentIcon: true, commentCount: commentdata[0].commentCount });
      this.itemLibraryService.invokeshowSelectedRightPanel(true)
      this.itemLibraryService.invokeshowCommentDetail({ ID: commentdata[0].itemId});
      this.itemGlobal.currentRightSelectedPanel =  RightSelectedPanel.CommentDetail;

    } else {
          this.itemLibraryService.invokeshowSelectedRightPanel(true)
    }
    this.bottamService.setSelectedItemIds({
      itemIds: this.selectedItemIDsArray,
      allItems: isCtrlA
    });
  }
  selectAllItems(isTrue: boolean) {
    this.itemGlobal.arrSelectedItemId = [];
    this.selectedItemIDsArray = [];
    if (isTrue) {
      this.itemGlobal.arrGalleryViewItemData.forEach(obj => {
        if (obj.checkbox = true) {
          this.itemGlobal.arrSelectedItemId.push(obj.itemId);
          this.selectedItemIDsArray.push(obj.itemId);
        }
      });
      }else {
          this.itemGlobal.arrGalleryViewItemData.forEach(obj => {
            if (obj.checkbox) {
              obj.checkbox = false;
            }
          });
        }
    this.bottamService.setSelectedItemIds({
      itemIds: this.selectedItemIDsArray,
      allItems: isTrue
    });
  }
  onchipFilterChange(filter: Filter) {
   // this.itemGlobal.selectedSortFieldValue = '';
    this.wfxFilterService.filterJson(filter).subscribe((resp: ApplyFilter) => {
      this.itemLibraryCommonService.updateAdvanceSearchFilterCriteria(resp.searchparams.filterCriteria);
      this.itemLibraryDataService.advanceSearchFilter(resp.searchparams.filterCriteria);
    });
    this.itemLibraryService.changeFilter({
      filter: filter
    });
  }
  clickOutSide($event) {
    this.deselect.emit(this.isSelection);

  }
  closeAllContextMenu($event) {
    // $event.preventDefault();
    $event.stopPropagation();
  }
  closeBottamPanel(val) {
    this.bottamService.setSelectedItemIds({
      itemIds: [],
      allItems: false
    });
  }
  // onMoreCardLoad(event) {
  //   if (event.end !== this.itemGlobal.arrGalleryViewItemData.length) { return; }
  //   if (this.itemGlobal.arrGalleryViewItemData.length < 100) {
  //     return;
  //   }
  //   this.itemGlobal.defaultPageParams.PageIndex = this.itemGlobal.defaultPageParams.PageIndex + 1;
  //   this.itemLibraryDataService.getList(ViewType.GALLERY, true);
  // }
  onMoreCardLoad(event) {

    // this.contextMenuService.closeAllContextMenus(event);

    // this.renderer.removeClass(this.itemGlobal.contextMenuopenId, 'selected_cardimage_container');

    if (event.end !== this.itemGlobal.arrGalleryViewItemData.length) { return; }

    if ((this.itemGlobal.arrGalleryViewItemData.length < 100)) {

      return;

    }

    if (this.isLoading === false) {

      this.isLoading = true;

      this.itemGlobal.defaultPageParams.PageIndex = this.itemGlobal.defaultPageParams.PageIndex + 1;

      this.itemLibraryDataService.getList(ViewType.GALLERY, true);

      this.bottamService.setSelectedItemIds({

        itemIds: this.itemGlobal.arrSelectedItemId,

        allItems: false,

        //deliveryIdExist: false

      });

      setTimeout(() => {

        this.isLoading = false;

      }, 1500);

    }

  }

 
  calculatecardvalue(valueAtZero: number , valueAtFifty: number , valueAtHundred: number) {
    let returnvalue = 0;
    const defaultSliderValue = this.itemGlobal.defaultsliderRange;
    const currentRange = Number(this.itemGlobal.sliderRange) ;
      if (currentRange < defaultSliderValue){
        returnvalue = valueAtZero + (this.calculateMultiplyfector(valueAtZero, valueAtFifty) * currentRange);
      } else {
          const  newslidervalue = currentRange - defaultSliderValue;
          returnvalue = valueAtFifty + (this.calculateMultiplyfector(valueAtFifty, valueAtHundred) * newslidervalue);
      }
      return returnvalue;
    }
  calculateMultiplyfector(minvalue: number , maxvalue: number) {
      const fector = (maxvalue - minvalue) / Number(this.itemGlobal.defaultsliderRange);
      return fector;
    }

  Onselectiong(id) {
      const res =   this.globals.arrGalleryViewItemData.find( x => Number(x.itemId) === Number(id));
        if ((res) && (!res.checkbox)) {
           res.checkbox = true;
           this.itemGlobal.arrSelectedItemId.push(res.itemId);
          // console.log( this.itemGlobal.arrSelectedItemId);
        }
        const commentdata = this.itemGlobal.arrGalleryViewItemData.filter(_x => _x.checkbox === true);
        if (this.itemGlobal.arrSelectedItemId.length === 1) {
          this.itemLibraryService.invokeshowComment({ showCommentIcon: true, commentCount: commentdata[0].commentCount });
          this.itemLibraryService.invokeshowSelectedRightPanel(true)
          this.itemLibraryService.invokeshowCommentDetail({ ID: commentdata[0].itemId});
          this.itemGlobal.currentRightSelectedPanel =  RightSelectedPanel.CommentDetail;

        } else {
              this.itemLibraryService.invokeshowSelectedRightPanel(true)
        }
        this.bottamService.setSelectedItemIds({
          itemIds: this.itemGlobal.arrSelectedItemId,
          allItems: false
        });
    }
    nextBatch(event) {

    }
}
