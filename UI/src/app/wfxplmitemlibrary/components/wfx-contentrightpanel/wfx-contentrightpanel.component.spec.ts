import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WfxContentRightPanelComponent } from './wfx-contentrightpanel.component';

describe('WfxContentRightPanelComponent', () => {
  let component: WfxContentRightPanelComponent;
  let fixture: ComponentFixture<WfxContentRightPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WfxContentRightPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WfxContentRightPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
