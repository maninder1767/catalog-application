import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WfxContentTopPanelComponent } from './wfx-contenttoppanel.component';

describe('WfxContentTopPanelComponent', () => {
  let component: WfxContentTopPanelComponent;
  let fixture: ComponentFixture<WfxContentTopPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WfxContentTopPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WfxContentTopPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
