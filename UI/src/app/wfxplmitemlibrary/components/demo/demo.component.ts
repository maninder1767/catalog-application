import { WfxCommentListComponent } from './../../../wfxitemshared/components/wfx-comment-list/wfx-comment-list.component';
import { WfxTab } from './../../../wfxlibrary/wfxsharedmodels/wfxcommonmodel';
import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-demo',
  templateUrl: './demo.component.html',
  styleUrls: ['./demo.component.css']
})
export class DemoComponent implements OnInit {
  @ViewChild('comment', { static: true }) CommentlistComponent: WfxCommentListComponent;
  componentObject: any;
  viewoptionsjsondata;
  itemJsonDef =
    [
      {
        readonly: false, mandatory: true, caption: 'First Name', disable: false,
        value: 'fname', valuetype: 'dbfield',
        tooltip: { tooltipvalue: 'First Name', tooltipvaluetype: 'dbfield', tooltipposition: 'below' },
        inputtype: 'textbox', itemid: 'txtFirstName'
      },
      {
        readonly: false, mandatory: true, caption: 'Hello',
        value: 'checkBoxValue', disable: false, labelposition: 'after', valuetype: 'dbfield',
        tooltip: { tooltipvalue: 'First Name', tooltipvaluetype: 'dbfield', tooltipposition: 'below' }
      },
      {
        readonly: false, mandatory: true, caption: '', valuetype: 'dbfield',
        value: 'selectedRadioValue', disable: false, labelposition: 'after',
        rbList: ['Winter', 'Spring', 'Summer', 'Autumn'],
        tooltip: { tooltipvalue: 'First Name', tooltipvaluetype: 'dbfield', tooltipposition: 'below' }
      },
      {
        readonly: false, mandatory: true, caption: 'First Name', valuetype: 'dbfield', value: 'idtesting1',
        valuetext: 'optionname', addclearicon: true, addimginddl: true , addsearchoptioninddl: false,
        multiselect: true, disable: false, inputtype: 'select', ddlvalue: 'idtesting',
        ddlvaluetext: 'text', tooltip: { tooltipvalue: 'First Name', tooltipvaluetype: 'dbfield', tooltipposition: 'below' }
      },
      {
        readonly: false, mandatory: true, caption: 'On', valuetype: 'dbfield',
        value: 'SwitchOn', disable: false,
        tooltip: { tooltipvalue: 'First Name', tooltipvaluetype: 'dbfield', tooltipposition: 'below' }
      },
      {
        readonly: false, mandatory: false, caption: 'Doc Date',
        value: 'DocDate', valuetype: 'dbfield', dateformat: 'll', maxvalue: 'today', disable: false,
        tooltip: { tooltipvalue: 'First Name', tooltipvaluetype: 'dbfield', tooltipposition: 'below' },
        inputdatedisable: true
      }
    ];
    sliderJsonData = {
      sliderDefaultvalue: 50
    };
    sliderJsonDef = {
      attribute: {
      visible: true,
      disable: false,
      }
    };

    avatarjsonData =  {
      imageUrl: 'https://via.placeholder.com/600/51aa97',
      profileName: 'Sanjay Amb'
    };
  tabJsonDef: WfxTab[] = [{
    text: 'LIBRARY', visible: false, disable: false, code: 'LIBRARY'
  },
  {
    text: 'COSTING', visible: true, disable: false, code: 'COSTING',
    tooltip: { tooltipvalue: 'PropertiesView', tooltipposition: 'below' }
  }
];
jsonData = {
  fname: 'Value', checkBoxValue: false, selectedRadioValue: 'Winter', SwitchOn: false,
  DocDate: 'Wed, Apr 24 2019', idtesting1: 'B', // ['A', 'B', 'C'],
  optionname: 'Bank B (Switzerland)' // ['Bank A (Switzerland)', 'Bank B (Switzerland)', 'Bank C (France)']
};

arrayItemsViewOptionData: any[] = [
  { Text: 'WFX Default', Code: 'WFX Default' },
  { Text: 'WFX Premimum', Code: 'WFX Premimum' },
  { Text: 'WFX Simple', Code: 'WFX Simple' }
];

lblJsonData = {
  fname: 'Value', checkBoxValue: false, selectedRadioValue: 'Winter', SwitchOn: false,
  DocDate: 'Wed, Apr 24 2019', idtesting1: 'B', // ['A', 'B', 'C'],
  optionname: 'Bank B (Switzerland)' // ['Bank A (Switzerland)', 'Bank B (Switzerland)', 'Bank C (France)']
};
viewoptionsJsonDef;
selectValues: any[] = [
  { text: 'Bank A (Switzerland)', idtesting: 'A' },
  { text: 'Bank B (Switzerland)', idtesting: 'B' },
  { text: 'Bank C (France)', idtesting: 'C' },
  { text: 'Bank D (France)', idtesting: 'D' },
  { text: 'Bank E (France)', idtesting: 'E' },
  { text: 'Bank F (Italy)', idtesting: 'F' },
  { text: 'Bank G (Italy)', idtesting: 'G' },
  { text: 'Bank H (Italy)', idtesting: 'H' },
  { text: 'Bank I (Italy)', idtesting: 'I' },
  { text: 'Bank J (Italy)', idtesting: 'J' },
  { text: 'Bank Kolombia (United States of America)', idtesting: 'K' },
  { text: 'Bank L (Germany)', idtesting: 'L' },
  { text: 'Bank M (Germany)', idtesting: 'M' },
  { text: 'Bank N (Germany)', idtesting: 'N' },
  { text: 'Bank O (Germany)', idtesting: 'O' },
  { text: 'Bank P (Germany)', idtesting: 'P' },
  { text: 'Bank Q (Germany)', idtesting: 'Q' },
  { text: 'Bank R (Germany)', idtesting: 'R' }
];

likeJsonDef = {
  attribute: '',
  dailog: {
    attribute : '',
    image: {
      attribute: {
        height: '20px',
        width: '20px',
      },
    }
  }
};


likeJsonData = {
  likeCount: 5,
  isLike: false,
};

bookMarkJsonDef = {
  attribute: {}
};

bookMarkJsonData = {
  isBookMark: false,
};

commentDefValue =
{
  attribute: {
  width: '25px',
  height: '15px',
  tooltip: 'Comment Here',
  hidden: false,
  disable: true,
  },
  comment:
  {
    showComment: true
  }
};

commentJsonData= {
  commentCount: '4',
 };

 cardJsonDef = {
  card: {
    disable: false,
    visible: true,
    tooltip: ''
  },
  cardHeight: 280 + 'px',
  cardWidth: 175 + 'px',
  cardImageHeight: 184 + 'px',
  cardImageWidth : 175 + 'px',
  cardTitleFontSize: 9 + 'px',
  cardTitleWidth: 91 + 'px',
  cardSubTitleFontSize: 14 + 'px',
  cardSubTitleWidth: 135 + 'px',
  cardContentFontSize: 14 + 'px',
  cardContentWidth: 135 + 'px',
};

card = {
  'SortOrder': 1,
  'PicFileName': 'http://192.168.1.193/Company/10006/Pictures/1BTK-SB_2078249.jpg',
  'ArticleCode': '!@#$%^&*()_{}\':<>?/.,;`=-`/',
  'BuyerReference': '897',
  'ArticleName': 'SPECIAL CHARACTERS',
  'plcName': 'Sample Phases',
  'SupplierReference': '',
  'LikeCount': 2,
  'IsLike': 1,
  'plcColor': '227-26-26',
  'plcColorChip': '',
  'CommentCount': 11,
  'ID': 100063738
};

textAreaJsonDef = {
    readonly: false, mandatory: true, caption: 'First Name', disable: false,
    value: 'fname', valuetype: 'dbfield',
    tooltip: { tooltipvalue: 'First Name', tooltipvaluetype: 'dbfield', tooltipposition: 'below' },
    inputtype: 'textbox', itemid: 'txtFirstName'
};

textAreajsonData = {
  fname: 'Value', checkBoxValue: false, selectedRadioValue: 'Winter', SwitchOn: false,
  DocDate: 'Wed, Apr 24 2019', idtesting1: 'B', // ['A', 'B', 'C'],
  optionname: 'Bank B (Switzerland)' // ['Bank A (Switzerland)', 'Bank B (Switzerland)', 'Bank C (France)']
};

commentListJsonData= {
  ObjectType: 'Asset',
  ObjectID : 0,
 } ;

 commentDetailDefValue =
 {
  attribute: {
   width: '25px',
   height: '420px',
   visible: false,
   tooltip: 'comment',
   color: '',
   disable: false,
  },
   placeholder: 'Write a comment',
   showCommentDetail: false
 };

  constructor() { }

  ngOnInit() {
    this.componentObject = this;

    this.viewoptionsJsonDef = {
      attribute: {
          visible: true,
          disable: false,
      },
      slider: {
        attribute: {
           visible: true,
           disable: false
        },
      },
    };

    this.viewoptionsjsondata = {
      sliderDefaultvalue: '50',
      selectedItemView: 'WFX Default',
      arrItemsViewOptionData: this.arrayItemsViewOptionData,
    };

    this.CommentlistComponent.onCommentListData('100063738');
  }

  onTabViewChangeEvent(val) {
    console.log(val);
  }

  onViewTypeChange($event): void {
  }

  onChange($event) {
  }

  onbookmarkclick(val) {
  }

  onCommentClick() {
  }
}
