import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WfxContentRightNavComponent } from './wfx-contentrightnav.component';

describe('WfxContentRightNavComponent', () => {
  let component: WfxContentRightNavComponent;
  let fixture: ComponentFixture<WfxContentRightNavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WfxContentRightNavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WfxContentRightNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
