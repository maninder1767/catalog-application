import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WfxTopPanelComponent } from './wfx-toppanel.component';

describe('WfxTopPanelComponent', () => {
  let component: WfxTopPanelComponent;
  let fixture: ComponentFixture<WfxTopPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WfxTopPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WfxTopPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
