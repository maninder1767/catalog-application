import { ItemLibraryDataService } from './../../../wfxitemshared/services/itemdata.service';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-wfx-toppanel',
  templateUrl: './wfx-toppanel.component.html',
  styleUrls: ['./wfx-toppanel.component.css']
})
export class WfxTopPanelComponent implements OnInit {

  titleBarJsonDef =
{
      width: 25,
      height: 25,
      tooltip: 'Bead Crumb',
      hidden: false,
      disabled: false,
      placeholder: 'Bead Crumb',
      visible: true,
      color: 'Red',
      label: 'Bead Crumb',
      addBtn: {
        isShowAddBtn :true
      },
    
};
titleBarJsonData =
{
    NoOfComment : '0',
    titleName : 'My Uploads',
    arrBreadCrumb : [],
    objSelectedAssetFolder : {},
    arrAssetFolderTypeOptionsData: []};

    assetToolBarJsonData=
    {
      recordType: 'Files',
      recordCount:0
    }
    addButtonJsonDef={
      visible :false
    };

  constructor(private itemDataService : ItemLibraryDataService) { }

  ngOnInit() {
  
    this.itemDataService.getCommonData().subscribe((res:any) => {
       if(res.data.userType == 'BUYER'){
        this.addButtonJsonDef.visible = true;
       }

    })
  }

  onaddBtnClick(event) {

}
OnCommentClick() {

}
Onclickbreadcrimb($event) {

}
}
