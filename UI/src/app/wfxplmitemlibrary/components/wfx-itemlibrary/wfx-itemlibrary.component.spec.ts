import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WfxItemLibraryComponent } from './wfx-itemlibrary.component';

describe('WfxItemLibraryComponent', () => {
  let component: WfxItemLibraryComponent;
  let fixture: ComponentFixture<WfxItemLibraryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WfxItemLibraryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WfxItemLibraryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
