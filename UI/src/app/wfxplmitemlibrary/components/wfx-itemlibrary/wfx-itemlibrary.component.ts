import { simpleDef } from './../../../wfxitemshared/components/wfx-tableview/columnDef';
import { ItemLibraryGlobals, RightSelectedPanel } from './../../itemLibrary.global';
import { DataService } from './../../../shared/services/data.service';
import { ItemLibraryService } from './../../../wfxitemshared/services/itemlibrary.service';
import { Component, OnInit, ViewChild, ElementRef, Input, OnChanges } from '@angular/core';
import { trigger, style, state, transition, animate } from '@angular/animations';
import { MatDrawer } from '@angular/material';
import { Globals } from '../../../globals';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-wfx-itemlibrary',
  templateUrl: './wfx-itemlibrary.component.html',
  styleUrls: ['./wfx-itemlibrary.component.css'],
  animations: [
    trigger(
      'slideRightAnimation', [
        state('close', style({ transform: 'translateX(100%)', opacity: 1 }), ),
        state('open', style({ transform: 'translateX(0)', opacity: 1 })),
        transition('* => open', animate('250ms ease-in', style({ transform: 'translateX(0)', opacity: 1 }))),
        transition('open => close', animate('250ms', style({ transform: 'translateX(100%)', opacity: 1 })))
      ]
    ),
    trigger(
      'slideLeftAnimation', [
        state('close', style({ width: '60px', opacity: 1 }), ),
        state('open', style({ width: '260px', opacity: 1 })),
        transition('* => open', animate('500ms ease-in')),
        transition('open => close', animate('500ms ease-in-out'))
      ]
    )
  ]
})
export class WfxItemLibraryComponent implements OnInit, OnChanges {
  @Input() showrightpanel: boolean;
  showPropertyView: boolean;
  showcomment: boolean;
  rightpanelstate;
  constructor(private itemLibraryService: ItemLibraryService, private globals: Globals,
    private route: ActivatedRoute, private dataService: DataService,private itemGlobals:ItemLibraryGlobals) { }
  @ViewChild('floatingRightPanel', { static: true }) floatingRightPanel: MatDrawer;
  ngOnInit() {

    this.route.queryParamMap.subscribe(res => {
      this.globals.guid = res.get('guid');
      this.globals.productCatCode = res.get('ProductCatCode');
      this.globals.menuType = res.get('type');

      this.dataService.getCommonData().subscribe((data: any) => {
        if (data && data.data.companyCode) {
          this.globals.companyCode = data.data.companyCode;
          this.globals.companyDivisionCode = data.data.companyDivisionCode;
          this.globals.languageCode = data.data.languageCode;
          this.globals.memberCompanyCode = data.data.memberCompanyCode;
          this.globals.menuBar = data.data.menuBar;
          this.globals.sessionID = data.data.sessionIDForPrint;
          this.globals.userName = data.data.userName;
          this.globals.costingApproval = data.data.costingApproval;
        }
    });
  });
    this.itemLibraryService.selectedPanelSubscription.subscribe(res => {
      if (res === true) {
        this.rightFloatingPanel();
      } if (res === false) {
        this.rightFloatingPanel();
      }
    });
  }
  ngOnChanges() {

  }

    onRemoveicon() {
      this.itemGlobals.activePanel = false;
      this.itemGlobals.currentRightSelectedPanel = undefined;
      this.floatingRightPanel.close();
    }

    rightFloatingPanel() {
      if (!this.itemGlobals.activePanel) {
        this.drawerClose();
      }
      if (this.itemGlobals.currentRightSelectedPanel === RightSelectedPanel.CommentDetail &&
        this.itemGlobals.arrSelectedItemId.length > 1) {
        this.drawerClose();
      } else if (this.itemGlobals.arrSelectedItemId.length === 1 && this.itemGlobals.activePanel) {
        this.floatingRightPanel.open();
      } else if (this.itemGlobals.currentRightSelectedPanel === RightSelectedPanel.PropertyView
        && this.itemGlobals.activePanel) {
          this.floatingRightPanel.open();
        }else if (this.itemGlobals.currentRightSelectedPanel === RightSelectedPanel.Filter
          && this.itemGlobals.activePanel) {
            this.floatingRightPanel.open();
          } else {
        this.drawerClose();
      }
    }

    drawerClose() {
      this.itemGlobals.activePanel = false;
      this.floatingRightPanel.close();
    }
}
