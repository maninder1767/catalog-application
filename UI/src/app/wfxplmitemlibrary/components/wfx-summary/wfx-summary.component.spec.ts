import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WfxSummaryComponent } from './wfx-summary.component';

describe('WfxSummaryComponent', () => {
  let component: WfxSummaryComponent;
  let fixture: ComponentFixture<WfxSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WfxSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WfxSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
