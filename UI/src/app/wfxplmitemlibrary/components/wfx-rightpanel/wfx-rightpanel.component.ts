import { ItemLibraryGlobals, RightSelectedPanel } from './../../itemLibrary.global';
import { ItemLibraryService } from './../../../wfxitemshared/services/itemlibrary.service';
import { Component, OnInit, ViewChild, OnDestroy, Input, AfterViewInit, AfterContentInit, OnChanges, Output, EventEmitter } from '@angular/core';
import { MatDrawer } from '@angular/material/sidenav';
import { trigger, style, animate, transition } from '@angular/animations';
import { WfxCommentListComponent } from '../../../wfxitemshared/components/wfx-comment-list/wfx-comment-list.component';
import { Subscription } from 'rxjs';
import { Filter, ApplyFilter } from '../../../wfxitemshared/modals/filter';
import { Globals } from '../../../globals';
import { ItemLibraryDataService } from '../../../wfxitemshared/services/itemdata.service';
import { JsonDef } from '../../../wfxitemshared/components/wfx-filter/wfx-filter.component';
import { WfxFilterService } from '../../../wfxitemshared/components/wfx-filter/wfx-filter.service';
import { ItemLibraryCommonService } from '../../../wfxitemshared/services/itemcommon.service';
declare var $: any;
@Component({
  selector: 'app-wfx-rightpanel',
  templateUrl: './wfx-rightpanel.component.html',
  styleUrls: ['./wfx-rightpanel.component.css'],
  animations: [
    trigger(
      'slideRightAnimation', [
      transition(':enter', [
        style({ transform: 'translateX(100%)', opacity: 0 }),
        animate('100ms', style({ transform: 'translateX(0)', opacity: 1 }))
      ]),
      transition(':leave', [
        style({ transform: 'translateX(0)', opacity: 1 }),
        animate('100ms', style({ transform: 'translateX(100%)', opacity: 0 }))
      ])
    ]
    )
  ]
})
export class WfxRightPanelComponent implements OnInit, OnChanges {

  constructor(private itemLibraryService: ItemLibraryService,
    private globals: Globals, private itemLibraryDataService: ItemLibraryDataService,
  private wfxFilterService: WfxFilterService, private itemLibraryCommonService: ItemLibraryCommonService,
  public itemGlobal: ItemLibraryGlobals) { }
  @ViewChild('searchdrawer', { static: true }) searchdrawer: MatDrawer;
  @ViewChild('comment', { static: false }) CommentlistComponent: WfxCommentListComponent;
  
  @Input() showright;
  @Input() showFilter = false;
  @Input() showPropertyView = false
  @Output() filterApplied = new EventEmitter();
  FilterJsonDef = new JsonDef();
  // filterJsonData: any;
  subscription: Subscription;
  filter: Filter = new Filter();
  itemID;

  commentListJsonData = {
    ID: '0',
  };

  commentDetailDefValue =
    {
      attribute: {
        width: '25px',
        height: '420px',
        visible: false,
        tooltip: 'comment',
        color: '',
        disable: false,
      },
      placeholder: 'Write a comment',
      showCommentDetail: false
    };
    propertyViewJsonDef=
    {
      showPropertyView: false,
    }

    propertyViewDetailJsonData=
    {
      id: '',
    }

  ngOnInit() {

    this.subscription = this.itemLibraryService.showCommentDetailSubscription.subscribe(message => {
      this.commentListJsonData = message;
    });
      this.itemLibraryService.showPropertyViewClickSubscription.subscribe(message => {
      this.propertyViewDetailJsonData = message;
    });

    this.itemLibraryService.filter
      .subscribe(message => {
        this.filter = message.filter;
        // this.filterJsonData = {
        //   filter: this.filter,
        // };
     });

  }
  ngOnChanges() {
    if (this.showFilter) {
      this.commentDetailDefValue.showCommentDetail = false;
    }
 
  }

  onFilterChange(filter: Filter) {
   // this.itemGlobal.isFilterApplied = true;
    this.wfxFilterService.filterJson(filter).subscribe((resp: ApplyFilter)  => {
      this.itemLibraryCommonService.updateAdvanceSearchFilterCriteria(resp.searchparams.filterCriteria);
      this.itemLibraryDataService.advanceSearchFilter(resp.searchparams.filterCriteria);
    });
    this.itemLibraryService.changeFilter({
      filter: filter
    });
    this.itemLibraryService.invokesFilterChip({
      filterChip:  Object.assign({}, filter)
    });
  }
  onApplyFilterChange(filter: Filter) {
    this.itemLibraryService.changeFilter({
      filter: filter
    });
    this.itemLibraryService.invokesFilterChip({
      filterChip:  Object.assign({}, filter)
    });
  }
  onFilterApplied(appliedFilter: ApplyFilter) {
    this.itemGlobal.activePanel = false;
    this.itemGlobal.currentRightSelectedPanel = undefined;
    this.itemLibraryService.invokeshowSelectedRightPanel(true);
    this.itemLibraryDataService.advanceSearchFilter(appliedFilter.searchparams.filterCriteria);
    this.itemLibraryService.invokesAppliedFilter({
      appliedFilter: appliedFilter
    });
  }


  ngOnChange() {

  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  onRemoveicon() {
    this.searchdrawer.close();
    this.commentDetailDefValue.showCommentDetail = false;
    this.showFilter = false;
    //  this.ItemLibraryService.invokeshowComment(false);
  }

  currentSelectedView() {
    if (this.itemGlobal.currentRightSelectedPanel === 0) {
      $('#filterBlock').show();
    } else {
      $('#filterBlock').hide();
    }
    return this.itemGlobal.currentRightSelectedPanel;
  }
}
