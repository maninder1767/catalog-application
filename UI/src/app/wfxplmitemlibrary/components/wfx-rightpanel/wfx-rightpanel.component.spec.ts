import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WfxRightPanelComponent } from './wfx-rightpanel.component';

describe('WfxRightPanelComponent', () => {
  let component: WfxRightPanelComponent;
  let fixture: ComponentFixture<WfxRightPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WfxRightPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WfxRightPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
