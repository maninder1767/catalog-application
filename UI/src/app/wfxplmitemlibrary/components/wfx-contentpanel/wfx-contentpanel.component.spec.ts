import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WfxContentPanelComponent } from './wfx-contentpanel.component';

describe('WfxContentPanelComponent', () => {
  let component: WfxContentPanelComponent;
  let fixture: ComponentFixture<WfxContentPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WfxContentPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WfxContentPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
