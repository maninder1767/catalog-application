import { ItemLibraryDataService } from './../../../wfxitemshared/services/itemdata.service';
import { WfxAvatarComponent } from './../../../wfxitemshared/components/wfx-avatar/wfx-avatar.component';
import { Component, OnInit, ViewChild, AfterViewChecked, OnChanges, Input, Output, EventEmitter } from '@angular/core';
import { trigger, transition, style, animate } from '@angular/animations';
import { Filter } from '../../../wfxitemshared/modals/filter';
import { MatDrawer } from '@angular/material';
import { ItemLibraryService } from '../../../wfxitemshared/services/itemlibrary.service';
import { ItemLibraryGlobals } from '../../itemLibrary.global';

@Component({
  selector: 'app-wfx-contentpanel',
  templateUrl: './wfx-contentpanel.component.html',
  styleUrls: ['./wfx-contentpanel.component.css'],
  animations: [
    trigger(
      'slideRightAnimation', [
      transition(':enter', [
        style({ transform: 'translateX(100%)', opacity: 0 }),
        animate('500ms', style({ transform: 'translateX(0)', opacity: 1 }))
      ]),
      transition(':leave', [
        style({ transform: 'translateX(0)', opacity: 1 }),
        animate('500ms', style({ transform: 'translateX(100%)', opacity: 0 }))
      ])
    ]
    )
  ]
})
export class WfxContentPanelComponent implements OnInit, AfterViewChecked, OnChanges {

  filter: Filter = new Filter();

  @Input() showfilter = false;
  @Output() advanceSearchClick = new EventEmitter();
  type = 'galleryview';
  avatarJsonDef;
  avatarJsonData;
  optionType;
  @ViewChild('avatar', { static: true }) avatar: WfxAvatarComponent;
  @ViewChild('searchdrawer', { static: false }) searchdrawer: MatDrawer;
  constructor(private itemLibraryService: ItemLibraryService,
    private itemDataService: ItemLibraryDataService,
    public itemGlobal: ItemLibraryGlobals) { }

  ngOnInit() {
    this.avatarJsonDef = {
      attribute: {
        tooltip: {
          tooltipvalue: 'value',
          tooltipvaluetype: '',
          tooltipposition: 'below'
        }
      }
    };

    this.avatarJsonData = {
      imageUrl: `https://www.google.com/url?sa=i&source=images&cd=&cad=rja&
      uact=8&ved=2ahUKEwj7odinxeHkAhVq6XMBHXpuCDoQjRx6BAgBEAQ&url=https%3A%2F%2Fwww.facebook.com
      %2FProfilePictures%2F&psig=AOvVaw0x-8pBLEiq-QZg4NQo9P7U&ust=1569142437318523`,
      profileName: 'Ankit Gupta'
    };

    this.itemLibraryService.filter
    .subscribe(message => {
      this.filter = message.filter;

     // this.searchdrawer.close();
    });
  }

  ngAfterViewChecked() {
  }

  ngOnChanges() {

  // this.commentDetailDefValue.showCommentDetail = false;

  }

  onButtonClick() {
    this.avatar.setwidth('10px');
    this.avatar.setheight('10px');
  }
   onAdvanceSearchClick(filter: Filter) {
     this.advanceSearchClick.emit(filter);

   }

  onRemoveicon() {
  }
  onCardClick($event) {
  }
  onViewoptionClick(type) {
    this.type = type;   
    this.itemGlobal.activePanel = false;
    this.itemLibraryService.invokeshowSelectedRightPanel(true);
  }

  onDdlChange(selectedValue: any) {
    if (this.itemGlobal.currentSelectedTab.Code === 'LIBRARY') {
      this.itemGlobal.tableSelectedView = {Text: selectedValue.optionname, Code: selectedValue.Code1};
      this.optionType = {Text: selectedValue.optionname, Code: selectedValue.Code1};
      this.itemGlobal.selectedTableSortParam = [];
    }
    if (this.itemGlobal.currentSelectedTab.Code === 'COSTING') {
      this.itemLibraryService.invokecostSheetDefSelectedValueChange(selectedValue);
    }
    // this.itemDataService.getList();
  }


  // onDeselection(){
  //   this.itemLibraryService.invokeshowComment(false);
  // }
}
