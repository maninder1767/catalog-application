import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
import 'hammerjs';
import {LicenseManager} from 'ag-grid-enterprise';
LicenseManager.setLicenseKey('__multi_1_Devs__30_July_2020_[v2]_MTU5NjA2NzIwMDAwMA==4b55fad89d77c4391c40685bbb1d90ad');

if (environment.production) {
  enableProdMode();
}

// platformBrowserDynamic().bootstrapModule(AppModule)
//   .catch(err => console.log(err));
  platformBrowserDynamic().bootstrapModule(AppModule).then(ref => {
    // Ensure Angular destroys itself on hot reloads.
    // if (window['ngRef']) {
    //   window['ngRef'].destroy();
    // }
    // window['ngRef'] = ref;
  
    // Otherwise, log the boot error
  }).catch(err => console.error(err));