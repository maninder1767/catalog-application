//Define Express Framework
const express = require("express")
const app = express();

//Define MongoClient for connecting to DataBase
var MongoClient = require('mongodb').MongoClient;

//Define Cros,  A web application executes a cross-origin HTTP request 
var cors = require('cors')
app.use(cors());
app.options('*', cors());
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");

  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

  next();

});

//Define BodyParser for getting Body Data
var bodyParser = require('body-parser')
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

//Global variable
let collection;
let db;

//Create Connection    
MongoClient.connect("mongodb://192.168.2.250/")
  .then(client => {
    db = client.db('MeanPOC')
    dbClient = client;
    collection = db.collection('xtArticleTest');
    collection1 = db.collection('xtFilterGallery');
    collection2 = db.collection('xtArticleTest');
    collection3 = db.collection('dbo.xtsearchnew');
    console.log("connection successfully")
  }).catch(error => console.error(error));



/*

* Created By : Ajit Kumar 

* Created On : 18-Nov-2019

* Purpose : Geting Column Name for Table View  in Catalog 

*/

app.get("/tablecolumn", async (req, res) => {
  let sortField = "ArticleCode", sortValue = -1, searchField = "ArticleName", searchValue = "A";
  const rows = await collection.aggregate([
    {
      $project: {
        "PRINT": "", "PublishCompanyImg": "", "PicFileName": "$PicFileName", "ArticleCode": "$ArticleCode",
        "ArticleName": "$ArticleName", "Season": "$Season", CompanyCode: 1,
        "UOM": "$UOM", "LifeCycleStage": "$ProductLifeCycleStage", "Catalog": "",
        "Last Update By": "$LastChangedBy", "Last Update On": "$LastChanged",
        "Article Track": "", "Where Used": "", "Notes": "", "CareLabelStatus": "", "MISC": ""
      }
    }], [
    { $sort: { [sortField]: sortValue } },
    { $match: { [searchField]: { $regex: (`\\b${searchValue}\\b`) } } }
    ,
    { allowDiskUse: true }
  ]).limit(1).toArray();
  var data = { data: rows, message: "", status: "Success" };
  res.send(data);
})

/*

* Created By : Ajit, Vagisha, Vishal

* Created On : 18-Nov-2019

* Purpose : Geting  data in Table View, Filter and Sorting 

*/

app.post("/table", cors(), async (req, res) => {
  let searchFields = [];
  console.log(req.query)
  let table = req.query.table;
  table = parseInt(table == undefined || table == "" ? 50000 : table, 10);
  let limit = req.query.limit;
  limit = parseInt(limit == undefined || limit == "" ? 100 : limit, 10);
  console.log(req.body)
  let sortField = "ArticleCode", sortValue = 1, skipValue = 0;
  for (var sort of req.body.sortparams) {
    sortField = sort.fieldId == undefined ? "_id" : sort.fieldId;
    sortValue = sort.sortDirection == "asc" || sort.sortDirection == undefined ? 1 : -1;
  }
  if (req.body.searchparams != []) {
    for (var filterCriteria of req.body.searchparams.filterCriteria) {

      var subFilter = [];
      for (var filterData of filterCriteria.filterData) {
        for (var values of filterData.values) {

          subFilter.push({ [filterCriteria.fieldId == "article_active" ? "ArticleCode" : filterCriteria.fieldId]: { $regex: (`${values.value == 'true' ? "" : values.value}`) } })

        }

      }
      if (filterData.length < 2)
        searchFields.push(subFilter);
      else {
        searchFields.push({ $or: subFilter });
      }
    }
  }

  for (var paging in req.body.pagingParams) {
    skipValue = req.body.pagingParams[paging] * 100 - 100;
    break;
  }
  const item = await collection.aggregate([
    {
      $project: {
        "sortOrder": "", "article_id": "$_id", "PRINT": "", "PublishCompanyImg": "",
        "PicFileName": { $concat: ["http://192.168.2.167/Company/PicturesTest1/", "$Custom1", ".jpg"] }, "ArticleCode": "$ArticleCode",
        "ArticleName": "$ArticleName", "Season": "$Season", CompanyCode: 1,
        "UOM": "$UOM", "LifeCycleStage": "$ProductLifeCycleStage", "Catalog": "",
        "Last Update By": "$LastChangedBy", "Last Update On": "$LastChanged",
        "Article Track": "", "Where Used": "", "Notes": "", "CareLabelStatus": "", "MISC": ""
      }
    },
    { $sort: { [sortField]: sortValue } },
    { $match: { $and: searchFields } }

  ]).skip(skipValue).limit(100).toArray();
  const count = await collection.aggregate([
    {
      $project: {
        "sortOrder": "", "article_id": "$_id", "PRINT": "", "PublishCompanyImg": "",
        "PicFileName": { $concat: ["http://192.168.2.167/Company/PicturesTest1/", "$Custom1", ".jpg"] }, "ArticleCode": "$ArticleCode",
        "ArticleName": "$ArticleName", "Season": "$Season", CompanyCode: 1,
        "UOM": "$UOM", "LifeCycleStage": "$ProductLifeCycleStage", "Catalog": "",
        "Last Update By": "$LastChangedBy", "Last Update On": "$LastChanged",
        "Article Track": "", "Where Used": "", "Notes": "", "CareLabelStatus": "", "MISC": ""
      }
    },
    { $sort: { [sortField]: sortValue } },
    { $match: { $and: searchFields } }, { $count: "count" }

  ]).toArray();

  sortField = "", sortValue = 1;
  console.log(count.length)
  const count1 = count.length == 0 ? 0 : count[0].count;
  var data = { data: { count: count1, item } };
  res.send(data);
});

/*

* Created By :  Maninder, Ajit, Vagisha, Vishal

* Created On : 20-Nov-2019

* Purpose : Geting  data in Gallery View, Filter and Sorting 

*/
app.post("/Gallery", cors(), async (req, res) => {

  let searchFields = [];
  let sortField = "ArticleCode", sortValue = 1, skipValue = 0, SearchValue = "";

  for (var pagingParamsObj in req.body.pagingParams) {
    skipValue = req.body.pagingParams[pagingParamsObj] * 100 - 100;
    break;

  }
  for (var searchparamsObj in req.body.searchparams) {
    SearchValue = ((req.body.searchparams.filterText == undefined) ? "" : req.body.searchparams[searchparamsObj]).toUpperCase();

    break;
  }
  if (req.body.searchparams != []) {
    for (var filterCriteria of req.body.searchparams.filterCriteria) {

      var subFilter = [];
      for (var filterData of filterCriteria.filterData) {
        for (var values of filterData.values) {

          if (filterData.values.length < 2) {
            console.log(values.value);
            if (filterCriteria.fieldId == "article_active")
              searchFields.push({ [filterCriteria.fieldId == "article_active" ? "Active" : filterCriteria.fieldId]: values.value == "true" || values.value == true ? values.value = "1" : values.value = "0" });//||values.value=="false"||values.value==false
            else
              searchFields.push({ [filterCriteria.fieldId == "article_active" ? "Active" : filterCriteria.fieldId]: { $regex: (`${values.value == "true" || values.value == true ? "1" : values.value}`) } });//||values.value=="false"||values.value==false
          } else {
            subFilter.push({ [filterCriteria.fieldId == "article_active" ? "Active" : filterCriteria.fieldId]: { $regex: (`${values.value == "true" || values.value == true ? "1" : values.value}`) } })//||values.value=="false"||values.value==false
          }
        }

        if (filterData.values.length > 1)
          searchFields.push({ $or: subFilter });
        break;
      }
      console.log(searchFields)
    }
  }
  for (var sortparamsObj of req.body.sortparams) {

    sortField = (sortparamsObj.fieldId == undefined ? "_id" : sortparamsObj.fieldId).replace("ART.", "").replace("ArticleCode", "itemCode").replace("ArticleName", "itemName");
    sortValue = sortparamsObj.sortDirection == 'DESC' || sortparamsObj.sortDirection == 'Desc' ? -1 : 1;

  }

  const item = await collection
    .aggregate([
      {
        $project: {
          sortOrder: "", ProductLifeCycleStage: 1, LastChanged: 1, CreatedOn: 1,
          plcName: "$PicFileName",
          likeCount: "$CatalogType",
          isLike: "$Active",
          plcColor: "$Comment",
          plcColorChip: "$Quality",
          commentCount: { $size: "$Comment" },
          isBookmark: "$Bookmark",
          imageUrl: { $concat: ["http://192.168.2.167/Company/PicturesTest1/", "$Custom1", ".jpg"] },//$PicFileName
          itemCode: { $toUpper: "$ArticleCode" },
          itemName: { $toUpper: "$ArticleName" },
          buyerReference: "$BuyerReference",
          supplierReference: "$Reference", CompanyCode: 1,
          CountryOfOriginCode: 1,
          ProductGroup: 1,
          Construction: 1,
          FiberContent: 1,
          Gender: 1,
          Theme: 1,
          Season: 1,
          UOM: 1,
          Agent: 1,
          Buyer: 1,
          WashType: 1,
          Supplier: 1,
          PackingType: 1,
          OurContact: 1,
          PrintType: 1, Active: 1,
          itemId: "$ID", _id: 0
        }
      },
      {
        $match: {
          $and: [{
            $or: [{ itemName: new RegExp(SearchValue) },
            { itemCode: new RegExp(SearchValue) }, { buyerReference: new RegExp(SearchValue) },
            { supplierReference: new RegExp(SearchValue) }]
          }, { $and: searchFields }]
        }
      }
      , { $sort: { [sortField]: sortValue } }

    ]).skip(skipValue).limit(100).toArray();

  try {
    var count = await collection
      .aggregate([{
        $project: {
          sortOrder: "", ProductLifeCycleStage: 1, LastChanged: 1, CreatedOn: 1,
          plcName: "$PicFileName",
          likeCount: "$CatalogType",
          isLike: "$Active",
          plcColor: "$Comment",
          plcColorChip: "$Quality",
          commentCount: "$Comment",
          isBookmark: "$Bookmark",
          imageUrl: { $concat: ["http://192.168.2.167/Company/PicturesTest1/", "$Custom1", ".jpg"] },
          itemCode: { $toUpper: "$ArticleCode" },
          itemName: { $toUpper: "$ArticleName" },
          buyerReference: "$BuyerReference",
          supplierReference: "$Reference", CompanyCode: 1,
          CountryOfOriginCode: 1,
          ProductGroup: 1,
          Construction: 1,
          FiberContent: 1,
          Gender: 1,
          Theme: 1,
          Season: 1,
          UOM: 1,
          Agent: 1,
          Buyer: 1,
          WashType: 1,
          Supplier: 1,
          PackingType: 1,
          OurContact: 1,
          PrintType: 1, Active: 1,
          itemId: "$ID", _id: 0
        }
      },
      { $match: { $and: [{ $or: [{ itemName: new RegExp(SearchValue) }, { itemCode: new RegExp(SearchValue) }, { buyerReference: new RegExp(SearchValue) }, { supplierReference: new RegExp(SearchValue) }] }, { $and: searchFields }] } }
        , { $sort: { [sortField]: sortValue } }, { $count: "count" }
      ]).toArray();
  } catch (e) {
    console.log(e)
  }
  const count1 = count.length == 0 ? 0 : count[0].count;

  var data = { data: { count: count1, item }, message: "", status: "Success" };

  res.send(data);
})

/*

* Created By : Vagisha

* Created On : 25-Nov-2019

* Purpose : Bookmark 

*/

app.put('/Bookmark', async (req, res) => {
  let getdata;
  let id = (req.body.id);
  console.log(id);
  console.log(req.body);

  let get = await collection.find({ ID: id }, { _id: 0, Bookmark: 1 })
    .limit(1).toArray();
  for( getdata in get) {
    console.log(get[getdata])
  }
  console.log(get[getdata].Bookmark)
  if (get[getdata].Bookmark === 0) {
    const row = await collection.updateOne({ ID: id }, { $set: { Bookmark: 1 } });
    console.log('updated' + row);

    res.send({
      "data": "",
      "message": "",
      "status": "Success"
    });
  }
  else {
    const row = await collection.updateOne({ ID: id }, { $set: { Bookmark: 0 } });
    console.log('not updated' + row);

    res.send({
      "data": "",
      "message": "",
      "status": "Success"
    });
  }
});


/*

* Created By : Vishal

* Created On : 25-Nov-2019

* Purpose : Active and Inactive 

*/

app.put('/active', async (req, res) => {

  console.log(req.body);

  let getdata;

  var id = (req.body[0].id);
  console.log(id);
  let get = await collection.aggregate({ $project: { _id: 0, Active: 1 } }, { $match: { ID: id } })
    .limit(1).toArray();
  console.log(get)
  if (get == []) {
    res.send({
      "data": "",
      "message": "",
      "status": "Not Data Found"
    });
  }
  for (getdata in get) {
    console.log(get[getdata])
  }

  if (get[getdata].Active === "1") {
    const row = await collection.updateOne({ ID: id }, { $set: { Active: "0" } });
    console.log(row)

    res.send({
      "data": "",
      "message": "",
      "status": "Success"
    });
  }
  else {
    const row = await collection.updateOne({ ID: id }, { $set: { Active: "1" } });
    console.log(row)

    res.send({
      "data": "",
      "message": "",
      "status": "Success"
    });
  }
});


/*

* Created By : Maninder Singh

* Created On : 25-Nov-2019

* Purpose : Article Detail in Gallery 

*/

app.get('/Property/:id', cors(), async (req, res) => {

  var id = req.params.id;
  var item1 = [], item = [];
  let arr = [{ _id: 0, "fieldId": "article_image", "fieldType": "Fixed", "caption": "Image", "value": { $concat: ["http://192.168.2.167/Company/PicturesTest1/", "$Custom1", ".jpg"] }, "controlType": "Image", "translation": { "translationID": "" } },
  { _id: 0, "fieldId": "article_division_name", "fieldType": "Fixed", "caption": "Division", "value": "$CompanyDivisionCode", "controlType": "DropDown", "translation": { "translationID": "LBL_215_1" } },
  { _id: 0, "fieldId": "article_productgroup_name", "fieldType": "Fixed", "caption": "Product Group", "value": "$ProductGroup", "controlType": "DropDown", "translation": { "translationId": "LBL_212_1" } },
  { _id: 0, "fieldId": "article_productsubcategory_name", "fieldType": "Fixed", "caption": "ProductSubCategory", "value": "$ProductSubCatCode", "controlType": "DropDown", "translation": { "translationId": "LBL_213_1" } },
  { _id: 0, "fieldId": "article_colordefinition", "fieldType": "Fixed", "caption": "Color Definition", "value": "$ColorType", "controlType": "DropDown", "translation": { "translationId": "LBL_320_1" } },
  { _id: 0, "fieldId": "article_code", "fieldType": "Fixed", "caption": "Article Code", "value": "$ArticleCode", "controlType": "Text", "translation": { "translationId": "LBL_194_1" } },
  { _id: 0, "fieldId": "article_name", "fieldType": "Fixed", "caption": "Article Name", "value": "$ArticleName", "controlType": "Text", "translation": { "translationId": "LBL_195_1" } },
  { _id: 0, "fieldId": "article_description", "fieldType": "Extra", "caption": "Description", "value": "$ArticleDesc", "controlType": "TextArea", "translation": { "translationId": "XLBL_118_1" } },
  { _id: 0, "fieldId": "article_buyingprice", "fieldType": "Extra", "caption": "Buying Price", "value": "$BuyingPrice", "controlType": "Price", "translation": { "translationId": "LBL_203_1" } },
  { _id: 0, "fieldId": "article_buyer_name", "fieldType": "Extra", "caption": "Buyer", "value": "$Buyer", "controlType": "DropDown", "translation": { "translationId": "LBL_196_1" } },
  { _id: 0, "fieldId": "article_buyerref", "fieldType": "Extra", "caption": "Buyer Ref.", "value": "$BuyerReference", "controlType": "Text", "translation": { "translationId": "LBL_197_1" } },
  { _id: 0, "fieldId": "article_colorcard_name", "fieldType": "Extra", "caption": "Color Card", "value": "", "controlType": "DropDown", "translation": { "translationId": "LBL_105_1" } },
  { _id: 0, "fieldId": "article_countryoforigin_name", "fieldType": "Extra", "caption": "Country of Origin", "value": "", "controlType": "DropDown", "translation": { "translationId": "LBL_553_1" } },
  { _id: 0, "fieldId": "article_gender_name", "fieldType": "Extra", "caption": "Gender", "value": "$Gender", "controlType": "DropDown", "translation": { "translationId": "LBL_154_1" } },
  { _id: 0, "fieldId": "article_lifecyclestage_name", "fieldType": "Extra", "caption": "Life Cycle Stage", "value": "$ProductLifeCycleStage", "controlType": "DropDown", "translation": { "translationId": "LBL_100_1" } },
  { _id: 0, "fieldId": "article_materialdescription", "fieldType": "Extra", "caption": "Material Description", "value": "", "controlType": "TextArea", "translation": { "translationId": "LBL_214_1" } },
  { _id: 0, "fieldId": "article_internalnotes", "fieldType": "Extra", "caption": "Internal Notes", "value": "", "controlType": "TextEditor", "translation": { "translationId": "XLBL_148_1" } },
  { _id: 0, "fieldId": "article_season_name", "fieldType": "Extra", "caption": "Season", "value": "$Season", "controlType": "DropDown", "translation": { "translationId": "LBL_122_1" } },
  { _id: 0, "fieldId": "article_sharednotes", "fieldType": "Extra", "caption": "Shared Notes", "value": "", "controlType": "TextEditor", "translation": { "translationId": "LBL_661_1" } },
  { _id: 0, "fieldId": "article_sizecard_name", "fieldType": "Extra", "caption": "Size Card", "value": "", "controlType": "DropDown", "translation": { "translationId": "LBL_116_1" } },
  { _id: 0, "fieldId": "article_sellingprice", "fieldType": "Extra", "caption": "Selling Price", "value": "", "controlType": "Price", "translation": { "translationId": "LBL_263_1" } },
  { _id: 0, "fieldId": "article_supplier_name", "fieldType": "Extra", "caption": "Supplier", "value": "", "controlType": "DropDown", "translation": { "translationId": "LBL_199_1" } },
  { _id: 0, "fieldId": "article_supplierref", "fieldType": "Extra", "caption": "Supplier Ref.", "value": "$Reference", "controlType": "Text", "translation": { "translationId": "LBL_202_1" } },
  { _id: 0, "fieldId": "article_UOM_name", "fieldType": "Extra", "caption": "Unit of Measurement (UOM)", "value": "$UOM", "controlType": "DropDown", "translation": { "translationId": "LBL_133_1" } }
  ];
  for (let arrcol of arr) {

    item1.push(await collection.aggregate(
      [
        { $match: { ID: id } },
        { $project: arrcol }
      ]).toArray());
  }
  for (let final1 of item1)
    for (let final of final1) {
      item.push(final)
    }
  var data = { data: { item, color: [{ "articlecolor_code": "2189", "articlecolor_name": "#39", "articlecolor_image_url": "", "articlecolor_firstcolor_rgb": "028-113-083" }, { "articlecolor_code": "1157", "articlecolor_name": "#7030 Talons", "articlecolor_image_url": "", "articlecolor_firstcolor_rgb": "255-255-255" }, { "articlecolor_code": "1312", "articlecolor_name": "#820 YKK", "articlecolor_image_url": "", "articlecolor_firstcolor_rgb": "255-000-000" }, { "articlecolor_code": "2188", "articlecolor_name": "#85", "articlecolor_image_url": "", "articlecolor_firstcolor_rgb": "015-079-118" }] }, message: "", status: "Success" };
  res.send(data);
})


/*

* Created By : Nitesh Mittal

* Created On : 19-Nov-2019

* Purpose : To delete a saved search

*/

app.delete("/Search", async (req, res) => {
  var id = req.query.id;
  console.log(id);
  var id = parseInt(id == undefined ? "0" : id)
  console.log(id);
  const rows = await collection3.deleteOne({ SearchId: id });
  console.log(rows);
  var data = { data: null, message: "", status: "Success" };
  res.send(data);
});

/*

* Created By : Nitesh Mittal

* Created On : 22-Nov-2019

* Purpose : To apply the selected Saved Search

*/

app.get("/Search/Id", async (req, res) => {
  let id1 = req.query.id;
  id1 = parseInt(id1 == undefined ? "0" : id1)
  const rows = await collection3.aggregate([
    { $match: { SearchId: id1 } },
    { $project: { _id: 0, SearchCriteria: 1 } }
  ]).toArray();
  for (let SearchCriteria in rows) {
    var data = { data: rows[SearchCriteria].SearchCriteria, message: "", status: "Success" };
    console.log(rows[SearchCriteria].SearchCriteria);
  }
  res.send(data);
});


/*

* Created By : Nitesh Mittal

* Created On : 21-Nov-2019

* Purpose : To get Saved Search List

*/

app.get("/Search", async (req, res) => {

  const rows = await collection3.aggregate([
    { $project: { _id: 0, "id": "$SearchId", "value": "$SearchName", "text": "$SearchName" } }, { $sort: { id: -1 } }
  ],
    { allowDiskUse: true }
  ).toArray();
  var data = { data: rows, message: "", status: "Success" };
  res.send(data);
});

/*

* Created By : Nitesh Mittal

* Created On : 20-Nov-2019

* Purpose : To save the Search  

*/

app.post("/Search", async (req, res) => {
  let id1 = req.body.filterName, body;
  id1 = (id1 == undefined ? "" : id1)
  console.log(id1)
  body = (req.body.filterCriteria);
  console.log(body);
  body = (body == undefined ? "" : body)
  const count = await collection3.find().count();
  console.log(count);
  const rows = await collection3.insert({ SearchId: count + 1, SearchName: id1, SearchCriteria: body });
  console.log(rows);
  var data = { data: rows, message: "", status: "Success" };
  res.send(data);
});

/*

* Created By : Nitesh Mittal

* Created On : 24-Nov-2019

* Purpose : Get Comment by ID 

*/


app.get("/Comment", async (req, res) => {
  let id = req.query.id; var count = 0;
  id = (id == undefined ? "0" : id)
  console.log(id)
  const rows = await collection2.aggregate([
    { $match: { ID: id } },
    { $project: { _id: 0, Comment: 1 } }
  ]).toArray();
  console.log(rows);
  const GetComment = await collection2.aggregate([
    { $match: { ID: id } }, {
      $project: { _id: 0, Comment: 1 }
    }
  ]).toArray();
  console.log(GetComment);
  console.log(rows);
  for (let Comment in GetComment) {
    count = [GetComment[Comment].Comment] == 'NULL' ? 0 : GetComment[Comment].Comment.length;
  }
  for (let comment in rows) {
    var data = { data: [{ count: count, comment: rows[comment].Comment == "" ? { "commentID": "1", "comment": "Default", "createdOn": "2019-11-22T10:37:17.158Z", "createdBy": { "fullName": "Sanjay Amb", "userImageURL": "http://192.168.2.167/Company/10006/Pictures/018_2088946_thumb.jpg" } } : rows[comment].Comment }], message: "", status: "Success" };
    res.send(data);
  }
});


/*

* Created By : Nitesh Mittal

* Created On : 22-Nov-2019

* Purpose : Comment  

*/

app.post("/Comment", async (req, res) => {
  var id = (req.body.id);
  var newComment = [];
  const GetComment = await collection2.aggregate([
    {
      $match: { ID: id }
    },
    {
      $project: { _id: 0, Comment: 1 }
    }
  ]).toArray();
  console.log(GetComment);
  for (let Comment in GetComment)
  newComment = [GetComment[Comment].Comment] == 'NULL' ? [] : GetComment[Comment].Comment;
  newComment.push({ commentID: "", comment: req.body.comment, createdOn: new Date(), createdBy: { "fullName": "Sanjay Amb", "userImageURL": "http://192.168.2.167/Company/10006/Pictures/018_2088946_thumb.jpg" } })
  const rows = await collection2.updateOne({ ID: req.body.id }, { $set: { "Comment": newComment } });
  console.log(rows);
  var data = { data: null, message: "", status: "Success" };
  res.send(data);

});

/*

* Created By : Nitesh Mittal

* Created On : 19-Nov-2019

* Purpose : Master Search List 

*/

app.get("/Search/Definition", async (req, res) => {
  const rows = await collection1.aggregate([{
    $project: { _id: 0, "fieldId": "$FieldId", "caption": "$Caption", "dataSource": "$dataSource", "controlType": "$ControlType", "fieldType": "$FieldType", "sortOrder": "$SortOrder" },

  }, { $sort: { 'dataSource': 1 } }]).toArray();
  var data = { data: rows, message: "", status: "Success" };
  res.send(data);
});

/*

* Created By : Nitesh Mittal

* Created On : 22-Nov-2019

* Purpose : Master sub category Search List 

*/

app.get("/Master/Data/Custom", async (req, res) => {
  let objectType = req.query.objectType;
  console.log(objectType);
  let dataSource = req.query.dataSource;
  dataSource = (dataSource == undefined ? "0" : dataSource)
  console.log(dataSource);
  let data1 = await GetMasterData(dataSource);
  res.send(data1);

});

/*

* Created By : Nitesh Mittal

* Created On : 23-Nov-2019

* Purpose : function to get sub Category List

*/

async function GetMasterData(dataSource) {
  let  gender, uom, dataArr = [];
  try {
    switch (dataSource) {
      case "Company Code": {
        collection7 = db.collection('xtArticleTest');
        const rows = await collection7.aggregate([
          { $group: { _id: { "value": "$CompanyCode", "text": "$CompanyCode" } } },
          { $project: { CompanyCode: 1 } }
        ]).limit(20).toArray();
        for (let CompanyCode in rows)
          dataArr.push({ id: parseInt(CompanyCode + 1), value: rows[CompanyCode]._id.value, text: rows[CompanyCode]._id.text });
        let code = { data: dataArr, message: "", status: "Success" };
        return code;
      }
      case "Collection": {
        let collection7 = db.collection('dbo.xtCollection');
        const rows = await collection7.aggregate([
          { $group: { _id: { "value": "$CollectionName", "text": "$CollectionName" } } },
          { $project: { "CollectionName": 1 } }
        ]).limit(50).toArray();
        for (let CollectionName in rows)
          dataArr.push({ id: parseInt(CollectionName + 1), value: rows[CollectionName]._id.value, text: rows[CollectionName]._id.text });
        let coll = { data: dataArr, message: "", status: "Success" };
        return coll;
      }



      case "Division": {
        let collection7 = db.collection('dbo.vwUserGroupDivisionPermissions');
        const rows = await collection7.aggregate([
          { $group: { _id: { "value": "$DivisionName", "text": "$DivisionName" } } },
          { $project: { "DivisionName": 1 } }
        ]).limit(15).toArray();
        for (let DivisionName in rows)
          dataArr.push({ id: parseInt(DivisionName + 1), value: rows[DivisionName]._id.value, text: rows[DivisionName]._id.text });
        let division = { data: dataArr, message: "", status: "Success" };
        return division;
      }

      case "Product Sub-Category": {
        let collection7 = db.collection('dbo.xtProductSubCategory');
        const rows = await collection7.aggregate([
          { $group: { _id: { "value": "$ProductSubCategory", "text": "$ProductSubCategory" } } },
          { $project: { "ProductSubCategory": 1 } }
        ]).limit(15).toArray();
        for (let ProductSubCategory in rows)
          dataArr.push({ id: parseInt(ProductSubCategory + 1), value: rows[ProductSubCategory]._id.value, text: rows[ProductSubCategory]._id.text });
        productsub = { data: dataArr, message: "", status: "Success" };
        return productsub;
      }
      case "Country of Origin": {
        let collection7 = db.collection('xtArticleTest');
        const rows = await collection7.aggregate([
          { $group: { _id: { "value": "$CountryOfOriginCode", "text": "$CountryOfOriginCode" } } },
          { $project: { "CountryOfOriginCode": 1 } },
          { $sort: { "CountryOfOriginCode": -1 } }
        ]).limit(30).toArray();
        for (let TranslatedString in rows)
          dataArr.push({ id: parseInt(TranslatedString + 1), value: rows[TranslatedString]._id.value, text: rows[TranslatedString]._id.text });
        let country = { data: dataArr, message: "", status: "Success" };
        return country;
      }
      case "Color Definition": {
        let collection7 = db.collection('dbo.xtTranslation');
        const rows = await collection7.aggregate([
          {
            $match: {
              ObjectName: "xtColorDefinition"

            }
          },
          { $group: { _id: { "value": "$TranslatedString", "text": "$TranslatedString" } } },
          { $project: { "TranslatedString": 1 } }
        ]).limit(15).toArray();
        for (let TranslatedString in rows)
          dataArr.push({ id: parseInt(TranslatedString + 1), value: rows[TranslatedString]._id.value, text: rows[TranslatedString]._id.text });
        let defination = { data: dataArr, message: "", status: "Success" };
        return defination;
      }

      case "Product Group": {
        collection7 = db.collection('xtArticleTest');
        const rows = await collection7.aggregate([
          { $group: { _id: { "value": "$ProductGroup", "text": "$ProductGroup" } } },
          { $project: { "ProductGroup": 1 } }
        ]).limit(15).toArray();
        for (let ProductGroup in rows)
          dataArr.push({ id: parseInt(ProductGroup + 1), value: rows[ProductGroup]._id.value, text: rows[ProductGroup]._id.text });
        product = { data: dataArr, message: "", status: "Success" };
        return product;
      }

      case "Fabric Construction": {
        let collection7 = db.collection('xtArticleTest');
        const rows = await collection7.aggregate([
          { $group: { _id: { "value": "$Construction", "text": "$Construction" } } },
          { $project: { "Construction": 1 } }
        ]).limit(15).toArray();
        for (let FabricConstruction in rows)
          dataArr.push({ id: parseInt(FabricConstruction + 1), value: rows[FabricConstruction]._id.value, text: rows[FabricConstruction]._id.text });
        fabriccoun = { data: dataArr, message: "", status: "Success" };
        return fabriccoun;
      }

      case "Fabric Count": {
        let collection7 = db.collection('xtArticleTest');
        const rows = await collection7.aggregate([
          { $group: { _id: { "value": "$FiberContent", "text": "$FiberContent" } } },
          { $project: { "FiberContent": 1 } }
        ]).limit(15).toArray();
        for (let FabricCount in rows)
          dataArr.push({ id: parseInt(FabricCount + 1), value: rows[FabricCount]._id.value, text: rows[FabricCount]._id.text });
        fabriccount = { data: dataArr, message: "", status: "Success" };
        return fabriccount;
      }

      case "Gender": {
        let collection7 = db.collection('xtArticleTest');
        const rows = await collection7.aggregate([
          { $group: { _id: { "value": "$Gender", "text": "$Gender" } } },
          { $project: { "Gender": 1 } }
        ]).limit(15).toArray();
        for (let Gender in rows)
          dataArr.push({ id: parseInt(Gender + 1), value: rows[Gender]._id.value, text: rows[Gender]._id.text });
        gender = { data: dataArr, message: "", status: "Success" };
        return gender;
      }

      case "Theme": {
        let collection7 = db.collection('xtArticleTest');
        const rows = await collection7.aggregate([
          { $group: { _id: { "value": "$Theme", "text": "$Theme" } } },
          { $project: { "Theme": 1 } }
        ]).limit(15).toArray();
        for (let ThemeCode in rows)
          dataArr.push({ id: parseInt(ThemeCode + 1), value: rows[ThemeCode]._id.value, text: rows[ThemeCode]._id.text });
        theme = { data: dataArr, message: "", status: "Success" };
        return theme;
      }

      case "Season": {
        let collection7 = db.collection('xtArticleTest');
        const rows = await collection7.aggregate([
          { $group: { _id: { "value": "$Season", "text": "$Season" } } },
          { $project: { "Season": 1 } }
        ]).limit(15).toArray();
        for (let SeasonCode in rows)
          dataArr.push({ id: parseInt(SeasonCode + 1), value: rows[SeasonCode]._id.value, text: rows[SeasonCode]._id.text });
        season = { data: dataArr, message: "", status: "Success" };
        return season;
      }


      case "Unit of Measurement (UOM)": {
        let collection7 = db.collection('xtArticleTest');
        const rows = await collection7.aggregate([
          { $group: { _id: { "value": "$UOM", "text": "$UOM" } } },
          { $project: { "UOM": 1 } }
        ]).limit(15).toArray();
        for (let UOM in rows)
          dataArr.push({ id: parseInt(UOM + 1), value: rows[UOM]._id.value, text: rows[UOM]._id.text });
        uom = { data: dataArr, message: "", status: "Success" };
        return uom;
      }

      case "Life Cycle Stage": {
        let collection7 = db.collection('xtArticleTest');
        const rows = await collection7.aggregate([
          { $group: { _id: { "value": "$ProductLifeCycleStage", "text": "$ProductLifeCycleStage" } } },
          { $project: { "ProductLifeCycleStage": 1 } }
        ]).limit(15).toArray();
        for (let ProductLifeCycleStageDesc in rows)
          dataArr.push({ id: parseInt(ProductLifeCycleStageDesc + 1), value: rows[ProductLifeCycleStageDesc]._id.value, text: rows[ProductLifeCycleStageDesc]._id.text });
        let Lifecyclestage = { data: dataArr, message: "", status: "Success" };
        return Lifecyclestage;
      }

      case "Agent": {
        let collection7 = db.collection('xtArticleTest');
        const rows = await collection7.aggregate([
          { $group: { _id: { "value": "$Agent", "text": "$Agent" } } },
          { $project: { "Agent": 1 } }
        ]).limit(100).toArray();
        for (let Agent in rows)
          dataArr.push({ id: parseInt(Agent + 1), value: rows[Agent]._id.value, text: rows[Agent]._id.text });
        let agent = { data: dataArr, message: "", status: "Success" };
        console.log(agent);
        return agent;
      }

      case "Buyer": {
        let collection7 = db.collection('xtArticleTest');
        const rows = await collection7.aggregate([
          { $group: { _id: { "value": "$Buyer", "text": "$Buyer" } } },
          { $project: { "Buyer": 1 } }
        ]).limit(15).toArray();
        for (let CompanyName in rows)
          dataArr.push({ id: parseInt(CompanyName + 1), value: rows[CompanyName]._id.value, text: rows[CompanyName]._id.text });
        let buyer = { data: dataArr, message: "", status: "Success" };
        return buyer;
      }

      case "Wash Type": {
        let collection7 = db.collection('xtArticleTest');
        const rows = await collection7.aggregate([
          { $group: { _id: { "value": "$WashType", "text": "$WashType" } } },
          { $project: { WashType: 1 } }
        ]).limit(30).toArray();
        for (let WashType in rows)
          dataArr.push({ id: parseInt(WashType + 1), value: rows[WashType]._id.value, text: rows[WashType]._id.text });
        let wash = { data: dataArr, message: "", status: "Success" };
        return wash;
      }

      case "Supplier": {
        let collection7 = db.collection('xtArticleTest');
        const rows = await collection7.aggregate([
          { $group: { _id: { "value": "$Supplier", "text": "$Supplier" } } },
          { $project: { Supplier: 1 } }
        ]).limit(15).toArray();
        for (let Supplier in rows)
          dataArr.push({ id: parseInt(Supplier + 1), value: rows[Supplier]._id.value, text: rows[Supplier]._id.text });
        let wash = { data: dataArr, message: "", status: "Success" };
        return wash;
      }

      case "Size Card": {
        let collection7 = db.collection('dbo.xtSizeGroup');
        const rows = await collection7.aggregate([
          { $group: { _id: { "value": "$Title", "text": "$Title" } } },
          { $project: { Title: 1 } }
        ]).limit(15).toArray();
        for (let Title in rows)
          dataArr.push({ id: parseInt(Title + 1), value: rows[Title]._id.value, text: rows[Title]._id.text });
        let sizecard = { data: dataArr, message: "", status: "Success" };
        return sizecard;
      }

      case "Color Card": {
        let collection7 = db.collection('dbo.xtColorGroup');
        const rows = await collection7.aggregate([
          { $group: { _id: { "value": "$Title", "text": "$Title" } } },
          { $project: { Title: 1 } }
        ]).limit(15).toArray();
        for (let Title in rows)
          dataArr.push({ id: parseInt(Title + 1), value: rows[Title]._id.value, text: rows[Title]._id.text });
        let colorcard = { data: dataArr, message: "", status: "Success" };
        return colorcard;
      }

      case "Package Type": {
        let collection7 = db.collection('xtArticleTest');
        const rows = await collection7.aggregate([
          { $group: { _id: { "value": "$PackingType", "text": "$PackingType" } } },
          { $project: { PackingType: 1 } }
        ]).limit(15).toArray();
        for (let PackageTypeName in rows)
          dataArr.push({ id: parseInt(PackageTypeName + 1), value: rows[PackageTypeName]._id.value, text: rows[PackageTypeName]._id.text });
        let pack = { data: dataArr, message: "", status: "Success" };
        return pack;
      }

      case "Our Contact": {
        let collection7 = db.collection("xtArticleTest");
        const rows = await collection7.aggregate([
          { $group: { _id: { "value": "$OurContact", "text": "$OurContact" } } },
          { $project: { "OurContact": 1 } }
        ]).limit(100).toArray();
        for (let FirstName in rows)
          dataArr.push({ id: parseInt(FirstName + 1), value: rows[FirstName]._id.value, text: rows[FirstName]._id.text });
        let name = { data: dataArr, message: "", status: "Success" };
        return name;
      }

      case "Print Type": {
        let collection7 = db.collection('xtArticleTest');
        const rows = await collection7.aggregate([
          { $group: { _id: { "value": "$PrintType", "text": "$PrintType" } } },
          { $project: { "PrintType": 1 } }
        ]).limit(1000).toArray();
        for (let PrintType in rows)
          dataArr.push({ id: parseInt(PrintType + 1), value: rows[PrintType]._id.value, text: rows[PrintType]._id.text });
        let print = { data: dataArr, message: "", status: "Success" };
        console.log(print)
        return print;
      }

      default: "nan"

        break;


    }
  }

  catch (e) {
    return [];

  }
}


/*

* Created By : Nitesh Mittal

* Created On : 29-Nov-2019

* Purpose : Tab

*/
app.get("/Tab", async (req, res) => {
  res.send({
    "data": [
      {
        "tabId": 1,
        "code": "LIBRARY",
        "text": "Library",
        "tooltip": "Item Library",
        "caption": "Item Library",
        "translation": {
          "translationId": "LBL_2069"
        }
      },
      {
        "tabId": 2,
        "code": "COSTING",
        "text": "Costing",
        "tooltip": "Item Costing",
        "caption": "Item Costing",
        "translation": {
          "translationId": "LBL_2070"
        }
      }
    ],
    "message": "",
    "status": "Success"
  });
});

/*

* Created By : Ajit Kumar

* Created On : 27-Nov-2019

* Purpose : Context Menu 

*/

app.get("/ContextMenu/:id", cors(), async (req, res) => {
  res.send({
    "data": {
      "fPerm": 3,
      "groupID": 100011790,
      "contextMenu": [
        {
          "id": 1,
          "code": "GOTO",
          "text": "Go-To",
          "sortOrder": 10,
          "subMenus": [
            {
              "id": 1,
              "code": "Techpacks",
              "text": "Techpacks",
              "sortOrder": 10
            },
            {
              "id": 2,
              "code": "Approvals",
              "text": "Approvals",
              "sortOrder": 20
            },
            {
              "id": 3,
              "code": "Log",
              "text": "Log",
              "sortOrder": 30
            },
            {
              "id": 4,
              "code": "Msg",
              "text": "Messages",
              "sortOrder": 40
            },
            {
              "id": 5,
              "code": "History",
              "text": "History",
              "sortOrder": 50
            },
            {
              "id": 6,
              "code": "BOMMaster",
              "text": "BOM",
              "sortOrder": 60
            },
            {
              "id": 7,
              "code": "CostSheet",
              "text": "Costing",
              "sortOrder": 70
            },
            {
              "id": 8,
              "code": "PrePacks",
              "text": "Barcodes/Pre-Packs",
              "sortOrder": 80
            },
            {
              "id": 9,
              "code": "ValuationZone",
              "text": "Valuations",
              "sortOrder": 90
            }
          ]
        },
        {
          "id": 2,
          "code": "SHARE",
          "text": "Share",
          "sortOrder": 20
        },
        {
          "id": 3,
          "code": "PRINT",
          "text": "Generate Techpack",
          "sortOrder": 30
        },
        {
          "id": 4,
          "code": "INACTIVE",
          "text": "Make Inactive",
          "sortOrder": 40
        }
      ]
    },
    "message": "",
    "status": "Success"
  })
})



/*

* Created By : Ajit Kumar

* Created On : 26-Nov-2019

* Purpose : Bottom Pannel 

*/

app.get("/BottomPanel", async (req, res) => {
  res.send({
    "data": {
      "tools": [
        {
          "id": 1,
          "code": "SHARE",
          "text": "Share",
          "sortOrder": 10
        },
        {
          "id": 2,
          "code": "PRINT",
          "text": "Generate Techpack",
          "sortOrder": 20
        },
        {
          "id": 3,
          "code": "UPDATE",
          "text": "Multi Article Update",
          "sortOrder": 30
        }
      ],
      "menus": [
        {
          "id": 1,
          "code": "ACTIVE",
          "text": "Make Active",
          "sortOrder": 10
        },
        {
          "id": 2,
          "code": "INACTIVE",
          "text": "Make Inactive",
          "sortOrder": 20
        },
        {
          "id": 3,
          "code": "WHEREUSED",
          "text": "Where Used",
          "sortOrder": 30
        },
        {
          "id": 4,
          "code": "CREATE",
          "text": "Create",
          "sortOrder": 40,
          "subMenus": [
            {
              "id": 1,
              "code": "RAISE_SR",
              "text": "Create Sample Request",
              "sortOrder": 10
            },
            {
              "id": 2,
              "code": "RAISE_PO",
              "text": "Create Purchase Order",
              "sortOrder": 20
            },
            {
              "id": 3,
              "code": "RAISE_INSPECTION",
              "text": "Create Inspection Schedule",
              "sortOrder": 30
            }
          ]
        },
        {
          "id": 5,
          "code": "TnA",
          "text": "Time & Action",
          "sortOrder": 50,
          "subMenus": [
            {
              "id": 1,
              "code": "ActTnA",
              "text": "Activate T&A",
              "sortOrder": 10
            },
            {
              "id": 2,
              "code": "DelTnA",
              "text": "Delete T&A",
              "sortOrder": 20
            },
            {
              "id": 3,
              "code": "ActDelvTnA",
              "text": "Activate Delivery T&A",
              "sortOrder": 30
            },
            {
              "id": 4,
              "code": "DelDelvTnA",
              "text": "Delete Delivery T&A",
              "sortOrder": 40
            }
          ]
        }
      ]
    },
    "message": "",
    "status": "Success"
  })
})

/*

* Created By : Vagisha

* Created On : 25-Nov-2019

* Purpose : Sorting DDL data in Gallery View 

*/
app.get("/sort", cors(), async (req, res) => {
  res.send({
    "data": [
      {
        "id": 1,
        "value": "ArticleCode",
        "text": "Article Code",
        "sortOrder": 10
      },
      {
        "id": 2,
        "value": "ArticleName",
        "text": "Article Name",
        "sortOrder": 20
      },
      {
        "id": 3,
        "value": "ProductLifeCycleStage",
        "text": "Life Cycle Stage",
        "sortOrder": 30
      },
      {
        "id": 4,
        "value": "ART.CreatedOn",
        "text": "Created On",
        "sortOrder": 40,
        "isSelected": true,
        "sortDirection": "DESC"
      },
      {
        "id": 5,
        "value": "LastChanged",
        "text": "Last Changed",
        "sortOrder": 50
      }
    ],
    "message": "",
    "status": "Success"
  })
});


/*

* Created By : Ajit Kumar

* Created On : 19-Nov-2019

* Purpose : DDL in Table Layout 

*/

app.get("/TableLayout", cors(), async (req, res) => {
  res.send({
    "data": [
      {
        "id": 1000678,
        "value": "Default",
        "text": "Default"
      },
      {
        "id": 10006198,
        "value": "SEP-01",
        "text": "SEP-01"
      },
      {
        "id": 10006199,
        "value": "SEP-02",
        "text": "SEP-02"
      },
      {
        "id": 10006201,
        "value": "SEP-03",
        "text": "SEP-03"
      },
      {
        "id": 10006202,
        "value": "SEP-04",
        "text": "SEP-04"
      },
      {
        "id": 10006204,
        "value": "Copy of WFX Default Test1806",
        "text": "Copy of WFX Default Test1806"
      },
      {
        "id": 10006206,
        "value": "Paging",
        "text": "Paging"
      },
      {
        "id": 10006208,
        "value": "Copy of WFX Default",
        "text": "Copy of WFX Default"
      },
      {
        "id": 10006210,
        "value": "Delivery LIST",
        "text": "Delivery LIST"
      },
      {
        "id": 10006211,
        "value": "list test",
        "text": "list test"
      },
      {
        "id": 10006212,
        "value": "test56",
        "text": "test56"
      },
      {
        "id": 10006213,
        "value": "testfilelist",
        "text": "testfilelist"
      }
    ],
    "message": "",
    "status": "Success"
  })
})


app.listen(1400, () => console.log("listen at 1400"))



