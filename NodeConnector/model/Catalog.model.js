var catalogSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    firstName: String,
    lastName: String,
    created: { 
        type: Date,
        default: Date.now
    }
});

var Catalog = db.model('Catalog', catalogSchema);  
module.exports = Catalog;   